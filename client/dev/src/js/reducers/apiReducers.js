/* eslint prop-types: 0 */
import { combineReducers } from "redux";
import { ActionTypes } from "../constants";

const carts = (state = {
  isFetching: false,
  data: [],
  showPopupAfterReceiveData: false,
}, action) => {
  switch (action.type) {
    case ActionTypes.REQUEST_CARTS:
      return { isFetching: true };
    case ActionTypes.RECEIVE_CARTS:
      return { isFetching: false, data: action.data, showPopupAfterReceiveData: action.showPopupAfterReceiveData };
    default:
      return state;
  }
};

const myAccount = (state = {
  isFetching: false,
  data: null,
}, action) => {
  switch (action.type) {
    case ActionTypes.REQUEST_MY_ACCOUNT:
      return { isFetching: true };
    case ActionTypes.RECEIVE_MY_ACCOUNT:
      return { isFetching: false, data: action.data };
    default:
      return state;
  }
};

export default combineReducers({
  carts,
  myAccount,
});
