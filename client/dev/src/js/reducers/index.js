/* eslint prop-types: 0 */
import { combineReducers } from "redux";
import layout from "./layoutReducers";
import apiRedux from "./apiReducers";

export default combineReducers({
  layout,
  apiRedux,
});
