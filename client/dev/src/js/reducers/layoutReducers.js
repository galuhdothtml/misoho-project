/* eslint prop-types: 0 */
import { ActionTypes } from "../constants";

const layout = (state = {
  showAppOverlay: false,
}, action) => {
  switch (action.type) {
    case ActionTypes.SHOW_APP_OVERLAY:
      return { showAppOverlay: action.showStatus };
    default:
      return state;
  }
};

export default layout;
