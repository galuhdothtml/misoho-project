/* eslint prop-types: 0 */
import ApiClient from "./api_client";


const getGoogleUrl = () => ApiClient.fetch("/api/get-google-url");
const setupGoogleAccount = payload => ApiClient.fetch("/api/setup-google-account", payload, "POST");
const login = payload => ApiClient.fetch("/api/login/user", payload, "POST");
const register = payload => ApiClient.fetch("/api/register", payload, "POST");
const uploadProfilePhoto = (payload, withFiles) => ApiClient.fetch("/api/upload_profile_photo", payload, "POST", withFiles);
const createReview = (payload, withFiles) => ApiClient.fetch("/api/review", payload, "POST", withFiles);
const updateReview = (payload, withFiles) => ApiClient.fetch("/api/review", payload, "PUT", withFiles);
const getMyAccount = () => ApiClient.fetch("/api/my/account");
const getCarts = () => ApiClient.fetch("/api/carts");
const getCities = () => ApiClient.fetch("/api/cities");
const getTopCities = () => ApiClient.fetch("/api/top-cities");
const getCitiesGroup = payload => ApiClient.fetch("/api/cities-group", payload);
const getCategories = () => ApiClient.fetch("/api/categories", { is_unlimited: "Y", with_image: true });
const getProducts = payload => ApiClient.fetch("/api/products", payload);
const getProduct = id => ApiClient.fetch(`/api/product/${id}`);
const deleteCart = idProduct => ApiClient.fetch("/api/delete_cart", { id_product: idProduct }, "POST");
const updateCart = (idProduct, qty) => ApiClient.fetch("/api/update_cart", { id_product: idProduct, qty }, "POST");
const addToCart = (idProduct, qty) => ApiClient.fetch("/api/add_to_cart", { id_product: idProduct, qty }, "POST");
const createAddress = payload => ApiClient.fetch("/api/address", payload, "POST");
const updateAddress = payload => ApiClient.fetch("/api/address", payload, "PUT");
const deleteAddress = idAddress => ApiClient.fetch("/api/address", { id: idAddress }, "DELETE");
const getPostalCode = idPostalCode => ApiClient.fetch("/api/urbans", { page: "1", limit: "5", orId: idPostalCode });
const getMyAddresses = () => ApiClient.fetch("/api/my/addresses", { page: 1 });
const getMyAddress = id => ApiClient.fetch(`/api/my/address/${id}`);
const getCouriers = payload => ApiClient.fetch("/api/cekongkir", payload, "POST");
const checkoutOrder = payload => ApiClient.fetch("/api/checkout_order", payload, "POST");
const getMyOrders = () => ApiClient.fetch("/api/my/orders", { limit: 5, page: 1, filterText: "" });
const getMyOrderDetail = id => ApiClient.fetch(`/api/my/order_detail/${id}`);
const setDefaultAddress = idAddress => ApiClient.fetch("/api/set_default_address", { id_address: idAddress }, "POST");
const createWishlist = payload => ApiClient.fetch("/api/add_to_wishlist", payload, "POST");
const getWishlists = payload => ApiClient.fetch("/api/my/wishlists", payload);

export {
  getGoogleUrl,
  setupGoogleAccount,
  login,
  register,
  uploadProfilePhoto,
  createReview,
  updateReview,
  getMyAccount,
  getCarts,
  getCategories,
  getProducts,
  getTopCities,
  getCities,
  getCitiesGroup,
  getProduct,
  addToCart,
  updateCart,
  deleteCart,
  createAddress,
  updateAddress,
  deleteAddress,
  getPostalCode,
  getMyAddresses,
  getMyAddress,
  getCouriers,
  checkoutOrder,
  getMyOrders,
  getMyOrderDetail,
  setDefaultAddress,
  createWishlist,
  getWishlists,
};
