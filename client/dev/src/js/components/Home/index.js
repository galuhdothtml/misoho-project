/* eslint prop-types: 0 */
import React from "react";
import Img1 from "../../images/banner/1.jpg";
import Img2 from "../../images/banner/2.jpg";
import Img5 from "../../images/banner/medium.jpg";
import Img6 from "../../images/banner/small1.jpg";
import Img7 from "../../images/banner/small2.jpg";
import LoadingModal from "../LoadingModal";
import ImageSlideShow from "./ImageSlideShow";
import CategorySection from "./CategorySection";
import ProductSection from "./ProductSection";
import { getCategories, getProducts } from "../../data";

const LIMIT_PRODUCT = 10;
class Home extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      images: [],
      categories: [],
      terbaruProducts: [],
      terpopulerProducts: [],
      tertinggiProducts: [],
      terendahProducts: [],
      sorting: "TERBARU",
      isLoading: false,
    };
  }

  componentWillMount = () => {
    const images = [Img1, Img2, Img1, Img2];
    this.setState({ images });
    this.fetchAllData();
  }

  fetchAllData = async () => {
    this.setState({ isLoading: true });
    await this.setupCategories();
    await this.setupTerbaruProducts();
    await this.setupTerpopulerProducts();
    await this.setupTertinggiProducts();
    await this.setupTerendahProducts();
    this.setState({ isLoading: false });
  }

  setupCategories = async () => {
    const res = await getCategories();

    if (res.status) {
      this.setState({ categories: res.data });
    }
  }

  setupTerbaruProducts = async () => {
    const res = await getProducts({
      sort: "TERBARU",
      limit: LIMIT_PRODUCT,
    });

    if (res.status) {
      this.setState({ terbaruProducts: res.data });
    }
  }

  setupTerpopulerProducts = async () => {
    const res = await getProducts({
      sort: "TERPOPULER",
      limit: LIMIT_PRODUCT,
    });

    if (res.status) {
      this.setState({ terpopulerProducts: res.data });
    }
  }

  setupTertinggiProducts = async () => {
    const res = await getProducts({
      sort: "HARGA_TERTINGGI",
      limit: LIMIT_PRODUCT,
    });

    if (res.status) {
      this.setState({ tertinggiProducts: res.data });
    }
  }

  setupTerendahProducts = async () => {
    const res = await getProducts({
      sort: "HARGA_TERENDAH",
      limit: LIMIT_PRODUCT,
    });

    if (res.status) {
      this.setState({ terendahProducts: res.data });
    }
  }

  navToAllCategories = () => {
    //
  }

  changeSort = (val) => {
    this.setState({ sorting: val }, () => {
      this.setupProducts();
    });
  }

  render() {
    const {
      images, categories, terpopulerProducts, terendahProducts, terbaruProducts, tertinggiProducts, sorting,
      isLoading,
    } = this.state;

    return (
      <div>
        <div className="container" style={{ marginTop: "25px" }}>
          <div className="row mb-lg">
            <div className="col-sm-6">
              <div className="banner-slideshow">
                <ImageSlideShow images={images} />
              </div>
            </div>
            <div className="col-sm-3 pl-0">
              <img className="banner-img" src={Img5} />
            </div>
            <div className="col-sm-3 pl-0">
              <div style={{ marginBottom: "18px" }}>
                <img className="banner-img" src={Img6} />
              </div>
              <div>
                <img className="banner-img" src={Img7} />
              </div>
            </div>
          </div>
          <div className="mb-lg">
            <CategorySection data={categories} />
          </div>
          <div>
            <ProductSection
              label="Produk Populer"
              data={terpopulerProducts} />
          </div>
          <div>
            <ProductSection
              label="Produk Terbaru"
              data={terbaruProducts} />
          </div>
          <div>
            <ProductSection
              label="Harga Termurah"
              data={terendahProducts} />
          </div>
          <div>
            <ProductSection
              label="Harga Termahal"
              data={tertinggiProducts} />
          </div>
          <div className="text-center">
            <button type="button" className="btn btn-selengkapnya">Lihat Selengkapnya</button>
          </div>
        </div>
        { isLoading && <LoadingModal /> }
      </div>
    );
  }
}

export default Home;
