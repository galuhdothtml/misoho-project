/* eslint prop-types: 0 */
import React from "react";
import {
  Link,
} from "react-router-dom";
import PropTypes from "prop-types";
import Slider from "react-slick";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
import { API_BASE_URL } from "../../constants";

export default class CategorySection extends React.Component {
  static propTypes = {
    data: PropTypes.array,
  }

  static defaultProps = {
    data: [],
  }

  render() {
    const { data } = this.props;
    const settings = {
      dots: false,
      infinite: true,
      speed: 300,
      slidesToShow: 1,
      centerMode: true,
      variableWidth: true,
    };
    return (
      <div className="category-section">
        <div className="category-section__title">Kategori</div>
        <div className="category-section__inner">
          <Slider {...settings}>
            {data.map(x => (
              <div className="category-section__item" key={x.id}>
                <Link to={`/category/${x.id}/${x.slug}`}>
                  <div>
                    <div className="icon-category" style={{ backgroundImage: `url(${API_BASE_URL}/uploads/${x.img})` }} />
                    <div style={{ color: "#333" }}>{x.name}</div>
                    <div style={{ color: "#888" }} className="lightweight-text">32 Produk</div>
                  </div>
                </Link>
              </div>))}
          </Slider>
        </div>
      </div>
    );
  }
}
