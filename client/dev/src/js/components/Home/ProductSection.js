/* eslint prop-types: 0 */
import React from "react";
import PropTypes from "prop-types";
import ProductList from "../ProductList";

export default class ProductSection extends React.Component {
  static propTypes = {
    data: PropTypes.array,
    label: PropTypes.string,
  }

  static defaultProps = {
    data: [],
    label: null,
  }

  render() {
    const {
      data, label,
    } = this.props;

    return (
      <div className="product-section mb-md">
        {label && (
          <div className="product-section__title">
            <div className="row">
              <div className="col-sm-6">{label}</div>
              <div className="col-sm-6 text-right"><button type="button" className="btn btn-default btn-xs">Lihat Lainnya</button></div>
            </div>
          </div>
        )}
        <div className="product-section__inner">
          <ProductList data={data} />
        </div>
      </div>
    );
  }
}
