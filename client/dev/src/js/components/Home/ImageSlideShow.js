/* eslint prop-types: 0 */
import React from "react";
import Slider from "react-slick";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";

export default class ImageSlider extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      images: [],
    };
  }

  componentWillMount() {
    const { images } = this.props;
    this.setState({ images });
  }

  render() {
    const settings = {
      dots: true,
      infinite: true,
      speed: 500,
      slidesToShow: 1,
      slidesToScroll: 1,
      autoplay: true,
      autoplaySpeed: 5000,
    };
    const { images } = this.state;
    return (
      <Slider {...settings}>
        {images.map((x, i) => (
          <div key={i}>
            <img style={{ height: "400px" }} src={x} />
          </div>
        ))
        }
      </Slider>
    );
  }
}
