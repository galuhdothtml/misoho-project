/* eslint prop-types: 0 */
import React from "react";
import { API_BASE_URL } from "../../constants";

class ImageList extends React.Component {
  render() {
    const { data } = this.props;

    return (
      <div className="image-wrapper">
        { (data.length > 0) && (
          <div className="right">
            <img className="product-img" src={`${API_BASE_URL}/uploads/${data[0].filename}`} alt={data[0].filename} />
          </div>
        )}
        <div className="left">
          <ul>
              {data.map((x, index) => (
                  <li key={x.id} className={index === 0 ? "active" : ""}>
                      <div className="product-img" style={{ backgroundImage: `url(${API_BASE_URL}/uploads/${x.filename})` }} alt={x.filename} />
                  </li>
              ))}
          </ul>
        </div>
      </div>
    );
  }
}

export default ImageList;
