/* eslint prop-types: 0 */
import React from "react";
import Select2 from "../form/Select2";
import Select from "../form/Select";
import {
  getCouriers,
} from "../../data";
import { currency, capitalizeFirstLetter } from "../../utils";

class CalculateShipping extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      cityId: "",
      courierData: [
        {
          id: "-",
          name: "Pilih Kurir",
        },
        {
          id: "jne",
          name: "JNE",
        },
        {
          id: "tiki",
          name: "TIKI",
        },
        {
          id: "pos",
          name: "POS Indonesia",
        },
      ],
      courierId: "-",
      serviceList: [{ id: "-", name: "Pilih Layanan" }],
      serviceId: "-",
      biayaAmount: 0,
      etdText: "",
    };
  }

  changeIdCityHandler = (val) => {
    this.setState({ cityId: val });
  }

  changeCourierIdHandler = (val) => {
    this.setState({
      biayaAmount: 0, courierId: val, serviceId: "-", serviceList: [{ id: "-", name: "Pilih Layanan" }],
    }, () => {
      this.fetchServiceList();
    });
  }

  changeIdServiceHandler = (val) => {
    this.setState({ serviceId: val }, () => {
      const { serviceList, serviceId } = this.state;
      const found = serviceList.find(x => (String(x.id) === String(serviceId)));
      if (found) {
        this.setState({ biayaAmount: found.price, etdText: found.etd });
      }
    });
  }

  fetchServiceList = async () => {
    let serviceList = [{ id: "-", name: "Pilih Layanan" }];
    let newData = [];
    const { cityId, courierId } = this.state;
    const weight = 600;
    if (courierId === "jne") {
      newData = await this.setupJneCouriers({ destination: cityId, weight });
    } else if (courierId === "tiki") {
      newData = await this.setupTikiCouriers({ destination: cityId, weight });
    } else if (courierId === "pos") {
      newData = await this.setupPosCouriers({ destination: cityId, weight });
    }

    serviceList = [...serviceList, ...newData];

    this.setState({ serviceList });
  }

  setupJneCouriers = async ({ destination, weight }) => {
    const newData = [];
    const payload = {
      destination,
      weight,
      courier: "jne",
    };
    const res = await getCouriers(payload);
    if (res.status && res.data.length > 0) {
      res.data[0].costs.forEach((x, i) => {
        const isValid = x.cost.length > 0;

        if (isValid) {
          newData.push({
            code: "jne",
            id: `${x.service}-${i}`,
            name: `${x.service} - ${x.description}`,
            service: x.service,
            price: x.cost[0].value,
            etd: String(x.cost[0].etd) === "1-1" ? "1 Hari" : `${x.cost[0].etd} Hari`,
          });
        }
      });
    }

    return newData;
  }

  setupPosCouriers = async ({ destination, weight }) => {
    const newData = [];
    const code = "pos";
    const payload = {
      destination,
      weight,
      courier: code,
    };
    const res = await getCouriers(payload);
    if (res.status && res.data.length > 0) {
      res.data[0].costs.forEach((x, i) => {
        const isValid = x.cost.length > 0;

        if (isValid) {
          newData.push({
            code,
            id: `${x.service}-${i}`,
            name: x.service,
            service: x.service,
            price: x.cost[0].value,
            etd: capitalizeFirstLetter(String(`${x.cost[0].etd}`).toLowerCase()),
          });
        }
      });
    }

    return newData;
  }

  setupTikiCouriers = async ({ destination, weight }) => {
    const newData = [];
    const code = "tiki";
    const payload = {
      destination,
      weight,
      courier: code,
    };
    const res = await getCouriers(payload);
    if (res.status && res.data.length > 0) {
      res.data[0].costs.forEach((x, i) => {
        const isValid = x.cost.length > 0;

        if (isValid) {
          newData.push({
            code,
            id: `${x.service}-${i}`,
            name: `${x.service} - ${x.description}`,
            service: x.service,
            price: x.cost[0].value,
            etd: `${x.cost[0].etd} Hari`,
          });
        }
      });
    }

    return newData;
  }

  biayaAmountComponent = () => {
    const { biayaAmount, etdText } = this.state;

    if (biayaAmount > 0) {
      return (
        <div>{currency(biayaAmount)}<span style={{ marginLeft: "5px" }} className="lightweight-text">Perkiraan Waktu Pengiriman: {etdText}</span></div>
      );
    }

    return (<span>-</span>);
  }

  render() {
    const {
      cityId, courierData, courierId, serviceList, serviceId,
    } = this.state;
    const { show } = this.props;

    return (
      <div className={show ? "calculate-shipping-wrapper" : "hidden"}>
        <div className="row mb-xs">
          <div className="col-sm-2 pr-0">
            <div className="label-text">Dikirim ke</div>
          </div>
          <div className="col-sm-9">
            <Select2
              url="/api/v2/cities"
              placeholder="Pilih Kabupaten / Kota"
              value={cityId}
              changeEvent={this.changeIdCityHandler}
            />
          </div>
        </div>
        <div className="row mb-xs">
          <div className="col-sm-2 pr-0">
            <div className="label-text">Pengiriman</div>
          </div>
          <div className="col-sm-4 pr-0">
            <Select
              placeholder="Pilih Kurir"
              data={courierData}
              value={courierId}
              changeEvent={this.changeCourierIdHandler}
            />
          </div>
          {(serviceList.length > 1) && (
            <div className="col-sm-5">
              <Select
                placeholder="Pilih Layanan"
                data={serviceList}
                value={serviceId}
                changeEvent={this.changeIdServiceHandler}
              />
            </div>
          )}
        </div>
        <div className="row mb-xs">
          <div className="col-sm-2 pr-0">
            <div className="lightweight-text">Biaya</div>
          </div>
          <div className="col-sm-10">
            <span>{this.biayaAmountComponent()}</span>
          </div>
        </div>
      </div>
    );
  }
}

export default CalculateShipping;
