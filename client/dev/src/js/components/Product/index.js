/* eslint prop-types: 0 */
import React from "react";
import { connect } from "react-redux";
import moment from "moment";
import QrCodeExample from "../../images/qr-code-example.png";
import { getProduct, addToCart, createWishlist } from "../../data";
import { currency } from "../../utils";
import ImageList from "./ImageList";
import QtyInput from "../form/QtyInput";
import Rating from "../Rating";
import CalculateShipping from "./CalculateShipping";
import { fetchCarts } from "../../actions/apiActions";
import CoreHoC from "../CoreHoC";
import DefaultPhoto from "../../images/avatar.png";
import { API_BASE_URL } from "../../constants";

class Product extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      id: "",
      name: "",
      price: 0,
      images: [],
      category: null,
      isLoading: false,
      isLoadingWishlist: false,
      weight: 0,
      qty: 1,
      description: "",
      ratingValue: 0,
      reviewNumber: 0,
      showCalculateShipping: false,
      currentTab: "1",
      productRatings: [],
    };
  }

  componentWillMount = () => {
    const {
      match: {
        params,
      },
    } = this.props;

    this.setupProduct(params.id);
  }

  selectTabHandler = (val) => {
    this.setState({ currentTab: val });
  }

  setupProduct = async (id) => {
    const res = await getProduct(id);

    if (res.status) {
      const { data } = res;

      const newProductRatings = [];

      data.ProductRatings.forEach((x) => {
        let customerPhoto = null;
        let reviewImages = [];

        if (x.profilePhoto) {
          customerPhoto = x.profilePhoto.filename;
        }

        if (x.images.length > 0) {
          reviewImages = x.images.map(img => (img.filename));
        }

        newProductRatings.push({
          id: x.id,
          customerName: x.customer.name,
          customerPhoto,
          reviewText: x.description,
          ratingValue: parseInt(x.rating, 10),
          images: reviewImages,
          createdAt: x.createdAt,
        });
      });

      this.setState({
        id: data.id,
        name: data.name,
        price: data.price,
        images: data.images,
        category: data.Category,
        weight: data.weight,
        description: data.description,
        ratingValue: 4,
        reviewNumber: 15,
        productRatings: newProductRatings,
      });
    }
  }

  startAddingToCart = async () => {
    const { doFetchingCarts } = this.props;
    const { id } = this.state;

    this.setState({ isLoading: true });
    await addToCart(id, 1);
    await doFetchingCarts(true);
    this.setState({ isLoading: false });
  }

  startAddingToWishlist = async () => {
    const { showNotification } = this.props;
    const { id } = this.state;

    this.setState({ isLoadingWishlist: true });
    const res = await createWishlist({ id_product: id });
    this.setState({ isLoadingWishlist: false });

    if (res.status) {
      showNotification("Berhasil ditambahkan!");
    }
  }

  createButtonCart = () => {
    const { isLoading } = this.state;

    if (isLoading) {
      return (
        <button type="button" className="btn btn-default grey btn-block" disabled>Loading ...</button>
      );
    }

    return (<button type="button" className="btn btn-default grey btn-block" onClick={this.startAddingToCart}><i className="fa fa-cart-plus" />&nbsp;&nbsp;Tambah ke Troli</button>);
  }

  createButtonWishlist = () => {
    const { isLoadingWishlist: isLoading } = this.state;

    if (isLoading) {
      return (
        <button type="button" className="btn btn-default grey btn-block" disabled>Loading ...</button>
      );
    }

    return (<button type="button" className="btn btn-default grey btn-block" onClick={this.startAddingToWishlist}><i className="fa fa-heart" />&nbsp;&nbsp;Wishlist</button>);
  }

  changeQtyHandler = (index, val) => {
    this.setState({ qty: val });
  }

  toggleCalculateShipping = () => {
    const { showCalculateShipping } = this.state;

    this.setState({ showCalculateShipping: !showCalculateShipping });
  }

  reviewListComponent = (data) => {
    const retval = [];

    if (data.length > 0) {
      data.forEach((x, i) => {
        let imgVal = DefaultPhoto;

        if (x.customerPhoto) {
          imgVal = `${API_BASE_URL}/uploads/${x.customerPhoto}`;
        }

        retval.push(
          <div className="review-row-wrapper" key={x.id} style={(i === (data.length - 1)) ? { border: "0px" } : {}}>
            <div className="review-row-header">
              <img src={imgVal} />
              <div className="review-info">
                <div>{x.customerName}</div>
                <div>
                  <Rating value={x.ratingValue} />
                  <span className="review-date">{moment(x.createdAt).format("DD MMMM YYYY HH:mm:ss")}</span>
                </div>
              </div>
            </div>
            <div className="review-row-body">
              {x.reviewText}
              {x.images.length > 0 && (
                <div className="review-img-list">
                  <ul>
                    {x.images.map(attachment => (<li key={attachment}><img src={`${API_BASE_URL}/uploads/${attachment}`} /></li>))}
                  </ul>
                </div>
              )}
            </div>
          </div>,
        );
      });
    } else {
      retval.push(<div key="empty-review-key">Belum ada ulasan</div>);
    }

    return (
      <div className="review-product-readonly-wrapper">
        {retval}
      </div>
    );
  }

  tabBodyComponent = () => {
    const { currentTab, productRatings } = this.state;

    if (String(currentTab) === "1") {
      return (
        <p>
          Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut posuere mi id justo vulputate, congue condimentum neque posuere. Etiam sodales ac felis ac semper. Proin fringilla lorem suscipit tellus feugiat, eget fermentum lectus placerat. Morbi euismod congue posuere. Aliquam eget faucibus elit. Nullam consectetur sapien vitae felis porta lacinia. Donec ut maximus mauris. Proin gravida volutpat justo. Integer eu mauris eget elit aliquam vehicula. Vestibulum non auctor urna. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla pulvinar justo in imperdiet sollicitudin. Vestibulum tincidunt purus quis quam mattis, iaculis hendrerit tortor molestie.
        </p>
      );
    }

    if (String(currentTab) === "2") {
      return this.reviewListComponent(productRatings);
    }

    return <div />;
  }

  render() {
    const {
      name, price, images, category, weight,
      qty, description, ratingValue, reviewNumber,
      showCalculateShipping, currentTab, productRatings,
    } = this.state;
    let categoryName = "";

    if (category) {
      categoryName = category.name;
    }

    return (
      <div className="container product-detail-wrapper">
        <div className="mt-xs">
          <div className="row">
            <div className="col-sm-12">
              <ol className="breadcrumb breadcrumb-style">
                <li><a href="#">Home</a></li>
                <li><a href="#">Kategori</a></li>
                <li><a href="#">Keripik</a></li>
                <li className="active">TEH CELUP DAUN TIN (ISI 2 POUCH @15 BAG)</li>
              </ol>
            </div>
          </div>
          <div className="row">
            <div className="col-sm-9 pr-0">
              <div className="product-detail-content-wrapper">
                <div className="row">
                  <div className="col-sm-4">
                    <ImageList data={images} />
                  </div>
                  <div className="col-sm-8">
                    <h4>{name}</h4>
                    <div><Rating value={ratingValue} /><span className="review-number">{`${reviewNumber} ulasan`}</span></div>
                    <hr />
                    <div className="row">
                      <div className="col-sm-2 pr-0"><label style={{ color: "#777" }}>HARGA</label></div>
                      <div className="col-sm-10">
                        <h3 className="price-text">Rp&nbsp;<span>{currency(price)}</span></h3>
                      </div>
                    </div>
                    <hr />
                    <div className="row mb-md">
                      <div className="col-sm-2 pr-0"><label style={{ color: "#777" }}>JUMLAH</label></div>
                      <div className="col-sm-10">
                        <div>
                          <QtyInput value={qty} changeEvent={this.changeQtyHandler} />
                        </div>
                      </div>
                    </div>
                    <hr />
                    <div className="row mb-md">
                      <div className="col-sm-2 pr-0"><label style={{ color: "#777" }}>INFO<br />PRODUK</label></div>
                      <div className="col-sm-10">
                        <ul className="info-product-list">
                          <li>
                            <div className="lightweight-text">Berat</div>
                            <div>750gr</div>
                          </li>
                          <li>
                            <div className="lightweight-text">Kondisi</div>
                            <div>Baru</div>
                          </li>
                          <li>
                            <div className="lightweight-text">Asuransi</div>
                            <div>Ya</div>
                          </li>
                          <li>
                            <div className="lightweight-text">Kategori</div>
                            <div>Makanan</div>
                          </li>
                        </ul>
                      </div>
                    </div>
                    <hr />
                    <div className="row mb-md">
                      <div className="col-sm-12">
                        <div className={`${showCalculateShipping ? "mb-md" : ""} show-calculate-button`} onClick={() => this.toggleCalculateShipping()}>
                          <div><span>Hitung Biaya Kirim</span>{" "}<i className={showCalculateShipping ? "fa fa-angle-up" : "fa fa-angle-down"} /></div>
                        </div>
                        <CalculateShipping show={showCalculateShipping} />
                      </div>
                    </div>
                    <hr />
                    <div className="row mb-md">
                      <div className="col-sm-4">
                        {this.createButtonWishlist()}
                      </div>
                      <div className="col-sm-4 pl-0">
                        {this.createButtonCart()}
                      </div>
                      <div className="col-sm-4 pl-0">
                        <button type="button" className="btn btn-primary btn-block">Beli Sekarang</button>
                      </div>
                    </div>
                    <div className="row hidden">
                      <div className="col-sm-12">
                        <div>Informasi Produk : </div>
                        <div className="lightweight-text" style={{ marginTop: "8px" }}>{description}</div>
                      </div>
                    </div>
                  </div>
                  <div className="col-sm-3 hidden">
                    <div className="product-box-info">
                      <div className="mb-sm">
                        <div className="lightweight-text">Kategori</div>
                        <div>{categoryName}</div>
                      </div>
                      <div className="mb-sm">
                        <div className="lightweight-text">Berat</div>
                        <div>{parseFloat(weight, 10)} gr</div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>

              <div className="product-detail-content-wrapper product-review-wrapper mt-xs">
                <div className="header-custom">
                  <ul>
                    <li className={currentTab === "1" ? "active" : ""} onClick={() => this.selectTabHandler("1")}>Deskripsi</li>
                    <li className={currentTab === "2" ? "active" : ""} onClick={() => this.selectTabHandler("2")}>{productRatings.length > 0 ? `Ulasan (${productRatings.length})` : "Ulasan"}</li>
                  </ul>
                </div>
                <div className="body-custom">
                  {this.tabBodyComponent()}
                </div>
              </div>

            </div>
            <div className="col-sm-3">
              <div style={{ marginTop: "7px" }}>
                <span className="lightweight-text">Bagikan Produk:</span>
                <ul className="social-media-product-share">
                  <li className="facebook-color"><i className="fa fa-facebook" /></li>
                  <li className="twitter-color"><i className="fa fa-twitter" /></li>
                  <li className="whatsapp-color"><i className="fa fa-whatsapp" /></li>
                </ul>
              </div>
              <div className="qr-code-wrapper">
                <div className="title-header">Kode QR</div>
                <div className="body-wrapper">
                  <div className="left">
                    <img src={QrCodeExample} />
                  </div>
                  <div className="right">
                    Scan QR Code untuk berbelanja produk ini langsung di aplikasi Mister Oleh Oleh
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  carts: state.apiRedux.carts,
});

const mapDispatchToProps = dispatch => ({
  doFetchingCarts: showPopupAfterReceiveData => dispatch(fetchCarts(showPopupAfterReceiveData)),
});

const ProductWithHoC = CoreHoC(Product);

export default connect(mapStateToProps, mapDispatchToProps)(ProductWithHoC);
