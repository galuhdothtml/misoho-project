/* eslint prop-types: 0 */
import React from "react";
import ReactPaginate from "react-paginate";

export default class Pagination extends React.Component {
  onPageChangeHandler = (val) => {
    const { onPageChange } = this.props;
    onPageChange(parseInt(val.selected, 10) + parseInt(1, 10));
  }

  render() {
    const { pageCount, currentPage } = this.props;

    return (
            <div className="my-pagination-wrapper">
                <ReactPaginate
                    disabledClassName="active"
                    activeClassName="active"
                    previousLabel={<i className="fa fa-angle-left" />}
                    nextLabel={<i className="fa fa-angle-right" />}
                    containerClassName="my-pagination"
                    pageCount={pageCount}
                    onPageChange={this.onPageChangeHandler}
                    pageRangeDisplayed={3}
                    marginPagesDisplayed={2}
                    forcePage={currentPage - 1}
                />
            </div>
    );
  }
}
