/* eslint prop-types: 0 */
import React from "react";
import PropTypes from "prop-types";
import {
  Link,
} from "react-router-dom";
import ProductItem from "./ProductItem";

export default class ProductList extends React.Component {
  static propTypes = {
    classes: PropTypes.string,
  }

  static defaultProps = {
    classes: "",
  }

  constructor(props) {
    super(props);
    this.state = {
      data: [],
    };
  }

  componentWillMount = () => {
    const { data } = this.props;

    this.setupData(data);
  }

  componentWillReceiveProps = (nextProps) => {
    const { data } = this.props;

    if (data !== nextProps.data) {
      this.setupData(nextProps.data);
    }
  }

  setupData = (val) => {
    const newData = val.map((x) => {
      const ratingValue = Math.floor(Math.random() * 5);
      const reviewNumber = Math.floor(Math.random() * 50);
      return Object.assign({}, x, { rating_value: ratingValue, review_number: ratingValue > 0 ? reviewNumber : 0 });
    });
    this.setState({ data: newData });
  }

  render() {
    const { modifierLi, modifierProductItem } = this.props;
    const { data } = this.state;

    return (
      <div className="product-list">
        <ul>
          {data.map(x => (
            <li key={x.id} className={`product-list__li ${modifierLi || ""}`}>
              <Link to={`/product/${x.id}/${x.slug}`}>
                <ProductItem data={x} modifierClass={modifierProductItem} />
              </Link>
            </li>))}
        </ul>
      </div>
    );
  }
}
