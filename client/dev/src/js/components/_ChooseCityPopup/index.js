/* eslint prop-types: 0 */
import React from "react";
import update from "immutability-helper";
import _ from "lodash";
import InputText from "../form/InputText";
import CityRowItem from "./CityRowItem";

class ChooseCityPopup extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      filterKeyword: "",
      cityList: [],
      cityKeyList: [],
      data: [],
    };
  }

  componentWillMount = () => {
    const { data } = this.props;
    this.updateData(data);
  }

  updateData = (data, modifyData = true) => {
    const { value } = this.props;
    const { filterKeyword } = this.state;
    let newData = data;

    if (modifyData) {
      newData = data.map(x => Object.assign({}, x, { checked: !!(value.find(v => v === x.id)) }));
    }

    const { cityKeyList, cityList } = this.filterData(filterKeyword, newData);

    this.setState({ data: newData, cityKeyList, cityList });
  }

  filterData = (filterKeyword, data) => {
    const filterText = new RegExp(filterKeyword.toLowerCase(), "g");
    const filteredData = data.filter(x => (x.name.toLowerCase().match(filterText) || x.province_name.toLowerCase().match(filterText)));
    const cityList = _.chain(filteredData)
      .groupBy("province_name")
      .value();

    return { cityKeyList: _.keys(cityList), cityList };
  }

  changeFilterKeyword = (val) => {
    const { data } = this.state;
    const { cityKeyList, cityList } = this.filterData(val, data);

    this.setState({ filterKeyword: val, cityKeyList, cityList });
  }

  changeHandler = (id, val) => {
    const { data } = this.state;
    const index = data.findIndex(x => String(x.id) === String(id));
    const newState = update(data, {
      [index]: {
        checked: {
          $set: val,
        },
      },
    });

    this.updateData(newState, false);
  }

  resetData = () => {
    const { data } = this.props;

    this.setState({ filterKeyword: "" }, () => {
      this.updateData(data);
    });
  }

  applyData = () => {
    const { data } = this.state;
    const { applyEvent } = this.props;
    // do nothing
    applyEvent(data.filter(x => (x.checked)).map(x => (x.id)));
  }

  hidePopup = () => {
    const { onHide } = this.props;

    onHide();
  }

  render() {
    const {
      filterKeyword, cityKeyList, cityList,
    } = this.state;
    const rows = [];

    cityKeyList.forEach((x) => {
      rows.push(<CityRowItem key={x} title={x} cities={cityList[x]} changeEvent={this.changeHandler} />);
    });

    return (
      <div className="choose-city-container show">
        <div className="header">
          <div className="form-group row">
            <div className="col-sm-2">
              <div
                style={{
                  position: "relative",
                  top: "7px",
                }}
              >Lokasi</div>
            </div>
            <div className="col-sm-8">
              <InputText
                placeholder="Cari Lokasi ..."
                changeEvent={this.changeFilterKeyword}
                value={filterKeyword} />
            </div>
            <div className="col-sm-2">
              <button
                type="button"
                className="close"
                onClick={this.hidePopup}
              >
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
          </div>
        </div>
        <div className="content">
          <div>
              {rows.length > 0 ? <div className="city-data">
                {rows}
              </div> : <div className="data-empty-wrapper">
                  <div className="data-empty-info">
                    <div>Lokasi tidak ditemukan</div>
                    <div>Hasil pencarian untuk '{filterKeyword}' tidak ditemukan</div>
                  </div></div>}
            </div>
        </div>
        <div className="footer text-right">
          <button className="btn btn-default" type="button" onClick={this.resetData}>Reset</button>
          &nbsp;&nbsp;&nbsp;
          <button className="btn btn-primary" type="button" onClick={this.applyData}>Terapkan</button>
        </div>
      </div>
    );
  }
}

export default ChooseCityPopup;
