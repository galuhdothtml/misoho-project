/* eslint prop-types: 0 */
import React from "react"; // eslint-disable-line no-unused-vars
import InputCheckbox from "../form/InputCheckbox";

const CityRowItem = ({
  title, cities, changeEvent,
}) => (
  <div className="city-row">
    <h5>{title}</h5>
    <ul>
      {cities.map(x => (<li key={x.id}>
        <div>
          <InputCheckbox
            value={x.checked}
            changeEvent={val => changeEvent(x.id, val)}
          />
          <label className="form-check-label">{x.name}</label>
        </div>
      </li>))}
    </ul>
  </div>
);

export default CityRowItem;
