/* eslint prop-types: 0 */
import React from "react";
import PropTypes from "prop-types";

class InputPassword extends React.Component {
  static propTypes = {
    label: PropTypes.string,
    value: PropTypes.string,
    placeholder: PropTypes.string,
    changeEvent: PropTypes.func,
    errorText: PropTypes.string,
    disabled: PropTypes.bool,
  }

  static defaultProps = {
    label: undefined,
    value: "",
    placeholder: "",
    changeEvent: () => { },
    errorText: undefined,
    disabled: false,
  }

  constructor(props) {
    super(props);

    this.state = {
      showPassword: false,
    };
  }

  changeHandler = (e) => {
    const { changeEvent } = this.props;

    changeEvent(e.target.value);
  }

  createInputComponent = () => {
    const {
      value, placeholder, disabled,
    } = this.props;
    const { showPassword } = this.state;
    if (showPassword) {
      return (
        <input
              type="text"
              className="form-control"
              placeholder={placeholder}
              value={value}
              onChange={this.changeHandler}
              disabled={disabled}
          />
      );
    }

    return (
      <input
            type="password"
            className="form-control"
            placeholder={placeholder}
            value={value}
            onChange={this.changeHandler}
            disabled={disabled}
        />
    );
  }

  togglePassword = () => {
    const { showPassword } = this.state;

    this.setState({ showPassword: !showPassword });
  }

  render() {
    const {
      label, errorText,
    } = this.props;
    const { showPassword } = this.state;

    return (
      <div className={`form-group mb-0 ${(errorText && errorText.length > 0) ? "has-error" : ""}`}>
        {label && <label>{label}</label>}
        <div className="input-group">
          {this.createInputComponent()}
          <span className="input-group-addon clickable" onClick={this.togglePassword}><i className={`fa ${showPassword ? "fa-eye" : "fa-eye-slash"}`} /></span>
        </div>
        {(errorText && errorText.length > 0) && <span className="help-block">{errorText}</span>}
      </div>
    );
  }
}

export default InputPassword;
