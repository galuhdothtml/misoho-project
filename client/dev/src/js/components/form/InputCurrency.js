/* eslint prop-types: 0 */
import React from "react";
import PropTypes from "prop-types";
import { currency } from "../../utils";

class InputCurrency extends React.Component {
  static propTypes = {
    label: PropTypes.string,
    value: PropTypes.oneOfType([
      PropTypes.string,
      PropTypes.number,
    ]),
    placeholder: PropTypes.string,
    changeEvent: PropTypes.func,
    errorText: PropTypes.string,
    disabled: PropTypes.bool,
  }

  static defaultProps = {
    label: undefined,
    value: "",
    placeholder: "",
    changeEvent: () => {},
    errorText: undefined,
    disabled: false,
  }

  changeHandler = (e) => {
    const isNumber = /^\d*$/.test(String(e.target.value).replace(/,/g, ""));
    const { changeEvent } = this.props;

    if (isNumber) {
      changeEvent(String(e.target.value).replace(/,/g, ""));
    }
  }

  render() {
    const {
      label, value, placeholder, errorText, disabled,
    } = this.props;

    return (
      <div className={`form-group mb-0 ${(errorText && errorText.length > 0) ? "has-error" : ""}`}>
        {label && <label>{label}</label>}
        <input
          type="text"
          className="form-control"
          placeholder={placeholder}
          value={currency(value)}
          onChange={this.changeHandler}
          disabled={disabled}
        />
        { (errorText && errorText.length > 0) && <span className="help-block">{errorText}</span> }
      </div>
    );
  }
}

export default InputCurrency;
