/* eslint prop-types: 0 */
import React from "react";
import PropTypes from "prop-types";
import DefaultPhoto from "../../images/avatar.png";

class UploadPhoto extends React.Component {
  changeHandler = (e) => {
    const { changeEvent } = this.props;

    changeEvent(e.target.value);
  }

  render() {
    const { changeEvent } = this.props;

    return (
      <div className="upload-photo-wrapper">
        <div className="mb-md">
            <img src={DefaultPhoto} />
        </div>
        <div>
            <button type="button" className="btn btn-default grey btn-block">Unggah Foto</button>
        </div>
        <div className="info-upload lightweight-text">
            <p>Besar file: maksimum 10.000.000 bytes (10 Megabytes)</p>
            <p>Ekstensi file yang diperbolehkan: .JPG .JPEG .PNG</p>
        </div>
      </div>
    );
  }
}

export default UploadPhoto;
