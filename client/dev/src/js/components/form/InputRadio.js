/* eslint prop-types: 0 */
import React from "react";
import PropTypes from "prop-types";

const createId = () => (Math.random().toString(36).substring(2, 15) + Math.random().toString(36).substring(2, 15));
class InputRadio extends React.Component {
  static propTypes = {
    label: PropTypes.string,
    value: PropTypes.string,
    name: PropTypes.string,
    data: PropTypes.arrayOf(PropTypes.shape({})),
    changeEvent: PropTypes.func,
    errorText: PropTypes.string,
  }

  static defaultProps = {
    label: undefined,
    value: "",
    name: createId(),
    data: [],
    changeEvent: () => { },
    errorText: undefined,
  }

  changeHandler = (val) => {
    const { changeEvent } = this.props;
    changeEvent(val);
  }

  render() {
    const {
      label, name, value, errorText, data,
    } = this.props;
    return (
      <div className={`form-group mb-0 ${(errorText && errorText.length > 0) ? "has-error" : ""}`}>
        {label && <label>{label}</label>}
        <div className="radio-wrapper">
          {data.map(x => (
            <div key={x.id} className="radio-container" onClick={() => this.changeHandler(x.id)}>{x.name}
              <input
                type="radio"
                name={name}
                checked={String(x.id) === String(value)}
                onChange={() => {}}
                value={x.id}
              />
              <span className="checkmark"></span>
            </div>
          ))}
        </div>
        {(errorText && errorText.length > 0) && <span className="help-block">{errorText}</span>}
      </div>
    );
  }
}

export default InputRadio;
