/* eslint prop-types: 0 */
import React from "react";
import PropTypes from "prop-types";

class InputCheckbox extends React.Component {
  constructor(props) {
    super(props);

    this.handleChange = this.handleChange.bind(this);
  }

  handleChange(e) {
    this.props.changeEvent(e.target.checked);
  }

  render() {
    const {
      label, value,
    } = this.props;

    return (
        <label className="checkbox-container"><span className="label lightweight-text">{label}</span>
          <input type="checkbox" onChange={this.handleChange} checked={value} />
          <span className="checkmark"></span>
        </label>
    );
  }
}

InputCheckbox.propTypes = {
  value: PropTypes.bool.isRequired,
  changeEvent: PropTypes.func.isRequired,
  label: PropTypes.string,
};

export default InputCheckbox;
