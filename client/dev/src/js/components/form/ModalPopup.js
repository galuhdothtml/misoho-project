/* eslint prop-types: 0 */
import React from "react";
import PropTypes from "prop-types";
import ScrollArea from "react-scrollbar";

const style = {
  modalBody: {
    height: `${(window.innerHeight - 210)}px`,
    padding: "0px",
  },
  modalBodyNoStaticHeight: {
    padding: "0px",
  },
  modalScrollArea: {
    height: "100%",
    width: "auto",
    padding: "0px",
  },
  modalScrollAreaContent: {
    padding: "0px",
  },
};
class ModalPopup extends React.Component {
  static propTypes = {
    width: PropTypes.number,
    hideModal: PropTypes.func,
    saveHandler: PropTypes.func,
    title: PropTypes.string,
    disableStaticHeight: PropTypes.bool,
    removeFooter: PropTypes.bool,
  }

  static defaultProps = {
    hideModal: () => { },
    width: 500,
    saveHandler: undefined,
    title: "",
    disableStaticHeight: false,
    removeFooter: false,
  }

  constructor(props) {
    super(props);

    this.state = {
      btnDisabled: false,
      btnText: "",
    };
  }

  applyValue = (e) => {
    const { btnDisabled } = this.state;

    if (e) {
      e.preventDefault();
    }

    if (!btnDisabled) {
      const { saveHandler } = this.props;
      this.setState({
        btnDisabled: true,
        btnText: "Tunggu ...",
      });

      saveHandler(this.buttonCallback);
    }
  }

  hideModalHandler = () => {
    const { btnDisabled } = this.state;
    const { hideModal } = this.props;

    if (!btnDisabled) {
      hideModal();
    }
  }

  buttonCallback = () => {
    this.setState({
      btnDisabled: false,
      btnText: "",
    });
  }

  confirmTextValue = () => {
    const { confirmText } = this.props;

    if (confirmText) {
      return confirmText;
    }

    return "Simpan";
  }

  render() {
    const {
      children,
      width,
      title,
      saveHandler,
      disableStaticHeight,
      removeFooter,
    } = this.props;
    const {
      btnDisabled,
      btnText,
    } = this.state;

    return (
      <div className="modal" style={{ display: "block" }}>
        <div className="modal-dialog" style={{ width: `${width}px` }}>
          <div className="modal-content" style={disableStaticHeight ? { top: "50px" } : {}}>
            <div className="modal-header">
              <button type="button" className="close close-custom" onClick={this.hideModalHandler} aria-label="Close">
                <span aria-hidden="true">×</span></button>
              <h4 className="modal-title text-left">{title}</h4>
            </div>
            <div className="modal-body" style={disableStaticHeight ? style.modalBodyNoStaticHeight : style.modalBody}>
              <ScrollArea
                smoothScrolling={false}
                className="modal-scrollarea"
                style={style.modalScrollArea}
                contentStyle={style.modalScrollAreaContent}
                horizontal={false}
                stopScrollPropagation={true}
              >
                <form onSubmit={this.applyValue}>
                  {children}
                </form>
              </ScrollArea>
              { btnDisabled && (<div className="modal-body-overlay" style={disableStaticHeight ? style.modalBodyNoStaticHeight : style.modalBody} />) }
            </div>
            { !removeFooter && (
              <div className="modal-footer">
                <button
                  style={{ minWidth: "80px" }}
                  type="button"
                  className={`btn btn-default ${btnDisabled ? "disabled" : ""} ${saveHandler ? "pull-left" : ""}`}
                  onClick={this.hideModalHandler}>{saveHandler ? "Batal" : "OK"}</button>
                {saveHandler && (
                  <button
                    style={{ minWidth: "80px" }}
                    type="button"
                    className={`btn btn-primary ${btnDisabled ? "disabled" : ""}`}
                    onClick={this.applyValue}>{btnText || this.confirmTextValue()}</button>
                )}
              </div>
            )}
          </div>
        </div>
      </div>
    );
  }
}

export default ModalPopup;
