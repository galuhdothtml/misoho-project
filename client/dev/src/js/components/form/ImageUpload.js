/* eslint prop-types: 0 */
import React from "react";
import PropTypes from "prop-types";
import Cropper from "react-cropper";
import ModalPopup from "./ModalPopup";
import DefaultPhoto from "../../images/avatar.png";

require("cropperjs/dist/cropper.css");

const style = { display: "inline-block" };
class ImageUpload extends React.Component {
  static propTypes = {
    errorText: PropTypes.string,
  }

  static defaultProps = {
    errorText: undefined,
  }

  constructor(props) {
    super(props);

    this.state = {
      isShowing: false,
      imgValue: "",
      tempImg: "",
    };
  }

  componentWillMount = () => {
    const { value } = this.props;

    this.setState({ imgValue: value });
  }

  componentWillReceiveProps = (nextProps) => {
    const { value } = this.props;

    if (value !== nextProps.value) {
      this.setState({ imgValue: nextProps.value });
    }
  }

  chooseFile = () => {
    this.fileUpload.click();
  }

  changeFileHandler = () => {
    const file = this.fileUpload.files[0];
    const reader = new FileReader();
    reader.addEventListener("load", () => {
      this.setState({ tempImg: reader.result }, () => {
        this.showModal();
      });
    }, false);

    if (file) {
      reader.readAsDataURL(file);
    }
  }

  showModal = () => {
    const { tempImg, imgValue } = this.state;
    let newTempImg = imgValue;

    if (tempImg && String(tempImg).length > 0) {
      newTempImg = tempImg;
    }

    this.setState({ isShowing: true, tempImg: newTempImg });
  }

  hideModal = () => {
    this.setState({ isShowing: false }, () => {
      this.fileUpload.value = null;
    });
  }

  applyCroppedImage = () => {
    setTimeout(() => {
      const croppedImg = this.cropper.getCroppedCanvas().toDataURL();
      this.setState({ imgValue: croppedImg, isShowing: false }, () => {
        const { imgValue } = this.state;

        const { changeEvent } = this.props;
        changeEvent(imgValue);
        this.fileUpload.value = null;
      });
    }, 500);
  }

  removeImage = () => {
    const { changeEvent } = this.props;

    this.setState({ imgValue: "", tempImg: "" });
    changeEvent("");
  }

  render() {
    const {
      errorText,
    } = this.props;
    const { isShowing, imgValue, tempImg } = this.state;
    let newImgValue = DefaultPhoto;

    if ((imgValue && imgValue.length > 0)) {
      newImgValue = imgValue;
    }

    return (
      <div style={style} className={`form-group mb-0 ${(errorText && errorText.length > 0) ? "has-error" : ""}`}>
        <div className="upload-photo-wrapper">
          <div className="mb-md">
              <img src={newImgValue} />
          </div>
          <div>
            <button type="button" onClick={this.chooseFile} className="btn btn-default grey btn-block">Pilih Foto</button>
          </div>
          <div className="info-upload lightweight-text">
            <p>Besar file: maksimum 10.000.000 bytes (10 Megabytes)</p>
            <p>Ekstensi file yang diperbolehkan: .JPG .JPEG .PNG</p>
          </div>
        </div>
        {(errorText && errorText.length > 0) && <span className="help-block">{errorText}</span>}
        <input ref={(c) => { this.fileUpload = c; }} onChange={this.changeFileHandler} className="hidden" type="file" accept="image/*" />
        {isShowing
          && <ModalPopup width={600} title="Custom Foto" hideModal={this.hideModal} saveHandler={this.applyCroppedImage}>
            <div>
              <Cropper
                ref={(c) => { this.cropper = c; }}
                src={tempImg}
                style={{ height: 400, width: "100%" }}
                aspectRatio={5 / 5}
                guides={false}
                crop={() => {}} />
            </div>
          </ModalPopup>
        }
      </div>
    );
  }
}

export default ImageUpload;
