/* eslint prop-types: 0 */
import React from "react";
import PropTypes from "prop-types";
import autosize from "autosize";

class TextArea extends React.Component {
  static propTypes = {
    label: PropTypes.string,
    value: PropTypes.string,
    placeholder: PropTypes.string,
    changeEvent: PropTypes.func,
    errorText: PropTypes.string,
    disabled: PropTypes.bool,
    rows: PropTypes.string,
  }

  static defaultProps = {
    label: undefined,
    value: "",
    placeholder: "",
    changeEvent: () => {},
    errorText: undefined,
    disabled: false,
    rows: "1",
  }

  componentDidMount = () => {
    setTimeout(() => {
      // eslint-disable-next-line no-undef
      autosize($("textarea"));
    }, 500);
  }

  changeHandler = (e) => {
    const { changeEvent } = this.props;

    changeEvent(e.target.value);
  }

  render() {
    const {
      label, value, placeholder, errorText, disabled, rows,
    } = this.props;

    return (
      <div className={`form-group mb-0 ${(errorText && errorText.length > 0) ? "has-error" : ""}`}>
        {label && <label>{label}</label>}
        <textarea
          className="form-control"
          placeholder={placeholder}
          rows={rows}
          style={{ resize: "none" }}
          value={value}
          onChange={this.changeHandler}
          disabled={disabled}
        />
        { (errorText && errorText.length > 0) && <span className="help-block">{errorText}</span> }
      </div>
    );
  }
}

export default TextArea;
