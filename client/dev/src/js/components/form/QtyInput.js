/* eslint prop-types: 0 */
import React from "react";
import PropTypes from "prop-types";

class QtyInput extends React.Component {
  static propTypes = {
    index: PropTypes.number,
    value: PropTypes.number,
    changeEvent: PropTypes.func,
  }

  static defaultProps = {
    index: 0,
    value: "",
    changeEvent: () => { },
  }

  changeHandler = (e) => {
    const { changeEvent, index } = this.props;
    let newValue = parseInt(e.target.value, 10);

    if (String(e.target.value).trim().length === 0) {
      newValue = 0;
    }

    changeEvent(index, newValue);
  }

  decreaseQtyHandler = () => {
    const { changeEvent, value, index } = this.props;
    const newValue = parseInt(value, 10) - 1;

    if (newValue > 0) {
      changeEvent(index, newValue);
    }
  }

  increaseQtyHandler = () => {
    const { changeEvent, value, index } = this.props;
    const newValue = parseInt(value, 10) + 1;

    changeEvent(index, newValue);
  }

  render() {
    const {
      value,
    } = this.props;

    return (
      <div className="form-group mb-0">
        <div className="input-group qty-input-wrapper">
          <span className="input-group-btn">
            <button className="btn btn-default" type="button" onClick={this.decreaseQtyHandler} disabled={parseInt(value, 10) < 2}><i className="icon-minus" /></button>
          </span>
          <input type="text" className="form-control" onChange={this.changeHandler} value={value} />
          <span className="input-group-btn">
            <button className="btn btn-default" type="button" onClick={this.increaseQtyHandler}><i className="icon-plus" /></button>
          </span>
        </div>
      </div>
    );
  }
}

export default QtyInput;
