/* eslint prop-types: 0 */
import React from "react";
import update from "immutability-helper";
import {
  Link, Redirect,
} from "react-router-dom";
import queryString from "query-string";
import InputText from "./form/InputText";
import InputPassword from "./form/InputPassword";
import InputCheckbox from "./form/InputCheckbox";
import Logo from "../images/logo-dark.png";
import * as Util from "../utils";
import { getGoogleUrl, login } from "../data";

const errorContent = (type, val) => {
  let retval = null;

  if (type === "email") {
    if (String(val).trim().length === 0) {
      retval = "* Email wajib diisi";
    } else if (!Util.isEmailValid(String(val))) {
      retval = "* Format email masih salah";
    }
  } else if (type === "password") {
    if (String(val).trim().length === 0) {
      retval = "* Password wajib diisi";
    }
  }

  return retval;
};
class Login extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      title: "Masuk",
      form: {
        email: "",
        password: "",
      },
      errorMsg: {
        email: "",
        password: "",
      },
      googleUrl: "",
      isRemembered: false,
      isAuthenticationFailed: false,
      isValidated: false,
    };
  }

  componentDidMount() {
    this.checkAuthentication();
    this.setupGoogleUrl();
  }

  setupGoogleUrl = async () => {
    const res = await getGoogleUrl();

    this.setState({ googleUrl: res.data });
  }

  checkAuthentication = () => {
    const { location } = this.props;
    const values = queryString.parse(location.search);

    if (values.is_error && String(values.is_error) === "1") {
      this.setState({ isAuthenticationFailed: true });
    }
  }

  changeValueHandler = (type, val) => {
    const { form } = this.state;
    const newValue = update(form, {
      [type]: { $set: val },
    });

    const errorMsg = this.createErrorMessage(type, val);
    this.setState({ form: newValue, errorMsg });
  }

  bulkCreateErrorMessage = () => {
    const { errorMsg, form } = this.state;
    const keys = Object.keys(errorMsg);
    let newErrorMsg = errorMsg;
    let isError = false;

    keys.forEach((x) => {
      let error = "";

      const ERROR_TEXT = errorContent(x, form[x]);

      if (ERROR_TEXT) {
        error = ERROR_TEXT;
        isError = true;
      }

      newErrorMsg = update(newErrorMsg, {
        [x]: { $set: error },
      });
    });

    return { msg: newErrorMsg, isError };
  }

  createErrorMessage = (type, val) => {
    const { errorMsg, isValidated } = this.state;
    let error = "";

    if (!isValidated) {
      return errorMsg;
    }

    const ERROR_TEXT = errorContent(type, val);

    if (ERROR_TEXT) {
      error = ERROR_TEXT;
    }

    const newValue = update(errorMsg, {
      [type]: { $set: error },
    });

    return newValue;
  }

  changeIsRemembered = (val) => {
    this.setState({ isRemembered: val });
  }

  doLogin = async (e) => {
    e.preventDefault();
    const { form } = this.state;
    const error = this.bulkCreateErrorMessage();
    const isValid = !error.isError;

    this.setState({ isValidated: true, errorMsg: error.msg });

    if (isValid) {
      const res = await login(form);

      if (res.status) {
        this.setState({ isAuthenticationFailed: false });
      } else {
        this.setState({ isAuthenticationFailed: true });
      }
    }
  }

  loginGoogle = () => {
    const { googleUrl } = this.state;

    location.href = googleUrl;
  }

  hideAuthError = () => {
    this.setState({ isAuthenticationFailed: false });
  }

  render() {
    const {
      errorMsg,
      isRemembered,
      form,
      isAuthenticationFailed,
    } = this.state;
    const isLogin = !!Util.getToken();

    if (isLogin) {
      return <Redirect to="/" />;
    }

    return (
      <div className="container">
        <br />
        <div className="row" style={{ paddingBottom: "20px" }}>
          <div className="col-md-12 text-center">
            <Link to="/"><img style={{ width: "250px" }} className="logo" src={Logo} /></Link>
          </div>
        </div>
        <div className="row">
          <div className="col-md-4 col-md-offset-4 text-center">
            <div className="panel panel-default">
              <div className="panel-body login-page m-auth">
                <h3 style={{ marginTop: "0px" }}>{this.state.title}</h3>
                <p>Belum punya akun? <Link to="/auth/register">Daftar</Link></p>
                {isAuthenticationFailed
                  && <div className="alert alert-danger text-left" role="alert">
                    <button type="button" onClick={this.hideAuthError} className="close" ><span>&times;</span></button>
                    Email atau kata sandi yang Anda masukkan kurang tepat
                    </div>
                }
                <form onSubmit={this.doLogin}>
                  <div className="form-group text-left">
                    <InputText
                      label="Email"
                      changeEvent={val => this.changeValueHandler("email", val)}
                      value={form.email}
                      errorText={errorMsg.email}
                    />
                  </div>
                  <div className="form-group text-left">
                    <InputPassword
                      label="Kata Sandi"
                      changeEvent={val => this.changeValueHandler("password", val)}
                      value={form.password}
                      errorText={errorMsg.password}
                    />
                  </div>
                  <div className="form-group">
                    <div className="row">
                      <div className="col-sm-6">
                        <InputCheckbox label="Ingat Saya" value={isRemembered} changeEvent={this.changeIsRemembered} />
                      </div>
                      <div className="col-sm-6 text-right">
                        <Link to="#">Lupa Kata Sandi?</Link>
                      </div>
                    </div>
                  </div>
                  <div>
                    <button type="submit" className="btn btn-primary btn-block">Masuk</button>
                    <div className="divider-with-text">
                      <hr />
                      <div className="text-wrapper">
                        <div className="text">atau</div>
                      </div>
                    </div>
                    <button type="button" onClick={this.loginGoogle} className="btn btn-default btn-google btn-block"><i className="fa fa-google-plus" /> <span style={{ marginLeft: "10px" }}>Masuk dengan Google</span></button>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default Login;
