/* eslint prop-types: 0 */
import React from "react";
import update from "immutability-helper";
import {
  Link, Redirect,
} from "react-router-dom";
import InputText from "./form/InputText";
import InputPassword from "./form/InputPassword";
import Logo from "../images/logo-dark.png";
import * as Util from "../utils";
import { getGoogleUrl, register } from "../data";

const errorContent = (type, val, toEqualVal = "") => {
  let retval = null;

  if (type === "fullname") {
    if (String(val).trim().length === 0) {
      retval = "* Nama lengkap wajib diisi";
    }
  } else if (type === "email") {
    if (String(val).trim().length === 0) {
      retval = "* Email wajib diisi";
    } else if (!Util.isEmailValid(String(val))) {
      retval = "* Format email masih salah";
    }
  } else if (type === "password") {
    if (String(val).trim().length === 0) {
      retval = "* Password wajib diisi";
    }
  } else if (type === "confirmPassword") {
    if (String(val).trim().length === 0) {
      retval = "* Ulangi password wajib diisi";
    } else if (String(val) !== String(toEqualVal)) {
      retval = "* Ulangi password masih belum benar";
    }
  }

  return retval;
};
class Register extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      title: "Daftar Sekarang",
      form: {
        fullname: "",
        email: "",
        password: "",
        confirmPassword: "",
      },
      errorMsg: {
        fullname: "",
        email: "",
        password: "",
        confirmPassword: "",
      },
      googleUrl: "",
      isEmailDuplicate: false,
      isValidated: false,
    };
  }

  componentDidMount() {
    this.setupGoogleUrl();
  }

  setupGoogleUrl = async () => {
    const res = await getGoogleUrl();

    this.setState({ googleUrl: res.data });
  }

  loginGoogle = () => {
    const { googleUrl } = this.state;

    location.href = googleUrl;
  }

  changeValueHandler = (type, val) => {
    const { form } = this.state;
    const newValue = update(form, {
      [type]: { $set: val },
    });

    const errorMsg = this.createErrorMessage(type, val);
    this.setState({ form: newValue, errorMsg });
  }

  bulkCreateErrorMessage = () => {
    const { errorMsg, form } = this.state;
    const keys = Object.keys(errorMsg);
    let newErrorMsg = errorMsg;
    let isError = false;

    keys.forEach((x) => {
      let error = "";

      const ERROR_TEXT = errorContent(x, form[x], form.password);

      if (ERROR_TEXT) {
        error = ERROR_TEXT;
        isError = true;
      }

      newErrorMsg = update(newErrorMsg, {
        [x]: { $set: error },
      });
    });

    return { msg: newErrorMsg, isError };
  }

  createErrorMessage = (type, val) => {
    const { errorMsg, isValidated, form } = this.state;
    let error = "";

    if (!isValidated) {
      return errorMsg;
    }

    const ERROR_TEXT = errorContent(type, val, form.password);
    if (ERROR_TEXT) {
      error = ERROR_TEXT;
    }

    const newValue = update(errorMsg, {
      [type]: { $set: error },
    });

    return newValue;
  }

  doRegister = async (e) => {
    e.preventDefault();
    const { form } = this.state;
    const error = this.bulkCreateErrorMessage();
    const isValid = !error.isError;

    this.setState({ isValidated: true, errorMsg: error.msg });

    if (isValid) {
      const payload = Object.assign({}, form, {
        name: form.fullname,
        confirm_password: form.confirmPassword,
      });
      const res = await register(payload);

      if (res.status) {
        this.setState({ isEmailDuplicate: false });
      } else {
        this.setState({ isEmailDuplicate: true });
      }
    }
  }

  hideErrorMsg = () => {
    this.setState({ isEmailDuplicate: false });
  }

  render() {
    const {
      errorMsg,
      form,
      isEmailDuplicate,
    } = this.state;
    const isLogin = !!Util.getToken();

    if (isLogin) {
      return <Redirect to="/" />;
    }

    return (
      <div className="container">
        <br />
        <div className="row" style={{ paddingBottom: "20px" }}>
          <div className="col-md-12 text-center">
            <Link to="/"><img style={{ width: "250px" }} className="logo" src={Logo} /></Link>
          </div>
        </div>
        <div className="row">
          <div className="col-md-4 col-md-offset-4 text-center">
            <div className="panel panel-default">
              <div className="panel-body login-page m-auth">
                <h3 style={{ marginTop: "0px" }}>{this.state.title}</h3>
                <p>Sudah punya akun? <Link to="/auth/login">Masuk</Link></p>
                {isEmailDuplicate
                  && <div className="alert alert-danger text-left" role="alert">
                    <button type="button" onClick={this.hideErrorMsg} className="close" ><span>&times;</span></button>
                    Email sudah digunakan
                    </div>
                }
                <form onSubmit={this.doRegister}>
                  <div className="form-group text-left">
                    <InputText
                      label="Nama Lengkap"
                      changeEvent={val => this.changeValueHandler("fullname", val)}
                      value={form.fullname}
                      errorText={errorMsg.fullname}
                    />
                  </div>
                  <div className="form-group text-left">
                    <InputText
                      label="Email"
                      changeEvent={val => this.changeValueHandler("email", val)}
                      value={form.email}
                      errorText={errorMsg.email}
                    />
                  </div>
                  <div className="form-group text-left">
                    <InputPassword
                      label="Password"
                      changeEvent={val => this.changeValueHandler("password", val)}
                      value={form.password}
                      errorText={errorMsg.password}
                    />
                  </div>
                  <div className="form-group text-left">
                    <InputPassword
                      label="Ulangi Password"
                      changeEvent={val => this.changeValueHandler("confirmPassword", val)}
                      value={form.confirmPassword}
                      errorText={errorMsg.confirmPassword}
                    />
                  </div>
                  <div>
                    <button type="submit" className="btn btn-primary btn-block">Daftar</button>
                    <div className="divider-with-text">
                      <hr />
                      <div className="text-wrapper">
                        <div className="text">atau</div>
                      </div>
                    </div>
                    <button type="button" onClick={this.loginGoogle} className="btn btn-default btn-google btn-block"><i className="fa fa-google-plus" /> <span style={{ marginLeft: "10px" }}>Daftar dengan Google</span></button>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default Register;
