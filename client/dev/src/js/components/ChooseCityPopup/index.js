/* eslint prop-types: 0 */
import React from "react";
import update from "immutability-helper";
import InputText from "../form/InputText";
import CityRowItem from "./CityRowItem";

class ChooseCityPopup extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      filterKeyword: "",
      choosenVal: props.value,
      data: [],
    };
  }

  componentDidMount = () => {
    this.fetchData();
  }

  fetchData = async () => {
    const { filterKeyword, choosenVal } = this.state;
    const { onFetch } = this.props;
    const data = await onFetch({ search: filterKeyword });
    let newData = data;

    choosenVal.forEach((x) => {
      newData = this.setupDataCheck(newData, x.id, x.idProvince, true);
    });

    this.setState({ data: newData });
  }

  changeFilterKeyword = (val) => {
    this.setState({ filterKeyword: val });

    clearTimeout(this.filterIdle);
    this.filterIdle = setTimeout(() => {
      this.setState({ filterKeyword: val }, () => {
        this.fetchData();
      });
    }, 500);
  }

  setupDataCheck = (data, id, idProvince, val) => {
    let newData = data;
    const findProvinceIndex = data.findIndex(x => (String(x.id) === String(idProvince)));

    if (findProvinceIndex > -1) {
      const { Cities: cities } = data[findProvinceIndex];
      let newCities = cities;
      const findCityIndex = cities.findIndex(x => (String(x.id) === String(id)));
      if (findCityIndex > -1) {
        newCities = update(newCities, {
          [findCityIndex]: {
            checked: {
              $set: val,
            },
          },
        });
      }
      newData = update(newData, {
        [findProvinceIndex]: {
          Cities: {
            $set: newCities,
          },
        },
      });
    }

    return newData;
  }

  changeHandler = (id, idProvince, val) => {
    const { data, choosenVal } = this.state;
    const newData = this.setupDataCheck(data, id, idProvince, val);
    let newChoosenVal = choosenVal;

    if (val) {
      newChoosenVal.push({ id, idProvince });
    } else {
      newChoosenVal = newChoosenVal.filter(x => String(x.id) !== String(id));
    }

    this.setState({ data: newData, choosenVal: newChoosenVal });
  }

  resetData = () => {
    this.setState({ choosenVal: [] }, () => {
      this.fetchData();
    });
  }

  applyData = () => {
    const { applyEvent } = this.props;
    const { choosenVal } = this.state;

    applyEvent(choosenVal);
  }

  hidePopup = () => {
    const { onHide } = this.props;

    onHide();
  }

  rowContent = (data) => {
    const rows = [];

    data.forEach((x) => {
      rows.push(<CityRowItem key={x.id} title={x.name} cities={x.Cities} changeEvent={this.changeHandler} />);
    });

    return rows;
  }

  render() {
    const { filterKeyword, data } = this.state;

    return (
      <div className="choose-city-container show">
        <div className="header">
          <div className="form-group row">
            <div className="col-sm-2">
              <div
                style={{
                  position: "relative",
                  top: "7px",
                }}
              >Lokasi</div>
            </div>
            <div className="col-sm-8">
              <InputText
                placeholder="Cari Lokasi ..."
                changeEvent={this.changeFilterKeyword}
                value={filterKeyword} />
            </div>
            <div className="col-sm-2">
              <button
                type="button"
                className="close"
                onClick={this.hidePopup}
              >
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
          </div>
        </div>
        <div className="content">
          <div>
              {data.length > 0 ? <div className="city-data">
                {this.rowContent(data)}
              </div> : <div className="data-empty-wrapper" style={String(filterKeyword).length > 0 ? { visibility: "visible" } : { visibility: "hidden" }}>
                  <div className="data-empty-info">
                    <div>Lokasi tidak ditemukan</div>
                    <div>Hasil pencarian untuk '{filterKeyword}' tidak ditemukan</div>
                  </div></div>}
            </div>
        </div>
        <div className="footer text-right">
          <button className="btn btn-default" type="button" onClick={this.resetData}>Reset</button>
          &nbsp;&nbsp;&nbsp;
          <button className="btn btn-primary" type="button" onClick={this.applyData}>Terapkan</button>
        </div>
      </div>
    );
  }
}

export default ChooseCityPopup;
