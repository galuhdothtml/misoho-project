/* eslint prop-types: 0 */
import React from "react"; // eslint-disable-line no-unused-vars
import PropTypes from "prop-types";
import InputCheckbox from "../form/InputCheckbox";

class CityRowItem extends React.Component {
  static propTypes = {
    title: PropTypes.string,
    cities: PropTypes.array,
    changeEvent: PropTypes.func,
  }

  static defaultProps = {
    title: "",
    cities: [],
    changeEvent: () => {},
  }

  render() {
    const { title, cities, changeEvent } = this.props;

    return (
      <div className="city-row">
        <h5>{title}</h5>
        <ul>
          {cities.map(x => (<li key={x.id}>
            <div>
              <InputCheckbox
                value={x.checked ? x.checked : false}
                changeEvent={val => changeEvent(x.id, x.id_province, val)}
              />
              <label className="form-check-label lightweight-text">{x.name}</label>
            </div>
          </li>))}
        </ul>
      </div>
    );
  }
}

export default CityRowItem;
