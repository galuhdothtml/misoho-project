/* eslint prop-types: 0 */
import React from "react";
import update from "immutability-helper";
import {
  getCarts, createAddress, getPostalCode, getMyAddresses,
  getMyAddress, getCouriers, checkoutOrder,
} from "../data";
import { API_BASE_URL } from "../constants";
import { currency, capitalizeFirstLetter } from "../utils";
import QtyInput from "./form/QtyInput";
import ModalPopup from "./form/ModalPopup";
import InputText from "./form/InputText";
import InputCheckbox from "./form/InputCheckbox";
import TextArea from "./form/TextArea";
import Select2 from "./form/Select2";

const errorContent = (type, val) => {
  let retval = null;

  if (type === "alias") {
    if (String(val).trim().length === 0) {
      retval = "* Nama alamat wajib diisi";
    }
  } else if (type === "receiver_name") {
    if (String(val).trim().length === 0) {
      retval = "* Nama penerima wajib diisi";
    }
  } else if (type === "address") {
    if (String(val).trim().length === 0) {
      retval = "* Alamat wajib diisi";
    }
  } else if (type === "phone") {
    if (String(val).trim().length === 0) {
      retval = "* Telepon wajib diisi";
    }
  } else if (type === "cityId") {
    if (String(val).trim().length === 0) {
      retval = "* Kota/Kabupaten wajib diisi";
    }
  } else if (type === "provinceId") {
    if (String(val).trim().length === 0) {
      retval = "* Provinsi wajib diisi";
    }
  } else if (type === "sub_district") {
    if (String(val).trim().length === 0) {
      retval = "* Kecamatan wajib diisi";
    }
  } else if (type === "urban") {
    if (String(val).trim().length === 0) {
      retval = "* Kelurahan wajib diisi";
    }
  }

  return retval;
};
class Cart extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      name: "Keranjang Belanja",
      carts: [],
      shippingCost: 0,
      totalBelanja: 0,
      totalWeight: 0,
      myAddresses: [],
      showAddAddress: false,
      showMyAddresses: false,
      showCourierList: false,
      current_id_address_delivery: "",
      myAddressData: null,
      currentCourier: null,
      courierList: [],
      form: {
        alias: "",
        defaultAddress: false,
        receiver_name: "",
        address: "",
        phone: "",
        provinceId: "",
        cityId: "",
        sub_district: "",
        urban: "",
        postcode: "",
      },
      errorMsg: {
        alias: "",
        receiver_name: "",
        address: "",
        phone: "",
        provinceId: "",
        cityId: "",
        sub_district: "",
        urban: "",
      },
      isValidated: false,
    };
  }

  componentWillMount = () => {
    this.setupCarts();
    this.setupMyAddresses();
  }

  setupCarts = async () => {
    const res = await getCarts();

    if (res.status) {
      let totalBelanja = 0;
      let totalWeight = 0;
      const carts = res.data.carts.map(x => ({
        id: x.id,
        name: x.product_name,
        price: parseFloat(x.product_price, 10),
        qty: x.qty,
        img: x.primary_img,
        weight: x.product_weight,
      }));
      carts.forEach((x) => {
        totalWeight += (parseFloat(x.weight, 10) * parseFloat(x.qty, 10));
        totalBelanja += (parseFloat(x.price, 10) * parseFloat(x.qty, 10));
      });
      this.setState({ carts, totalBelanja, totalWeight });
    }
  }

  setupMyAddresses = async () => {
    const { current_id_address_delivery: currentIdState } = this.state;
    const res = await getMyAddresses();
    let newData = [];
    let currentId = "";

    if (res.status) {
      newData = res.data;
      const found = newData.find(x => (x.is_default));

      if (!currentIdState && found) {
        currentId = found.id;
      }
    }

    this.setState({ myAddresses: newData, current_id_address_delivery: currentId }, () => {
      this.setupMyAddress();
    });
  }

  setupMyAddress = async () => {
    const { current_id_address_delivery: currentId } = this.state;

    if (currentId) {
      const res = await getMyAddress(currentId);

      if (res.status) {
        this.setState({ myAddressData: res.data }, () => {
          this.setupCouriers();
        });
      }
    }
  }

  setupCouriers = async () => {
    const { currentCourier } = this.state;
    const fetchJne = this.setupJneCouriers();
    const fetchPos = this.setupPosCouriers();
    const fetchTiki = this.setupTikiCouriers();

    const getJne = await fetchJne;
    const getPos = await fetchPos;
    const getTiki = await fetchTiki;

    const retval = [...getJne, ...getPos, ...getTiki];

    if (!currentCourier) {
      const newData = {
        code: retval[0].code,
        service: retval[0].service,
        price: retval[0].price,
      };
      this.setState({ courierList: retval, currentCourier: newData, shippingCost: newData.price });
    } else {
      this.setState({ courierList: retval });
    }
  }

  setupJneCouriers = async () => {
    const { myAddressData, totalWeight } = this.state;
    const newData = [];
    const payload = {
      destination: myAddressData.id_city,
      weight: totalWeight,
      courier: "jne",
    };
    const res = await getCouriers(payload);
    if (res.status && res.data.length > 0) {
      res.data[0].costs.forEach((x) => {
        const isValid = x.cost.length > 0;

        if (isValid) {
          newData.push({
            code: "jne",
            service: x.service,
            price: x.cost[0].value,
            etd: String(x.cost[0].etd) === "1-1" ? "1 Hari" : `${x.cost[0].etd} Hari`,
          });
        }
      });
    }

    return newData;
  }

  setupPosCouriers = async () => {
    const { myAddressData, totalWeight } = this.state;
    const newData = [];
    const code = "pos";
    const payload = {
      destination: myAddressData.id_city,
      weight: totalWeight,
      courier: code,
    };
    const res = await getCouriers(payload);
    if (res.status && res.data.length > 0) {
      res.data[0].costs.forEach((x) => {
        const isValid = x.cost.length > 0;

        if (isValid) {
          newData.push({
            code,
            service: x.service,
            price: x.cost[0].value,
            etd: capitalizeFirstLetter(String(`${x.cost[0].etd}`).toLowerCase()),
          });
        }
      });
    }

    return newData;
  }

  setupTikiCouriers = async () => {
    const { myAddressData, totalWeight } = this.state;
    const newData = [];
    const code = "tiki";
    const payload = {
      destination: myAddressData.id_city,
      weight: totalWeight,
      courier: code,
    };
    const res = await getCouriers(payload);
    if (res.status && res.data.length > 0) {
      res.data[0].costs.forEach((x) => {
        const isValid = x.cost.length > 0;

        if (isValid) {
          newData.push({
            code,
            service: x.service,
            price: x.cost[0].value,
            etd: `${x.cost[0].etd} Hari`,
          });
        }
      });
    }

    return newData;
  }

  changeCartHandler = (index, val) => {
    let totalBelanja = 0;
    const { carts } = this.state;
    const newData = update(carts, {
      [index]: {
        qty: { $set: parseInt(val, 10) ? parseInt(val, 10) : 0 },
      },
    });

    newData.forEach((x) => {
      totalBelanja += (parseFloat(x.price, 10) * parseFloat(x.qty, 10));
    });

    this.setState({ carts: newData, totalBelanja });
  }

  createComponent = (carts) => {
    const retval = [];

    if (carts.length > 0) {
      carts.forEach((x, i) => {
        const newImg = `${API_BASE_URL}/uploads/${x.img}`;
        retval.push(
          <div className="cart-item" key={x.id}>
            <div className="left"><img src={newImg} /></div>
            <div className="middle">
              <div className="title">{x.name}</div>
              <div className="subtitle">{currency(x.price)}</div>
              <div>
                <QtyInput index={i} value={x.qty} changeEvent={this.changeCartHandler} />
              </div>
            </div>
            <div className="right">
              <div>Jumlah: {x.qty}</div>
              <div className="trash-button-wrapper"><button type="button"><i className="fa fa-trash-o" /></button></div>
            </div>
          </div>,
        );
      });
    }

    return retval;
  }

  showAddAddressModal = () => {
    this.setState({ showAddAddress: true, showMyAddresses: false });
  }

  hideAddAddressModal = () => {
    this.setState({ showAddAddress: false });
  }

  showMyAddressesModal = () => {
    this.setState({ showMyAddresses: true });
  }

  hideMyAddressesModal = () => {
    this.setState({ showMyAddresses: false });
  }

  showCourierModal = () => {
    this.setState({ showCourierList: true });
  }

  hideCourierModal = () => {
    this.setState({ showCourierList: false });
  }

  saveAddressHandler = async (callback) => {
    const { form } = this.state;
    const error = this.bulkCreateErrorMessage();
    const isValid = !error.isError;

    this.setState({ isValidated: true, errorMsg: error.msg });

    if (isValid) {
      const payload = Object.assign({}, form, { id_postal_code: form.urban });
      await createAddress(payload);
      this.setupMyAddresses();
      this.hideAddAddressModal();
    }

    callback();
  }

  changeValueHandler = (type, val) => {
    const { form } = this.state;
    const newValue = update(form, {
      [type]: { $set: val },
    });

    const errorMsg = this.createErrorMessage(type, val);
    this.setState({ form: newValue, errorMsg });
  }

  createErrorMessage = (type, val) => {
    const { errorMsg, isValidated } = this.state;
    let error = "";

    if (!isValidated) {
      return errorMsg;
    }

    const ERROR_TEXT = errorContent(type, val);

    if (ERROR_TEXT) {
      error = ERROR_TEXT;
    }

    const newValue = update(errorMsg, {
      [type]: { $set: error },
    });

    return newValue;
  }

  bulkCreateErrorMessage = () => {
    const { errorMsg, form } = this.state;
    const keys = Object.keys(errorMsg);
    let newErrorMsg = errorMsg;
    let isError = false;

    keys.forEach((x) => {
      let error = "";
      const ERROR_TEXT = errorContent(x, form[x]);

      if (ERROR_TEXT) {
        error = ERROR_TEXT;
        isError = true;
      }

      newErrorMsg = update(newErrorMsg, {
        [x]: { $set: error },
      });
    });

    return { msg: newErrorMsg, isError };
  }

  changeIdProvinceHandler = (val) => {
    const { form } = this.state;
    const newValue = update(form, {
      provinceId: { $set: val },
      cityId: { $set: "" },
      sub_district: { $set: "" },
      urban: { $set: "" },
      postcode: { $set: "" },
    });

    const errorMsg = this.createErrorMessage("provinceId", val);
    this.setState({ form: newValue, errorMsg });
  }

  changeIdCityHandler = (val) => {
    const { form } = this.state;
    const newValue = update(form, {
      cityId: { $set: val },
      sub_district: { $set: "" },
      urban: { $set: "" },
      postcode: { $set: "" },
    });

    const errorMsg = this.createErrorMessage("cityId", val);
    this.setState({ form: newValue, errorMsg });
  }

  changeSubDistrictHandler = (val) => {
    const { form } = this.state;
    const newValue = update(form, {
      sub_district: { $set: val },
      urban: { $set: "" },
      postcode: { $set: "" },
    });

    const errorMsg = this.createErrorMessage("sub_district", val);
    this.setState({ form: newValue, errorMsg });
  }

  changeUrbanHandler = (val) => {
    const { form } = this.state;
    const newValue = update(form, {
      urban: { $set: val },
      postcode: { $set: "" },
    });

    const errorMsg = this.createErrorMessage("urban", val);
    this.setState({ form: newValue, errorMsg }, () => {
      this.setPostalCode();
    });
  }

  setPostalCode = async () => {
    const { form: { urban }, form } = this.state;
    const res = await getPostalCode(urban);

    if (res.status && res.data.length > 0) {
      const newValue = update(form, {
        postcode: { $set: res.data[0].postal_code },
      });
      this.setState({ form: newValue });
    }
  }

  chooseAddressHandler = (val) => {
    this.setState({ current_id_address_delivery: val }, () => {
      this.setupMyAddress();
      this.hideMyAddressesModal();
    });
  }

  createMyAddressList = (data) => {
    const { current_id_address_delivery: currentId } = this.state;
    const retval = [];

    retval.push(
      <div key="add-address-key" className="my-address-item add-address" onClick={() => { this.showAddAddressModal(); }}>
        <div className="add-address-placeholder">
          <div><i className="fa fa-plus-circle" /></div>
          <div><h4>Tambahkan Alamat</h4></div>
        </div>
      </div>,
    );

    data.forEach((x) => {
      if ((String(x.id) !== String(currentId))) {
        retval.push(
          <div key={x.id} className="my-address-item">
            <div className="row">
              <div className="col-sm-6"><h5>{x.receiver_name}</h5></div>
              <div className="col-sm-6 text-right"><a href="#"><i className="fa fa-times" /></a></div>
            </div>
            <div className="mt-xs">{x.address}</div>
            <div>{x.PostalCode.urban}</div>
            <div>{x.sub_district}</div>
            <div>{x.city_name}</div>
            <div>{x.province_name}</div>
            <div className="mb-md">{x.phone}</div>
            <div>
              <button type="button" className="btn btn-default btn-block green" onClick={() => this.chooseAddressHandler(x.id)}>Pilih</button>
            </div>
          </div>,
        );
      }
    });

    return (<div className="my-address-page-container popup">{retval}</div>);
  }

  chooseCourierHandler = (val) => {
    this.setState({ currentCourier: val, shippingCost: val.price }, () => {
      this.hideCourierModal();
    });
  }

  createCourierList = (data) => {
    const { currentCourier } = this.state;
    const retval = [];

    data.forEach((x) => {
      retval.push(
        <li key={`${x.code}-${x.service}`} className={(String(`${x.code}-${x.service}`) === String(`${currentCourier.code}-${currentCourier.service}`)) ? "choosen" : ""} onClick={() => this.chooseCourierHandler({ code: x.code, service: x.service, price: x.price })}>
          <div className="left">
            <div>{`${String(x.code).toUpperCase()} - ${x.service}`}</div>
            <div>{x.etd}</div>
          </div>
          <div className="right">
            <div>{currency(x.price)}</div>
          </div>
        </li>,
      );
    });

    return (<ul className="my-courier-list">{retval}</ul>);
  }

  createCurrentCourier = () => {
    const { currentCourier } = this.state;

    if (currentCourier) {
      return (
        <div>
          <div>{String(currentCourier.code).toUpperCase()}</div>
          <div>{currentCourier.service}</div>
          <div>{currency(currentCourier.price)}</div>
        </div>
      );
    }

    return (
      <div>Pilih jasa pengiriman</div>
    );
  }

  createCurrentAddress = () => {
    const { myAddressData } = this.state;

    if (myAddressData) {
      return (
        <div>
          <div>{myAddressData.receiver_name}</div>
          <div>{myAddressData.address}</div>
          <div>{`${myAddressData.PostalCode.urban}, ${myAddressData.PostalCode.sub_district}`}</div>
          <div>{`${myAddressData.city_name}, ${myAddressData.province_name}`}</div>
        </div>
      );
    }

    return (
      <div>Klik disini untuk membuat alamat pengiriman</div>
    );
  }

  processCheckoutOrder = async () => {
    const { current_id_address_delivery: idAddress, currentCourier } = this.state;

    if (idAddress && currentCourier) {
      const payload = {
        id_address: idAddress,
        shipping_courier: currentCourier.code,
        shipping_service: currentCourier.service,
      };

      const res = await checkoutOrder(payload);

      if (res.status) {
        // eslint-disable-next-line no-restricted-globals
        location.href = "/";
      }
    }
  }

  render() {
    const {
      carts, shippingCost, totalBelanja, showAddAddress, form, errorMsg, showMyAddresses, myAddresses,
      current_id_address_delivery: currentId, courierList, showCourierList,
    } = this.state;
    const subtotal = parseFloat(totalBelanja, 10) + parseFloat(shippingCost, 10);

    return (
      <div className="container cart-page-container">
        <div className="mt-sm">
          <div style={{ position: "relative", width: "82%", margin: "0 auto" }}>
            <div className="row">
              <div className="col-sm-7">
                <div className="panel panel-default">
                  <div className="panel-heading">
                    <span><i className="fa fa-building-o" />&nbsp;&nbsp;Alamat Pengiriman</span>
                  </div>
                  <div className="panel-body clickable" onClick={currentId ? this.showMyAddressesModal : this.showAddAddressModal}>
                    <div className="row">
                      <div className="col-sm-8">
                        {this.createCurrentAddress()}
                      </div>
                      <div className="col-sm-4 text-right">
                        <div><i className="fa fa-angle-right fa-2x" /></div>
                      </div>
                    </div>
                  </div>
                </div>
                <div className="panel panel-default">
                  <div className="panel-heading">
                    <span><i className="fa fa-shopping-cart" />&nbsp;&nbsp;Pesanan Anda</span>
                  </div>
                  <div className="panel-body" style={{ padding: "0px" }}>
                    <div className="cart-list-wrapper" style={{ width: "100%" }}>
                      {this.createComponent(carts)}
                    </div>
                  </div>
                </div>
              </div>
              <div className="col-sm-5">
                <div className="panel panel-default">
                  <div className="panel-heading">
                    <span><i className="fa fa-send-o" />&nbsp;&nbsp;Jasa Pengiriman</span>
                  </div>
                  <div className="panel-body clickable" onClick={this.showCourierModal}>
                    <div className="row">
                      <div className="col-sm-8">
                        {this.createCurrentCourier()}
                      </div>
                      <div className="col-sm-4 text-right">
                        <div><i className="fa fa-angle-right fa-2x" /></div>
                      </div>
                    </div>
                  </div>
                </div>
                <div className="panel panel-default">
                  <div className="panel-heading">
                    <span><i className="fa fa-calculator" />&nbsp;&nbsp;Rincian Pembayaran</span>
                  </div>
                  <div className="panel-body">
                    <div className="row mb-xs">
                      <div className="col-sm-6">
                        <div>Total Belanja</div>
                      </div>
                      <div className="col-sm-6 text-right">
                        <div>{currency(totalBelanja)}</div>
                      </div>
                    </div>
                    <div className="row mb-xs">
                      <div className="col-sm-6">
                        <div>Biaya Pengiriman</div>
                      </div>
                      <div className="col-sm-6 text-right">
                        <div>{currency(shippingCost)}</div>
                      </div>
                    </div>
                    <div className="row mb-xs">
                      <div className="col-sm-6">
                        <div>SUBTOTAL</div>
                      </div>
                      <div className="col-sm-6 text-right">
                        <div>{currency(subtotal)}</div>
                      </div>
                    </div>
                    <div>
                      <button onClick={this.processCheckoutOrder} className="button" className="btn btn-default btn-block">Lanjut</button>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        {showAddAddress && (
          <ModalPopup
            width={450}
            title="Buat Alamat Pengiriman"
            hideModal={this.hideAddAddressModal}
            saveHandler={this.saveAddressHandler}>
            <div className="row mb-xs">
              <div className="col-sm-12">
                <InputText
                  label="Label Alamat"
                  changeEvent={val => this.changeValueHandler("alias", val)}
                  value={form.alias}
                  errorText={errorMsg.alias}
                />
              </div>
            </div>
            <div className="row mb-xs">
              <div className="col-sm-12">
                <InputCheckbox label="Jadikan Alamat Utama" value={form.defaultAddress} changeEvent={val => this.changeValueHandler("defaultAddress", val)} />
              </div>
            </div>
            <div className="row mb-xs">
              <div className="col-sm-12">
                <InputText
                  label="Nama Penerima"
                  changeEvent={val => this.changeValueHandler("receiver_name", val)}
                  value={form.receiver_name}
                  errorText={errorMsg.receiver_name}
                />
              </div>
            </div>
            <div className="row mb-xs">
              <div className="col-sm-12">
                <TextArea
                  label="Alamat"
                  changeEvent={val => this.changeValueHandler("address", val)}
                  value={form.address}
                  errorText={errorMsg.address}
                />
              </div>
            </div>
            <div className="row mb-xs">
              <div className="col-sm-12">
                <InputText
                  label="No Telepon Penerima"
                  changeEvent={val => this.changeValueHandler("phone", val)}
                  value={form.phone}
                  errorText={errorMsg.phone}
                />
              </div>
            </div>
            <div className="row mb-xs">
              <div className="col-sm-12">
                <Select2
                  label="Provinsi"
                  url="/api/v2/provinces"
                  placeholder="- Pilih Provinsi -"
                  value={form.provinceId}
                  changeEvent={this.changeIdProvinceHandler}
                  errorText={errorMsg.provinceId}
                />
              </div>
            </div>
            <div className="row mb-xs">
              <div className="col-sm-12">
                <Select2
                  label="Kab./Kota"
                  url="/api/v2/cities"
                  additionalParam={{ idProvince: form.provinceId }}
                  placeholder="- Pilih Kab./Kota -"
                  value={form.cityId}
                  changeEvent={this.changeIdCityHandler}
                  errorText={errorMsg.cityId}
                />
              </div>
            </div>
            <div className="row mb-xs">
              <div className="col-sm-12">
                <Select2
                  label="Kecamatan"
                  url="/api/sub_districts"
                  additionalParam={{ idCity: form.cityId }}
                  placeholder="- Pilih Kecamatan -"
                  value={form.sub_district}
                  changeEvent={this.changeSubDistrictHandler}
                  errorText={errorMsg.sub_district}
                />
              </div>
            </div>
            <div className="row mb-xs">
              <div className="col-sm-12">
                <Select2
                  label="Kelurahan"
                  url="/api/urbans"
                  additionalParam={{ idCity: form.cityId, subDistrict: form.sub_district }}
                  placeholder="- Pilih Kelurahan -"
                  value={form.urban}
                  changeEvent={this.changeUrbanHandler}
                  errorText={errorMsg.urban}
                />
              </div>
            </div>
            <div className="row mb-xs">
              <div className="col-sm-12">
                <InputText
                  label="Kode Pos"
                  changeEvent={() => { }}
                  value={form.postcode}
                />
              </div>
            </div>
          </ModalPopup>
        )}
        {showMyAddresses && (
          <ModalPopup
            width={750}
            title="Pilih Alamat Pengiriman"
            hideModal={this.hideMyAddressesModal}>
            <div style={{ padding: "15px" }}>
              <div className="row">
                <div className="col-sm-12">
                  {this.createMyAddressList(myAddresses)}
                </div>
              </div>
            </div>
          </ModalPopup>
        )}
        {showCourierList && (
          <ModalPopup
            width={450}
            title="Pilih Jasa Pengiriman"
            hideModal={this.hideCourierModal}>
            <div style={{ padding: "5px 20px" }}>
              <div className="row">
                <div className="col-sm-12">
                  {this.createCourierList(courierList)}
                </div>
              </div>
            </div>
          </ModalPopup>
        )}
      </div>
    );
  }
}

export default Cart;
