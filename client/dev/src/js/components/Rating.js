/* eslint prop-types: 0 */
import React from "react";
import PropTypes from "prop-types";

export default class Rating extends React.Component {
  static propTypes = {
    value: PropTypes.number,
    changeEvent: PropTypes.func,
  }

  static defaultProps = {
    value: undefined,
    changeEvent: () => {},
  }

  ratingItemComponent = ratingValue => (<div key={ratingValue} className="rating-item" onClick={() => this.ratingClickHandler(ratingValue)}><i className="fa fa-star" /></div>);

  emptyRatingItemComponent = ratingValue => (<div key={ratingValue} className="rating-item empty" onClick={() => this.ratingClickHandler(ratingValue)}><i className="fa fa-star" /></div>);

  createComponent = (value) => {
    const retval = [];
    let ratingValue = 0;

    if (parseInt(value, 10) > 0) {
      for (let i = 0; i < parseInt(value, 10); i += 1) {
        ratingValue += 1;
        retval.push(this.ratingItemComponent(ratingValue));
      }
    }

    for (let i = 0; i < (5 - parseInt(value, 10)); i += 1) {
      ratingValue += 1;
      retval.push(this.emptyRatingItemComponent(ratingValue));
    }

    return retval;
  }

  ratingClickHandler = (val) => {
    const { changeEvent } = this.props;

    changeEvent(val);
  }

  render() {
    const { value } = this.props;

    return (
      <div className="rating-list-wrapper">
        {this.createComponent(value)}
      </div>
    );
  }
}
