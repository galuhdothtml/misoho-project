/* eslint prop-types: 0 */
import React from "react"; // eslint-disable-line no-unused-vars
import LoadingImage from "../images/loading.gif";

const component = ({ isAbsolute, customClasses }) => {
  let wrapperClass = "loading-wrapper";

  if (isAbsolute) {
    wrapperClass = "loading-wrapper absolute-loading";
  }

  if (customClasses) {
    wrapperClass = `${wrapperClass} ${customClasses}`;
  }

  return (
      <div className={wrapperClass}>
        <div className="table-wrapper">
          <div className="loading-content">
            <img src={LoadingImage} />
            <h5>Mohon tunggu sebentar...</h5>
          </div>
        </div>
      </div>
  );
};

export default component;
