import React, { Fragment } from "react";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";

const CloseButtonToast = ({ closeToast }) => <div style={{
  position: "relative",
  top: "13px",
}} onClick={closeToast}>OK</div>;
const HoC = HocComponent => class extends React.Component {
  showNotification = (val) => {
    toast.success(val, {
      position: "top-center",
    });
  }

  render() {
    return (
      <Fragment>
        <HocComponent {...this.props} showNotification={this.showNotification} />
        <ToastContainer
          position="bottom-left"
          autoClose={5000}
          hideProgressBar
          newestOnTop={false}
          closeOnClick
          rtl={false}
          pauseOnVisibilityChange
          draggable={false}
          pauseOnHover={false}
          closeOnClick={false}
          closeButton={<CloseButtonToast />}
        />
      </Fragment>
    );
  }
};

export default HoC;
