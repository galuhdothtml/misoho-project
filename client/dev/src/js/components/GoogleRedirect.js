/* eslint prop-types: 0 */
import React from "react";
import queryString from "query-string";
import {
  setupGoogleAccount,
} from "../data";
import * as Util from "../utils";

class GoogleRedirect extends React.Component {
  componentDidMount = () => {
    const { location } = this.props;
    const values = queryString.parse(location.search);

    this.setupData(values.code);
  }

  setupData = async (code) => {
    const res = await setupGoogleAccount({ code });

    if (res.status) {
      Util.setToken(res.token);
      location.href = "/";
    }
  }

  render = () => (<div />);
}

export default GoogleRedirect;
