/* eslint prop-types: 0 */
import React from "react";
import HoC from "../AuthHoC";
import { getMyAddresses, setDefaultAddress } from "../../../data";

class Address extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      data: [],
    };
  }

  componentWillMount = () => {
    this.fetchData();
    this.setupBreadcrumb();
  }

  setupBreadcrumb = () => {
    const { addBreadcrumbData } = this.props;

    addBreadcrumbData([
      { title: "Home", link: "/" },
      { title: "Alamat Pengiriman", isActive: true },
    ]);
  }

  fetchData = async () => {
    const res = await getMyAddresses();

    if (res.status) {
      this.setState({ data: res.data });
    }
  }

  applyDefaultAddress = async (idAddress) => {
    const res = await setDefaultAddress(idAddress);

    if (res.status) {
      // eslint-disable-next-line no-restricted-globals
      location.reload();
    }
  }

  navToAddressDetail = (id) => {
    const { history } = this.props;

    history.push(`/my-addresses/detail/${id}`);
  }

  createComponentData = (rawData, showDefaultOnly = false) => {
    const retval = [];
    let data = rawData.filter(x => (!x.is_default));

    if (showDefaultOnly) {
      data = rawData.filter(x => (x.is_default));
    }

    data.forEach((x) => {
      retval.push(
        <div key={x.id} className="my-address-item">
          <div className="row">
            <div className="col-sm-6"><h5>{x.receiver_name}</h5></div>
            <div className="col-sm-6 text-right"><a href="#"><i className="fa fa-times" /></a></div>
          </div>
          <div className="mt-xs">{x.address}</div>
          <div>{x.PostalCode.urban}</div>
          <div>{x.sub_district}</div>
          <div>{x.city_name}</div>
          <div>{x.province_name}</div>
          <div className="mb-md">{x.phone}</div>
          <div className="mb-sm">
            <button type="button" className="btn btn-default btn-block" onClick={() => this.navToAddressDetail(x.id)}>Ubah Data Alamat</button>
          </div>
          <div>
            <button type="button" className="btn btn-default btn-block green" onClick={() => this.applyDefaultAddress(x.id)}>Pilih Sebagai Default</button>
          </div>
        </div>,
      );
    });

    if (!showDefaultOnly) {
      retval.push(
        <div key="add-address-key" className="my-address-item add-address" onClick={() => this.navToAddressDetail("add")}>
          <div className="add-address-placeholder">
            <div><i className="fa fa-plus-circle" /></div>
            <div><h4>Tambahkan Alamat</h4></div>
          </div>
        </div>,
      );
    }

    return (
      <div className="my-address-page-container">{retval}</div>
    );
  }

  render() {
    const { data } = this.state;

    return (
      <div>
        <div className="my-address-box">
          <h5>Alamat Default</h5>
          {this.createComponentData(data, true)}
        </div>
        <div className="my-address-box">
          <h5>Alternatif Alamat</h5>
          {this.createComponentData(data)}
        </div>
      </div>
    );
  }
}

export default HoC(Address);
