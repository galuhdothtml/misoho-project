/* eslint prop-types: 0 */
import React from "react";
import update from "immutability-helper";
import {
  getPostalCode, getMyAddress, updateAddress,
  createAddress,
} from "../../../data";
import HoC from "../AuthHoC";
import InputText from "../../form/InputText";
import InputCheckbox from "../../form/InputCheckbox";
import TextArea from "../../form/TextArea";
import Select2 from "../../form/Select2";

const errorContent = (type, val) => {
  let retval = null;

  if (type === "alias") {
    if (String(val).trim().length === 0) {
      retval = "* Nama alamat wajib diisi";
    }
  } else if (type === "receiver_name") {
    if (String(val).trim().length === 0) {
      retval = "* Nama penerima wajib diisi";
    }
  } else if (type === "address") {
    if (String(val).trim().length === 0) {
      retval = "* Alamat wajib diisi";
    }
  } else if (type === "phone") {
    if (String(val).trim().length === 0) {
      retval = "* Telepon wajib diisi";
    }
  } else if (type === "cityId") {
    if (String(val).trim().length === 0) {
      retval = "* Kota/Kabupaten wajib diisi";
    }
  } else if (type === "provinceId") {
    if (String(val).trim().length === 0) {
      retval = "* Provinsi wajib diisi";
    }
  } else if (type === "sub_district") {
    if (String(val).trim().length === 0) {
      retval = "* Kecamatan wajib diisi";
    }
  } else if (type === "urban") {
    if (String(val).trim().length === 0) {
      retval = "* Kelurahan wajib diisi";
    }
  }

  return retval;
};
class AddressDetail extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      title: "Address Detail",
      type: "create",
      form: {
        id: "",
        alias: "",
        defaultAddress: false,
        receiver_name: "",
        address: "",
        phone: "",
        provinceId: "",
        cityId: "",
        sub_district: "",
        urban: "",
        postcode: "",
      },
      errorMsg: {
        alias: "",
        receiver_name: "",
        address: "",
        phone: "",
        provinceId: "",
        cityId: "",
        sub_district: "",
        urban: "",
      },
    };
  }

  componentWillMount = () => {
    this.fetchDetail();
    this.setupBreadcrumb();
  }

  setupBreadcrumb = () => {
    const {
      match: {
        params,
      },
      addBreadcrumbData,
    } = this.props;

    let data = [
      { title: "Home", link: "/" },
      { title: "Alamat Pengiriman", link: "/my-addresses" },
      { title: "Ubah Alamat", isActive: true },
    ];

    if (String(params.id) === "add") {
      data = [
        { title: "Home", link: "/" },
        { title: "Alamat Pengiriman", link: "/my-addresses" },
        { title: "Tambah Alamat", isActive: true },
      ];
    }

    addBreadcrumbData(data);
  }

  fetchDetail = async () => {
    const {
      match: {
        params
      },
    } = this.props;

    if (String(params.id) === "add") {
      const newData = {
        id: "",
        alias: "",
        defaultAddress: false,
        receiver_name: "",
        address: "",
        phone: "",
        provinceId: "",
        cityId: "",
        sub_district: "",
        urban: "",
        postcode: "",
      };

      this.setState({ form: newData, type: "create" });
    } else {
      const res = await getMyAddress(params.id);

      if (res.status) {
        const { data } = res;

        const newData = {
          id: data.id,
          alias: data.alias,
          defaultAddress: data.is_default,
          receiver_name: data.receiver_name,
          address: data.address,
          phone: data.phone,
          provinceId: String(data.id_province),
          cityId: String(data.id_city),
          sub_district: String(data.sub_district),
          urban: String(data.id_postal_code),
          postcode: data.postcode,
        };

        this.setState({ form: newData, type: "edit" });
      }
    }
  }

  changeValueHandler = (type, val) => {
    const { form } = this.state;
    const newValue = update(form, {
      [type]: { $set: val },
    });

    const errorMsg = this.createErrorMessage(type, val);
    this.setState({ form: newValue, errorMsg });
  }


  changeIdProvinceHandler = (val) => {
    const { form } = this.state;
    const newValue = update(form, {
      provinceId: { $set: val },
      cityId: { $set: "" },
      sub_district: { $set: "" },
      urban: { $set: "" },
      postcode: { $set: "" },
    });

    const errorMsg = this.createErrorMessage("provinceId", val);
    this.setState({ form: newValue, errorMsg });
  }

  changeIdCityHandler = (val) => {
    const { form } = this.state;
    const newValue = update(form, {
      cityId: { $set: val },
      sub_district: { $set: "" },
      urban: { $set: "" },
      postcode: { $set: "" },
    });

    const errorMsg = this.createErrorMessage("cityId", val);
    this.setState({ form: newValue, errorMsg });
  }

  changeSubDistrictHandler = (val) => {
    const { form } = this.state;
    const newValue = update(form, {
      sub_district: { $set: val },
      urban: { $set: "" },
      postcode: { $set: "" },
    });

    const errorMsg = this.createErrorMessage("sub_district", val);
    this.setState({ form: newValue, errorMsg });
  }

  changeUrbanHandler = (val) => {
    const { form } = this.state;
    const newValue = update(form, {
      urban: { $set: val },
      postcode: { $set: "" },
    });

    const errorMsg = this.createErrorMessage("urban", val);
    this.setState({ form: newValue, errorMsg }, () => {
      this.setPostalCode();
    });
  }

  createErrorMessage = (type, val) => {
    const { errorMsg, isValidated } = this.state;
    let error = "";

    if (!isValidated) {
      return errorMsg;
    }

    const ERROR_TEXT = errorContent(type, val);

    if (ERROR_TEXT) {
      error = ERROR_TEXT;
    }

    const newValue = update(errorMsg, {
      [type]: { $set: error },
    });

    return newValue;
  }

  bulkCreateErrorMessage = () => {
    const { errorMsg, form } = this.state;
    const keys = Object.keys(errorMsg);
    let newErrorMsg = errorMsg;
    let isError = false;

    keys.forEach((x) => {
      let error = "";
      const ERROR_TEXT = errorContent(x, form[x]);

      if (ERROR_TEXT) {
        error = ERROR_TEXT;
        isError = true;
      }

      newErrorMsg = update(newErrorMsg, {
        [x]: { $set: error },
      });
    });

    return { msg: newErrorMsg, isError };
  }

  saveHandler = async () => {
    const { form, type } = this.state;
    const error = this.bulkCreateErrorMessage();
    const isValid = !error.isError;

    this.setState({ isValidated: true, errorMsg: error.msg });

    if (isValid) {
      const payload = Object.assign({}, form, { id_postal_code: form.urban });
      let res = null;

      if (type === "create") {
        res = await createAddress(payload);
      } else {
        res = await updateAddress(payload);
      }

      if (res.status) {
        this.navToMyAddresses();
      }
    }
  }

  navToMyAddresses = () => {
    const { history } = this.props;

    history.push("/my-addresses");
  }

  setPostalCode = async () => {
    const { form: { urban }, form } = this.state;
    const res = await getPostalCode(urban);

    if (res.status && res.data.length > 0) {
      const newValue = update(form, {
        postcode: { $set: res.data[0].postal_code },
      });
      this.setState({ form: newValue });
    }
  }

  render() {
    const { form, errorMsg } = this.state;

    return (
      <div>
        <div className="row mb-xs">
          <div className="col-sm-8">
            <InputText
              label="Label Alamat"
              changeEvent={val => this.changeValueHandler("alias", val)}
              value={form.alias}
              errorText={errorMsg.alias}
            />
          </div>
        </div>
        <div className="row mb-xs">
          <div className="col-sm-8">
            <InputCheckbox label="Jadikan Alamat Utama" value={form.defaultAddress} changeEvent={val => this.changeValueHandler("defaultAddress", val)} />
          </div>
        </div>
        <div className="row mb-xs">
          <div className="col-sm-8">
            <InputText
              label="Nama Penerima"
              changeEvent={val => this.changeValueHandler("receiver_name", val)}
              value={form.receiver_name}
              errorText={errorMsg.receiver_name}
            />
          </div>
        </div>
        <div className="row mb-xs">
          <div className="col-sm-8">
            <TextArea
              label="Alamat"
              changeEvent={val => this.changeValueHandler("address", val)}
              value={form.address}
              errorText={errorMsg.address}
            />
          </div>
        </div>
        <div className="row mb-xs">
          <div className="col-sm-8">
            <InputText
              label="No Telepon Penerima"
              changeEvent={val => this.changeValueHandler("phone", val)}
              value={form.phone}
              errorText={errorMsg.phone}
            />
          </div>
        </div>
        <div className="row mb-xs">
          <div className="col-sm-8">
            <Select2
              label="Provinsi"
              url="/api/v2/provinces"
              placeholder="- Pilih Provinsi -"
              value={form.provinceId}
              changeEvent={this.changeIdProvinceHandler}
              errorText={errorMsg.provinceId}
            />
          </div>
        </div>
        <div className="row mb-xs">
          <div className="col-sm-8">
            <Select2
              label="Kab./Kota"
              url="/api/v2/cities"
              additionalParam={{ idProvince: form.provinceId }}
              placeholder="- Pilih Kab./Kota -"
              value={form.cityId}
              changeEvent={this.changeIdCityHandler}
              errorText={errorMsg.cityId}
            />
          </div>
        </div>
        <div className="row mb-xs">
          <div className="col-sm-8">
            <Select2
              label="Kecamatan"
              url="/api/sub_districts"
              additionalParam={{ idCity: form.cityId }}
              placeholder="- Pilih Kecamatan -"
              value={form.sub_district}
              changeEvent={this.changeSubDistrictHandler}
              errorText={errorMsg.sub_district}
            />
          </div>
        </div>
        <div className="row mb-xs">
          <div className="col-sm-8">
            <Select2
              label="Kelurahan"
              url="/api/urbans"
              additionalParam={{ idCity: form.cityId, subDistrict: form.sub_district }}
              placeholder="- Pilih Kelurahan -"
              value={form.urban}
              changeEvent={this.changeUrbanHandler}
              errorText={errorMsg.urban}
            />
          </div>
        </div>
        <div className="row mb-xs">
          <div className="col-sm-8">
            <InputText
              label="Kode Pos"
              changeEvent={() => { }}
              value={form.postcode}
            />
          </div>
        </div>
        <div className="row mb-xs">
          <div className="col-sm-3">
            <button type="button" className="btn btn-primary btn-block" onClick={this.saveHandler}>Simpan</button>
          </div>
        </div>
      </div>
    );
  }
}

export default HoC(AddressDetail);
