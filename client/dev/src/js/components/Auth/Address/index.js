/* eslint prop-types: 0 */
import React from "react";
import update from "immutability-helper";
import HoC from "../AuthHoC";
import {
  getMyAddresses, updateAddress, deleteAddress,
  createAddress, getPostalCode, setDefaultAddress,
} from "../../../data";
import Select from "../../form/Select";
import InputText from "../../form/InputText";
import TextArea from "../../form/TextArea";
import Select2 from "../../form/Select2";
import ModalPopup from "../../form/ModalPopup";
import CoreHoC from "../../CoreHoC";

const errorContent = (type, val) => {
  let retval = null;

  if (type === "alias") {
    if (String(val).trim().length === 0) {
      retval = "* Nama alamat wajib diisi";
    }
  } else if (type === "receiver_name") {
    if (String(val).trim().length === 0) {
      retval = "* Nama penerima wajib diisi";
    }
  } else if (type === "address") {
    if (String(val).trim().length === 0) {
      retval = "* Alamat wajib diisi";
    }
  } else if (type === "phone") {
    if (String(val).trim().length === 0) {
      retval = "* Telepon wajib diisi";
    }
  } else if (type === "cityId") {
    if (String(val).trim().length === 0) {
      retval = "* Kota/Kabupaten wajib diisi";
    }
  } else if (type === "provinceId") {
    if (String(val).trim().length === 0) {
      retval = "* Provinsi wajib diisi";
    }
  } else if (type === "sub_district") {
    if (String(val).trim().length === 0) {
      retval = "* Kecamatan wajib diisi";
    }
  } else if (type === "urban") {
    if (String(val).trim().length === 0) {
      retval = "* Kelurahan wajib diisi";
    }
  }

  return retval;
};
const sortingList = [
  {
    id: "1",
    name: "Alamat Terbaru",
  },
  {
    id: "2",
    name: "Nama Alamat",
  },
  {
    id: "3",
    name: "Nama Penerima",
  },
];
class Address extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      data: [],
      sortingId: "1",
      form: {
        id: "",
        alias: "",
        receiver_name: "",
        address: "",
        phone: "",
        provinceId: "",
        provinceName: "",
        cityId: "",
        cityName: "",
        sub_district: "",
        urban: "",
        urbanName: "",
        postcode: "",
        defaultAddress: false,
      },
      errorMsg: {
        alias: "",
        receiver_name: "",
        address: "",
        phone: "",
        provinceId: "",
        cityId: "",
        sub_district: "",
        urban: "",
      },
      isAdding: true,
      showPopup: false,
      showConfirmationPopup: false,
      showConfirmationDeletePopup: false,
      isValidated: false,
    };
  }

  componentWillMount = () => {
    this.fetchData();
    this.setupBreadcrumb();
  }

  setupBreadcrumb = () => {
    const { addBreadcrumbData } = this.props;

    addBreadcrumbData([
      { title: "Home", link: "/" },
      { title: "Alamat Pengiriman", isActive: true },
    ]);
  }

  fetchData = async () => {
    const res = await getMyAddresses();

    if (res.status) {
      this.setState({ data: res.data });
    }
  }

  createComponentData = (data) => {
    const bodyComponent = [];

    data.forEach((x) => {
      bodyComponent.push(
        <tr key={x.id}>
          <td className="text-center">
            <div className="radio-wrapper" style={{
              position: "relative",
              bottom: "6px",
              left: "12px",
            }}>
              <div className="radio-container" onClick={() => this.startToApplyDefaultAddress(x.id, x.alias, x.is_default)}>
                <input
                  type="radio"
                  name="courier-choice"
                  checked={x.is_default}
                  onChange={() => { }}
                  value={x.id}
                />
                <span className="checkmark"></span>
              </div>
            </div>
          </td>
          <td>
            <div>
              <div>{x.receiver_name}</div>
              <div className="lightweight-text">{x.phone}</div>
            </div>
          </td>
          <td>
            <div>
              <div>{x.alias}</div>
              <div className="lightweight-text">{x.address}</div>
            </div>
          </td>
          <td>
            <div className="lightweight-text">
              {x.PostalCode.urban}, {x.sub_district}
              <br />
              {x.city_name}, {x.province_name}, {x.postcode}
            </div>
          </td>
          <td>
            <button type="button" className="btn btn-default grey btn-sm" onClick={() => this.startToEditData(x.id)}><i className="fa fa-edit" />&nbsp;&nbsp;Ubah</button>
            &nbsp;&nbsp;
            <button type="button" className="btn btn-default grey btn-sm" onClick={() => this.startToApplyRemoveAddress(x.id, x.alias)}><i className="fa fa-trash-o" />&nbsp;&nbsp;Hapus</button>
          </td>
        </tr>,
      );
    });

    return (
      <table className="table">
        <thead>
          <tr>
            <th></th>
            <th>Penerima</th>
            <th>Alamat Pengiriman</th>
            <th>Daerah Pengiriman</th>
            <th></th>
          </tr>
        </thead>
        <tbody>
          {bodyComponent}
        </tbody>
      </table>
    );
  }

  startToApplyDefaultAddress = (id, alias, isDefault) => {
    if (!isDefault) {
      const { form } = this.state;
      const newValue = update(form, {
        id: { $set: id },
        alias: { $set: alias },
      });

      this.setState({ form: newValue }, () => {
        this.startShowingConfirmationPopup();
      });
    }
  }

  startToApplyRemoveAddress = (id, alias) => {
    const { form } = this.state;
    const newValue = update(form, {
      id: { $set: id },
      alias: { $set: alias },
    });

    this.setState({ form: newValue }, () => {
      this.startShowingConfirmationDeletePopup();
    });
  }

  startToAddData = () => {
    const newForm = {
      id: "",
      alias: "",
      receiver_name: "",
      address: "",
      phone: "",
      provinceId: "",
      provinceName: "",
      cityId: "",
      cityName: "",
      sub_district: "",
      urban: "",
      urbanName: "",
      postcode: "",
      defaultAddress: false,
    };
    const errorMsg = {
      alias: "",
      receiver_name: "",
      address: "",
      phone: "",
      provinceId: "",
      cityId: "",
      sub_district: "",
      urban: "",
    };

    this.setState({
      isValidated: false, isAdding: true, form: newForm, errorMsg,
    }, () => {
      this.startShowingPopup();
    });
  }

  startToEditData = (id) => {
    const { data } = this.state;
    const found = data.find(x => (String(x.id) === String(id)));

    if (found) {
      const newForm = {
        id: found.id,
        alias: found.alias,
        receiver_name: found.receiver_name,
        address: found.address,
        phone: found.phone,
        provinceId: String(found.id_province),
        provinceName: found.province_name,
        cityId: String(found.id_city),
        cityName: found.city_name,
        sub_district: found.sub_district,
        urban: String(found.PostalCode.id),
        postcode: found.postcode,
        urbanName: found.PostalCode.urban,
        defaultAddress: found.is_default,
      };
      const errorMsg = {
        alias: "",
        receiver_name: "",
        address: "",
        phone: "",
        provinceId: "",
        cityId: "",
        sub_district: "",
        urban: "",
      };

      this.setState({
        isValidated: false, isAdding: false, form: newForm, errorMsg,
      }, () => {
        this.startShowingPopup();
      });
    }
  }

  startShowingPopup = () => {
    this.setState({ showPopup: true });
  }

  endShowingPopup = () => {
    this.setState({ showPopup: false });
  }

  startShowingConfirmationPopup = () => {
    this.setState({ showConfirmationPopup: true });
  }

  endShowingConfirmationPopup = () => {
    this.setState({ showConfirmationPopup: false });
  }

  startShowingConfirmationDeletePopup = () => {
    this.setState({ showConfirmationDeletePopup: true });
  }

  endShowingConfirmationDeletePopup = () => {
    this.setState({ showConfirmationDeletePopup: false });
  }

  changeValueHandler = (type, val) => {
    const { form } = this.state;
    const newValue = update(form, {
      [type]: { $set: val },
    });

    const errorMsg = this.createErrorMessage(type, val);
    this.setState({ form: newValue, errorMsg });
  }

  changeIdProvinceHandler = (val) => {
    const { form } = this.state;
    const newValue = update(form, {
      provinceId: { $set: val },
      cityId: { $set: "" },
      sub_district: { $set: "" },
      urban: { $set: "" },
      postcode: { $set: "" },
    });

    const errorMsg = this.createErrorMessage("provinceId", val);
    this.setState({ form: newValue, errorMsg });
  }

  changeIdCityHandler = (val) => {
    const { form } = this.state;
    const newValue = update(form, {
      cityId: { $set: val },
      sub_district: { $set: "" },
      urban: { $set: "" },
      postcode: { $set: "" },
    });

    const errorMsg = this.createErrorMessage("cityId", val);
    this.setState({ form: newValue, errorMsg });
  }

  changeSubDistrictHandler = (val) => {
    const { form } = this.state;
    const newValue = update(form, {
      sub_district: { $set: val },
      urban: { $set: "" },
      postcode: { $set: "" },
    });

    const errorMsg = this.createErrorMessage("sub_district", val);
    this.setState({ form: newValue, errorMsg });
  }

  changeUrbanHandler = (val) => {
    const { form } = this.state;
    const newValue = update(form, {
      urban: { $set: val },
      postcode: { $set: "" },
    });

    const errorMsg = this.createErrorMessage("urban", val);
    this.setState({ form: newValue, errorMsg }, () => {
      this.setPostalCode();
    });
  }

  setPostalCode = async () => {
    const { form: { urban }, form } = this.state;
    const res = await getPostalCode(urban);

    if (res.status && res.data.length > 0) {
      const newValue = update(form, {
        postcode: { $set: res.data[0].postal_code },
      });
      this.setState({ form: newValue });
    }
  }

  createErrorMessage = (type, val) => {
    const { errorMsg, isValidated } = this.state;
    let error = "";

    if (!isValidated) {
      return errorMsg;
    }

    const ERROR_TEXT = errorContent(type, val);

    if (ERROR_TEXT) {
      error = ERROR_TEXT;
    }

    const newValue = update(errorMsg, {
      [type]: { $set: error },
    });

    return newValue;
  }

  bulkCreateErrorMessage = () => {
    const { errorMsg, form } = this.state;
    const keys = Object.keys(errorMsg);
    let newErrorMsg = errorMsg;
    let isError = false;

    keys.forEach((x) => {
      let error = "";
      const ERROR_TEXT = errorContent(x, form[x]);

      if (ERROR_TEXT) {
        error = ERROR_TEXT;
        isError = true;
      }

      newErrorMsg = update(newErrorMsg, {
        [x]: { $set: error },
      });
    });

    return { msg: newErrorMsg, isError };
  }

  saveHandler = async (callback) => {
    const { showNotification } = this.props;
    const { form, isAdding } = this.state;
    const error = this.bulkCreateErrorMessage();
    const isValid = !error.isError;

    this.setState({ isValidated: true, errorMsg: error.msg });

    if (isValid) {
      let res = null;
      const payload = Object.assign({}, form, { id_postal_code: form.urban });

      if (isAdding) {
        res = await createAddress(payload);
      } else {
        res = await updateAddress(payload);
      }

      if (res.status) {
        this.refreshDataThenHidePopup();
        if (isAdding) {
          showNotification("Alamat pengiriman berhasil ditambahkan!");
        } else {
          showNotification("Berhasil mengubah alamat pengiriman!");
        }
      }
    } else {
      callback();
    }
  }

  refreshDataThenHidePopup = async () => {
    await this.fetchData();
    this.endShowingPopup();
  }

  applyDefaultAddress = async () => {
    const { form } = this.state;

    await setDefaultAddress(form.id);
    this.endShowingConfirmationPopup();
    this.fetchData();
  }

  applyRemoveAddress = async () => {
    const { form } = this.state;

    await deleteAddress(form.id);
    this.endShowingConfirmationDeletePopup();
    this.fetchData();
  }

  render() {
    const {
      data, sortingId, showPopup, isAdding,
      form, errorMsg, showConfirmationPopup,
      showConfirmationDeletePopup,
    } = this.state;

    return (
      <div className="profile-wrapper">
        <h4 className="header-title">Alamat Pengiriman</h4>
        <hr className="divider" />
        <div style={{
          border: "1px solid #efefef",
          background: "rgb(255, 255, 255)",
          padding: "15px 15px 0px",
          boxShadow: "0 1px 6px 0 rgba(49, 53, 59, 0.12)",
        }}>
          <div className="row mb-md">
            <div className="col-sm-4">
              <button type="button" className="btn btn-primary" onClick={this.startToAddData}><i style={{ display: "inline-block" }} className="mdi mdi-plus" />&nbsp;Tambah Alamat Baru</button>
            </div>
            <div className="col-sm-4 pr-0">
              <div className="lightweight-text" style={{
                display: "inline-block",
                width: "25%",
                textAlign: "right",
                paddingRight: "10px",
              }}>Urutkan</div>
              <div style={{
                display: "inline-block",
                width: "75%",
              }}>
                <Select
                  data={sortingList}
                  value={sortingId}
                  changeEvent={() => { }}
                />
              </div>
            </div>
            <div className="col-sm-4">
              <div className="input-group">
                <input style={{ borderRadius: "4px 0px 0px 4px" }} type="text" className="form-control" placeholder="Cari di Alamat Pengiriman" />
                <span className="input-group-btn">
                  <button className="btn btn-input-group-search" type="button"><i style={{ display: "inline-block" }} className="mdi mdi-magnify" /></button>
                </span>
              </div>
            </div>
          </div>
          <div className="row">
            <div className="col-sm-12">
              {this.createComponentData(data)}
            </div>
          </div>
        </div>
        {showPopup && (
          <ModalPopup
            width={550}
            title={isAdding ? "Tambah Alamat Pengiriman" : "Ubah Alamat Pengiriman"}
            hideModal={this.endShowingPopup}
            saveHandler={this.saveHandler}>
            <div style={{ padding: "18px 28px" }}>
              <div className="row mb-xs">
                <div className="col-sm-12">
                  <InputText
                    label="Label Alamat"
                    changeEvent={val => this.changeValueHandler("alias", val)}
                    value={form.alias}
                    errorText={errorMsg.alias}
                    disabled={false}
                  />
                </div>
              </div>
              <div className="row mb-xs">
                <div className="col-sm-12">
                  <InputText
                    label="Nama Penerima"
                    changeEvent={val => this.changeValueHandler("receiver_name", val)}
                    value={form.receiver_name}
                    errorText={errorMsg.receiver_name}
                  />
                </div>
              </div>
              <div className="row mb-xs">
                <div className="col-sm-12">
                  <TextArea
                    label="Alamat"
                    changeEvent={val => this.changeValueHandler("address", val)}
                    value={form.address}
                    errorText={errorMsg.address}
                  />
                </div>
              </div>
              <div className="row mb-xs">
                <div className="col-sm-12">
                  <InputText
                    label="No Telepon Penerima"
                    changeEvent={val => this.changeValueHandler("phone", val)}
                    value={form.phone}
                    errorText={errorMsg.phone}
                  />
                </div>
              </div>
              <div className="row mb-xs">
                <div className="col-sm-12">
                  <Select2
                    label="Provinsi"
                    url="/api/v2/provinces"
                    placeholder="- Pilih Provinsi -"
                    value={form.provinceId}
                    changeEvent={this.changeIdProvinceHandler}
                    errorText={errorMsg.provinceId}
                  />
                </div>
              </div>
              <div className="row mb-xs">
                <div className="col-sm-12">
                  <Select2
                    label="Kab./Kota"
                    url="/api/v2/cities"
                    additionalParam={{ idProvince: form.provinceId }}
                    placeholder="- Pilih Kab./Kota -"
                    value={form.cityId}
                    changeEvent={this.changeIdCityHandler}
                    errorText={errorMsg.cityId}
                  />
                </div>
              </div>
              <div className="row mb-xs">
                <div className="col-sm-12">
                  <Select2
                    label="Kecamatan"
                    url="/api/sub_districts"
                    additionalParam={{ idCity: form.cityId }}
                    placeholder="- Pilih Kecamatan -"
                    value={form.sub_district}
                    changeEvent={this.changeSubDistrictHandler}
                    errorText={errorMsg.sub_district}
                  />
                </div>
              </div>
              <div className="row mb-xs">
                <div className="col-sm-12">
                  <Select2
                    label="Kelurahan"
                    url="/api/urbans"
                    additionalParam={{ idCity: form.cityId, subDistrict: form.sub_district }}
                    placeholder="- Pilih Kelurahan -"
                    value={form.urban}
                    changeEvent={this.changeUrbanHandler}
                    errorText={errorMsg.urban}
                  />
                </div>
              </div>
              <div className="row mb-md">
                <div className="col-sm-12">
                  <InputText
                    label="Kode Pos"
                    changeEvent={() => { }}
                    value={form.postcode}
                    disabled
                  />
                </div>
              </div>
            </div>
          </ModalPopup>
        )}
        {showConfirmationPopup && (
          <ModalPopup
            width={500}
            confirmText="Jadikan Alamat Utama"
            title="Jadikan Alamat Utama"
            hideModal={this.endShowingConfirmationPopup}
            saveHandler={this.applyDefaultAddress}
            disableStaticHeight
          >
            <div className="text-center lightweight-text" style={{ padding: "25px 30px" }}>
              {`Apakah Anda yakin ingin menjadikan "${form.alias}" sebagai alamat utama? `}
              <br />
              Anda hanya dapat memilih satu alamat utama.
            </div>
          </ModalPopup>
        )}
        {showConfirmationDeletePopup && (
          <ModalPopup
            width={500}
            confirmText="Hapus Alamat"
            title="Hapus Alamat"
            hideModal={this.endShowingConfirmationDeletePopup}
            saveHandler={this.applyRemoveAddress}
            disableStaticHeight
          >
            <div className="text-center lightweight-text" style={{ padding: "25px 30px" }}>
              {`Apakah Anda yakin untuk menghapus "${form.alias}" ? `}
              <br />
              Anda tidak dapat mengembalikan alamat yang sudah dihapus.
          </div>
          </ModalPopup>
        )}
      </div>
    );
  }
}

export default CoreHoC(HoC(Address));
