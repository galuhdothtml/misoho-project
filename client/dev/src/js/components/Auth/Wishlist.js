/* eslint prop-types: 0 */
import React from "react";
import HoC from "./AuthHoC";
import { getWishlists } from "../../data";
import ProductList from "../ProductList";

class Wishlist extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      products: [],
    };
  }

  componentWillMount = () => {
    this.setupBreadcrumb();
    this.fetchData();
  }

  setupBreadcrumb = () => {
    const { addBreadcrumbData } = this.props;

    addBreadcrumbData([
      { title: "Home", link: "/" },
      { title: "Wishlist", isActive: true },
    ]);
  }

  fetchData = async () => {
    const newProducts = [];
    const res = await getWishlists({ page: 1 });

    if (res.status) {
      res.data.forEach((x) => {
        newProducts.push(x.Product);
      });
    }

    this.setState({ products: newProducts });
  }

  render() {
    const { products } = this.state;

    return (
      <div className="profile-wrapper">
        <h4 className="header-title">Wishlist</h4>
        <hr className="divider" />
        <div className="row mb-md">
          <div className="col-sm-4">
            <div className="input-group">
              <input style={{ borderRadius: "4px 0px 0px 4px" }} type="text" className="form-control" placeholder="Cari di Wishlist" />
              <span className="input-group-btn">
                <button className="btn btn-input-group-search" type="button"><i style={{ display: "inline-block" }} className="mdi mdi-magnify" /></button>
              </span>
            </div>
          </div>
          <div className="col-sm-4 pl-0">
            <button type="button" className="btn btn-default grey">Ubah Wishlist</button>
          </div>
        </div>
        <div className="row">
          <div className="col-sm-12">
            <ProductList data={products} modifierLi="product-list__li--nth-child-4" modifierProductItem="product-item--small" />
          </div>
        </div>
      </div>
    );
  }
}

export default HoC(Wishlist);
