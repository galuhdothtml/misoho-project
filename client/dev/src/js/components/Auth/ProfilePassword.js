/* eslint prop-types: 0 */
import React from "react";
import update from "immutability-helper";
import HoC from "./AuthHoC";
import InputText from "../form/InputText";

const errorContent = (type, val) => {
  let retval = null;

  if (type === "old_password") {
    if (String(val).trim().length === 0) {
      retval = "* Password lama wajib diisi";
    }
  } else if (type === "new_password") {
    if (String(val).trim().length === 0) {
      retval = "* Password baru wajib diisi";
    }
  } else if (type === "confirm_new_password") {
    if (String(val).trim().length === 0) {
      retval = "* Konfirmasi password baru wajib diisi";
    }
  }

  return retval;
};
class Profile extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      form: {
        old_password: "",
        new_password: "",
        confirm_new_password: "",
      },
      errorMsg: {
        old_password: "",
        new_password: "",
        confirm_new_password: "",
      },
      isValidated: false,
    };
  }

  componentWillMount = () => {
    this.setupBreadcrumb();
  }

  setupBreadcrumb = () => {
    const { addBreadcrumbData } = this.props;

    addBreadcrumbData([
      { title: "Home", link: "/" },
      { title: "Profil", link: "/my-profile" },
      { title: "Ubah Password", isActive: true },
    ]);
  }

  bulkCreateErrorMessage = () => {
    const { form, errorMsg } = this.state;
    const keys = Object.keys(errorMsg);
    let newErrorMsg = errorMsg;
    let isError = false;

    keys.forEach((x) => {
      let error = "";

      const ERROR_TEXT = errorContent(x, form[x]);

      if (ERROR_TEXT) {
        error = ERROR_TEXT;
        isError = true;
      }

      newErrorMsg = update(newErrorMsg, {
        [x]: { $set: error },
      });
    });

    return { msg: newErrorMsg, isError };
  }

  createErrorMessage = (type, val) => {
    const { errorMsg, isValidated } = this.state;
    let error = "";

    if (!isValidated) {
      return errorMsg;
    }

    const ERROR_TEXT = errorContent(type, val);

    if (ERROR_TEXT) {
      error = ERROR_TEXT;
    }

    const newValue = update(errorMsg, {
      [type]: { $set: error },
    });

    return newValue;
  }

  changeValueHandler = (type, val) => {
    const { form } = this.state;
    const newData = update(form, {
      [type]: { $set: val },
    });

    const errorMsg = this.createErrorMessage(type, val);
    this.setState({ form: newData, errorMsg });
  }

  saveHandler = () => {
    const { form } = this.state;
    const error = this.bulkCreateErrorMessage();
    const isValid = !error.isError;
    const payload = form;

    this.setState({ isValidated: true, errorMsg: error.msg });

    if (!isValid) {
      return;
    }

    console.log('DEBUG-SAVE: ', payload);
  }

  navToProfile = () => {
    // eslint-disable-next-line no-restricted-globals
    location.href = "/my-profile";
  }

  render() {
    const {
      form, errorMsg,
    } = this.state;

    return (
      <div>
        <h4 className="header-title">Profil</h4>
        <div className="row mb-md">
          <div className="col-sm-8">
            <InputText
              label="Password Lama"
              changeEvent={val => this.changeValueHandler("old_password", val)}
              value={String(form.old_password)}
              errorText={errorMsg.old_password}
            />
          </div>
        </div>
        <div className="row mb-md">
          <div className="col-sm-8">
            <InputText
              label="Password Baru"
              changeEvent={val => this.changeValueHandler("new_password", val)}
              value={String(form.new_password)}
              errorText={errorMsg.new_password}
            />
          </div>
        </div>
        <div className="row mb-md">
          <div className="col-sm-8">
            <InputText
              label="Konfirmasi Password Baru"
              changeEvent={val => this.changeValueHandler("confirm_new_password", val)}
              value={String(form.confirm_new_password)}
              errorText={errorMsg.confirm_new_password}
            />
          </div>
        </div>
        <div className="row mb-md">
          <div className="col-sm-2 pr-0">
            <button type="button" className="btn btn-primary btn-block" onClick={this.navToProfile}>Kembali</button>
          </div>
          <div className="col-sm-3">
            <button type="button" className="btn btn-primary btn-block" onClick={this.saveHandler}>Ubah Password</button>
          </div>
        </div>
      </div>
    );
  }
}

export default HoC(Profile);
