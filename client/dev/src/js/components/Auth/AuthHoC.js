import React from "react";
import {
  Link,
} from "react-router-dom";
import { connect } from "react-redux";
import Breadcrumb from "../Breadcrumb";
import DefaultPhoto from "../../images/avatar.png";
import { fetchMyAccount } from "../../actions/apiActions";

const HoC = (HocComponent) => {
  class AuthWrap extends React.Component {
    constructor(props) {
      super(props);

      this.state = {
        myAccountData: {},
        menuData: [
          {
            id: "1",
            name: "Profil",
            link: "/my-profile",
          },
          {
            id: "2",
            name: "Pesanan",
            link: "/my-orders",
          },
          {
            id: "3",
            name: "Wishlist",
            link: "/my-wishlists",
          },
          {
            id: "4",
            name: "Voucher",
            link: "/my-vouchers",
          },
          // {
          //   id: "5",
          //   name: "Ulasan",
          //   link: "/my-reviews",
          // },
          {
            id: "6",
            name: "Alamat Pengiriman",
            link: "/my-addresses",
          },
          {
            id: "7",
            name: "Keluar",
            link: "/auth/logout",
          },
        ],
        breadcrumbData: [],
      };
    }

    componentDidMount = () => {
      const { myAccount } = this.props;
      const { isFetching } = myAccount;

      if (!isFetching) {
        const { data } = myAccount;
        this.setState({ myAccountData: data });
      }

      window.scrollTo(0, 0);
    }

    componentWillReceiveProps = (nextProps) => {
      const { myAccount } = this.props;

      if (myAccount !== nextProps.myAccount) {
        const { isFetching } = nextProps.myAccount;

        if (!isFetching) {
          const { data } = nextProps.myAccount;
          this.setState({ myAccountData: data });
        }
      }
    }

    createMenuData = (data) => {
      const { myAccountData } = this.state;

      let newImg = DefaultPhoto;

      if (myAccountData && myAccountData.profile_photo) {
        newImg = myAccountData.profile_photo;
      }

      const retval = [
        <li key="account-spoiler-key11232">
          <div className="account-spoiler">
            <div>
              <div className="left">
                <img src={newImg} style={{ width: "100%" }} />
              </div>
              <div className="right">
                {myAccountData.name}
                <div className="small-text">
                  Member sejak Mei 2019
                </div>
              </div>
            </div>
            <div className="row saldo-wrapper hidden">
              <div className="col-sm-6">
                <i className="icon-coin-wallet" /><span style={{ borderBottom: "1px dashed #333" }} className="lightweight-text">Miso Coins</span>
              </div>
              <div className="col-sm-6 text-right">
                <span  style={{ color: "#bd1e6a" }}>250</span>
              </div>
            </div>
          </div>
        </li>,
      ];

      data.forEach((x) => {
        // eslint-disable-next-line no-restricted-globals
        let splitResults = String(location.pathname).split("/");
        splitResults = splitResults.filter(sr => (String(sr).length > 0));
        let isActive = false;

        if (splitResults.length > 0) {
          isActive = (`/${splitResults[0]}` === String(x.link));
        }

        retval.push(
          <li key={x.id} className={isActive ? "active" : ""}><Link to={x.link}>{x.name}</Link></li>,
        );
      });

      return (<ul className="auth-user-menu">{retval}</ul>);
    }

    addBreadcrumbData = (val) => {
      this.setState({ breadcrumbData: val });
    }

    render() {
      const { doFetchingMyAccount } = this.props;
      const { menuData, breadcrumbData, myAccountData } = this.state;

      return (
        <div className="container profile-container">
          <div className="mt-xs hidden">
            {(breadcrumbData.length > 0) && (
              <Breadcrumb data={breadcrumbData} />
            )}
          </div>
          <div className="row mt-sm">
            <div className="col-sm-3">{this.createMenuData(menuData)}</div>
            <div className="col-sm-9"><HocComponent {...this.props} fetchMyAccount={doFetchingMyAccount} addBreadcrumbData={this.addBreadcrumbData} myAccountData={myAccountData} /></div>
          </div>
        </div>
      );
    }
  }

  const mapStateToProps = state => ({
    myAccount: state.apiRedux.myAccount,
  });

  const mapDispatchToProps = dispatch => ({
    doFetchingMyAccount: () => dispatch(fetchMyAccount()),
  });

  return connect(mapStateToProps, mapDispatchToProps)(AuthWrap);
};

export default HoC;
