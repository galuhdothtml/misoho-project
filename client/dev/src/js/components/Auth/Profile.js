/* eslint prop-types: 0 */
import React from "react";
import update from "immutability-helper";
import HoC from "./AuthHoC";
import * as Util from "../../utils";
import InputCheckbox from "../form/InputCheckbox";
import ImageUpload from "../form/ImageUpload";
import { uploadProfilePhoto } from "../../data";

const errorContent = (type, val) => {
  let retval = null;

  if (type === "name") {
    if (String(val).trim().length === 0) {
      retval = "* Nama wajib diisi";
    }
  } else if (type === "email") {
    if (String(val).trim().length === 0) {
      retval = "* Email wajib diisi";
    } else if (!Util.isEmailValid(val)) {
      retval = "* Format email masih belum benar";
    }
  }

  return retval;
};
class Profile extends React.Component {
  constructor(props) {
    super(props);

    const { myAccountData } = this.props;

    this.state = {
      form: {
        name: "",
        email: "",
        day: "",
        month: "",
        year: "",
        phone: "",
        gender: "L",
        is_newsletter: false,
      },
      errorMsg: {
        name: "",
        email: "",
      },
      fileImage: myAccountData.profile_photo,
      isValidated: false,
    };
  }

  componentWillMount = () => {
    this.fetchMyAccount();
    this.setupBreadcrumb();
  }

  componentWillReceiveProps = (nextProps) => {
    const { myAccountData } = this.props;

    if (myAccountData !== nextProps.myAccountData) {
      this.setState({ fileImage: nextProps.myAccountData.profile_photo });
    }
  }

  fetchMyAccount = () => {
    const { fetchMyAccount: fetchMyAccountProp } = this.props;

    fetchMyAccountProp();
  }

  setupBreadcrumb = () => {
    const { addBreadcrumbData } = this.props;

    addBreadcrumbData([
      { title: "Home", link: "/" },
      { title: "Profil", isActive: true },
    ]);
  }

  bulkCreateErrorMessage = () => {
    const { form, errorMsg } = this.state;
    const keys = Object.keys(errorMsg);
    let newErrorMsg = errorMsg;
    let isError = false;

    keys.forEach((x) => {
      let error = "";

      const ERROR_TEXT = errorContent(x, form[x]);

      if (ERROR_TEXT) {
        error = ERROR_TEXT;
        isError = true;
      }

      newErrorMsg = update(newErrorMsg, {
        [x]: { $set: error },
      });
    });

    return { msg: newErrorMsg, isError };
  }

  createErrorMessage = (type, val) => {
    const { errorMsg, isValidated } = this.state;
    let error = "";

    if (!isValidated) {
      return errorMsg;
    }

    const ERROR_TEXT = errorContent(type, val);

    if (ERROR_TEXT) {
      error = ERROR_TEXT;
    }

    const newValue = update(errorMsg, {
      [type]: { $set: error },
    });

    return newValue;
  }

  changeValueHandler = (type, val) => {
    const { form } = this.state;
    const newData = update(form, {
      [type]: { $set: val },
    });

    const errorMsg = this.createErrorMessage(type, val);
    this.setState({ form: newData, errorMsg });
  }

  changeImageHandler = (val) => {
    this.setState({ fileImage: val }, () => {
      this.doUploadingPhoto();
    });
  }

  dataURLtoFile = (dataurl, filename) => {
    const arr = dataurl.split(",");
    const mime = arr[0].match(/:(.*?);/)[1];
    const bstr = atob(arr[1]);
    let n = bstr.length;
    const u8arr = new Uint8Array(n);
    while (n > -1) {
      u8arr[n] = bstr.charCodeAt(n);
      n -= 1;
    }
    return new File([u8arr], filename, { type: mime });
  }

  doUploadingPhoto = async () => {
    const { fileImage } = this.state;
    let file = null;

    if (String(fileImage).trim().length > 0) {
      file = this.dataURLtoFile(fileImage, "image.png");
    }

    const res = await uploadProfilePhoto({}, [file]);

    if (res.status) {
      this.fetchMyAccount();
    }
  }

  saveHandler = () => {
    const { form } = this.state;
    const error = this.bulkCreateErrorMessage();
    const isValid = !error.isError;
    const payload = form;

    this.setState({ isValidated: true, errorMsg: error.msg });

    if (!isValid) {
      return;
    }

    console.log("DEBUG-SAVE: ", payload);
  }

  navToPassword = () => {
    // eslint-disable-next-line no-restricted-globals
    location.href = "/my-profile/edit-password";
  }

  render() {
    const {
      form, errorMsg, fileImage,
    } = this.state;
    const { myAccountData } = this.props;

    return (
      <div className="profile-wrapper">
        <h4 className="header-title">Profil</h4>
        <hr className="divider" />
        <div className="row mb-md">
          <div className="col-sm-7">
            <div className="row mb-lg">
              <div className="col-sm-6">
                <label>Nama Lengkap</label>
                <div className="content-text">{myAccountData.name}<a className="link-action" href="#">Ubah</a></div>
              </div>
              <div className="col-sm-6">
                <div
                  style={{
                    position: "relative",
                    top: "7px",
                  }}
                >
                  <button
                    type="button"
                    className="btn btn-default grey btn-block"
                    onClick={() => {}}><i className="fa fa-key" />&nbsp;&nbsp;Ubah Kata Sandi</button>
                </div>
              </div>
            </div>
            <div className="row mb-lg">
              <div className="col-sm-6">
                <label>Email</label>
                <div className="content-text">{myAccountData.email}<a className="link-action" href="#">Ubah</a></div>
              </div>
              <div className="col-sm-6">
                <label>Nomor Handphone</label>
                <div className="content-text">0852-3616-9724<a className="link-action" href="#">Ubah</a></div>
              </div>
            </div>
            <div className="mb-lg">
              <InputCheckbox label="Aktifkan newsletter untuk info terbaru &amp; promo spesial" value={form.is_newsletter} changeEvent={val => this.changeValueHandler("is_newsletter", val)} />
            </div>
          </div>
          <div className="col-sm-5">
            <ImageUpload value={fileImage} changeEvent={this.changeImageHandler} />
          </div>
        </div>
      </div>
    );
  }
}

export default HoC(Profile);
