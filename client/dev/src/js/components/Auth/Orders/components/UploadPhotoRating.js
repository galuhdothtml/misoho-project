/* eslint prop-types: 0 */
import React from "react";
import PropTypes from "prop-types";

const createId = () => (Math.random().toString(36).substring(2, 15) + Math.random().toString(36).substring(2, 15));
export default class UploadPhotoRating extends React.Component {
  static propTypes = {
    value: PropTypes.array,
    changeEvent: PropTypes.func,
  }

  static defaultProps = {
    value: [],
    changeEvent: () => { },
  }

  chooseFile = () => {
    this.fileUpload.click();
  }

  changeFileHandler = () => {
    const { value, changeEvent } = this.props;
    const { files } = this.fileUpload;
    let newData = [];

    for (let i = 0; i < files.length; i += 1) {
      newData.push(Object.assign(files[i], { id: createId(), preview: URL.createObjectURL(files[i]) }));
    }
    newData = newData.filter(x => (!value.find(v => (String(x.name) === String(v.name)))));
    changeEvent([...value, ...newData]);
  }

  removeImage = (id) => {
    const { value, changeEvent } = this.props;
    const newData = value.filter(x => (String(x.id) !== String(id)));

    changeEvent(newData);
  };

  thumbsComponent = () => {
    const { value } = this.props;

    if (value.length > 0) {
      const newValue = value.map(file => (
        <div key={file.id} className="thumb-img-wrapper">
          <div className="thumb-img-inner">
            <img
              src={file.preview}
              className="thumb-img"
            />
            <button type="button" className="thumb-img-remove hidden" onClick={() => this.removeImage(file.id)}><i className="fa fa-close i-default" /></button>
          </div>
        </div>
      ));

      newValue.push(
        <div key="add-rating-photo-key" className="thumb-img-wrapper button" onClick={this.chooseFile}>
          <div className="thumb-img-inner" style={{
            overflow: "unset",
            width: "100%",
          }}>
            <div style={{
              position: "absolute",
              width: "100%",
              textAlign: "center",
              top: "11px",
            }}>
              <div><i className="mdi mdi-plus" /></div>
              <div className="lightweight-text" style={{ fontSize: "12px" }}>Tambah Foto</div>
            </div>
          </div>
        </div>,
      );

      return newValue;
    }

    return (
      <div className="upload-rating-photo-button" onClick={this.chooseFile}>
        <div><i className="mdi mdi-camera i-default" /></div>
        <div className="lightweight-text">Tambah Foto</div>
      </div>
    );
  }

  render() {
    return (
      <div>
        <input ref={(c) => { this.fileUpload = c; }} onChange={this.changeFileHandler} className="hidden" type="file" accept="image/*" multiple />
        {this.thumbsComponent()}
      </div>
    );
  }
}
