/* eslint prop-types: 0 */
import React, { Fragment } from "react";
import moment from "moment";
import update from "immutability-helper";
import { API_BASE_URL } from "../../../constants";
import HoC from "../AuthHoC";
import ModalPopup from "../../form/ModalPopup";
import TextArea from "../../form/TextArea";
import { getMyOrders, createReview, updateReview } from "../../../data";
import Rating from "../../Rating";
import * as Util from "../../../utils";
import UploadPhotoRating from "./components/UploadPhotoRating";

const createId = () => (Math.random().toString(36).substring(2, 15) + Math.random().toString(36).substring(2, 15));
const getOrderStateName = (val) => {
  let retval = "";

  if (String(val) === "1") {
    retval = "Menunggu Pembayaran";
  } else if (String(val) === "2") {
    retval = "Terbayar";
  } else if (String(val) === "3") {
    retval = "Sedang Diproses";
  } else if (String(val) === "4") {
    retval = "Sudah Dikirim";
  } else if (String(val) === "5") {
    retval = "Sudah Diterima";
  }

  return retval;
};
class Orders extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      data: [],
      ratingData: [],
      showRatingPopup: false,
    };
  }

  componentWillMount = () => {
    this.fetchData();
    this.setupBreadcrumb();
  }

  dataURLtoFile = (id, dataurl, filename) => {
    const arr = dataurl.split(",");
    const mime = arr[0].match(/:(.*?);/)[1];
    const bstr = atob(arr[1]);
    let n = bstr.length;
    const u8arr = new Uint8Array(n);
    while (n > -1) {
      u8arr[n] = bstr.charCodeAt(n);
      n -= 1;
    }

    return new File([u8arr], filename, { type: mime });
  }

  showRatingPopupButton = (orderData) => {
    const idOrder = orderData.id;
    let reviewData = [];
    const isReviewed = orderData.ProductRatings.length > 0;
    let buttonText = "Beri Penilaian";

    if (isReviewed) {
      buttonText = "Lihat Penilaian";
      reviewData = orderData.OrderDetails.map((od) => {
        const productImg = `${API_BASE_URL}/uploads/${od.Product.primary_img}`;
        const foundRating = orderData.ProductRatings.find(r => (String(r.id_product) === String(od.id_product)));
        let idRating = "";
        let newRating = 0;
        let newNote = "";
        let newPhoto = [];

        if (foundRating) {
          idRating = foundRating.id;
          newRating = parseInt(foundRating.rating, 10);
          newNote = foundRating.description;
          foundRating.images.forEach((img) => {
            newPhoto.push(this.dataURLtoFile(img.id, img.base64Data, img.filename));
          });

          newPhoto = newPhoto.map(img => (Object.assign(img, { id: createId(), preview: URL.createObjectURL(img) })));
        }

        return {
          id: od.id_product,
          id_review: idRating,
          id_order: idOrder,
          name: od.product_name,
          img: productImg,
          rating: newRating,
          photo: newPhoto,
          note: newNote,
          type: "update",
        };
      });
    } else {
      reviewData = orderData.OrderDetails.map((od) => {
        const productImg = `${API_BASE_URL}/uploads/${od.Product.primary_img}`;

        return {
          id: od.id_product,
          id_order: idOrder,
          name: od.product_name,
          img: productImg,
          rating: 0,
          photo: [],
          note: "",
          type: "create",
        };
      });
    }

    return (
      <button
        className="btn btn-default btn-sm btn-block" onClick={() => this.startShowingRatingPopup(reviewData)}><i className="mdi mdi-star-circle i-default" />&nbsp;&nbsp;{buttonText}</button>
    );
  }

  startShowingRatingPopup = (data) => {
    this.setState({ showRatingPopup: true, ratingData: data });
  }

  endShowingRatingPopup = () => {
    this.setState({ showRatingPopup: false });
  }

  sendRatingHandler = async () => {
    const { ratingData } = this.state;

    const promises = ratingData.map(async (review) => {
      const payload = {
        id_product: review.id,
        id_order: review.id_order,
        rating: review.rating,
        description: review.note,
      };

      let resData = null;

      if (review.type === "update") {
        Object.assign(payload, { id: review.id_review });
        resData = await updateReview(payload, review.photo);
      } else {
        resData = await createReview(payload, review.photo);
      }

      return resData;
    });

    await Promise.all(promises);

    this.fetchData();
    this.setState({ showRatingPopup: false });
  }

  setupBreadcrumb = () => {
    const { addBreadcrumbData } = this.props;

    addBreadcrumbData([
      { title: "Home", link: "/" },
      { title: "Pesanan", isActive: true },
    ]);
  }

  fetchData = async () => {
    const res = await getMyOrders();

    if (res.status) {
      const newData = res.data.map(x => (Object.assign({}, x, { is_show_content: true })));
      this.setState({ data: newData });
    }
  }

  navToOrderDetail = (id) => {
    // eslint-disable-next-line no-restricted-globals
    location.href = `/my-orders/detail/${id}`;
  }

  toggleContent = (id) => {
    const { data } = this.state;
    const index = data.findIndex(x => (String(x.id) === String(id)));
    const newValue = !(data[index].is_show_content);
    const newData = update(data, {
      [index]: {
        is_show_content: { $set: newValue },
      },
    });

    this.setState({ data: newData });
  }

  ratingDataChangeHandler = (id, val, type) => {
    const { ratingData } = this.state;
    const index = ratingData.findIndex(x => (String(x.id) === String(id)));
    const newData = update(ratingData, {
      [index]: {
        [type]: { $set: val },
      },
    });
    this.setState({ ratingData: newData });
  }

  createComponentModalRating = () => {
    const { ratingData } = this.state;
    const retval = [];

    if (ratingData.length > 0) {
      ratingData.forEach((x, i) => {
        retval.push(
          <div className="rating-row-wrapper" key={x.id} style={(i === (ratingData.length - 1)) ? { marginBottom: "0px", borderBottom: "0px" } : {}}>
            <div className="rating-header">
              <div className="left">
                <img src={x.img} />
              </div>
              <div className="right lightweight-text">
                {x.name}
              </div>
            </div>
            <div style={{ padding: "10px 20px" }}>
              <div className="text-center" style={{ marginBottom: "10px" }}>
                <Rating value={x.rating} changeEvent={val => this.ratingDataChangeHandler(x.id, val, "rating")} />
              </div>
              <div>
                <UploadPhotoRating value={x.photo} changeEvent={val => this.ratingDataChangeHandler(x.id, val, "photo")} />
              </div>
              <div>
                <TextArea
                  changeEvent={val => this.ratingDataChangeHandler(x.id, val, "note")}
                  value={x.note}
                  placeholder="Beritahu pengguna lain mengapa Anda sangat menyukai produk ini"
                  rows="3"
                />
              </div>
            </div>
          </div>,
        );
      });
    }

    return retval;
  }

  createTableBody = (data) => {
    if (data.length > 0) {
      const retval = [];

      data.forEach((x) => {
        const products = [];
        x.OrderDetails.forEach((od) => {
          const productImg = `${API_BASE_URL}/uploads/${od.Product.primary_img}`;
          const productWeight = parseInt(od.Product.weight, 10);

          products.push(
            <div className="row order-product-item" key={od.id_product}>
              <div className="col-sm-2">
                <img src={productImg} />
              </div>
              <div className="col-sm-5 pl-0">
                <div className="product-name">{`x${parseInt(od.qty, 10)} - ${od.product_name}`}</div>
                <div className="lightweight-text">{productWeight} gr</div>
                <div className="lightweight-text">{Util.currency(od.product_price)}</div>
                <div className="mt-xs">
                  <button className="btn btn-default btn-sm">Beli Lagi</button>
                </div>
              </div>
            </div>,
          );
        });

        retval.push(
          <div className="order-list-item" key={x.id}>
            <div className="summary">
              <div className="row">
                <div className="col-xs-3">
                  <div>Pesanan <span className="special">#{x.reference}</span></div>
                  <strong>{moment(x.createdAt).format("DD MMM YYYY HH:mm:ss")}</strong>
                </div>
                <div className="col-xs-2">
                  <div>Total</div>
                  <strong>{Util.currency(x.grand_total)}</strong>
                </div>
                <div className="col-xs-3">
                  <div>Status Order</div>
                  <strong>{getOrderStateName(x.order_state)}</strong>
                </div>
                <div className="col-xs-3">
                  <div>Pembayaran</div>
                  <strong>Bank Transfer</strong>
                </div>
                <div className="col-xs-1 text-right">
                  <div className="collapse-button" onClick={() => this.toggleContent(x.id)}><i className={`fa ${x.is_show_content ? "fa-angle-down" : "fa-angle-right"}`} /></div>
                </div>
              </div>
            </div>
            <div className={`content-body ${x.is_show_content ? "" : "hidden"}`}>
              {products}
              <div style={{
                position: "absolute", right: "28px", top: "20px", width: "150px",
              }}>
                { (String(x.order_state) === "4") && (
                  <div className="mb-xs">
                    {this.showRatingPopupButton(x)}
                  </div>
                )}
                <div>
                  <button className="btn btn-primary btn-sm btn-block" onClick={() => this.navToOrderDetail(x.id)}>Detail Pesanan</button>
                </div>
              </div>
            </div>
          </div>,
        );
      });

      return retval;
    }

    return (
      <div>
        <span>Data belum ada</span>
      </div>
    );
  }

  render() {
    const { data, showRatingPopup } = this.state;

    return (
      <Fragment>
        <div className="profile-wrapper">
          <h4 className="header-title">Pesanan</h4>
          <hr className="divider" />
          {this.createTableBody(data)}
        </div>
        { showRatingPopup && (
          <ModalPopup
            width={500}
            title="Nilai Produk"
            hideModal={this.endShowingRatingPopup}
            saveHandler={this.sendRatingHandler}>
            <div style={{ background: "rgb(227, 227, 227)", height: "100%" }}>
              {this.createComponentModalRating()}
            </div>
          </ModalPopup>
        )}
      </Fragment>
    );
  }
}

export default HoC(Orders);
