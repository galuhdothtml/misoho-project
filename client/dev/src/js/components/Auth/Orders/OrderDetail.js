/* eslint prop-types: 0 */
import React from "react";
import HoC from "../AuthHoC";
import { API_BASE_URL } from "../../../constants";
import { getMyOrderDetail } from "../../../data";
import * as Util from "../../../utils";

class OrderDetail extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      id: "",
      reference: "",
      receiver_name: "",
      address_delivery: "",
      postcode: "",
      order_state: "",
      orderStateText: "",
      orderHistoryData: [],
      orderDetailData: [],
      shipping: {
        courier: "",
        service: "",
        shipment_number: "",
      },
      subtotal: 0,
      totalShipping: 0,
      grandTotal: 0,
      isModalShowing: false,
      formShipment: {
        shipmentNumber: "",
      },
      errorShipmentMsg: {
        shipmentNumber: "",
      },
      isValidated: false,
    };
  }

  componentWillMount = () => {
    const {
      match: {
        params,
      },
    } = this.props;
    this.setupData(params.id);
    this.setupBreadcrumb();
  }

  setupBreadcrumb = () => {
    const { addBreadcrumbData } = this.props;

    addBreadcrumbData([
      { title: "Home", link: "/" },
      { title: "Pesanan", link: "/my-orders" },
      { title: "Detail Pesanan", isActive: true },
    ]);
  }

  setupData = async (id) => {
    const res = await getMyOrderDetail(id);

    if (res.status) {
      const { data } = res;

      // eslint-disable-next-line no-undef
      const newOrderHistory = data.order_history.map(x => Object.assign({}, x, { createdAt: moment(x.createdAt).format("DD MMMM YYYY HH:mm:ss") }));

      this.setState({
        id: data.id,
        reference: data.reference,
        receiver_name: data.receiver_name,
        address_delivery: data.address_delivery_text,
        postcode: data.postcode ? data.postcode : "-",
        order_state: data.order_state,
        orderStateText: data.order_state_text,
        orderHistoryData: newOrderHistory,
        orderDetailData: data.orderDetailData,
        shipping: JSON.parse(data.shipping),
        subtotal: data.subtotal,
        totalShipping: data.total_shipping,
        grandTotal: data.grand_total,
        shipmentNumber: "",
      });
    }
  }

  createProductComponent = (data) => {
    if (data.length > 0) {
      const retval = [];

      data.forEach((od, i) => {
        const productImg = `${API_BASE_URL}/uploads/${od.Product.primary_img}`;
        const productWeight = parseInt(od.Product.weight, 10);

        retval.push(
          <div className={`row order-product-item ${i === (data.length - 1) ? "mb-0" : ""}`} key={od.id_product}>
            <div className="col-sm-4">
              <img src={productImg} />
            </div>
            <div className="col-sm-8">
              <div className="product-name">{od.product_name}</div>
              <div className="lightweight-text">{productWeight} gr</div>
              <div className="lightweight-text">{Util.currency(od.product_price)}</div>
              <div><span className="lightweight-text">Jumlah:</span> {parseInt(od.qty, 10)} item</div>
            </div>
          </div>,
        );
      });

      return retval;
    }

    return (
      <div>
        <span>Data belum ada</span>
      </div>
    );
  }

  render() {
    const {
      reference,
      receiver_name: receiverName,
      address_delivery: addressDelivery,
      postcode, orderHistoryData, orderDetailData,
      shipping, orderStateText,
      subtotal, totalShipping, grandTotal, order_state: orderState,
    } = this.state;

    return (
      <div className="profile-wrapper order-detail-wrapper">
        <h4 className="header-title">Pesanan #{reference}</h4>
        <hr className="divider" />
        <div className="row">
          <div className="col-sm-6">
            <div className="row">
              <div className="col-sm-6">
                <div>
                  <div className="lightweight-text">NO. PESANAN</div>
                  <div>{reference}</div>
                </div>
              </div>
              <div className="col-sm-6">
                <div>
                  <div className="lightweight-text">TOTAL PEMBAYARAN</div>
                  <div>{Util.currency(grandTotal)}</div>
                </div>
              </div>
            </div>
            <div className="row mt-sm">
              <div className="col-sm-8">
                <div>
                  <div className="lightweight-text">ALAMAT PENGIRIMAN</div>
                  <div>{receiverName}</div>
                  <div className="lightweight-text">{addressDelivery}, {postcode}</div>
                </div>
              </div>
            </div>
            <div className="row mt-sm">
              <div className="col-sm-6">
                <div>
                  <div className="lightweight-text">JASA PENGIRIMAN</div>
                  <div><span className="uppercase-text">{shipping.courier}</span> - {shipping.service}</div>
                </div>
              </div>
              {(String(orderState) === "4") && (
                  <div className="col-sm-6">
                    <div>
                      <div className="lightweight-text">NOMOR RESI PENGIRIMAN</div>
                      <div>{shipping.shipment_number}</div>
                    </div>
                  </div>
              )}
            </div>
            <div className="mt-sm">
                <div className="lightweight-text" style={{ marginBottom: "5px" }}>HISTORI PESANAN</div>
                <div>
                  <table className="table table-bordered">
                    <thead>
                      <tr>
                        <th>Waktu</th>
                        <th>Status Pesanan</th>
                      </tr>
                    </thead>
                    <tbody className="lightweight-text">
                      {orderHistoryData.map(x => (
                        <tr key={x.id}>
                          <td>{x.createdAt}</td>
                          <td>{x.order_state_text}</td>
                        </tr>
                      ))}
                    </tbody>
                  </table>
                </div>
              </div>
          </div>
          <div className="col-sm-6">
            <div className="row">
              <div className="col-sm-6">
                <div>
                  <div className="lightweight-text">TANGGAL TRANSAKSI</div>
                  <div>08 Oct 2019</div>
                </div>
              </div>
              <div className="col-sm-6 text-right">
                <div>
                  <div className="lightweight-text">STATUS PESANAN</div>
                  <div>{orderStateText}</div>
                </div>
              </div>
            </div>
            <div className="mt-sm">
              <div className="lightweight-text">RINGKASAN PEMBAYARAN</div>
              <div className="row">
                <div className="col-sm-12">
                  <div className="row">
                    <div className="col-sm-6 lightweight-text">Total Belanja :</div>
                    <div className="col-sm-6 text-right">{Util.currency(subtotal)}</div>
                  </div>
                  <div className="row">
                    <div className="col-sm-6 lightweight-text">Biaya Pengiriman :</div>
                    <div className="col-sm-6 text-right">{Util.currency(totalShipping)}</div>
                  </div>
                  <div className="row">
                    <div className="col-sm-6 lightweight-text">Total Terbayar :</div>
                    <div className="col-sm-6 text-right">{Util.currency(grandTotal)}</div>
                  </div>
                </div>
              </div>
            </div>
            <div className="mt-sm">
              <div className="lightweight-text" style={{ marginBottom: "5px" }}>PRODUK</div>
              <div className="order-product-item-container">
                {this.createProductComponent(orderDetailData)}
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default HoC(OrderDetail);
