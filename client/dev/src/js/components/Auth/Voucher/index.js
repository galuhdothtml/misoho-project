/* eslint prop-types: 0 */
import React from "react";
import HoC from "../AuthHoC";
import VoucherItem from "./VoucherItem";

const fakeVoucherData = [
  {
    id: "1",
    voucherCode: "HJAS87654",
    expDate: "2020-06-10",
    img: "voucher-1.jpg",
  },
  {
    id: "2",
    voucherCode: "BNVF98744",
    expDate: "2020-06-12",
    img: "voucher-2.jpg",
  },
  {
    id: "3",
    voucherCode: "JGFF32254",
    expDate: "2020-05-28",
    img: "voucher-3.jpg",
  },
  {
    id: "4",
    voucherCode: "JKNB87A15G",
    expDate: "2020-05-28",
    img: "voucher-4.jpg",
  },
];
class Voucher extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      title: "Halaman Voucher",
      voucherData: fakeVoucherData,
    };
  }

  componentWillMount = () => {
    this.setupBreadcrumb();
  }

  setupBreadcrumb = () => {
    const { addBreadcrumbData } = this.props;

    addBreadcrumbData([
      { title: "Home", link: "/" },
      { title: "Voucher", isActive: true },
    ]);
  }

  render() {
    const { title, voucherData } = this.state;

    return (
      <div className="profile-wrapper">
        <h4 className="header-title">Voucher</h4>
        <hr className="divider" />
        <div>
          {voucherData.map(x => (<VoucherItem value={x} />))}
        </div>
      </div>
    );
  }
}

export default HoC(Voucher);
