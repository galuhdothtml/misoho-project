/* eslint prop-types: 0 */
import React from "react";
import moment from "moment";
import { API_BASE_URL } from "../../../constants";

const style = {
  hr: {
    marginTop: "10px",
    marginBottom: "10px",
    borderTop: "1px dashed #eee",
  },
};
class VoucherItem extends React.Component {
  render() {
    const { value } = this.props;

    return (
      <div className="voucher-item">
        <div>
          <img className="voucher-item__img" src={`${API_BASE_URL}/uploads/${value.img}`} />
        </div>
        <div style={{ padding: "10px", borderRadius: "5px", boxShadow: "0 2px 3px 0 rgba(0, 0, 0, 0.14)" }}>
          <div className="row">
            <div className="col-sm-7">
              <div className="voucher-item__exp">
                <div className="lightweight-text" style={{ fontSize: "12px" }}>Kode Voucher</div>
                <div>{value.voucherCode}</div>
              </div>
            </div>
            <div className="col-sm-5 text-right">
              <button style={{
                position: "relative",
                top: "3px",
              }} type="button" className="btn btn-transparent btn-sm">SALIN</button>
            </div>
          </div>
          <hr style={style.hr} />
          <div className="row">
            <div className="col-sm-7">
              <i className="icon-planner" />
              <div className="voucher-item__exp">
                <div className="lightweight-text" style={{ fontSize: "12px" }}>Berlaku Hingga</div>
                <div>{moment(value.expDate).format("DD MMM YYYY")}</div>
              </div>
            </div>
            <div className="col-sm-5 text-right">
              <button style={{
                position: "relative",
                top: "3px",
              }} type="button" className="btn btn-primary btn-sm">Gunakan</button>
            </div>
          </div>
        </div>
        <div className="voucher_item__circle-left" />
        <div className="voucher_item__circle-right" />
      </div>
    );
  }
}

export default VoucherItem;
