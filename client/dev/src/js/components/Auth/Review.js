/* eslint prop-types: 0 */
import React from "react";
import HoC from "./AuthHoC";

class Review extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      title: "Halaman Review",
    };
  }

  componentWillMount = () => {
    this.setupBreadcrumb();
  }

  setupBreadcrumb = () => {
    const { addBreadcrumbData } = this.props;

    addBreadcrumbData([
      { title: "Home", link: "/" },
      { title: "Ulasan", isActive: true },
    ]);
  }

  render() {
    const { title } = this.state;

    return (
      <div>
        <h5>{title}</h5>
      </div>
    );
  }
}

export default HoC(Review);
