/* eslint prop-types: 0 */
import React from "react";
import { Link } from "react-router-dom";

class Breadcrumb extends React.Component {
  render() {
    const { data } = this.props;

    return (
            <nav>
                <ol className="breadcrumb-ol breadcrumb">
                    {data.map((x, i) => {
                      if (x.isActive) {
                        return (<li key={i} className="breadcrumb-item active lightweight-text">{x.title}</li>);
                      }

                      return (<li key={i} className="breadcrumb-item lightweight-text"><Link to={x.link}>{x.title}</Link></li>);
                    })}
                </ol>
            </nav>
    );
  }
}

export default Breadcrumb;
