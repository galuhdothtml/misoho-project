/* eslint prop-types: 0 */
import React from "react"; // eslint-disable-line no-unused-vars
import PropTypes from "prop-types";
import Slider from "react-slick";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
import VoucherItem from "../Auth/Voucher/VoucherItem";

const fakeVoucherData = [
  {
    id: "1",
    voucherCode: "HJAS87654",
    expDate: "2020-06-10",
    img: "voucher-1.jpg",
  },
  {
    id: "2",
    voucherCode: "BNVF98744",
    expDate: "2020-06-12",
    img: "voucher-2.jpg",
  },
  {
    id: "3",
    voucherCode: "JGFF32254",
    expDate: "2020-05-28",
    img: "voucher-3.jpg",
  },
  {
    id: "4",
    voucherCode: "JKNB87A15G",
    expDate: "2020-05-28",
    img: "voucher-4.jpg",
  },
];
class VoucherSlide extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      voucherData: fakeVoucherData,
    };
  }

  render() {
    const { voucherData } = this.state;
    const settings = {
      dots: false,
      infinite: true,
      speed: 300,
      slidesToShow: 1,
      centerMode: true,
      variableWidth: true,
    };

    return (
      <div className="voucher-slide">
        <Slider {...settings}>
          {voucherData.map(x => (<VoucherItem value={x} />))}
        </Slider>
      </div>
    );
  }
}

export default VoucherSlide;
