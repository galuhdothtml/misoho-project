/* eslint prop-types: 0 */
import React from "react";
import {
  Link,
} from "react-router-dom";
import Logo from "../../images/logo-dark.png";
import {
  getCarts, getMyAddresses, checkoutOrder,
} from "../../data";
import { currency } from "../../utils";
import { API_BASE_URL } from "../../constants";
import AddressForm from "./AddressForm";
import ShippingForm from "./ShippingForm";
import PaymentForm from "./PaymentForm";
import VoucherSlide from "./VoucherSlide";
import InputText from "../form/InputText";
import LoadingModal from "../LoadingModal";
import ModalPopup from "../form/ModalPopup";

class Checkout extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      totalBelanja: 0,
      carts: [],
      voucherCode: "",
      totalWeight: 0,
      destination: "", // id_city
      /**
       * 0: Informasi Pembeli
       * 1: Metode Pengiriman
       * 2: Metode Pembayaran
       */
      checkoutPosition: "0",
      isLoading: true,
      addressList: [],
      currentCourier: null,
      showPopup: false,
    };
  }

  componentWillMount = () => {
    this.fetchCarts();
    this.fetchMyAddresses();
  }

  changeVoucherCodeHandler = (val) => {
    this.setState({ voucherCode: val });
  }

  fetchMyAddresses = async () => {
    this.setLoader(true);
    const res = await getMyAddresses();
    this.setLoader(false);

    let addressList = [];

    if (res.status) {
      addressList = res.data;
    }

    this.setState({ addressList });
  }

  fetchCarts = async () => {
    const res = await getCarts();

    if (res.status) {
      let totalBelanja = 0;
      let totalWeight = 0;
      const carts = res.data.carts.map(x => ({
        id: x.id,
        name: x.product_name,
        price: parseFloat(x.product_price, 10),
        qty: x.qty,
        img: x.primary_img,
        weight: x.product_weight,
      }));
      carts.forEach((x) => {
        totalWeight += (parseFloat(x.weight, 10) * parseFloat(x.qty, 10));
        totalBelanja += (parseFloat(x.price, 10) * parseFloat(x.qty, 10));
      });

      this.setState({ carts, totalBelanja, totalWeight });
    }
  }

  createComponentProductList = (carts) => {
    const retval = [];

    if (carts.length > 0) {
      carts.forEach((x) => {
        const newImg = `${API_BASE_URL}/uploads/${x.img}`;
        retval.push(
          <div className="cart-item-checkout" key={x.id}>
            <div className="left"><img src={newImg} /><div className="qty-display">{x.qty}</div></div>
            <div className="middle">
              <div className="lightweight-text">{x.name}</div>
            </div>
            <div className="right">
              <div>{currency(x.price * x.qty)}</div>
            </div>
          </div>,
        );
      });
    }

    return retval;
  }

  navToAddressForm = (e) => {
    if (e) {
      e.preventDefault();
    }

    this.setState({ checkoutPosition: "0", currentCourier: null });
  }

  navToShippingForm = (e) => {
    if (e) {
      e.preventDefault();
    }

    this.setState({ checkoutPosition: "1" });
  }

  saveAddressHandler = () => {
    this.setState({ checkoutPosition: "1" });
  }

  changeShippingHandler = (val) => {
    this.setState({ currentCourier: val });
  }

  saveShippingHandler = (val) => {
    this.setState({ checkoutPosition: "2", currentCourier: val });
  }

  processCheckoutOrder = async () => {
    const { destination, currentCourier } = this.state;

    if (destination && currentCourier) {
      const payload = {
        id_address: destination.id,
        shipping_courier: currentCourier.code,
        shipping_service: currentCourier.service,
      };

      this.setLoader(true);

      const res = await checkoutOrder(payload);

      if (res.status) {
        // eslint-disable-next-line no-restricted-globals
        location.href = "/";
      } else {
        this.setLoader(false);
      }
    }
  }

  createBreadcrumb = (checkoutPosition) => {
    const retval = [];
    let key = 0;

    key += 1;
    retval.push(<li key={key}><a href="#">Keranjang</a></li>);

    if (checkoutPosition === "0") {
      key += 1;
      retval.push(<li key={key}>Alamat Pengiriman</li>);
    } else {
      key += 1;
      retval.push(<li key={key}><a href="#" onClick={this.navToAddressForm}>Alamat Pengiriman</a></li>);
    }

    if (checkoutPosition === "1") {
      key += 1;
      retval.push(<li key={key}>Metode Pengiriman</li>);
    } else {
      key += 1;
      retval.push(<li key={key}><a href="#" onClick={this.navToShippingForm}>Metode Pengiriman</a></li>);
    }

    if (checkoutPosition === "2") {
      key += 1;
      retval.push(<li key={key}>Metode Pembayaran</li>);
    } else {
      key += 1;
      retval.push(<li key={key} className="active">Metode Pembayaran</li>);
    }

    return (<ol className="breadcrumb breadcrumb-style">{retval}</ol>);
  }

  changeDestinationHandler = (val) => {
    this.setState({ destination: val });
  }

  createPositionComponent = (checkoutPosition) => {
    const { destination, addressList } = this.state;
    let retval = (<div />);

    if (checkoutPosition === "0") {
      retval = (<AddressForm addressList={addressList} fetchData={this.fetchMyAddresses} setLoader={this.setLoader} currentDestination={destination} onSave={this.saveAddressHandler} changeEvent={this.changeDestinationHandler} />);
    } else if (checkoutPosition === "1") {
      const { totalWeight } = this.state;
      retval = (<ShippingForm changeEvent={this.changeShippingHandler} setLoader={this.setLoader} onSave={this.saveShippingHandler} destination={destination} weight={totalWeight} />);
    } else if (checkoutPosition === "2") {
      retval = (<PaymentForm onSave={this.processCheckoutOrder} />);
    }

    return retval;
  }

  setLoader = (val) => {
    this.setState({ isLoading: val });
  }

  startShowingPopup = () => {
    this.setState({ showPopup: true });
  }

  endShowingPopup = () => {
    this.setState({ showPopup: false });
  }

  redeemVoucherHandler = () => {

  }

  render() {
    const {
      carts, voucherCode, checkoutPosition, isLoading, totalBelanja, currentCourier,
      showPopup,
    } = this.state;

    let grandTotal = totalBelanja;

    if (currentCourier) {
      grandTotal = parseFloat(totalBelanja, 10) + parseFloat(currentCourier.price, 10);
    }

    return (
      <div className="checkout-page-wrapper">
        <div className="left">
          <div style={{ paddingBottom: "20px" }}>
            <Link to="/"><img style={{ width: "200px" }} className="logo" src={Logo} /></Link>
          </div>
          <div>
            {this.createBreadcrumb(checkoutPosition)}
          </div>
          <div>
            {this.createPositionComponent(checkoutPosition)}
          </div>
        </div>
        <div className="right">
          <div className="cart-list">
            {this.createComponentProductList(carts)}
          </div>
          <hr className="divider" />
          <div className="row text-right hidden">
            <div className="col-sm-8 pr-0">
              <InputText
                changeEvent={this.changeVoucherCodeHandler}
                value={voucherCode}
                placeholder="Kode Voucher"
              />
            </div>
            <div className="col-sm-4">
              <button type="button" className="btn btn-primary btn-block">Gunakan</button>
            </div>
          </div>
          <div>
            <div className="choose-voucher-button" onClick={this.startShowingPopup}>
              <div>
                <div style={{
                  float: "left",
                  position: "relative",
                  top: "10px",
                }}>
                  <i className="icon-discount" />
                </div>
                <div style={{
                  display: "table-cell",
                  height: "40px",
                  verticalAlign: "middle",
                }}>
                  <div>Voucher 50,000</div>
                  <div className="lightweight-text">1 voucher dipakai</div>
                  {/* <div>Makin hemat pakai voucher</div> */}
                </div>
              </div>
              <div style={{
                position: "absolute",
                right: "19px",
                top: "19px",
                fontSize: "22px",
              }}>
                <i className="mdi mdi-chevron-right" />
              </div>
            </div>
          </div>
          <hr className="divider" />
          <div className="row">
            <div className="col-sm-6">
              <h5 className="lightweight-text">Subtotal</h5>
            </div>
            <div className="col-sm-6 text-right">
              <h5>{currency(totalBelanja)}</h5>
            </div>
          </div>
          <div className="row">
            <div className="col-sm-6">
              <h5 className="lightweight-text">Pengiriman</h5>
            </div>
            <div className="col-sm-6 text-right">
              <h5>{currentCourier ? currency(currentCourier.price) : "Calculated at next step"}</h5>
            </div>
          </div>
          <hr className="divider" />
          <div className="row">
            <div className="col-sm-6">
              <h5 className="lightweight-text">Total</h5>
            </div>
            <div className="col-sm-6 text-right">
              <h4>{currency(grandTotal)}</h4>
            </div>
          </div>
        </div>
        {isLoading && <LoadingModal customClasses="full-page-loading" />}
        {showPopup && (
          <ModalPopup
            width={550}
            title="Gunakan Voucher"
            hideModal={this.endShowingPopup}
            removeFooter>
            <div style={{ padding: "20px" }}>
              <div className="voucher-code-input mb-lg">
                <div className="row">
                  <div className="col-sm-9">
                    <InputText
                      label="Kode Voucher"
                      changeEvent={this.changeVoucherCodeHandler}
                      value={voucherCode}
                      disabled={false}
                      placeholder="Ketik kode voucher"
                    />
                  </div>
                  <div className="col-sm-3 pl-0">
                    <button style={{
                      position: "relative",
                      top: "25px",
                    }} type="button" className="btn btn-primary btn-block">Gunakan</button>
                  </div>
                </div>
              </div>
              <div>
                <div className="lightweight-text mb-xs">
                  Voucher Milik Saya
                </div>
                <VoucherSlide />
              </div>
            </div>
          </ModalPopup>
        )}
      </div>
    );
  }
}

export default Checkout;
