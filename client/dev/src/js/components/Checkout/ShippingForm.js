/* eslint prop-types: 0 */
import React from "react";
import {
  getCouriers,
} from "../../data";
import { capitalizeFirstLetter, currency } from "../../utils";

class ShippingForm extends React.Component {
  constructor(props) {
    super(props);

    const { destination: found } = props;

    this.state = {
      courierList: [],
      formReadOnly: {
        id: found.id,
        alias: found.alias,
        receiver_name: found.receiver_name,
        address: found.address,
        phone: found.phone,
        provinceId: String(found.id_province),
        provinceName: found.province_name,
        cityId: String(found.id_city),
        cityName: found.city_name,
        sub_district: found.sub_district,
        urban: String(found.PostalCode.id),
        postcode: found.postcode,
        urbanName: found.PostalCode.urban,
        defaultAddress: found.is_default,
      },
      currentCourier: null,
    };
  }

  componentWillMount = () => {
    this.setupCouriers();
  }

  setupCouriers = async () => {
    const { weight, setLoader, changeEvent } = this.props;
    const { currentCourier, formReadOnly } = this.state;
    const payload = {
      destination: formReadOnly.cityId,
      weight,
    };
    const fetchJne = this.setupJneCouriers(payload);
    const fetchPos = this.setupPosCouriers(payload);
    const fetchTiki = this.setupTikiCouriers(payload);

    setLoader(true);

    const getJne = await fetchJne;
    const getPos = await fetchPos;
    const getTiki = await fetchTiki;

    setLoader(false);

    const retval = [...getJne, ...getPos, ...getTiki];

    if (!currentCourier) {
      const newData = {
        code: retval[0].code,
        service: retval[0].service,
        price: retval[0].price,
      };
      this.setState({ courierList: retval, currentCourier: newData, shippingCost: newData.price }, () => {
        changeEvent(newData);
      });
    } else {
      this.setState({ courierList: retval });
    }
  }

  setupJneCouriers = async ({ destination, weight }) => {
    const newData = [];
    const payload = {
      destination,
      weight,
      courier: "jne",
    };
    const res = await getCouriers(payload);
    if (res.status && res.data.length > 0) {
      res.data[0].costs.forEach((x) => {
        const isValid = x.cost.length > 0;

        if (isValid) {
          newData.push({
            code: "jne",
            service: x.service,
            price: x.cost[0].value,
            etd: String(x.cost[0].etd) === "1-1" ? "1 Hari" : `${x.cost[0].etd} Hari`,
          });
        }
      });
    }

    return newData;
  }

  setupPosCouriers = async ({ destination, weight }) => {
    const newData = [];
    const code = "pos";
    const payload = {
      destination,
      weight,
      courier: code,
    };
    const res = await getCouriers(payload);
    if (res.status && res.data.length > 0) {
      res.data[0].costs.forEach((x) => {
        const isValid = x.cost.length > 0;

        if (isValid) {
          newData.push({
            code,
            service: x.service,
            price: x.cost[0].value,
            etd: capitalizeFirstLetter(String(`${x.cost[0].etd}`).toLowerCase()),
          });
        }
      });
    }

    return newData;
  }

  setupTikiCouriers = async ({ destination, weight }) => {
    const newData = [];
    const code = "tiki";
    const payload = {
      destination,
      weight,
      courier: code,
    };
    const res = await getCouriers(payload);
    if (res.status && res.data.length > 0) {
      res.data[0].costs.forEach((x) => {
        const isValid = x.cost.length > 0;

        if (isValid) {
          newData.push({
            code,
            service: x.service,
            price: x.cost[0].value,
            etd: `${x.cost[0].etd} Hari`,
          });
        }
      });
    }

    return newData;
  }

  saveHandler = () => {
    const { currentCourier } = this.state;
    const { onSave } = this.props;

    onSave(currentCourier);
  }

  chooseCourierHandler = (val) => {
    const { changeEvent } = this.props;
    this.setState({ currentCourier: val }, () => {
      const { currentCourier } = this.state;
      changeEvent(currentCourier);
    });
  }

  createCourierList = (data) => {
    const { currentCourier } = this.state;
    const retval = [];

    data.forEach((x) => {
      const isChecked = (String(`${x.code}-${x.service}`) === String(`${currentCourier.code}-${currentCourier.service}`));
      retval.push(
        <li key={`${x.code}-${x.service}`} className={isChecked ? "choosen" : ""} onClick={() => this.chooseCourierHandler({ code: x.code, service: x.service, price: x.price })}>
          <div className="radio-wrapper">
            <div className="radio-container" onClick={() => { }}>
              <input
                type="radio"
                name="courier-choice"
                checked={isChecked}
                onChange={() => this.chooseCourierHandler({ code: x.code, service: x.service, price: x.price })}
                value={`${x.code}-${x.service}`}
              />
              <span className="checkmark"></span>
            </div>
          </div>
          <div></div>
          <div className="mcl-left">
            <div>{`${String(x.code).toUpperCase()} - ${x.service}`}</div>
            <div className="lightweight-text">{x.etd}</div>
          </div>
          <div className="mcl-right">
            <div>{currency(x.price)}</div>
          </div>
        </li>,
      );
    });

    return (<ul className="my-courier-list">{retval}</ul>);
  }

  render() {
    const {
      courierList, formReadOnly,
    } = this.state;

    if (courierList.length > 0) {
      return (
        <div>
          <h5>Alamat Pengiriman</h5>
          <div style={{
            borderTop: "1px solid rgb(219, 222, 226)",
            borderBottom: "1px solid rgb(219, 222, 226)",
            padding: "12px 0px 2px 0px",
            marginBottom: "22px",
          }}>
            <div className="row">
              <div className="col-sm-7">
                <div className="mb-md">
                  <label>Nama Penerima</label>
                  <div className="lightweight-text">{formReadOnly.receiver_name}</div>
                </div>
                <div className="mb-md">
                  <label>Alamat Penerima</label>
                  <div className="lightweight-text">
                    {formReadOnly.address}
                    <br />
                    {formReadOnly.urbanName}, {formReadOnly.sub_district}
                    <br />
                    {formReadOnly.cityName}, {formReadOnly.provinceName}, {formReadOnly.postcode}
                  </div>
                </div>
              </div>
              <div className="col-sm-5">
                <div className="mb-md">
                  <label>No Telepon Penerima</label>
                  <div className="lightweight-text">{formReadOnly.phone}</div>
                </div>
              </div>
            </div>
          </div>
          <div className="row mb-xs">
            <div className="col-sm-12">
              <h5>Pilih Kurir Pengiriman</h5>
              {this.createCourierList(courierList)}
            </div>
          </div>
          <div className="row mb-md">
            <div className="col-sm-6"></div>
            <div className="col-sm-6 text-right">
              <button type="button" className="btn btn-primary" onClick={this.saveHandler}>Lanjut ke pembayaran</button>
            </div>
          </div>
        </div>
      );
    }

    return (
      <div />
    );
  }
}

export default ShippingForm;
