/* eslint prop-types: 0 */
import React from "react";
import update from "immutability-helper";
import InputText from "../form/InputText";
import TextArea from "../form/TextArea";
import Select2 from "../form/Select2";
import Select from "../form/Select";
import ModalPopup from "../form/ModalPopup";
import {
  getPostalCode, updateAddress,
  createAddress,
} from "../../data";
import CoreHoC from "../CoreHoC";

const errorContent = (type, val) => {
  let retval = null;

  if (type === "alias") {
    if (String(val).trim().length === 0) {
      retval = "* Nama alamat wajib diisi";
    }
  } else if (type === "receiver_name") {
    if (String(val).trim().length === 0) {
      retval = "* Nama penerima wajib diisi";
    }
  } else if (type === "address") {
    if (String(val).trim().length === 0) {
      retval = "* Alamat wajib diisi";
    }
  } else if (type === "phone") {
    if (String(val).trim().length === 0) {
      retval = "* Telepon wajib diisi";
    }
  } else if (type === "cityId") {
    if (String(val).trim().length === 0) {
      retval = "* Kota/Kabupaten wajib diisi";
    }
  } else if (type === "provinceId") {
    if (String(val).trim().length === 0) {
      retval = "* Provinsi wajib diisi";
    }
  } else if (type === "sub_district") {
    if (String(val).trim().length === 0) {
      retval = "* Kecamatan wajib diisi";
    }
  } else if (type === "urban") {
    if (String(val).trim().length === 0) {
      retval = "* Kelurahan wajib diisi";
    }
  }

  return retval;
};
class AddressForm extends React.Component {
  constructor(props) {
    super(props);
    const { currentDestination, addressList } = props;
    let latestSaved = null;

    if (currentDestination) {
      latestSaved = currentDestination.id;
    }

    this.state = {
      isValidated: false,
      addressSelectId: { id: "" },
      addressSelectData: [],
      showPopup: false,
      isAdding: false,
      latestSaved,
      addressList,
      form: {
        id: "",
        alias: "",
        receiver_name: "",
        address: "",
        phone: "",
        provinceId: "",
        provinceName: "",
        cityId: "",
        cityName: "",
        sub_district: "",
        urban: "",
        urbanName: "",
        postcode: "",
        defaultAddress: false,
      },
      formReadOnly: {
        id: "",
        alias: "",
        receiver_name: "",
        address: "",
        phone: "",
        provinceId: "",
        provinceName: "",
        cityId: "",
        cityName: "",
        sub_district: "",
        urban: "",
        urbanName: "",
        postcode: "",
        defaultAddress: false,
      },
      errorMsg: {
        alias: "",
        receiver_name: "",
        address: "",
        phone: "",
        provinceId: "",
        cityId: "",
        sub_district: "",
        urban: "",
      },
    };
  }

  componentWillMount = () => {
    this.fetchMyAddresses();
  }

  componentWillReceiveProps = (nextProps) => {
    const { addressList } = this.props;

    if (addressList !== nextProps.addressList) {
      this.setState({ addressList: nextProps.addressList }, () => {
        this.fetchMyAddresses();
      });
    }
  }

  fetchMyAddresses = async () => {
    const { latestSaved, addressList } = this.state;

    let addressSelectData = [];
    let addressSelectId = { id: "" };
    let newForm = {
      id: "",
      alias: "",
      receiver_name: "",
      address: "",
      phone: "",
      provinceId: "",
      provinceName: "",
      cityId: "",
      cityName: "",
      sub_district: "",
      urban: "",
      postcode: "",
      urbanName: "",
      defaultAddress: false,
    };

    let found = addressList.find(x => (x.is_default));

    if (latestSaved) {
      found = addressList.find(x => (String(x.id) === String(latestSaved)));
    }

    if (found) {
      addressSelectId = found;
      newForm = {
        id: found.id,
        alias: found.alias,
        receiver_name: found.receiver_name,
        address: found.address,
        phone: found.phone,
        provinceId: String(found.id_province),
        provinceName: found.province_name,
        cityId: String(found.id_city),
        cityName: found.city_name,
        sub_district: found.sub_district,
        urban: String(found.PostalCode.id),
        postcode: found.postcode,
        urbanName: found.PostalCode.urban,
        defaultAddress: found.is_default,
      };
    }

    addressSelectData = addressList.map(x => ({ id: x.id, name: `${x.alias} - ${x.city_name} (${x.receiver_name})` }));
    addressSelectData = [...addressSelectData, { id: "add-new-address", name: "+ Tambah Alamat Baru" }];

    this.setState({
      addressSelectData, addressSelectId, form: newForm, formReadOnly: newForm,
    }, () => {
      this.changeDestination(addressSelectId);
    });
  }

  changeDestination = (val) => {
    const { changeEvent } = this.props;

    changeEvent(val);
  }

  saveHandler = async (callback) => {
    const { showNotification } = this.props;
    const { form, isAdding } = this.state;
    const error = this.bulkCreateErrorMessage();
    const isValid = !error.isError;

    this.setState({ isValidated: true, errorMsg: error.msg });

    if (isValid) {
      let res = null;
      const payload = Object.assign({}, form, { id_postal_code: form.urban });

      if (isAdding) {
        res = await createAddress(payload);
      } else {
        res = await updateAddress(payload);
      }

      if (res.status) {
        this.setState({ latestSaved: res.data.id }, () => {
          this.refreshDataThenHidePopup();
          if (isAdding) {
            showNotification("Alamat pengiriman berhasil ditambahkan!");
          } else {
            showNotification("Berhasil mengubah alamat pengiriman!");
          }
        });
      }
    } else {
      callback();
    }
  }

  refreshDataThenHidePopup = async () => {
    const { fetchData } = this.props;

    await fetchData();
    this.fetchMyAddresses();
    this.endShowingPopup();
  }

  changeValueHandler = (type, val) => {
    const { form } = this.state;
    const newValue = update(form, {
      [type]: { $set: val },
    });

    const errorMsg = this.createErrorMessage(type, val);
    this.setState({ form: newValue, errorMsg });
  }

  changeIdProvinceHandler = (val) => {
    const { form } = this.state;
    const newValue = update(form, {
      provinceId: { $set: val },
      cityId: { $set: "" },
      sub_district: { $set: "" },
      urban: { $set: "" },
      postcode: { $set: "" },
    });

    const errorMsg = this.createErrorMessage("provinceId", val);
    this.setState({ form: newValue, errorMsg });
  }

  changeIdCityHandler = (val) => {
    const { form } = this.state;
    const newValue = update(form, {
      cityId: { $set: val },
      sub_district: { $set: "" },
      urban: { $set: "" },
      postcode: { $set: "" },
    });

    const errorMsg = this.createErrorMessage("cityId", val);
    this.setState({ form: newValue, errorMsg });
  }

  changeSubDistrictHandler = (val) => {
    const { form } = this.state;
    const newValue = update(form, {
      sub_district: { $set: val },
      urban: { $set: "" },
      postcode: { $set: "" },
    });

    const errorMsg = this.createErrorMessage("sub_district", val);
    this.setState({ form: newValue, errorMsg });
  }

  changeUrbanHandler = (val) => {
    const { form } = this.state;
    const newValue = update(form, {
      urban: { $set: val },
      postcode: { $set: "" },
    });

    const errorMsg = this.createErrorMessage("urban", val);
    this.setState({ form: newValue, errorMsg }, () => {
      this.setPostalCode();
    });
  }

  changeAddressSelectId = (val) => {
    const { errorMsg: errorMsgState, addressList } = this.state;

    if (String(val) !== "add-new-address") {
      const found = addressList.find(x => (String(x.id) === String(val)));
      let newState = {
        id: "",
        alias: "",
        receiver_name: "",
        address: "",
        phone: "",
        provinceId: "",
        provinceName: "",
        cityId: "",
        cityName: "",
        sub_district: "",
        urban: "",
        postcode: "",
        urbanName: "",
        defaultAddress: false,
      };
      let errorMsg = errorMsgState;

      if (found) {
        newState = {
          id: found.id,
          alias: found.alias,
          receiver_name: found.receiver_name,
          address: found.address,
          phone: found.phone,
          provinceId: String(found.id_province),
          provinceName: found.province_name,
          cityId: String(found.id_city),
          cityName: found.city_name,
          sub_district: found.sub_district,
          urban: String(found.PostalCode.id),
          postcode: found.postcode,
          urbanName: found.PostalCode.urban,
          defaultAddress: found.is_default,
        };
        errorMsg = {
          alias: "",
          receiver_name: "",
          address: "",
          phone: "",
          provinceId: "",
          cityId: "",
          sub_district: "",
          urban: "",
        };
      }

      this.setState({
        isAdding: false, addressSelectId: found, form: newState, formReadOnly: newState, errorMsg,
      }, () => {
        this.changeDestination(found);
      });
    } else {
      const newState = {
        id: "",
        alias: "",
        receiver_name: "",
        address: "",
        phone: "",
        provinceId: "",
        provinceName: "",
        cityId: "",
        cityName: "",
        sub_district: "",
        urban: "",
        postcode: "",
        urbanName: "",
        defaultAddress: false,
      };
      const errorMsg = {
        alias: "",
        receiver_name: "",
        address: "",
        phone: "",
        provinceId: "",
        cityId: "",
        sub_district: "",
        urban: "",
      };

      this.setState({
        isAdding: true, form: newState, errorMsg,
      }, () => {
        this.startShowingPopup();
      });
    }
  }

  setPostalCode = async () => {
    const { form: { urban }, form } = this.state;
    const res = await getPostalCode(urban);

    if (res.status && res.data.length > 0) {
      const newValue = update(form, {
        postcode: { $set: res.data[0].postal_code },
      });
      this.setState({ form: newValue });
    }
  }

  createErrorMessage = (type, val) => {
    const { errorMsg, isValidated } = this.state;
    let error = "";

    if (!isValidated) {
      return errorMsg;
    }

    const ERROR_TEXT = errorContent(type, val);

    if (ERROR_TEXT) {
      error = ERROR_TEXT;
    }

    const newValue = update(errorMsg, {
      [type]: { $set: error },
    });

    return newValue;
  }

  bulkCreateErrorMessage = () => {
    const { errorMsg, form } = this.state;
    const keys = Object.keys(errorMsg);
    let newErrorMsg = errorMsg;
    let isError = false;

    keys.forEach((x) => {
      let error = "";
      const ERROR_TEXT = errorContent(x, form[x]);

      if (ERROR_TEXT) {
        error = ERROR_TEXT;
        isError = true;
      }

      newErrorMsg = update(newErrorMsg, {
        [x]: { $set: error },
      });
    });

    return { msg: newErrorMsg, isError };
  }

  prepareToEdit = () => {
    const { formReadOnly } = this.state;
    const errorMsg = {
      alias: "",
      receiver_name: "",
      address: "",
      phone: "",
      provinceId: "",
      cityId: "",
      sub_district: "",
      urban: "",
    };

    this.setState({
      isValidated: false, showPopup: true, isAdding: false, form: formReadOnly, errorMsg,
    });
  }

  startShowingPopup = () => {
    this.setState({ showPopup: true });
  }

  endShowingPopup = () => {
    this.setState({ showPopup: false });
  }

  nextStep = () => {
    const { onSave } = this.props;

    onSave();
  }

  render() {
    const {
      form, formReadOnly, errorMsg, addressSelectData, addressSelectId, showPopup,
      isAdding,
    } = this.state;

    return (
      <div>
        <h5>Pilih Alamat Pengiriman</h5>
        <div className="row mb-xs">
          <div className="col-sm-9">
            <Select
              data={addressSelectData}
              value={String(addressSelectId.id)}
              changeEvent={this.changeAddressSelectId}
            />
          </div>
          <div className="col-sm-3 pl-0">
            <div>
              <button type="button" className="btn btn-default grey btn-block" onClick={this.prepareToEdit}>Ubah Alamat</button>
            </div>
          </div>
        </div>
        <div style={{
          borderTop: "1px solid rgb(219, 222, 226)",
          borderBottom: "1px solid rgb(219, 222, 226)",
          padding: "12px 0px 2px 0px",
          marginBottom: "22px",
        }}>
          <div className="row">
            <div className="col-sm-7">
              <div className="mb-md">
                <label>Nama Penerima</label>
                <div className="lightweight-text">{formReadOnly.receiver_name}</div>
              </div>
              <div className="mb-md">
                <label>Alamat Penerima</label>
                <div className="lightweight-text">
                  {formReadOnly.address}
                  <br />
                  {formReadOnly.urbanName}, {formReadOnly.sub_district}
                  <br />
                  {formReadOnly.cityName}, {formReadOnly.provinceName}, {formReadOnly.postcode}
                </div>
              </div>
            </div>
            <div className="col-sm-5">
              <div className="mb-md">
                <label>No Telepon Penerima</label>
                <div className="lightweight-text">{formReadOnly.phone}</div>
              </div>
            </div>
          </div>
        </div>
        <div className="row mb-md">
          <div className="col-sm-6"></div>
          <div className="col-sm-6 text-right">
            <button type="button" className="btn btn-primary" onClick={this.nextStep}>Lanjut ke pengiriman</button>
          </div>
        </div>
        {showPopup && (
          <ModalPopup
            width={550}
            title={isAdding ? "Tambah Alamat Pengiriman" : "Ubah Alamat Pengiriman"}
            hideModal={this.endShowingPopup}
            saveHandler={this.saveHandler}>
            <div style={{ padding: "15px" }}>
              <div className="row mb-xs">
                <div className="col-sm-12">
                  <InputText
                    label="Label Alamat"
                    changeEvent={val => this.changeValueHandler("alias", val)}
                    value={form.alias}
                    errorText={errorMsg.alias}
                    disabled={false}
                  />
                </div>
              </div>
              <div className="row mb-xs">
                <div className="col-sm-12">
                  <InputText
                    label="Nama Penerima"
                    changeEvent={val => this.changeValueHandler("receiver_name", val)}
                    value={form.receiver_name}
                    errorText={errorMsg.receiver_name}
                  />
                </div>
              </div>
              <div className="row mb-xs">
                <div className="col-sm-12">
                  <TextArea
                    label="Alamat"
                    changeEvent={val => this.changeValueHandler("address", val)}
                    value={form.address}
                    errorText={errorMsg.address}
                  />
                </div>
              </div>
              <div className="row mb-xs">
                <div className="col-sm-12">
                  <InputText
                    label="No Telepon Penerima"
                    changeEvent={val => this.changeValueHandler("phone", val)}
                    value={form.phone}
                    errorText={errorMsg.phone}
                  />
                </div>
              </div>
              <div className="row mb-xs">
                <div className="col-sm-12">
                  <Select2
                    label="Provinsi"
                    url="/api/v2/provinces"
                    placeholder="- Pilih Provinsi -"
                    value={form.provinceId}
                    changeEvent={this.changeIdProvinceHandler}
                    errorText={errorMsg.provinceId}
                  />
                </div>
              </div>
              <div className="row mb-xs">
                <div className="col-sm-12">
                  <Select2
                    label="Kab./Kota"
                    url="/api/v2/cities"
                    additionalParam={{ idProvince: form.provinceId }}
                    placeholder="- Pilih Kab./Kota -"
                    value={form.cityId}
                    changeEvent={this.changeIdCityHandler}
                    errorText={errorMsg.cityId}
                  />
                </div>
              </div>
              <div className="row mb-xs">
                <div className="col-sm-12">
                  <Select2
                    label="Kecamatan"
                    url="/api/sub_districts"
                    additionalParam={{ idCity: form.cityId }}
                    placeholder="- Pilih Kecamatan -"
                    value={form.sub_district}
                    changeEvent={this.changeSubDistrictHandler}
                    errorText={errorMsg.sub_district}
                  />
                </div>
              </div>
              <div className="row mb-xs">
                <div className="col-sm-12">
                  <Select2
                    label="Kelurahan"
                    url="/api/urbans"
                    additionalParam={{ idCity: form.cityId, subDistrict: form.sub_district }}
                    placeholder="- Pilih Kelurahan -"
                    value={form.urban}
                    changeEvent={this.changeUrbanHandler}
                    errorText={errorMsg.urban}
                  />
                </div>
              </div>
              <div className="row mb-md">
                <div className="col-sm-12">
                  <InputText
                    label="Kode Pos"
                    changeEvent={() => { }}
                    value={form.postcode}
                    disabled
                  />
                </div>
              </div>
            </div>
          </ModalPopup>
        )}
      </div>
    );
  }
}

export default CoreHoC(AddressForm);
