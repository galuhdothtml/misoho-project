/* eslint prop-types: 0 */
import React from "react";
import Select from "../form/Select";

const PaymentMethodType = {
  BANK_TRANSFER: "BANK_TRANSFER",
  VIRTUAL_ACCOUNT: "VIRTUAL_ACCOUNT",
  CICILAN: "CICILAN",
  KLIKPAY: "KLIKPAY",
};
class PaymentForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      title: "Halaman Metode Pembayaran",
      currentPaymentMethod: PaymentMethodType.BANK_TRANSFER,
      virtualAccountId: "",
      virtualAccountList: [
        {
          id: "-",
          name: "- Pilih Bank Penyedia Virtual Account -",
        },
        {
          id: "1",
          name: "Virtual Account BCA",
        },
        {
          id: "2",
          name: "Virtual Account BNI",
        },
        {
          id: "3",
          name: "Virtual Account BRI",
        },
        {
          id: "4",
          name: "Virtual Account Mandiri",
        },
      ],
    };
  }

  choosePaymentHandler = (val) => {
    this.setState({ currentPaymentMethod: val });
  }

  changeVirtualAccountIdHandler = (val) => {
    this.setState({ virtualAccountId: val });
  }

  createPaymentMethodList = () => {
    const { currentPaymentMethod, virtualAccountList, virtualAccountId } = this.state;
    const retval = [];

    retval.push(
      <li key={PaymentMethodType.BANK_TRANSFER} className={(PaymentMethodType.BANK_TRANSFER === currentPaymentMethod) ? "choosen" : ""} onClick={() => this.choosePaymentHandler(PaymentMethodType.BANK_TRANSFER)}>
        <div className="header-content">
          <div className="radio-wrapper">
            <div className="radio-container" onClick={() => {}}>
              <input
                type="radio"
                name="courier-choice"
                checked={(PaymentMethodType.BANK_TRANSFER === currentPaymentMethod)}
                onChange={() => this.choosePaymentHandler(PaymentMethodType.BANK_TRANSFER)}
                value={PaymentMethodType.BANK_TRANSFER}
              />
              <span className="checkmark"></span>
            </div>
          </div>
          <div></div>
          <div className="mcl-left">
            <div>Bank Transfer</div>
          </div>
          <div className="mcl-right">
            <div><i className={`fa ${(PaymentMethodType.BANK_TRANSFER === currentPaymentMethod) ? "fa-angle-down" : "fa-angle-right"}`} /></div>
          </div>
        </div>
        <div className="list-content" style={{ display: (PaymentMethodType.BANK_TRANSFER === currentPaymentMethod) ? "block" : "none" }}>
          <div>Transfer ditujukan kepada PT Misoho Indonesia di salah satu bank berikut:</div>
          <div style={{ marginTop: "15px", marginBottom: "10px" }}>
            <ul className="bank-list-icon">
              <li><i className="icon-bca" /></li>
              <li><i className="icon-bni" /></li>
              <li><i className="icon-bri" /></li>
              <li><i className="icon-mandiri" /></li>
            </ul>
          </div>
          <div>
            <ul className="list-normal">
              <li>Transfer dapat dilakukan melalui ATM/Internet Banking/SMS Banking setelah order dipesan (setelah menekan tombol "Bayar Melalui Bank Transfer" di langkah ke 4 - Review)</li>
              <li>Instruksi lebih lanjut mengenai bagaimana cara transfer akan diberikan setelah order dipesan.</li>
              <li>Barang akan langsung dikirim setelah pembayaran diproses.</li>
            </ul>
          </div>
        </div>
      </li>,
    );

    retval.push(
      <li key={PaymentMethodType.VIRTUAL_ACCOUNT} className={PaymentMethodType.VIRTUAL_ACCOUNT === currentPaymentMethod ? "choosen" : ""} onClick={() => this.choosePaymentHandler(PaymentMethodType.VIRTUAL_ACCOUNT)}>
        <div className="header-content">
          <div className="radio-wrapper">
          <div className="radio-container" onClick={() => {}}>
              <input
                type="radio"
                name="courier-choice"
                checked={(PaymentMethodType.VIRTUAL_ACCOUNT === currentPaymentMethod)}
                onChange={() => this.choosePaymentHandler(PaymentMethodType.VIRTUAL_ACCOUNT)}
                value={PaymentMethodType.VIRTUAL_ACCOUNT}
              />
              <span className="checkmark"></span>
            </div>
          </div>
          <div></div>
          <div className="mcl-left">
            <div>Virtual Account</div>
          </div>
          <div className="mcl-right">
            <div><i className={`fa ${(PaymentMethodType.VIRTUAL_ACCOUNT === currentPaymentMethod) ? "fa-angle-down" : "fa-angle-right"}`} /></div>
          </div>
        </div>
        <div className="list-content" style={{ display: (PaymentMethodType.VIRTUAL_ACCOUNT === currentPaymentMethod) ? "block" : "none" }}>
          <div className="mb-md">
            <Select
                data={virtualAccountList}
                value={virtualAccountId}
                changeEvent={this.changeVirtualAccountIdHandler}
            />
          </div>
          <div>
            <ul className="list-normal">
              <li>Pembayaran akan terverifikasi secara otomatis.</li>
              <li>Nomor Virtual Account dan instruksi lebih lanjut mengenai bagaimana cara pembayaran akan diberikan setelah order diterima.</li>
              <li>Mohon selesaikan pembayaran dalam waktu 2 x 24 jam setelah order diterima.</li>
            </ul>
          </div>
        </div>
      </li>,
    );

    retval.push(
      <li key={PaymentMethodType.CICILAN} className={(PaymentMethodType.CICILAN === currentPaymentMethod) ? "choosen" : ""} onClick={() => this.choosePaymentHandler(PaymentMethodType.CICILAN)}>
        <div className="header-content">
          <div className="radio-wrapper">
            <div className="radio-container" onClick={() => {}}>
              <input
                type="radio"
                name="courier-choice"
                checked={(PaymentMethodType.CICILAN === currentPaymentMethod)}
                onChange={() => this.choosePaymentHandler(PaymentMethodType.CICILAN)}
                value={PaymentMethodType.CICILAN}
              />
              <span className="checkmark"></span>
            </div>
          </div>
          <div></div>
          <div className="mcl-left">
            <div>Kartu Kredit / Debit / Cicilan</div>
          </div>
          <div className="mcl-right">
            <div><i className={`fa ${(PaymentMethodType.CICILAN === currentPaymentMethod) ? "fa-angle-down" : "fa-angle-right"}`} /></div>
          </div>
        </div>
        <div className="list-content" style={{ display: (PaymentMethodType.CICILAN === currentPaymentMethod) ? "block" : "none" }}>
          content Cicilan
        </div>
      </li>,
    );

    retval.push(
      <li key={PaymentMethodType.KLIKPAY} className={(PaymentMethodType.KLIKPAY === currentPaymentMethod) ? "choosen" : ""} onClick={() => this.choosePaymentHandler(PaymentMethodType.KLIKPAY)}>
        <div className="header-content">
          <div className="radio-wrapper">
            <div className="radio-container" onClick={() => {}}>
              <input
                type="radio"
                name="courier-choice"
                checked={(PaymentMethodType.KLIKPAY === currentPaymentMethod)}
                onChange={() => this.choosePaymentHandler(PaymentMethodType.KLIKPAY)}
                value={PaymentMethodType.KLIKPAY}
              />
              <span className="checkmark"></span>
            </div>
          </div>
          <div></div>
          <div className="mcl-left">
            <div>BCA Klikpay</div>
          </div>
          <div className="mcl-right">
            <div><i className={`fa ${(PaymentMethodType.KLIKPAY === currentPaymentMethod) ? "fa-angle-down" : "fa-angle-right"}`} /></div>
          </div>
        </div>
        <div className="list-content" style={{ display: (PaymentMethodType.KLIKPAY === currentPaymentMethod) ? "block" : "none" }}>
          content BCA KlikPay
        </div>
      </li>,
    );

    return (<ul className="my-courier-list my-payment-list">{retval}</ul>);
  }

  saveHandler = () => {
    const { onSave } = this.props;

    onSave();
  }

  render() {
    const {
      title,
    } = this.state;

    return (
      <div>
        <div className="row mb-xs">
          <div className="col-sm-12">
            {this.createPaymentMethodList()}
          </div>
        </div>
        <div className="row mb-md">
          <div className="col-sm-6"></div>
          <div className="col-sm-6 text-right">
            <button type="button" className="btn btn-primary" onClick={this.saveHandler}>Selesai</button>
          </div>
        </div>
      </div>
    );
  }
}

export default PaymentForm;
