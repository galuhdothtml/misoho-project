/* eslint prop-types: 0 */
import React from "react";
import Rating from "./Rating";
import { API_BASE_URL } from "../constants";
import * as Util from "../utils";

export default class ProductItem extends React.Component {

  render() {
    const { data, modifierClass } = this.props;

    return (
      <div className={`product-item ${modifierClass || ""}`}>
        <img src={`${API_BASE_URL}/uploads/${data.primary_img}`} className="product-item__img" />
        <div className="product_item--padding-xs">
            <span className="product-item__title">{data.name}</span>
            <br />
            <span className="product-item__price">{Util.currency(data.price)}</span>
            <div className="product-item__rating" style={{ visibility: (parseInt(data.rating_value, 10) > 0) ? "unset" : "hidden" }}>
                <Rating value={data.rating_value} /><span className="product-item--review-total lightweight-text">({data.review_number})</span>
            </div>
        </div>
      </div>
    );
  }
}
