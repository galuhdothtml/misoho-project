/* eslint prop-types: 0 */
import React from "react";
import queryString from "query-string";
import Breadcrumb from "../Breadcrumb";
import { getCategories, getProducts, getCitiesGroup } from "../../data";
import InputText from "../form/InputText";
import LoadingModal from "../LoadingModal";
import InputCheckbox from "../form/InputCheckbox";
import Select from "../form/Select";
import ChooseCityPopup from "../ChooseCityPopup";
import ProductList from "../ProductList";
import Pagination from "../Pagination";

class Category extends React.Component {
  constructor(props) {
    super(props);

    const { params } = props.match;

    this.state = {
      idCategory: params.id,
      categoryName: "Dodol",
      minimumPrice: "",
      maximumPrice: "",
      choosenCities: [],
      categories: [],
      chooseCityPopupShowing: false,
      topCities: [
        {
          id: "DKI",
          name: "DKI Jakarta",
          checked: false,
        },
        {
          id: "22",
          name: "Kota Bandung",
          checked: false,
        },
        {
          id: "444",
          name: "Surabaya",
          checked: false,
        },
        {
          id: "255",
          name: "Malang",
          checked: false,
        },
      ],
      sortingList: [
        {
          id: "TERBARU",
          name: "Terbaru",
        },
        {
          id: "TERPOPULER",
          name: "Terpopuler",
        },
        {
          id: "HARGA_TERENDAH",
          name: "Termurah",
        },
        {
          id: "HARGA_TERTINGGI",
          name: "Termahal",
        },
      ],
      sorting: "TERBARU",
      products: [],
      currentPage: 1,
      pageCount: 1,
      isLoading: false,
      isLoadingProduct: true,
    };
  }

  componentDidMount = () => {
    const { page, sorting, cities } = this.getQueryParams();
    this.updateStateBeforeFetchData({ currentPage: page, sorting, choosenCities: cities }, () => {
      this.fetchAllData();
    });
  }

  updateStateBeforeFetchData = ({ currentPage, sorting, choosenCities }, callback) => {
    const {
      currentPage: currentPageState,
      sorting: sortingState,
      choosenCities: choosenCitiesState,
    } = this.state;

    let newPage = currentPageState;
    let newSort = sortingState;
    let newCities = JSON.stringify(choosenCitiesState);

    if (currentPage) {
      newPage = currentPage;
    }

    if (sorting) {
      newSort = sorting;
    }

    if (choosenCities) {
      newCities = choosenCities;
    }

    this.setState({
      currentPage: newPage,
      sorting: newSort,
      choosenCities: JSON.parse(newCities),
    }, () => {
      if (callback) {
        callback();
      }
    });
  }

  fetchAllData = async () => {
    this.setState({ isLoading: true });
    await this.fetchCategories();
    await this.fetchProducts();
    this.setState({ isLoading: false });
  }

  fetchCategories = async () => {
    const res = await getCategories();

    if (res.status) {
      const data = [{ id: "all", name: "Semua Kategori", slug: "all" }, ...res.data];

      this.setState({ categories: data });
    }
  }

  fetchAllCities = async (payload = {}) => {
    let data = [];
    try {
      const res = await getCitiesGroup(payload);

      if (res.status) {
        ({ data } = res);
      }
    } catch (e) {
      console.log("DEBUG-ERR: ", e);
    }

    return data;
  }

  fetchProducts = async () => {
    const {
      currentPage, idCategory, minimumPrice, maximumPrice,
      sorting,
    } = this.state;

    const minPrice = parseInt(minimumPrice, 10);
    const maxPrice = parseInt(maximumPrice, 10);
    const payload = {
      sort: sorting,
      limit: 16,
      id_category: idCategory === "all" ? null : idCategory,
      page: currentPage,
    };
    if (minPrice) {
      Object.assign(payload, { min_price: minPrice });
    }

    if (maxPrice) {
      Object.assign(payload, { max_price: maxPrice });
    }

    window.scrollTo(0, 0);

    this.setState({ isLoadingProduct: true });

    const res = await getProducts(payload);
    const products = res.data;

    this.setState({
      products,
      pageCount: res.page_total,
      isLoadingProduct: false,
    });
  }

  pushUrl = () => {
    const { history, location } = this.props;
    const { currentPage, sorting, choosenCities } = this.state;
    let queryParams = `page=${currentPage}&sorting=${sorting}`;

    if (choosenCities.length > 0) {
      queryParams = `${queryParams}&cities=${JSON.stringify(choosenCities)}`;
    }

    history.push(`${location.pathname}?${queryParams}`);
  }

  changeCategoryHandler = (e, id, slug) => {
    const { history } = this.props;

    e.preventDefault();
    history.push(`/category/${id}/${slug}`);
    this.setState({
      idCategory: id,
      sorting: "TERBARU",
      currentPage: 1,
      choosenCities: [],
      minimumPrice: "",
      maximumPrice: "",
    }, () => {
      this.fetchProducts();
    });
  }

  onPageChangeHandler = (val) => {
    this.updateStateBeforeFetchData({ currentPage: val }, () => {
      this.fetchProducts();
      this.pushUrl();
    });
  }

  getQueryParams = () => (queryString.parse(this.props.location.search));

  changeMinimumPriceHandler = (val) => {
    this.setState({ minimumPrice: val });

    clearTimeout(this.filterMinPriceIdle);
    this.filterMinPriceIdle = setTimeout(() => {
      this.setState({ minimumPrice: val, isLoadingProduct: true }, () => {
        this.fetchProducts();
        this.pushUrl();
      });
    }, 1000);
  }

  changeMaximumPriceHandler = (val) => {
    this.setState({ maximumPrice: val });

    clearTimeout(this.filterMaxPriceIdle);
    this.filterMaxPriceIdle = setTimeout(() => {
      this.setState({ maximumPrice: val, isLoadingProduct: true }, () => {
        this.fetchProducts();
        this.pushUrl();
      });
    }, 1000);
  }

  showChooseCity = (e) => {
    e.preventDefault();
    this.setState({ chooseCityPopupShowing: true });
  }

  hideChooseCity = () => {
    this.setState({ chooseCityPopupShowing: false });
  }

  applyCityHandler = (val) => {
    console.log('DEBUG-VAL: ', val);
    this.setState({ chooseCityPopupShowing: false, choosenCities: val });
  }

  changeSortingHandler = (val) => {
    this.updateStateBeforeFetchData({ sorting: val }, () => {
      this.fetchProducts();
      this.pushUrl();
    });
  }

  render() {
    const {
      categoryName, minimumPrice, maximumPrice,
      categories, topCities, choosenCities,
      currentPage, sortingList, sorting,
      products, pageCount,
      chooseCityPopupShowing, isLoading,
      isLoadingProduct,
    } = this.state;
    const { params } = this.props.match;

    return (
      <div>
        <div className="page-breadcrumb-wrapper">
          <div className="breadcrumb-wrapper-inner">
            <div className="container">
              <Breadcrumb data={[
                { title: "Home", link: "/" },
                { title: "Kategori", link: "/" },
                { title: categoryName, isActive: true },
              ]} />
            </div>
          </div>
          <div className="container">
            <div className="row">
              <div className="col-sm-8">
                <div style={{
                  position: "relative",
                  top: "7px",
                }}><span className="lightweight-text">Kategori</span> Keripik <span className="lightweight-text">(Daftar Produk 1 - 50 dari 100)</span></div>
              </div>
              <div className="col-sm-4">
                <div className="form-group row" style={{ marginBottom: "0px" }}>
                  <label className="col-sm-3 text-right pr-0 lightweight-text" style={{ paddingTop: "5px", fontWeight: "normal" }}>Urutkan: </label>
                  <div className="col-sm-9">
                    <Select
                      changeEvent={value => this.changeSortingHandler(value)}
                      data={sortingList}
                      value={sorting}
                    />
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className="container" style={{ paddingTop: "30px" }}>
          <div className="row">
            <div className="col-sm-3">
              <ul className="custom-list-group list-group">
                <li className="list-group-item">
                  <h6>Kategori</h6>
                  <ul>
                    {categories.map(x => (<li className={`list-category category-name ${((String(params.id) === String(x.id)) && (params.slug === x.slug)) && "active"}`} key={x.id}><a onClick={e => this.changeCategoryHandler(e, x.id, x.slug)} href="#">{x.name}</a></li>))}
                  </ul>
                </li>
                <li className="list-group-item">
                  <h6>Harga</h6>
                  <div style={{ marginBottom: "8px" }}>
                    <InputText placeholder="Minimum" changeEvent={this.changeMinimumPriceHandler} value={minimumPrice} />
                  </div>
                  <div>
                    <InputText placeholder="Maksimum" changeEvent={this.changeMaximumPriceHandler} value={maximumPrice} />
                  </div>
                </li>
                <li className="list-group-item">
                  <h6>Asal Oleh-Oleh</h6>
                  <ul className="lightweight-text">
                    {topCities.map((x, index) => (<li key={x.id} className="list-category">
                      <div>
                        <InputCheckbox value={x.checked} changeEvent={val => this.changeCityHandler(index, val)} />
                        <label style={{ fontWeight: "normal" }}>{x.name}</label>
                      </div>
                    </li>))
                    }
                    <li className="list-category">
                      <div>
                        <a onClick={this.showChooseCity} href="#">Lihat Semua</a>
                      </div>
                    </li>
                  </ul>
                  {chooseCityPopupShowing && <ChooseCityPopup applyEvent={this.applyCityHandler} onFetch={this.fetchAllCities} value={choosenCities} onHide={this.hideChooseCity} />}
                </li>
              </ul>
            </div>
            <div className="col-sm-9">
              <div style={{ position: "relative" }}>
                <div>
                  <ProductList data={products} modifierLi="product-list__li--nth-child-4" modifierProductItem="product-item--small" />
                  <div className="text-center">
                    <Pagination currentPage={currentPage} onPageChange={this.onPageChangeHandler} pageCount={pageCount} />
                  </div>
                </div>
                {isLoadingProduct && <LoadingModal isAbsolute={true} />}
              </div>
            </div>
          </div>
          {isLoading && <LoadingModal />}
        </div>
      </div>
    );
  }
}

export default Category;
