/* eslint prop-types: 0 */
import React from "react";
import queryString from "query-string";
import Slider from "rc-slider";
import update from "immutability-helper";
import "rc-slider/assets/index.css";
import Breadcrumb from "../Breadcrumb";
import {
  getCategories, getProducts, getCitiesGroup, getTopCities,
} from "../../data";
import LoadingModal from "../LoadingModal";
import InputCheckbox from "../form/InputCheckbox";
import InputCurrency from "../form/InputCurrency";
import Select from "../form/Select";
import ChooseCityPopup from "../ChooseCityPopup";
import ProductList from "../ProductList";
import Pagination from "../Pagination";

const { Range } = Slider;

class Category extends React.Component {
  constructor(props) {
    super(props);

    const { params } = props.match;

    this.state = {
      idCategory: params.id,
      categoryName: "",
      minimumPrice: 1,
      maximumPrice: 1000000,
      choosenCities: [],
      categories: [],
      chooseCityPopupShowing: false,
      sliderValue: [0, 100],
      topCities: [],
      sortingList: [
        {
          id: "TERBARU",
          name: "Terbaru",
        },
        {
          id: "TERPOPULER",
          name: "Terpopuler",
        },
        {
          id: "HARGA_TERENDAH",
          name: "Termurah",
        },
        {
          id: "HARGA_TERTINGGI",
          name: "Termahal",
        },
      ],
      sorting: "TERBARU",
      products: [],
      currentPage: 1,
      pageCount: 1,
      isLoading: false,
      isLoadingProduct: true,
    };
  }

  componentDidMount = () => {
    let {
      currentPage, sorting, minimumPrice, maximumPrice,
    } = this.state;
    const { choosenCities } = this.state;
    const {
      page, sorting: sortingParam, locs, min_price: minPrice, max_price: maxPrice,
    } = this.getQueryParams();

    if (page) {
      currentPage = page;
    }

    if (sortingParam) {
      sorting = sortingParam;
    }

    if (minPrice) {
      minimumPrice = minPrice;
    }

    if (maxPrice) {
      maximumPrice = maxPrice;
    }

    if (locs) {
      const rawCities = locs.split(",");
      rawCities.forEach((x) => {
        const splits = x.split("P");
        choosenCities.push({ id: splits[0], idProvince: splits[1] });
      });
    }

    this.setState({
      currentPage, sorting, choosenCities, minimumPrice, maximumPrice,
    }, () => {
      this.fetchAllData();
    });
  }

  fetchAllData = async () => {
    this.setState({ isLoading: true });
    await this.fetchCategories();
    await this.fetchProducts();
    await this.fetchTopCities();
    this.setState({ isLoading: false });
  }

  fetchCategories = async () => {
    const res = await getCategories();

    if (res.status) {
      const data = [{ id: "all", name: "Semua Kategori", slug: "all" }, ...res.data];

      this.setState({ categories: data });
    }
  }

  fetchAllCities = async (payload = {}) => {
    let data = [];
    try {
      const res = await getCitiesGroup(payload);

      if (res.status) {
        ({ data } = res);
      }
    } catch (e) {
      console.log("DEBUG-ERR: ", e);
    }

    return data;
  }

  fetchProducts = async (callback) => {
    const {
      currentPage, idCategory, sorting, categories,
      minimumPrice, maximumPrice, choosenCities,
    } = this.state;

    const payload = {
      sort: sorting,
      limit: 16,
      id_category: idCategory === "all" ? null : idCategory,
      page: currentPage,
    };

    if (minimumPrice) {
      Object.assign(payload, {
        min_price: parseFloat(minimumPrice, 10),
      });
    }

    if (maximumPrice) {
      Object.assign(payload, {
        max_price: parseFloat(maximumPrice, 10),
      });
    }

    if (choosenCities.length > 0) {
      Object.assign(payload, {
        origin: choosenCities.map(x => (x.id)),
      });
    }

    window.scrollTo(0, 0);

    this.setState({ isLoadingProduct: true });

    const res = await getProducts(payload);
    const products = res.data;

    let categoryName = "";
    const found = categories.find(x => (String(x.id) === String(idCategory)));

    if (found) {
      categoryName = found.name;
    }

    this.setState({
      products,
      pageCount: res.page_total,
      isLoadingProduct: false,
      categoryName,
    }, () => {
      if (callback) {
        callback();
      }
    });
  }

  fetchTopCities = async () => {
    const { choosenCities } = this.state;
    const res = await getTopCities();
    let topCities = [];

    if (res.status) {
      topCities = res.data.map((x) => {
        const checked = choosenCities.find(cc => (String(cc.id) === String(x.origin)));

        return {
          id: x.origin,
          name: x.name,
          id_province: x.id_province,
          checked: !!checked,
        };
      });
    }

    this.setState({ topCities });
  }

  pushUrl = (customUrl = null) => {
    const { history, location } = this.props;
    const { currentPage, sorting, minimumPrice, maximumPrice } = this.state;
    const queryParams = { page: currentPage };
    let newParams = "";

    if (sorting) {
      Object.assign(queryParams, { sorting });
    }

    const citiesData = this.convertCitiesToString();

    if (citiesData) {
      Object.assign(queryParams, { locs: citiesData });
    }

    if (minimumPrice) {
      Object.assign(queryParams, { min_price: minimumPrice });
    }

    if (maximumPrice) {
      Object.assign(queryParams, { max_price: maximumPrice });
    }

    Object.keys(queryParams).forEach((x) => {
      if (newParams.length > 0) {
        newParams = `${newParams}&${x}=${queryParams[x]}`;
      } else {
        newParams = `?${x}=${queryParams[x]}`;
      }
    });

    this.fetchProducts(() => {
      if (customUrl) {
        history.push(`${customUrl}${newParams}`);
      } else {
        history.push(`${location.pathname}${newParams}`);
      }
    });
  }

  changeCategoryHandler = (e, id, slug) => {
    e.preventDefault();
    this.setState({ idCategory: id, currentPage: 1, sorting: "TERBARU" }, () => {
      this.pushUrl(`/category/${id}/${slug}`);
    });
  }

  onPageChangeHandler = (val) => {
    this.setState({ currentPage: val }, () => {
      this.pushUrl();
    });
  }

  getQueryParams = () => (queryString.parse(this.props.location.search));

  changeMinimumPriceHandler = (val) => {
    this.setState({ minimumPrice: val });
  }

  changeMaximumPriceHandler = (val) => {
    this.setState({ maximumPrice: val });
  }

  showChooseCity = (e) => {
    e.preventDefault();
    this.setState({ chooseCityPopupShowing: true });
  }

  hideChooseCity = () => {
    this.setState({ chooseCityPopupShowing: false });
  }

  applyCityHandler = (val) => {
    const { topCities } = this.state;
    const newTopCities = topCities.map((x) => {
      const checked = val.find(cc => (String(cc.id) === String(x.id)));

      return {
        id: x.id,
        name: x.name,
        id_province: x.id_province,
        checked: !!checked,
      };
    });
    this.setState({ chooseCityPopupShowing: false, topCities: newTopCities, choosenCities: val }, () => {
      this.pushUrl();
    });
  }

  convertCitiesToString = () => {
    const { choosenCities } = this.state;
    const newData = choosenCities.map(x => (`${x.id}P${x.idProvince}`));
    return newData.join();
  }

  changeSortingHandler = (val) => {
    this.setState({ sorting: val }, () => {
      this.pushUrl();
    });
  }

  sliderChangeHandler = (val) => {
    const [minPercent, maxPercent] = val;
    const newMinimumPrice = 1000000 * (parseFloat(minPercent, 10) / 100);
    const newMaximumPrice = 1000000 * (parseFloat(maxPercent, 10) / 100);

    this.setState({
      sliderValue: val,
      minimumPrice: String(parseInt(newMinimumPrice, 10)),
      maximumPrice: String(parseInt(newMaximumPrice, 10)),
    });
  }

  applyPriceRange = () => {
    this.setState({ currentPage: 1 }, () => {
      this.pushUrl();
    });
  }

  changeCityHandler = (index, idCity, idProvince, val) => {
    const { topCities, choosenCities } = this.state;
    const newData = update(topCities, {
      [index]: {
        checked: {
          $set: val,
        },
      },
    });
    let newChooseCities = [];

    if (val) {
      newChooseCities = update(choosenCities, {
        $push: [{ id: idCity, idProvince }],
      });
    } else {
      newChooseCities = choosenCities.filter(x => (String(x.id) !== String(idCity)));
    }

    this.setState({ topCities: newData, choosenCities: newChooseCities }, () => {
      this.pushUrl();
    });
  }

  render() {
    const {
      categoryName, minimumPrice, maximumPrice,
      categories, topCities, choosenCities,
      currentPage, sortingList, sorting,
      products, pageCount,
      chooseCityPopupShowing, isLoading,
      isLoadingProduct, sliderValue,
    } = this.state;
    const { params } = this.props.match;

    return (
      <div>
        <div className="page-breadcrumb-wrapper">
          <div className="breadcrumb-wrapper-inner">
            <div className="container">
              <Breadcrumb data={[
                { title: "Home", link: "/" },
                { title: "Kategori", link: "/" },
                { title: categoryName, isActive: true },
              ]} />
            </div>
          </div>
          <div className="container">
            <div className="row">
              <div className="col-sm-8">
                <div style={{
                  position: "relative",
                  top: "7px",
                }}><span className="lightweight-text">Kategori</span> {categoryName} <span className="lightweight-text">(Daftar Produk 1 - 50 dari 100)</span></div>
              </div>
              <div className="col-sm-4">
                <div className="form-group row" style={{ marginBottom: "0px" }}>
                  <label className="col-sm-3 text-right pr-0 lightweight-text" style={{ paddingTop: "5px", fontWeight: "normal" }}>Urutkan: </label>
                  <div className="col-sm-9">
                    <Select
                      changeEvent={value => this.changeSortingHandler(value)}
                      data={sortingList}
                      value={sorting}
                    />
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className="container" style={{ paddingTop: "30px" }}>
          <div className="row">
            <div className="col-sm-3">
              <ul className="custom-list-group list-group">
                <li className="list-group-item">
                  <h6>Kategori</h6>
                  <ul>
                    {categories.map(x => (<li className={`list-category category-name ${((String(params.id) === String(x.id)) && (params.slug === x.slug)) && "active"}`} key={x.id}><a onClick={e => this.changeCategoryHandler(e, x.id, x.slug)} href="#">{x.name}</a></li>))}
                  </ul>
                </li>
                <li className="list-group-item">
                  <h6>Harga</h6>
                  <div style={{ marginBottom: "12px", padding: "0 6px" }}>
                    <Range allowCross={false} value={sliderValue} onChange={this.sliderChangeHandler} />
                  </div>
                  <div style={{ marginBottom: "12px" }}>
                    <div style={{ display: "inline-block", width: "47%" }}>
                      <InputCurrency placeholder="Minimum" changeEvent={this.changeMinimumPriceHandler} value={minimumPrice} />
                    </div>
                    <div className="text-center" style={{ display: "inline-block", width: "6%" }}>-</div>
                    <div style={{ display: "inline-block", width: "47%" }}>
                      <InputCurrency placeholder="Maksimum" changeEvent={this.changeMaximumPriceHandler} value={maximumPrice} />
                    </div>
                  </div>
                  <div>
                    <button type="button" onClick={this.applyPriceRange} className="btn btn-primary btn-sm btn-block">Cari</button>
                  </div>
                </li>
                <li className="list-group-item">
                  <h6>Asal Oleh-Oleh</h6>
                  <ul className="lightweight-text">
                    {topCities.map((x, index) => (<li key={x.id} className="list-category">
                      <div>
                        <InputCheckbox value={x.checked} changeEvent={val => this.changeCityHandler(index, x.id, x.id_province, val)} />
                        <label style={{ fontWeight: "normal" }}>{x.name}</label>
                      </div>
                    </li>))
                    }
                    <li className="list-category">
                      <div>
                        <a onClick={this.showChooseCity} href="#">Lihat Semua</a>
                      </div>
                    </li>
                  </ul>
                  {chooseCityPopupShowing && <ChooseCityPopup applyEvent={this.applyCityHandler} onFetch={this.fetchAllCities} value={choosenCities} onHide={this.hideChooseCity} />}
                </li>
              </ul>
            </div>
            <div className="col-sm-9">
              <div style={{ position: "relative" }}>
                <div>
                  <ProductList data={products} modifierLi="product-list__li--nth-child-4" modifierProductItem="product-item--small" />
                  <div className="text-center">
                    <Pagination currentPage={currentPage} onPageChange={this.onPageChangeHandler} pageCount={pageCount} />
                  </div>
                </div>
                {isLoadingProduct && <LoadingModal isAbsolute={true} />}
              </div>
            </div>
          </div>
          {isLoading && <LoadingModal />}
        </div>
      </div>
    );
  }
}

export default Category;
