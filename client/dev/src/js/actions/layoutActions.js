import { ActionTypes } from "../constants";

// eslint-disable-next-line import/prefer-default-export
export const showAppOverlay = showStatus => ({
  type: ActionTypes.SHOW_APP_OVERLAY,
  showStatus,
});
