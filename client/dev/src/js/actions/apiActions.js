import { ActionTypes } from "../constants";
import { getCarts, getMyAccount } from "../data";

const requestCarts = () => ({
  type: ActionTypes.REQUEST_CARTS,
});

const receiveCarts = (data, showPopupAfterReceiveData) => ({
  type: ActionTypes.RECEIVE_CARTS,
  data,
  showPopupAfterReceiveData,
});

// eslint-disable-next-line import/prefer-default-export
export const fetchCarts = (showPopupAfterReceiveData = false) => (dispatch) => {
  dispatch(requestCarts());

  const promise = new Promise(((resolve, reject) => {
    getCarts().then((res) => {
      if (res.status) {
        dispatch(receiveCarts(res.data, showPopupAfterReceiveData));
        resolve(true);
      }
    }).catch((err) => {
      console.log("API-REDUX-ERR: ", err);
      reject(err);
    });
  }));

  return promise;
};

const requestMyAccount = () => ({
  type: ActionTypes.REQUEST_MY_ACCOUNT,
});

const receiveMyAccount = data => ({
  type: ActionTypes.RECEIVE_MY_ACCOUNT,
  data,
});

// eslint-disable-next-line import/prefer-default-export
export const fetchMyAccount = () => (dispatch) => {
  dispatch(requestMyAccount());

  const promise = new Promise(((resolve, reject) => {
    getMyAccount().then((res) => {
      if (res.status) {
        dispatch(receiveMyAccount(res.data));
        resolve(true);
      }
    }).catch((err) => {
      console.log("API-REDUX-ERR: ", err);
      reject(err);
    });
  }));

  return promise;
};
