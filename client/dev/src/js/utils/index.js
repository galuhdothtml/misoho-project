import numeral from "numeral";
import { API_BASE_URL } from "../constants";

export const getToken = () => localStorage.getItem("usertoken"); // eslint-disable-line no-undef

export const setToken = (_token) => {
  localStorage.setItem("usertoken", _token); // eslint-disable-line no-undef
};

export const removeToken = () => {
  localStorage.removeItem("usertoken"); // eslint-disable-line no-undef
};

export const capitalizeFirstLetter = (string) => {
  const results = string.split(/\s/);
  let retval = "";

  results.forEach((x) => {
    retval = `${retval} ${x.charAt(0).toUpperCase()}${x.slice(1)}`;
  });

  return retval;
};

export const isEmailValid = (_email) => {
  // eslint-disable-next-line no-useless-escape
  if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(_email)) {
    return true;
  }

  return false;
};

export const currency = (val) => {
  const retval = numeral(val).format("0,0");

  return retval;
};

export const createAjax = (url, extendParam) => ({
  headers: {
    Authorization: `Bearer ${getToken()}`,
  },
  url: `${API_BASE_URL}${url}`,
  dataType: "json",
  delay: 250,
  data: (params) => {
    const query = {
      filterText: params.term,
      page: 1,
      limit: 5,
    };

    if (extendParam) {
      Object.assign(query, extendParam);
    }

    return query;
  },
  processResults: res => ({
    results: res.data.map(x => ({ id: x.id, text: x.name })),
  }),
  cache: true,
});
