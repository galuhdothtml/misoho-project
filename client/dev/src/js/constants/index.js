/* eslint prop-types: 0 */

// eslint-disable-next-line import/prefer-default-export
export const API_BASE_URL = "http://localhost:3300";
export const ActionTypes = {
  SHOW_APP_OVERLAY: "SHOW_APP_OVERLAY",
  REQUEST_CARTS: "REQUEST_CARTS",
  RECEIVE_CARTS: "RECEIVE_CARTS",
  REQUEST_MY_ACCOUNT: "REQUEST_MY_ACCOUNT",
  RECEIVE_MY_ACCOUNT: "RECEIVE_MY_ACCOUNT",
};
