/* eslint prop-types: 0 */
import React from "react";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import posed from "react-pose";
import update from "immutability-helper";
import ScrollArea from "react-scrollbar";
import { API_BASE_URL } from "../../constants";
import { currency } from "../../utils";
import QtyInput from "../../components/form/QtyInput";
import LoadingImage from "../../images/loading.gif";
import { deleteCart, updateCart } from "../../data";
import { fetchCarts } from "../../actions/apiActions";

const style = {
  modalBody: {
    height: `${(window.innerHeight - 120)}px`,
    padding: "0px",
  },
  modalScrollArea: {
    height: "100%",
    width: "auto",
    padding: "0px",
  },
  modalScrollAreaContent: {
    padding: "16px",
  },
};
const Box = posed.div({
  hidden: {
    opacity: 0,
    transition: { duration: 300 },
  },
  visible: {
    opacity: 1,
    transition: { duration: 300 },
  },
});
const InnerBox = posed.div({
  hidden: {
    right: "-500px",
    transition: { duration: 300 },
  },
  visible: {
    right: "0px",
    transition: { duration: 300 },
  },
});
class SidePopup extends React.Component {
  static propTypes = {
    saveEvent: PropTypes.func,
    onHide: PropTypes.func,
    children: PropTypes.node,
    title: PropTypes.string,
  }

  static defaultProps = {
    saveEvent: () => {},
    onHide: () => {},
    children: null,
    title: undefined,
  }

  constructor(props) {
    super(props);

    this.state = {
      isVisible: false,
      isLoading: false,
      updatedCarts: [],
    };
  }

  componentDidMount() {
    this.setState({ isVisible: true });
  }

  saveHandler = (e) => {
    const { saveEvent } = this.props;

    if (e) {
      e.preventDefault();
    }

    saveEvent(this.hidePopup);
  }

  hidePopup = () => {
    const { onHide } = this.props;
    this.setState({ isVisible: false }, () => {
      setTimeout(() => {
        onHide();
      }, 300);
    });
  }

  changeQtyHandler = (idProduct, qty) => {
    const { updatedCarts } = this.state;
    const foundIndex = updatedCarts.findIndex(uc => (String(uc.id_product) === String(idProduct)));
    let newData = updatedCarts;

    if (foundIndex > -1) {
      newData = update(updatedCarts, {
        [foundIndex]: {
          qty: { $set: parseInt(qty, 10) },
        },
      });
    } else {
      newData = update(updatedCarts, {
        $push: [{ id_product: idProduct, qty }],
      });
    }

    this.setState({ updatedCarts: newData });

    clearTimeout(this.filterIdle);
    this.filterIdle = setTimeout(() => {
      this.setState({ updatedCarts: newData }, () => {
        this.doUpdatingData();
      });
    }, 500);
  }

  doUpdatingData = async () => {
    const { doFetchingCarts } = this.props;
    const { updatedCarts } = this.state;

    this.setState({ isLoading: true });

    const promises = updatedCarts.map(async (x) => {
      await updateCart(x.id_product, x.qty);

      return x;
    });

    await Promise.all(promises);
    await doFetchingCarts();

    this.setState({ isLoading: false, updatedCarts: [] });
  }

  deleteCartHandler = async (id) => {
    try {
      const { doFetchingCarts } = this.props;

      this.setState({ isLoading: true });

      await deleteCart(id);
      await doFetchingCarts();

      this.setState({ isLoading: false });
    } catch (e) {
      console.log("DEBUG-ERR: ", e);
    }
  }

  setupCartList = () => {
    const retval = [];
    const { updatedCarts } = this.state;
    const { data: carts } = this.props;

    carts.forEach((x, i) => {
      const newImg = `${API_BASE_URL}/uploads/${x.img}`;
      const found = updatedCarts.find(uc => (String(uc.id_product) === String(x.id_product)));
      let newQty = x.qty;

      if (found) {
        newQty = found.qty;
      }

      retval.push(
        <div className="cart-item" key={x.id}>
          <div className="left"><img src={newImg} /></div>
          <div className="middle">
            <div className="title">{x.name}</div>
            <div className="subtitle">{currency(x.price)}</div>
            <div style={{ marginTop: "8px" }}>
                <QtyInput index={i} value={newQty} changeEvent={(index, val) => { this.changeQtyHandler(x.id_product, val); }} />
            </div>
          </div>
          <div className="right">
            <button type="button" onClick={() => { this.deleteCartHandler(x.id_product); }}>×</button>
          </div>
        </div>,
      );
    });

    return retval;
  }

  render() {
    const { isVisible, isLoading } = this.state;
    const { data, title } = this.props;
    let cartTotal = 0;

    data.forEach((x) => {
      cartTotal += (parseFloat(x.price, 10) * x.qty);
    });

    return (
      <div className="sidepopup-container">
        <Box className="modal" pose={isVisible ? "visible" : "hidden"} style={{ display: "block" }}>
          <InnerBox className="modal-dialog" pose={isVisible ? "visible" : "hidden"} style={{ width: "500px" }}>
            <div className="modal-content">
              { title && <div className="modal-header">
                  <button type="button" className="close" onClick={this.hidePopup}>×</button>
                  <h4 className="modal-title">{title}</h4>
                </div>
              }
              <div className="modal-body" style={style.modalBody}>
                <ScrollArea
                  smoothScrolling={false}
                  className="modal-scrollarea"
                  style={style.modalScrollArea}
                  contentStyle={style.modalScrollAreaContent}
                  horizontal={false}
                  stopScrollPropagation={true}
                >
                  <div>
                    <div className="cart-list-wrapper">
                      {this.setupCartList()}
                    </div>
                  </div>
                </ScrollArea>
              </div>
              <div className="modal-footer">
                <div className="btn pull-left sidepopup-note">
                  <div className="lightweight-text">Total Belanja</div>
                  <div style={{ fontSize: "18px" }}>{currency(cartTotal)}</div>
                </div>
                <div style={{ padding: "14px" }}>
                  <button
                    type="button"
                    className="btn btn-primary"
                    onClick={this.saveHandler}>Checkout</button>
                </div>
              </div>
            </div>
          </InnerBox>
        </Box>
        { isLoading && (
          <div className="cart-loading-wrapper">
            <div className="table-wrapper">
              <div className="loading-content">
                <img src={LoadingImage} />
                <h5 className="lightweight-text">Loading ...</h5>
              </div>
            </div>
          </div>
        )}
      </div>
    );
  }
}

const mapStateToProps = state => ({
  apiRedux: state.apiRedux,
});

const mapDispatchToProps = dispatch => ({
  doFetchingCarts: () => dispatch(fetchCarts(false)),
});

export default connect(mapStateToProps, mapDispatchToProps)(SidePopup);
