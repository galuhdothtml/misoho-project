/* eslint prop-types: 0 */
import React, { Fragment } from "react";
import PropTypes from "prop-types";
import Header from "./Header";
import TopSearch from "./TopSearch";

let lastScrollTop = 0;

class FullHeader extends React.Component {
  static propTypes = {
    showAppOverlay: PropTypes.bool,
  }

  static defaultProps = {
    showAppOverlay: false,
  }

  constructor(props) {
    super(props);

    this.state = {
      title: "Wrapper",
      fakeTopSearch: ["bakso malang", "keripik buah", "dodol", "sambel enak", "kopi luwak"],
      showTopSearch: true,
    };
  }

  componentDidMount = () => {
    document.addEventListener("scroll", this.handleWheel);
    window.scrollTo(0, 0);
  }

  componentWillUnmount = () => {
    document.removeEventListener("scroll", this.handleWheel);
  }

  handleWheel = () => {
    const st = window.pageYOffset || document.documentElement.scrollTop;
    if (st > lastScrollTop) {
      this.setState({ showTopSearch: false });
    } else {
      this.setState({ showTopSearch: true });
    }
    lastScrollTop = st <= 0 ? 0 : st;
  }

  render() {
    const { fakeTopSearch, showTopSearch } = this.state;

    return (
      <Fragment>
        <Header />
        <TopSearch data={fakeTopSearch} show={showTopSearch} />
      </Fragment>
    );
  }
}

export default FullHeader;
