/* eslint prop-types: 0 */
import React from "react";

class SearchInput extends React.Component {
  changeQuery = (e) => {
    const { changeEvent } = this.props;
    const val = e.target.value;
    changeEvent(val);
  }

  render() {
    const { value } = this.props;

    return (
      <div className="search-input-wrapper">
        <div className="input-group">
          <div className="input-group-btn">
            <div className="btn">
              <i className="ic-search"></i>
            </div>
          </div>
          <input
            type="text"
            onChange={this.changeQuery}
            value={value}
            style={{ width: "20em", height: "40px" }}
            className="form-control"
            placeholder="Cari di Mister Oleh Oleh"
            name="search"
            autoComplete="off" />
          <span className="input-group-btn">
            <div className="btn btn-primary" style={{ zIndex: 3 }}>
              Cari
            </div>
          </span>
        </div>
      </div>
    );
  }
}


export default SearchInput;
