/* eslint prop-types: 0 */
import React from "react";
import { API_BASE_URL } from "../../../constants";
import { currency } from "../../../utils";

class Dropdown extends React.Component {
  navToCart = () => {
    // eslint-disable-next-line no-restricted-globals
    location.href = "/carts";
  }

  createComponent = (carts) => {
    let cartProduct = 0;
    let cartTotal = 0;
    const retval = [];

    retval.push(
      <div className="cart-item header" key="cart-item-header">
        <div className="title"><i className="fa fa-shopping-cart" /> Keranjang Belanja</div>
      </div>,
    );

    if (carts.length > 0) {
      carts.forEach((x) => {
        const newImg = `${API_BASE_URL}/uploads/${x.img}`;
        cartProduct += 1;
        cartTotal += (parseFloat(x.price, 10) * x.qty);
        retval.push(
          <div className="cart-item" key={x.id}>
            <div className="left"><img src={newImg} /></div>
            <div className="middle">
              <div className="title">{x.name}</div>
              <div className="subtitle">{currency(x.price)}</div>
            </div>
            <div className="right">
              <div>Jumlah: {x.qty}</div>
              <div><button type="button"><i className="fa fa-trash-o" /></button></div>
            </div>
          </div>,
        );
      });
      retval.push(
        <div className="cart-item footer" key="cart-item-footer">
          <div className="left">
            <div className="title">Total ({cartProduct}):</div>
            <div className="subtitle">{currency(cartTotal)}</div>
          </div>
          <div className="right">
            <button type="button" className="btn btn-primary" onClick={this.navToCart}>Checkout</button>
          </div>
        </div>,
      );
    } else {
      retval.push(<div className="cart-item" key="cart-item-empty"><div className="text">Keranjang belanja masih kosong.</div></div>);
    }

    return retval;
  }

  render() {
    const { carts } = this.props;

    return (
      <div className="dropdown-menu cart-list-wrapper">
        {this.createComponent(carts)}
      </div>
    );
  }
}

export default Dropdown;
