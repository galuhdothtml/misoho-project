/* eslint prop-types: 0 */
import React from "react";
import { connect } from "react-redux";
import onClickOutside from "react-onclickoutside";
import PropTypes from "prop-types";
import Dropdown from "./Dropdown";
import { showAppOverlay } from "../../../actions/layoutActions";

class CartMenu extends React.Component {
  static propTypes = {
    showOverlay: PropTypes.func,
  }

  static defaultProps = {
    showOverlay: () => { },
  }

  constructor(props) {
    super(props);

    this.state = {
      showDropdown: false,
    };
  }

  handleClickOutside = () => {
    const { showDropdown } = this.state;
    const { showOverlay } = this.props;

    if (showDropdown) {
      showOverlay(false);
      this.setState({ showDropdown: false });
    }
  }

  toggleDropdown = (e) => {
    if (e) {
      e.preventDefault();
    }

    const { clickEvent } = this.props;
    clickEvent();
  }

  render() {
    const { carts } = this.props;
    const { showDropdown } = this.state;

    return (
      <li className={`dropdown ${showDropdown ? "open" : ""}`}><a href="#" onClick={this.toggleDropdown}>
        <div className="icon-content">
          <i className="fa fa-shopping-cart" />
        </div>
        <div className="title-content">
          <span>Keranjang</span>
        </div>
        { carts.length > 0 && (
          <div className="badge-cart">
            <div>{carts.length}</div>
          </div>
        )}
      </a>
      </li>
    );
  }
}

const mapDispatchToProps = dispatch => ({
  showOverlay: showStatus => dispatch(showAppOverlay(showStatus)),
});

export default connect(null, mapDispatchToProps)(onClickOutside(CartMenu));
