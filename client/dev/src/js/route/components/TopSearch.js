/* eslint prop-types: 0 */
import React from "react";
import {
  Link,
} from "react-router-dom";
import posed from "react-pose";

const Box = posed.div({
  hidden: {
    top: "12px",
    transition: { duration: 300 },
  },
  visible: {
    top: "57px",
    transition: { duration: 300 },
  },
});
export default class TopSearch extends React.Component {
  render() {
    const { data, show } = this.props;
    return (
      <Box className="top-search-container" pose={show ? "visible" : "hidden"}>
        <div className="container-fluid">
          <div className="title">
            <h5>Pencarian Populer</h5>
          </div>
          <div className="content">
            <ul>
              {data.map((x, i) => (
                <li key={i}>
                  <Link to={`/search?q=${x}`}>{x}</Link>
                </li>))}
            </ul>
          </div>
        </div>
      </Box>
    );
  }
}
