/* eslint prop-types: 0 */
import React from "react";

class FloatChat extends React.Component {
  render = () => {
    return (
      <div className="float-chat-button">
        <i className="icon-float-chat float-chat-button__icon" />
      </div>
    );
  }
}

export default FloatChat;
