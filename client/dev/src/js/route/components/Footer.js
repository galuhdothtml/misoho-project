/* eslint prop-types: 0 */
import React from "react";
import {
  Link,
} from "react-router-dom";
import { connect } from "react-redux";
import ImgPattern from "../../images/pattern.png";
import DownloadAndroid from "../../images/ic-download-android.svg";
import DownloadIos from "../../images/ic-download-ios.svg";

class Footer extends React.Component {
  render = () => {
    const { showAppOverlay } = this.props;

    return (
      <div>
        <div className="misoho-benefit-wrapper">
          <div className="container">
            <div className="row">
              <div className="col-lg-3">
                <div className="benefit-box">
                  <i className="footer-icn-1 large" />
                  <h6>Free &amp; Next Day Delivery</h6>
                  <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut vel.</p>
                </div>
              </div>
              <div className="col-lg-3">
                <div className="benefit-box">
                  <i className="footer-icn-2 large" />
                  <h6>100% Satisfaction Guarantee</h6>
                  <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut vel.</p>
                </div>
              </div>
              <div className="col-lg-3">
                <div className="benefit-box">
                  <i className="footer-icn-3 large" />
                  <h6>Great Daily Deals Discount</h6>
                  <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut vel.</p>
                </div>
              </div>
              <div className="col-lg-3">
                <div className="benefit-box">
                  <i className="footer-icn-4 large" />
                  <h6>100% Satisfaction Guarantee</h6>
                  <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut vel.</p>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className="footer-container" style={{
          paddingTop: "0px",
          backgroundImage: `url(${ImgPattern})`,
        }}>
          <div style={{
            background: "#FFF",
            paddingTop: "1em",
            paddingBottom: "1em",
            color: "#333",
          }}>
            <div className="container">
              <div className="company-short-bio">
                <h1 className="title">
                  <span>Tokopedia - Jual Beli Online Aman dan Nyaman</span>
                </h1>
                <p className="lightweight-text"><span>Tokopedia merupakan pasar / mal online terbesar di Indonesia yang memungkinkan individu maupun pemilik usaha di Indonesia untuk membuka dan mengelola toko online mereka secara mudah dan gratis, sekaligus memberikan pengalaman berbelanja online yang lebih aman dan nyaman. Jual beli online menjadi lebih menyenangkan. Punya toko online? Buka cabang nya di Tokopedia sekarang! Gratis!</span></p>
              </div>
              <div className="footer-menu-info">
                <div className="row">
                  <div className="col-sm-3">
                    <h5>Mister Oleh Oleh</h5>
                    <ul>
                      <li><Link to={"#"}>Tentang Kami</Link></li>
                      <li><Link to={"#"}>Kerja Sama</Link></li>
                      <li><Link to={"#"}>Kebijakan Privacy</Link></li>
                      <li><Link to={"#"}>Syarat Penggunaan</Link></li>
                      <li><Link to={"#"}>Kontak Kami</Link></li>
                    </ul>
                  </div>
                  <div className="col-sm-3">
                    <h5>Layanan Konsumen</h5>
                    <ul>
                      <li><i className="fa fa-calendar" />&nbsp;&nbsp;Senin - Jum'at</li>
                      <li><i className="fa fa-clock-o" />&nbsp;&nbsp;10:00 - 17:00</li>
                      <li><i className="fa fa-whatsapp" />&nbsp;&nbsp;WhatsApp: +62-812-8726-9030</li>
                      <li><i className="fa fa-envelope-o" />&nbsp;&nbsp;Email: info@misteroleholeh.com</li>
                    </ul>
                  </div>
                  <div className="col-sm-3">
                    <h5>Ikuti Kami</h5>
                    <div className="social-media-list">
                      <ul>
                        <li className="fb"><a href="#"><i className="fa fa-facebook"></i></a></li>
                        <li className="ig"><a href="#"><i className="fa fa-instagram"></i></a></li>
                        <li className="tw"><a href="#"><i className="fa fa-twitter"></i></a></li>
                      </ul>
                    </div>
                  </div>
                  <div className="col-sm-3">
                    <h5>Download Aplikasi</h5>
                    <img style={{ height: "35px", marginRight: "10px" }} src={DownloadAndroid} />
                    <img style={{ height: "35px" }} src={DownloadIos} />
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className="footer-container text-center copyright lightweight-text">
          <p>Copyright &copy; 2017-2018. Mister Oleh Oleh - Toko Online Oleh Oleh Indonesia.</p>
        </div>
        {showAppOverlay && <div className="app-overlay" />}
      </div>
    );
  }
}

const mapStateToProps = state => ({
  showAppOverlay: state.layout.showAppOverlay,
});

export default connect(mapStateToProps)(Footer);
