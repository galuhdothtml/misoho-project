/* eslint prop-types: 0 */
import React from "react";
import { connect } from "react-redux";
import onClickOutside from "react-onclickoutside";
import PropTypes from "prop-types";
import LoginDropdown from "./Dropdown";
import DefaultPhoto from "../../../images/avatar.png";
import { showAppOverlay } from "../../../actions/layoutActions";

const style = {
  container: {
    borderRadius: "0px",
    boxShadow: "none",
  },
};
class LoginMenu extends React.Component {
  static propTypes = {
    showOverlay: PropTypes.func,
  }

  static defaultProps = {
    showOverlay: () => { },
  }

  constructor(props) {
    super(props);

    this.state = {
      showDropdown: false,
    };
  }

  handleClickOutside = () => {
    const { showDropdown } = this.state;
    const { showOverlay } = this.props;

    if (showDropdown) {
      showOverlay(false);
      this.setState({ showDropdown: false });
    }
  }

  toggleDropdown = (e) => {
    if (e) {
      e.preventDefault();
    }

    const { showOverlay } = this.props;
    const { showDropdown } = this.state;

    showOverlay(!showDropdown);
    this.setState({ showDropdown: !showDropdown });
  }

  createLoginDropdown = () => {
    const { isLogin, accountName } = this.props;

    if (!isLogin) {
      return (<LoginDropdown />);
    }

    return (
      <ul style={style.container} className="dropdown-menu">
        <li><a href="/my-profile"><i className="mdi mdi-account-outline"></i>{accountName}</a></li>
        <li><a href="#"><i className="mdi mdi-format-list-bulleted"></i>Pesanan</a></li>
        <li><a href="#"><i className="mdi mdi-heart-outline"></i>Favorit</a></li>
        <li><a href="#"><i className="mdi mdi-map-marker-circle"></i>Alamat Pengiriman</a></li>
        <li role="separator" className="divider"></li>
        <li><a href="/auth/logout"><i className="mdi mdi-lock"></i>Keluar</a></li>
      </ul>
    );
  }

  profilePhotoComponent = () => {
    const { profilePhoto } = this.props;

    if (profilePhoto) {
      return <i className="profile-photo" style={{ backgroundImage: `url(${profilePhoto})`, borderRadius: "50%", border: "1px solid #FFFF" }} />;
    }

    return <i className="profile-photo" />;
  }

  render() {
    const { isLogin } = this.props;
    const { showDropdown } = this.state;
    return (
      <li className={`dropdown ${showDropdown ? "open" : ""}`}><a href="#" onClick={this.toggleDropdown}>
        <div className="icon-content special">
          { isLogin ? this.profilePhotoComponent() : <i className="profile-photo" /> }
        </div>
        <div className="title-content">
          <span>{isLogin ? "Akun Saya" : "Masuk"} <i className="fa fa-caret-down" /></span>
        </div>
      </a>
        {showDropdown && this.createLoginDropdown()}
      </li>
    );
  }
}

const mapDispatchToProps = dispatch => ({
  showOverlay: showStatus => dispatch(showAppOverlay(showStatus)),
});

export default connect(null, mapDispatchToProps)(onClickOutside(LoginMenu));
