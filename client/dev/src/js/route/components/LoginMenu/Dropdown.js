/* eslint prop-types: 0 */
import React from "react";
import {
  Link,
} from "react-router-dom";
import { getGoogleUrl, login } from "../../../data";
import InputText from "../../../components/form/InputText";
import InputPassword from "../../../components/form/InputPassword";
import InputCheckbox from "../../../components/form/InputCheckbox";
import * as Util from "../../../utils";

const style = {
  container: {
    width: "26em",
    padding: "22px",
    borderRadius: "0px",
    boxShadow: "none",
  },
};
class LoginDropdown extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      email: "",
      password: "",
      isRemembered: false,
      googleUrl: "",
    };
  }

  componentDidMount() {
    this.setupGoogleUrl();
  }

  setupGoogleUrl = async () => {
    const res = await getGoogleUrl();

    this.setState({ googleUrl: res.data });
  }

  loginGoogle = () => {
    const { googleUrl } = this.state;

    location.href = googleUrl;
  }

  createAccount = () => {
    location.href = "/auth/register";
  }

  changeValueHandler = (type, value) => {
    this.setState({ [type]: value });
  }

  doLogin = async (e) => {
    e.preventDefault();
    const { email, password } = this.state;

    const res = await login({ email, password });

    if (res.status) {
      Util.setToken(res.token);
      location.href = "/";
    } else {
      // eslint-disable-next-line no-restricted-globals
      location.href = "/auth/login?is_error=1";
    }
  }

  render() {
    const { email, password, isRemembered } = this.state;

    return (
      <div className="dropdown-menu" style={style.container}>
        <form onSubmit={this.doLogin}>
          <div className="form-group">
            <InputText
              placeholder="Email"
              changeEvent={(val) => { this.changeValueHandler("email", val); }}
              value={email}
            />
          </div>
          <div className="form-group">
            <InputPassword
              placeholder="Password"
              changeEvent={(val) => { this.changeValueHandler("password", val); }}
              value={password}
            />
          </div>
          <div className="form-group">
            <div className="row">
              <div className="col-sm-6">
                <InputCheckbox label={"Ingat Saya"} value={isRemembered} changeEvent={(val) => { this.changeValueHandler("isRemembered", val); }} />
              </div>
              <div className="col-sm-6 text-right">
                <Link to="#">Lupa Kata Sandi?</Link>
              </div>
            </div>
          </div>
          <button type="submit" className="btn btn-primary btn-block">Masuk</button>
        </form>
        <div className="divider-with-text">
          <hr />
          <div className="text-wrapper">
            <div className="text">atau</div>
          </div>
        </div>
        <button type="button" onClick={this.loginGoogle} className="btn btn-default btn-google btn-block mb-xs"><i className="fa fa-google-plus" />Masuk Dengan Google</button>
        <button type="button" onClick={this.createAccount} className="btn btn-default btn-block">Buat Akun</button>
      </div>
    );
  }
}

export default LoginDropdown;
