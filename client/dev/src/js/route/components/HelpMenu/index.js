/* eslint prop-types: 0 */
import React from "react";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import onClickOutside from "react-onclickoutside";
import { showAppOverlay } from "../../../actions/layoutActions";

const style = {
  container: {
    borderRadius: "0px",
    boxShadow: "none",
  },
};
class HelpMenu extends React.Component {
  static propTypes = {
    showOverlay: PropTypes.func,
  }

  static defaultProps = {
    showOverlay: () => { },
  }

  constructor(props) {
    super(props);

    this.state = {
      showDropdown: false,
    };
  }

  handleClickOutside = () => {
    const { showDropdown } = this.state;
    const { showOverlay } = this.props;

    if (showDropdown) {
      showOverlay(false);
      this.setState({ showDropdown: false });
    }
  }

  toggleDropdown = (e) => {
    if (e) {
      e.preventDefault();
    }

    const { showOverlay } = this.props;
    const { showDropdown } = this.state;

    showOverlay(!showDropdown);
    this.setState({ showDropdown: !showDropdown });
  }

  render() {
    const { showDropdown } = this.state;

    return (
      <li className={`dropdown ${showDropdown ? "open" : ""}`}><a href="#" onClick={this.toggleDropdown}>
        <div className="icon-content">
          <i className="fa fa-question-circle" />
        </div>
        <div className="title-content">
          <span>Bantuan <i className="fa fa-caret-down" /></span>
        </div>
      </a>
        {showDropdown && (
          <ul style={style.container} className="dropdown-menu">
            <li><a href="#">Hubungi Kami</a></li>
            <li><a href="#">Pusat Bantuan</a></li>
          </ul>
        )}
      </li>
    );
  }
}

const mapDispatchToProps = dispatch => ({
  showOverlay: showStatus => dispatch(showAppOverlay(showStatus)),
});

export default connect(null, mapDispatchToProps)(onClickOutside(HelpMenu));
