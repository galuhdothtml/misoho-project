/* eslint prop-types: 0 */
import React, { Fragment } from "react";
import { connect } from "react-redux";
import {
  Link,
} from "react-router-dom";
import Logo from "../../images/logo.png";
import SearchInput from "./SearchInput";
import SidePopup from "./SidePopup";
import LoginMenu from "./LoginMenu";
import HelpMenu from "./HelpMenu";
import CartMenu from "./CartMenu";
import * as Util from "../../utils";
import { fetchCarts, fetchMyAccount } from "../../actions/apiActions";
import { getProducts } from "../../data";
import { currency } from "../../utils";
import { showAppOverlay } from "../../actions/layoutActions";

class Header extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      searchValue: "",
      showLogin: false,
      accountName: "",
      carts: [],
      showPopup: false,
      profilePhoto: "",
      searchResults: [],
    };
  }

  componentWillMount = () => {
    const isAuthenticated = (Util.getToken() && Util.getToken().length > 0);

    if (isAuthenticated) {
      const { doFetchingMyAccount } = this.props;

      doFetchingMyAccount();
    }
  }

  componentWillReceiveProps = (nextProps) => {
    const { carts, myAccount } = this.props;

    if (carts !== nextProps.carts) {
      const { isFetching } = nextProps.carts;

      if (!isFetching) {
        const { data, showPopupAfterReceiveData } = nextProps.carts;

        const cartData = data.carts.map(x => ({
          id: x.id,
          id_product: x.id_product,
          name: x.product_name,
          price: parseFloat(x.product_price, 10),
          qty: x.qty,
          img: x.primary_img,
        }));

        let newState = { carts: cartData };

        if (showPopupAfterReceiveData) {
          newState = {
            carts: cartData,
            showPopup: true,
          };
        }

        this.setState(newState);
      }
    }

    if (myAccount !== nextProps.myAccount) {
      const { isFetching } = nextProps.myAccount;

      if (!isFetching) {
        const { data } = nextProps.myAccount;
        this.setupAccount(data);
      }
    }
  }

  setupAccount = async (data) => {
    const isLogin = !!Util.getToken();

    if (isLogin) {
      this.setState({ accountName: data.name, profilePhoto: data.profile_photo });
      this.setupCarts();
    }
  }

  setupCarts = async () => {
    const { doFetchingCarts } = this.props;

    doFetchingCarts(false);
  }

  changeSearchValue = (val) => {
    this.setState({ searchValue: val });

    clearTimeout(this.filterIdle);
    this.filterIdle = setTimeout(() => {
      this.setState({ searchValue: val }, () => {
        this.fetchProducts();
      });
    }, 500);
  }

  fetchProducts = async () => {
    const { showOverlay } = this.props;
    const { searchValue } = this.state;
    let searchResults = [];
    const payload = {
      limit: 7,
      search: searchValue,
    };

    if (String(searchValue).length > 0) {
      const res = await getProducts(payload);
      if (res.status) {
        searchResults = res.data;
      }
    }
    showOverlay((searchResults.length > 0));
    this.setState({ searchResults });
  }

  clickCartHandler = () => {
    this.setState({ showPopup: true });
  }

  hidePopup = () => {
    this.setState({ showPopup: false });
  }

  checkoutHandler = () => {
    // eslint-disable-next-line no-restricted-globals
    location.href = "/checkout";
  }

  searchResultsComponent = (searchResults) => {
    const retval = [];

    if (searchResults.length > 0) {
      searchResults.forEach((x, i) => {
        retval.push(
          <div key={x.id} className="search-result-row" style={(i === (searchResults.length - 1)) ? { border: "0px" } : {}}>
            <a href={`/product/${x.id}/${x.slug}`}>
              <div className="row">
                <div className="col-sm-8">{this.boldSpecificWord(x.name)}</div>
                <div className="col-sm-4 text-right">Rp&nbsp;<span>{currency(x.price)}</span></div>
              </div>
            </a>
          </div>,
        );
      });
    }

    return (
      <div className="search-result-wrapper lightweight-text">
        {retval}
      </div>
    );
  }

  boldSpecificWord = (val) => {
    const { searchValue } = this.state;

    // const lowercaseVal = val.toLowerCase();
    // const startIndex = lowercaseVal.indexOf(searchValue);
    // const substringVal = val.substring(startIndex, (startIndex + String(searchValue).length));

    // console.log(`DEBUG-${val}: `, val.split(substringVal));

    // return val.replace(substringVal, `<b>${substringVal}<b>`);

    const parts = val.split(new RegExp(`(${searchValue})`, "gi"));

    return <span> { parts.map((part, i) => <span key={i} className={part.toLowerCase() === searchValue.toLowerCase() ? "bold-text" : "" }>
            { part }
        </span>)
    } </span>;
  }

  render() {
    const {
      searchValue, accountName, carts, showPopup, profilePhoto,
      searchResults,
    } = this.state;
    const isLogin = !!Util.getToken();

    return (
      <Fragment>
        <nav className="navbar navbar-default navbar-custom">
          <div className="container-fluid">
            <div className="navbar-header">
              <a className="navbar-brand" href="/">
                <img alt="Brand" src={Logo} />
              </a>
            </div>
            <div className="collapse navbar-collapse">
              <form className="navbar-form navbar-left">
                <div style={{ position: "relative" }}>
                  <SearchInput value={searchValue} changeEvent={this.changeSearchValue} />
                  { searchResults.length > 0 && this.searchResultsComponent(searchResults) }
                </div>
              </form>
              <ul className="nav navbar-nav navbar-right">
                <li><a href="#">
                  <div className="icon-content">
                    <i className="fa fa-check-square-o" />
                  </div>
                  <div className="title-content">
                    <span>Konfirmasi Pembayaran</span>
                  </div>
                </a></li>
                <li><a href="#">
                  <div className="icon-content">
                    <i className="fa fa-truck" />
                  </div>
                  <div className="title-content">
                    <span>Cek Pesanan</span>
                  </div>
                </a></li>
                <HelpMenu />
                { isLogin && <CartMenu carts={carts} clickEvent={this.clickCartHandler} /> }
                <LoginMenu isLogin={isLogin} accountName={accountName} profilePhoto={profilePhoto} />
              </ul>
            </div>
          </div>
        </nav>
        {showPopup && <SidePopup
          onHide={this.hidePopup}
          title="Keranjang Belanja"
          saveEvent={this.checkoutHandler}
          data={carts}
        />}
      </Fragment>
    );
  }
}

const mapStateToProps = state => ({
  carts: state.apiRedux.carts,
  myAccount: state.apiRedux.myAccount,
});

const mapDispatchToProps = dispatch => ({
  doFetchingCarts: showPopupAfterReceiveData => dispatch(fetchCarts(showPopupAfterReceiveData)),
  doFetchingMyAccount: () => dispatch(fetchMyAccount()),
  showOverlay: showStatus => dispatch(showAppOverlay(showStatus)),
});

export default connect(mapStateToProps, mapDispatchToProps)(Header);
