/* eslint prop-types: 0 */
import React, { Fragment } from "react"; // eslint-disable-line no-unused-vars
import {
  BrowserRouter as Router, Route, Switch, Redirect,
} from "react-router-dom";
import Header from "./components/FullHeader";
import Footer from "./components/Footer";
import FloatChat from "./components/FloatChat";
import asyncRoute from "./components/AsyncRoute";
import * as Util from "../utils";

require("../sass/styles.scss");

const Logout = () => {
  Util.removeToken();
  location.href = "/"; // eslint-disable-line no-restricted-globals

  return (<div />);
};
const isAuthenticated = (Util.getToken() && Util.getToken().length > 0);
// eslint-disable-next-line react/prop-types
const PrivateRoute = ({ component: Component, ...rest }) => (
  <Route
    {...rest}
    render={props => (isAuthenticated ? (
        <Component {...props} />
    ) : (
        <Redirect
          to={{
            pathname: "/auth/login",
            state: { from: props.location }, // eslint-disable-line react/prop-types
          }}
        />
    ))
    }
  />
);
class App extends React.Component {
  render = () => (
    <Router>
      <Switch>
        <Route exact path="/auth/login" component={asyncRoute(() => import("../components/Login"))} />
        <Route exact path="/auth/register" component={asyncRoute(() => import("../components/Register"))} />
        <Route exact path="/auth/logout" component={Logout} />
        <Route exact path="/google-redirect" component={asyncRoute(() => import("../components/GoogleRedirect"))} />
        <PrivateRoute exact path="/checkout" component={asyncRoute(() => import("../components/Checkout"))} />
        <Route path="/">
          <Fragment>
            <Header />
            <div style={{ paddingTop: "104px" }}>
              <Route exact path="/" component={asyncRoute(() => import("../components/Home"))} />
              <Route path="/category/:id/:slug" component={asyncRoute(() => import("../components/Category"))} />
              <Route path="/product/:id/:slug" component={asyncRoute(() => import("../components/Product"))} />
              <Route path="/carts" component={asyncRoute(() => import("../components/Cart"))} />
              <PrivateRoute path="/my-profile/edit-password" component={asyncRoute(() => import("../components/Auth/ProfilePassword"))} />
              <PrivateRoute exact path="/my-profile" component={asyncRoute(() => import("../components/Auth/Profile"))} />
              <PrivateRoute path="/my-orders/detail/:id" component={asyncRoute(() => import("../components/Auth/Orders/OrderDetail"))} />
              <PrivateRoute exact path="/my-orders" component={asyncRoute(() => import("../components/Auth/Orders"))} />
              <PrivateRoute path="/my-addresses/detail/:id" component={asyncRoute(() => import("../components/Auth/Address/AddressDetail"))} />
              <PrivateRoute exact path="/my-addresses" component={asyncRoute(() => import("../components/Auth/Address"))} />
              <PrivateRoute path="/my-reviews" component={asyncRoute(() => import("../components/Auth/Review"))} />
              <PrivateRoute path="/my-vouchers" component={asyncRoute(() => import("../components/Auth/Voucher"))} />
              <PrivateRoute path="/my-wishlists" component={asyncRoute(() => import("../components/Auth/Wishlist"))} />
            </div>
            <FloatChat />
            <Footer />
          </Fragment>
        </Route>
      </Switch>
    </Router>
  )
}

export default App;
