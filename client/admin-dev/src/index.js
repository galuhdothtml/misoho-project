/* eslint prop-types: 0 */
import React from "react"; // eslint-disable-line no-unused-vars
import ReactDOM from "react-dom";
import thunkMiddleware from "redux-thunk";
import { createLogger } from "redux-logger";
import { Provider } from "react-redux"; // eslint-disable-line no-unused-vars
import { createStore, applyMiddleware } from "redux";
import reducers from "./js/reducers";
import App from "./js/route"; // eslint-disable-line no-unused-vars

const loggerMiddleware = createLogger();

const store = createStore(
  reducers,
  applyMiddleware(
    thunkMiddleware, // lets us dispatch() functions
    loggerMiddleware, // neat middleware that logs actions
  ),
);

const wrapper = document.getElementById("root"); // eslint-disable-line no-undef
wrapper // eslint-disable-line no-unused-expressions
  ? ReactDOM.render(
      <Provider store={store}>
        <App />
      </Provider>,
      wrapper,
  )
  : false;
