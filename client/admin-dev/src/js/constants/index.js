/* eslint prop-types: 0 */

export const ActionTypes = {
  FETCHING_APP_MENU: "FETCHING_APP_MENU",
  GET_APP_MENU: "GET_APP_MENU",
  GET_APP_MENU_FAILED: "GET_APP_MENU_FAILED",
  UPDATE_DATE_RANGE: "UPDATE_DATE_RANGE",

};
export const API_BASE_URL = "http://localhost:3300";
