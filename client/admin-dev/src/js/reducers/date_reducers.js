/* eslint prop-types: 0 */
import { ActionTypes } from "../constants";

const defaultValue = {
  start: moment().startOf("month").format("YYYY-MM-DD"),
  end: moment().endOf("month").format("YYYY-MM-DD"),
};
const dateRange = (state = defaultValue, action) => {
  switch (action.type) {
    case ActionTypes.UPDATE_DATE_RANGE:
      return action.data;
    default:
      return state;
  }
};

export {
  dateRange,
};
