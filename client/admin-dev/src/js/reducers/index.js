/* eslint prop-types: 0 */
import { combineReducers } from "redux";
import {
  appMenu,
} from "./api_reducers";
import {
  dateRange,
} from "./date_reducers";

export default combineReducers({
  appMenu,
  dateRange,
});
