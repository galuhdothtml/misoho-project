/* eslint prop-types: 0 */
import { ActionTypes } from "../constants";

const appMenu = (state = "", action) => {
  switch (action.type) {
    case ActionTypes.FETCHING_APP_MENU:
      return { isFetching: true };
    case ActionTypes.GET_APP_MENU:
      return { isFetching: false, data: action.data, isSuccess: true };
    case ActionTypes.GET_APP_MENU_FAILED:
      return { isFetching: false, error: action.error, isSuccess: false };
    default:
      return state;
  }
};

export {
  appMenu,
};
