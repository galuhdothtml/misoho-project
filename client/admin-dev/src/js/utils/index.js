/* eslint prop-types: 0 */
import _ from "lodash";
import numeral from "numeral";
import { API_BASE_URL } from "../constants";

const currency = (val) => {
  const retval = numeral(val).format("0,0");

  return retval;
};

const getToken = () => (localStorage.getItem("usertoken"));

const setToken = (_token) => {
  localStorage.setItem("usertoken", _token); // eslint-disable-line no-undef
};

const removeToken = () => {
  localStorage.removeItem("usertoken"); // eslint-disable-line no-undef
};

const findHeaderMenuIdByLink = (currentLink, menu) => {
  const exist = menu.find((x) => {
    const found = x.content.find((fc) => {
      if (fc.link && fc.link === currentLink) {
        return true;
      } if (fc.content) {
        return _.find(fc.content, ["link", currentLink]);
      }

      return false;
    });

    return found;
  });

  if (exist) {
    const { id } = exist;

    return String(id);
  }

  return "";
};

const findSideMenuChildrenId = (sideMenu, currentSideMenuId, currentLink) => {
  let found = _.find(sideMenu, ["id", currentSideMenuId]);

  if (found) {
    const { content } = found;

    if (content) {
      const sideMenuChildren = content;
      found = _.find(sideMenuChildren, ["link", currentLink]);

      if (found) {
        return found.id;
      }
    }
  }

  return "";
};

const findSideMenuIdByLink = (currentLink, rawMenu) => {
  const found = rawMenu.find((x) => {
    if (x.content) {
      return _.find(x.content, ["link", currentLink]);
    }

    return x.link === currentLink;
  });
  let retval = {
    noContent: "",
    withContent: "",
  };

  if (found) {
    const { content } = found;
    if (content) {
      retval = {
        noContent: "",
        withContent: found.id,
      };
    } else {
      retval = {
        noContent: found.id,
        withContent: "",
      };
    }
  }

  return retval;
};

const createSideMenuData = (headerMenuId, rawMenu) => {
  const found = _.find(rawMenu, ["id", Number(headerMenuId)]);
  const sideMenu = found ? found.content : [];

  return sideMenu;
};

const createAjax = (url, extendParam) => ({
  headers: {
    Authorization: `Bearer ${getToken()}`,
  },
  url: `${API_BASE_URL}${url}`,
  dataType: "json",
  delay: 250,
  data: (params) => {
    const query = {
      filterText: params.term,
      page: 1,
      limit: 5,
    };

    if (extendParam) {
      Object.assign(query, extendParam);
    }

    return query;
  },
  processResults: res => ({
    results: res.data.map(x => ({ id: x.id, text: x.name })),
  }),
  cache: true,
});

const validateEmail = email => /\S+@\S+\.\S+/.test(email);

const ishasProperty = (obj, key) => Object.hasOwnProperty.call(obj, key);

const createPathPreview = (rawPath) => {
  if (rawPath) {
    const path = rawPath.replace("public/", `${API_BASE_URL}/`);

    return path;
  }

  return rawPath;
};

const Util = {
  getToken,
  setToken,
  removeToken,
  findHeaderMenuIdByLink,
  findSideMenuChildrenId,
  findSideMenuIdByLink,
  createSideMenuData,
  createAjax,
  currency,
  validateEmail,
  createPathPreview,
  ishasProperty,
};

export default Util;
