/* eslint prop-types: 0 */
import { ActionTypes } from "../constants";

const updatePeriode = data => (dispatch) => {
  dispatch({ type: ActionTypes.UPDATE_DATE_RANGE, data });
};

export {
  updatePeriode,
};
