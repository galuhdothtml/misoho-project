/* eslint prop-types: 0 */
import {
  getAppMenu,
} from "../data";
import { ActionTypes } from "../constants";

const fetchAppMenu = () => (dispatch) => {
  dispatch({ type: ActionTypes.FETCHING_APP_MENU });

  getAppMenu().then((res) => {
    dispatch({ type: ActionTypes.GET_APP_MENU, data: res.data });
  }, (err) => {
    dispatch({ type: ActionTypes.GET_APP_MENU_FAILED, error: err });
  });
};

export {
  // eslint-disable-next-line import/prefer-default-export
  fetchAppMenu,
};
