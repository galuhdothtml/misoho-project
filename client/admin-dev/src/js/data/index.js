/* eslint prop-types: 0 */
import ApiClient from "./api_client";

const getAppMenu = payload => ApiClient.fetch("/api/appmenu", payload);
const login = payload => ApiClient.fetch("/api/login/employee", payload, "POST");
const uploadFile = withFile => ApiClient.fetch("/api/uploadFile", {}, "POST", withFile);
const uploadFiles = withFiles => ApiClient.fetch("/api/uploadFiles", {}, "POST", withFiles);
const getProducts = payload => ApiClient.fetch("/api/backoffice-products", payload);
const getCategories = payload => ApiClient.fetch("/api/backoffice-categories", payload);
const getCategory = id => ApiClient.fetch(`/api/category/${id}`);
const createCategory = payload => ApiClient.fetch("/api/category", payload, "POST");
const updateCategory = (payload, withFile) => ApiClient.fetch("/api/category", payload, "PUT", withFile);
const deleteCategory = payload => ApiClient.fetch("/api/category", payload, "DELETE");
const bulkDeleteCategory = payload => ApiClient.fetch("/api/category/bulk", payload, "DELETE");
const getCustomers = payload => ApiClient.fetch("/api/customers", payload);
const getCustomer = id => ApiClient.fetch(`/api/customer/${id}`);
const createCustomer = payload => ApiClient.fetch("/api/customer", payload, "POST");
const updateCustomer = payload => ApiClient.fetch("/api/customer", payload, "PUT");
const deleteCustomer = payload => ApiClient.fetch("/api/customer", payload, "DELETE");
const bulkDeleteCustomer = payload => ApiClient.fetch("/api/customer/bulk", payload, "DELETE");
const updateCustomerPassword = payload => ApiClient.fetch("/api/update_customer_password", payload, "POST");
const getProvinces = payload => ApiClient.fetch("/api/v2/provinces", payload);
const getCities = payload => ApiClient.fetch("/api/v2/cities", payload);
const getProduct = id => ApiClient.fetch(`/api/product/${id}`);
const getProductImages = id => ApiClient.fetch(`/api/product_images/${id}`);
const createProduct = payload => ApiClient.fetch("/api/product", payload, "POST");
const updateProduct = payload => ApiClient.fetch("/api/product", payload, "PUT");
const deleteProduct = payload => ApiClient.fetch("/api/product", payload, "DELETE");
const bulkDeleteProduct = payload => ApiClient.fetch("/api/product/bulk", payload, "DELETE");
const getAddresses = payload => ApiClient.fetch("/api/addresses", payload);
const getAddress = id => ApiClient.fetch(`/api/address/${id}`);
const createAddress = payload => ApiClient.fetch("/api/address", payload, "POST");
const updateAddress = payload => ApiClient.fetch("/api/address", payload, "PUT");
const deleteAddress = payload => ApiClient.fetch("/api/address", payload, "DELETE");
const bulkDeleteAddress = payload => ApiClient.fetch("/api/address/bulk", payload, "DELETE");
const getOrders = payload => ApiClient.fetch("/api/orders", payload);
const getOrderDetail = id => ApiClient.fetch(`/api/order_detail/${id}`);
const getPaymentConfirmations = payload => ApiClient.fetch("/api/payment_confirmations", payload);
const acceptPayment = payload => ApiClient.fetch("/api/accept_payment", payload, "POST");
const rejectPayment = payload => ApiClient.fetch("/api/reject_payment", payload, "POST");
const processOrder = payload => ApiClient.fetch("/api/process_order", payload, "POST");
const shipOrder = payload => ApiClient.fetch("/api/ship_order", payload, "POST");
const getPostalCode = idPostalCode => ApiClient.fetch("/api/urbans", { page: "1", limit: "5", orId: idPostalCode });

export {
  getAppMenu,
  login,
  uploadFile,
  uploadFiles,
  getCustomers,
  getCustomer,
  createCustomer,
  updateCustomer,
  deleteCustomer,
  bulkDeleteCustomer,
  getProducts,
  getCategories,
  getCategory,
  createCategory,
  updateCategory,
  deleteCategory,
  bulkDeleteCategory,
  getProvinces,
  getCities,
  getProduct,
  createProduct,
  updateProduct,
  deleteProduct,
  bulkDeleteProduct,
  getProductImages,
  updateCustomerPassword,
  getAddresses,
  getAddress,
  createAddress,
  updateAddress,
  deleteAddress,
  bulkDeleteAddress,
  getOrders,
  getPaymentConfirmations,
  getOrderDetail,
  acceptPayment,
  rejectPayment,
  processOrder,
  shipOrder,
  getPostalCode,
};
