/* eslint prop-types: 0 */
import request from "superagent";
import { API_BASE_URL } from "../constants";
import Util from "../utils";

const fetch = (url, param = null, method = "GET", withFiles = null) => new Promise((resolve, reject) => {
  let req = null;

  if (method === "GET") {
    req = request
      .get(`${API_BASE_URL}${url}`);
  } else if (method === "POST") {
    req = request
      .post(`${API_BASE_URL}${url}`);
  } else if (method === "PUT") {
    req = request
      .put(`${API_BASE_URL}${url}`);
  } else if (method === "DELETE") {
    req = request
      .delete(`${API_BASE_URL}${url}`);
  }

  if (Util.getToken() && Util.getToken().trim().length > 0) {
    req = req.set("authorization", `Bearer ${Util.getToken()}`);
  }

  if (param) {
    if (method === "GET") {
      req = req.query(param);
    } else if (withFiles) {
      withFiles.forEach((x) => {
        req = req.attach("photos", x);
      });
      req = req.field({ data: JSON.stringify(param) });
    } else {
      req = req.send(param);
    }
  }

  req.end((err, res) => {
    if (err) {
      reject(err);
    }

    if (Object.prototype.hasOwnProperty.call(res.body, "token")) {
      Util.setToken(res.body.token);
    }

    resolve(res.body);
  });
});

const ApiClient = {
  fetch,
};

export default ApiClient;
