/* eslint prop-types: 0 */
import React from "react";
import CoreHoC from "./CoreHoC";

class OrderHistory extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      title: "Histori Pesanan",
    };
  }


  render() {
    const { title } = this.state;

    return (
      <div>
        <h1>{title}</h1>
      </div>
    );
  }
}

export default CoreHoC(OrderHistory);
