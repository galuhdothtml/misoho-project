/* eslint prop-types: 0 */
import React from "react";
import InputText from "./form/InputText";
import InputCheckbox from "./form/InputCheckbox";
import {
  login,
} from "../data";
import Util from "../utils";

const mt8 = {
  marginTop: "7px",
};
class Login extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      email: "superadmin",
      password: "Rah4sia123",
      rememberPassword: false,
    };
  }

  changeValueHandler = (type, val) => {
    this.setState({ [type]: val });
  }

  login = async (e) => {
    const { email, password } = this.state;
    e.preventDefault();

    const payload = {
      username: email,
      password,
    };

    const res = await login(payload);

    if (res.status) {
      Util.setToken(res.token);
      location.href = "/dashboard";
    }
  }

  render() {
    const { email, password, rememberPassword } = this.state;

    return (
      <div className="login-page">
        <div className="login-box">
          <div className="login-logo">
            <a href="#"><b>Admin</b>LTE</a>
          </div>
          <div className="login-box-body">
            <form onSubmit={this.login}>
              <p className="login-box-msg">Sign in too start your session</p>
              <div className="row mb-md">
                <div className="col-xs-12">
                  <InputText
                    placeholder="Email"
                    changeEvent={val => this.changeValueHandler("email", val)}
                    value={email}
                  />
                </div>
              </div>
              <div className="row mb-md">
                <div className="col-xs-12">
                  <InputText
                    placeholder="Password"
                    changeEvent={val => this.changeValueHandler("password", val)}
                    value={password}
                  />
                </div>
              </div>
              <div className="row">
                <div className="col-xs-8">
                  <div style={mt8}>
                    <InputCheckbox
                      checked={rememberPassword}
                      changeEvent={val => this.changeValueHandler("rememberPassword", val)}
                    />
                    <span>Ingat Login</span>
                  </div>
                </div>
                <div className="col-xs-4">
                  <button
                    type="submit"
                    className="btn btn-primary btn-block btn-flat">Masuk</button>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    );
  }
}

export default Login;
