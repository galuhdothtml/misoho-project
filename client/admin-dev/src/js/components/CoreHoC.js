/* eslint prop-types: 0 */
import React from "react";
import { connect } from "react-redux";
import { ToastContainer, toast } from "react-toastify";
import { Link } from "react-router-dom";
import "react-toastify/dist/ReactToastify.css";
import DaterangePicker from "./form/DaterangePicker";
import { updatePeriode } from "../actions/date_actions";

const style = {
  contentWrapper: {
    minHeight: `${(window.innerHeight - 52)}px`,
  },
};
const Breadcrumb = ({ data }) => {
  return (
    <ol className="breadcrumb">
      { data.map(x => (<li key={x.id}><a href={x.link ? `/${x.link}` : "#"}>{x.name}</a></li>)) }
    </ol>
  );
};
const CoreHoC = (WrappedComponent) => {
  class Wrapper extends React.Component {
    constructor(props) {
      super(props);

      this.state = {
        title: "Halaman Dashboard",
        buttons: [],
        menuData: [],
        breadcrumbData: [],
        daterange: {
          start: moment().startOf("month").format("YYYY-MM-DD"),
          end: moment().endOf("month").format("YYYY-MM-DD"),
        },
        changeCalendarEvent: null,
      };
    }

    componentWillMount = () => {
      const { appMenu, dateRange, location } = this.props;

      this.setState({ daterange: dateRange, menuData: appMenu.data ? appMenu.data : [] }, () => {
        this.setupBreadcrumbData(location.pathname);
      });
    }

    componentWillReceiveProps = (nextProps) => {
      const { appMenu } = this.props;

      if (appMenu !== nextProps.appMenu) {
        const result = nextProps.appMenu;
        if (!result.isFetching && result.isSuccess) {
          this.setState({ menuData: result.data }, () => {
            const { location } = nextProps;

            this.setupBreadcrumbData(location.pathname);
          });
        }
      }
    }

    setupBreadcrumbData = (currentPath, callback) => {
      const { menuData } = this.state;
      let i = 0;
      let done = false;
      const result = [{ id: "1", name: "Sistem" }];
      let val = currentPath.replace("/", "");
      val = val.split("/");

      if (val.length > 0) {
        ([val] = val);
      }

      while (!done && i < menuData.length && menuData.length > 0) {
        if (Array.isArray(menuData[i].content)) {
          const exist = menuData[i].content.find(x => (x.link === val));

          if (exist) {
            const parentTitle = menuData[i].title;
            result.push({ id: "2", name: parentTitle });
            result.push({ id: "3", name: exist.title, link: exist.link });

            done = true;
          }
        } else {
          const exist = (menuData[i].link === val);

          if (exist) {
            result.push({ id: "2", name: menuData[i].title, link: menuData[i].link });
            done = true;
          }
        }

        i += 1;
      }

      this.setState({ breadcrumbData: result }, () => {
        if (callback) {
          callback();
        }
      });
    }

    assignCalendar = (func) => {
      this.setState({ changeCalendarEvent: func });
    }

    assignButtons = (buttons) => {
      this.setState({ buttons });
    }

    showNotification = ({ type, msg }) => {
      if (type === "success") {
        toast.success(msg);
      } else if (type === "error") {
        toast.error(msg);
      }
    }

    changeDateHandler = (val) => {
      const { changeCalendarEvent } = this.state;
      const { doUpdatingPeriode } = this.props;

      doUpdatingPeriode(val);
      changeCalendarEvent(val);
      this.setState({ daterange: val });
    }

    render() {
      const {
        buttons, breadcrumbData, daterange, changeCalendarEvent,
      } = this.state;

      return (
        <div className="content-wrapper" style={style.contentWrapper}>
          <section className="content-header">
            <div className="component-wrapper">
              <div className="item">
                { changeCalendarEvent && (
                  <div className="date-range-wrapper">
                    <DaterangePicker changeEvent={this.changeDateHandler} id="periode-data" value={daterange} />
                  </div>
                )}
                {buttons.map(x => (
                <button key={x.id} className="btn btn-primary" type="button" onClick={x.clickEvent}><i className={x.icon} /> {x.title}</button>
                ))}
              </div>
            </div>
            <Breadcrumb data={breadcrumbData} />
          </section>
          <section className="content">
            <WrappedComponent {...this.props} showNotification={this.showNotification} assignButtons={this.assignButtons} assignCalendar={this.assignCalendar} calendar={daterange} />
          </section>
          <ToastContainer autoClose={3000} />
        </div>
      );
    }
  }

  const mapStateToProps = state => ({
    appMenu: state.appMenu,
    dateRange: state.dateRange,
  });
  const mapDispatchToProps = dispatch => ({
    doUpdatingPeriode: payload => dispatch(updatePeriode(payload)),
  });
  return connect(mapStateToProps, mapDispatchToProps)(Wrapper);
};

export default CoreHoC;
