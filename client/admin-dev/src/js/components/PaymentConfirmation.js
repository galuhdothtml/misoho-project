import React from "react";
import PropTypes from "prop-types";
import CoreHoC from "./CoreHoC";
import Table from "./form/Table";
import {
  getPaymentConfirmations,
  acceptPayment,
  rejectPayment,
} from "../data";
import InputText from "./form/InputText";
import Select from "./form/Select";
import ModalPopup from "./form/ModalPopup";

// eslint-disable-next-line react/prop-types
const OptionButton = ({ value, clickEvent }) => (
  <div className="text-center table-option">
    <div className="btn-group">
      <button
        onClick={() => clickEvent("accept", value)}
        type="button"
        className="btn btn-default btn-sm text-center table-option"><i className="fa fa-edit" /> Terima</button>
      <button
        onClick={() => clickEvent("reject", value)}
        type="button"
        className="btn btn-default btn-sm text-center table-option"><i className="fa fa-trash-o" /> Tolak</button>
    </div>
  </div>
);
const columns = clickEvent => [
  {
    Header: "Kode",
    accessor: "id",
    resizable: false,
    width: 80,
  },
  {
    Header: "Nomor Pesanan",
    accessor: "reference_number",
    resizable: false,
  },
  {
    Header: "Metode Konfirmasi",
    accessor: "status_text",
    resizable: false,
  },
  {
    Header: "Tanggal Transfer",
    accessor: "transfer_date",
    resizable: false,
  },
  {
    Header: "Tanggal Dibuat",
    accessor: "createdAt",
    resizable: false,
  },
  {
    Header: "",
    accessor: "edit_column",
    sortable: false,
    resizable: false,
    width: 200,
    className: "option-column",
    // eslint-disable-next-line react/prop-types
    Cell: ({ value }) => (
      <OptionButton value={value} clickEvent={clickEvent} />
    ),
  },
];
class Product extends React.Component {
  static propTypes = {
    assignCalendar: PropTypes.func.isRequired,
    assignButtons: PropTypes.func.isRequired,
    showNotification: PropTypes.func,
  }

  static defaultProps = {
    showNotification: () => { },
  }

  constructor(props) {
    super(props);

    this.state = {
      data: [],
      filterText: "",
      limitValue: "5",
      limitData: [
        { id: "5", name: "5" },
        { id: "10", name: "10" },
        { id: "20", name: "20" },
        { id: "50", name: "50" },
        { id: "100", name: "100" },
      ],
      isModalShowing: false,
      confirmType: "",
      confirmId: "",
    };
  }

  fetchData = async (state) => {
    const { filterText, limitValue } = this.state;
    const payload = {
      page: state.page + 1,
      limit: limitValue,
    };

    if (state.sorted) {
      Object.assign(payload, {
        sorted: JSON.stringify(state.sorted),
      });
    }

    if (filterText && filterText.length > 0) {
      Object.assign(payload, {
        filterText,
      });
    }

    const res = await getPaymentConfirmations(payload);

    if (res.status) {
      const newData = res.data.map(x => (Object.assign({}, x, {
        edit_column: x.id,
        status_text: x.is_auto ? "Otomatis" : "Manual",
        // eslint-disable-next-line no-undef
        transfer_date: moment(x.transfer_date).format("DD MMM YYYY HH:mm:ss"),
        // eslint-disable-next-line no-undef
        createdAt: moment(x.createdAt).format("DD MMM YYYY HH:mm:ss"),
      })));

      return { data: newData, pageTotal: res.page_total };
    }

    return { data: [], pageTotal: 0 };
  }

  changeFilterTextHandler = (val) => {
    this.setState({ filterText: val });

    clearTimeout(this.filterIdle);
    this.filterIdle = setTimeout(() => {
      this.table.resetCurrentPage(() => {
        this.setState({ filterText: val }, () => {
          this.table.refreshData();
        });
      });
    }, 500);
  }

  changeLimitValueHandler = (val) => {
    this.table.resetCurrentPage(() => {
      this.setState({ limitValue: val }, () => {
        this.table.refreshData();
      });
    });
  }

  clickValueHandler = (type, id) => {
    this.setState({
      confirmType: type,
      confirmId: id,
    }, () => {
      this.showModal();
    });
  }

  showModal = () => {
    this.setState({ isModalShowing: true });
  }

  hideModal = () => {
    this.setState({ isModalShowing: false });
  }

  confirmPaymentHandler = async (callback) => {
    const { confirmType, confirmId } = this.state;
    const payload = { id: confirmId };

    if (confirmType === "accept") {
      const res = await acceptPayment(payload);
      if (res.status) {
        this.hideModal();
        this.table.refreshData();
      } else {
        callback();
      }
    } else {
      const res = await rejectPayment(payload);
      if (res.status) {
        this.hideModal();
        this.table.refreshData();
      } else {
        callback();
      }
    }
  }

  render = () => {
    const {
      filterText,
      limitValue,
      limitData,
      isModalShowing,
      confirmType,
    } = this.state;

    return (
      <div>
        <div className="box box-solid">
          <div className="box-header with-border">
            <div className="row mb-0">
              <div className="col-sm-8">
                <h3 className="box-title with-title">Konfirmasi Pembayaran</h3>
              </div>
              <div className="col-sm-4">
                <div className="row">
                  <div className="col-sm-9">
                    <InputText
                      placeholder="Pencarian ..."
                      changeEvent={this.changeFilterTextHandler}
                      value={filterText}
                    />
                  </div>
                  <div className="col-sm-3 pl-0">
                    <Select
                      data={limitData}
                      value={limitValue}
                      changeEvent={this.changeLimitValueHandler}
                    />
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div className="box-body">
            <Table
              ref={(c) => { this.table = c; }}
              columns={columns(this.clickValueHandler)}
              onFetch={this.fetchData}
              showPagination={true}
            />
          </div>
        </div>
        { isModalShowing && (
            <ModalPopup
              width={340}
              title={confirmType === "accept" ? "Terima Pembayaran" : "Tolak Pembayaran"}
              hideModal={this.hideModal}
              saveHandler={this.confirmPaymentHandler}>
              <div>
                { confirmType === "accept" ? "Anda yakin ingin menerima pembayaran ?" : "Anda yakin ingin menolak pembayaran ?" }
              </div>
            </ModalPopup>
        )}
      </div>
    );
  }
}

export default CoreHoC(Product);
