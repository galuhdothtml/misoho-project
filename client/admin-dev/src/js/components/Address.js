import React from "react";
import PropTypes from "prop-types";
import moment from "moment";
import update from "immutability-helper";
import CoreHoC from "./CoreHoC";
import RawTable from "./form/Table";
import {
  getAddresses,
  getAddress,
  createAddress,
  updateAddress,
  deleteAddress,
  bulkDeleteAddress,
  getPostalCode,
} from "../data";
import Util from "../utils";
import SidePopup from "./form/SidePopup";
import withEditDelete from "./form/Table/withEditDelete";
import withCheckbox from "./form/Table/withCheckbox";
import InputText from "./form/InputText";
import TextArea from "./form/TextArea";
import Select from "./form/Select";
import Select2 from "./form/Select2";

const errorContent = (type, val) => {
  let retval = null;

  if (type === "id_customer") {
    if (String(val).trim().length === 0) {
      retval = "* Nama pelanggan wajib diisi";
    }
  } else if (type === "alias") {
    if (String(val).trim().length === 0) {
      retval = "* Nama alamat wajib diisi";
    }
  } else if (type === "receiver_name") {
    if (String(val).trim().length === 0) {
      retval = "* Nama penerima wajib diisi";
    }
  } else if (type === "address") {
    if (String(val).trim().length === 0) {
      retval = "* Alamat wajib diisi";
    }
  } else if (type === "phone") {
    if (String(val).trim().length === 0) {
      retval = "* Telepon wajib diisi";
    }
  } else if (type === "cityId") {
    if (String(val).trim().length === 0) {
      retval = "* Kota/Kabupaten wajib diisi";
    }
  } else if (type === "provinceId") {
    if (String(val).trim().length === 0) {
      retval = "* Provinsi wajib diisi";
    }
  } else if (type === "sub_district") {
    if (String(val).trim().length === 0) {
      retval = "* Kecamatan wajib diisi";
    }
  } else if (type === "urban") {
    if (String(val).trim().length === 0) {
      retval = "* Kelurahan wajib diisi";
    }
  }

  return retval;
};
const Table = withCheckbox(withEditDelete(RawTable));
const columns = [
  {
    Header: "Kode",
    accessor: "id",
    resizable: false,
    width: 80,
  },
  {
    Header: "Pelanggan",
    accessor: "Customer.name",
    resizable: false,
    width: 230,
  },
  {
    Header: "Nama Alamat",
    accessor: "alias",
    resizable: false,
    width: 140,
  },
  {
    Header: "Alamat",
    accessor: "address",
    resizable: false,
    width: 390,
  },
];
class Address extends React.Component {
  static propTypes = {
    assignButtons: PropTypes.func.isRequired,
    showNotification: PropTypes.func,
  }

  static defaultProps = {
    showNotification: () => { },
  }

  constructor(props) {
    super(props);

    this.state = {
      data: [],
      deleteIds: [],
      filterText: "",
      limitValue: "5",
      limitData: [
        { id: "5", name: "5" },
        { id: "10", name: "10" },
        { id: "20", name: "20" },
        { id: "50", name: "50" },
        { id: "100", name: "100" },
      ],
      form: {
        id: "",
        id_customer: "",
        alias: "",
        receiver_name: "",
        address: "",
        postcode: "",
        phone: "",
        cityId: "",
        provinceId: "",
        sub_district: "",
        urban: "",
      },
      errorMsg: {
        id_customer: "",
        alias: "",
        receiver_name: "",
        address: "",
        phone: "",
        cityId: "",
        provinceId: "",
        sub_district: "",
        urban: "",
      },
      isValidated: false,
      showPopup: false,
      type: "",
    };
  }

  componentWillMount = () => {
    const { assignButtons } = this.props;
    assignButtons([{
      id: "1", title: "Tambah Data", icon: "fa fa-plus-square", clickEvent: () => this.callCreateHandler(),
    }]);
  }

  checkedHandler = (values) => {
    this.setState({ deleteIds: values }, () => {
      this.table.refreshData();
    });
  }

  bulkDeleteHandler = async () => {
    const { showNotification } = this.props;

    try {
      const { deleteIds } = this.state;
      await bulkDeleteAddress({ ids: JSON.stringify(deleteIds) });
      this.table.refreshData();
      this.setState({ deleteIds: [] });
      showNotification({
        type: "success",
        msg: "Berhasil menghapus data!",
      });
    } catch (err) {
      console.log(err);
    }
  }

  editDeleteHandler = async (id, type) => {
    const { showNotification } = this.props;
    if (type === "edit") {
      const res = await getAddress(id);
      if (res.status) {
        const { data } = res;
        this.setState({
          type: "edit",
          form: {
            id: data.id,
            id_customer: String(data.id_customer),
            alias: data.alias,
            receiver_name: data.receiver_name,
            address: data.address,
            postcode: data.postcode ? data.postcode : "",
            phone: data.phone,
            cityId: String(data.id_city),
            provinceId: String(data.City.id_province),
            sub_district: String(data.sub_district),
            urban: String(data.id_postal_code),
          },
          errorMsg: {
            id_customer: "",
            alias: "",
            receiver_name: "",
            address: "",
            phone: "",
            cityId: "",
            provinceId: "",
            sub_district: "",
            urban: "",
          },
          isValidated: false,
        }, () => {
          this.showPopup();
        });
      }
    } else {
      const { deleteIds } = this.state;
      const res = await deleteAddress({ id });
      if (res.status) {
        this.setState({ deleteIds: deleteIds.filter(x => (String(x) !== String(id))) });
        showNotification({
          type: "success",
          msg: "Berhasil menghapus data!",
        });
        this.table.refreshData();
      }
    }
  }

  callCreateHandler = () => {
    this.setState({
      type: "create",
      form: {
        id: "",
        id_customer: "",
        alias: "",
        receiver_name: "",
        address: "",
        postcode: "",
        phone: "",
        cityId: "",
        provinceId: "",
        sub_district: "",
        urban: "",
      },
      errorMsg: {
        id_customer: "",
        alias: "",
        receiver_name: "",
        address: "",
        phone: "",
        cityId: "",
        provinceId: "",
        sub_district: "",
        urban: "",
      },
      isValidated: false,
    }, () => {
      this.showPopup();
    });
  }

  showPopup = () => {
    this.setState({ showPopup: true });
  }

  hidePopup = () => {
    this.setState({ showPopup: false });
  }

  fetchData = async (state) => {
    const { filterText, limitValue } = this.state;
    const payload = {
      page: state.page + 1,
      limit: limitValue,
    };

    if (state.sorted) {
      Object.assign(payload, {
        sorted: JSON.stringify(state.sorted),
      });
    }

    if (filterText && filterText.length > 0) {
      Object.assign(payload, {
        filterText,
      });
    }

    const res = await getAddresses(payload);

    if (res.status) {
      const newData = res.data.map(x => (Object.assign({}, x, {
        price: Util.currency(x.price),
        updatedAt: moment(x.updatedAt).format("DD MMM YYYY HH:mm:ss"),
      })));

      return { data: newData, pageTotal: res.page_total };
    }

    return { data: [], pageTotal: 0 };
  }

  changeFilterTextHandler = (val) => {
    this.setState({ filterText: val });

    clearTimeout(this.filterIdle);
    this.filterIdle = setTimeout(() => {
      this.table.resetCurrentPage(() => {
        this.setState({ filterText: val }, () => {
          this.table.refreshData();
        });
      });
    }, 500);
  }

  changeLimitValueHandler = (val) => {
    this.table.resetCurrentPage(() => {
      this.setState({ limitValue: val }, () => {
        this.table.refreshData();
      });
    });
  }

  changeValueHandler = (type, val) => {
    const { form } = this.state;
    const newValue = update(form, {
      [type]: { $set: val },
    });

    const errorMsg = this.createErrorMessage(type, val);
    this.setState({ form: newValue, errorMsg });
  }

  changeIdProvinceHandler = (val) => {
    const { form } = this.state;
    const newValue = update(form, {
      provinceId: { $set: val },
      cityId: { $set: "" },
      sub_district: { $set: "" },
      urban: { $set: "" },
      postcode: { $set: "" },
    });

    const errorMsg = this.createErrorMessage("provinceId", val);
    this.setState({ form: newValue, errorMsg });
  }

  changeIdCityHandler = (val) => {
    const { form } = this.state;
    const newValue = update(form, {
      cityId: { $set: val },
      sub_district: { $set: "" },
      urban: { $set: "" },
      postcode: { $set: "" },
    });

    const errorMsg = this.createErrorMessage("cityId", val);
    this.setState({ form: newValue, errorMsg });
  }

  changeSubDistrictHandler = (val) => {
    const { form } = this.state;
    const newValue = update(form, {
      sub_district: { $set: val },
      urban: { $set: "" },
      postcode: { $set: "" },
    });

    const errorMsg = this.createErrorMessage("sub_district", val);
    this.setState({ form: newValue, errorMsg });
  }

  changeUrbanHandler = (val) => {
    const { form } = this.state;
    const newValue = update(form, {
      urban: { $set: val },
      postcode: { $set: "" },
    });

    const errorMsg = this.createErrorMessage("urban", val);
    this.setState({ form: newValue, errorMsg }, () => {
      this.setPostalCode();
    });
  }

  setPostalCode = async () => {
    const { form: { urban }, form } = this.state;
    const res = await getPostalCode(urban);

    if (res.status && res.data.length > 0) {
      const newValue = update(form, {
        postcode: { $set: res.data[0].postal_code },
      });
      this.setState({ form: newValue });
    }
  }

  bulkCreateErrorMessage = () => {
    const { errorMsg, form } = this.state;
    const keys = Object.keys(errorMsg);
    let newErrorMsg = errorMsg;
    let isError = false;

    keys.forEach((x) => {
      let error = "";
      const ERROR_TEXT = errorContent(x, form[x]);

      if (ERROR_TEXT) {
        error = ERROR_TEXT;
        isError = true;
      }

      newErrorMsg = update(newErrorMsg, {
        [x]: { $set: error },
      });
    });

    return { msg: newErrorMsg, isError };
  }

  createErrorMessage = (type, val) => {
    const { errorMsg, isValidated } = this.state;
    let error = "";

    if (!isValidated) {
      return errorMsg;
    }

    const ERROR_TEXT = errorContent(type, val);

    if (ERROR_TEXT) {
      error = ERROR_TEXT;
    }

    const newValue = update(errorMsg, {
      [type]: { $set: error },
    });

    return newValue;
  }

  saveHandler = async (hide) => {
    const { showNotification } = this.props;
    const { form, type } = this.state;
    const error = this.bulkCreateErrorMessage();
    const isValid = !error.isError;

    this.setState({ isValidated: true, errorMsg: error.msg });

    if (isValid) {
      const payload = Object.assign({}, form, { id_postal_code: form.urban });

      if (type === "create") {
        await createAddress(payload);
        showNotification({
          type: "success",
          msg: "Berhasil menambah data!",
        });
      } else {
        await updateAddress(payload);
        showNotification({
          type: "success",
          msg: "Berhasil mengubah data!",
        });
      }

      this.table.refreshData();
      hide();
    }
  }

  render = () => {
    const {
      deleteIds,
      filterText,
      limitValue,
      limitData,
      showPopup,
      type,
      form,
      errorMsg,
    } = this.state;

    return (
      <div>
        <div className="box box-solid">
          <div className="box-header with-border">
            <div className="row mb-0">
              <div className="col-sm-8">
                <h3 className="box-title with-title">Alamat Pelanggan</h3>
              </div>
              <div className="col-sm-4">
                <div className="col-sm-9">
                  <InputText
                    placeholder="Pencarian ..."
                    changeEvent={this.changeFilterTextHandler}
                    value={filterText}
                  />
                </div>
                <div className="col-sm-3 pl-0">
                  <Select
                    data={limitData}
                    value={limitValue}
                    changeEvent={this.changeLimitValueHandler}
                  />
                </div>
              </div>
            </div>
          </div>
          <div className="box-body">
            <Table
              ref={(c) => { this.table = c; }}
              columns={columns}
              onFetch={this.fetchData}
              showPagination={true}
              checkedValues={deleteIds}
              checkedEvent={this.checkedHandler}
              editDeleteEvent={this.editDeleteHandler}
            />
          </div>
        </div>
        {showPopup && <SidePopup
          onHide={this.hidePopup}
          title={type === "create" ? "Tambah Data" : "Edit Data"}
          saveEvent={this.saveHandler}
        >
          <div className="row mb-sm">
            <div className="col-sm-12">
              <Select2
                label="Pelanggan"
                url="/api/v2/customers"
                placeholder="- Pilih Pelanggan -"
                value={form.id_customer}
                changeEvent={val => this.changeValueHandler("id_customer", val)}
                errorText={errorMsg.id_customer}
              />
            </div>
          </div>
          <div className="row mb-sm">
            <div className="col-sm-12">
              <InputText
                label="Nama Alamat *"
                changeEvent={val => this.changeValueHandler("alias", val)}
                value={form.alias}
                errorText={errorMsg.alias}
              />
            </div>
          </div>
          <div className="row mb-sm">
            <div className="col-sm-12">
              <InputText
                label="Nama Penerima *"
                changeEvent={val => this.changeValueHandler("receiver_name", val)}
                value={form.receiver_name}
                errorText={errorMsg.receiver_name}
              />
            </div>
          </div>
          <div className="row mb-sm">
            <div className="col-sm-12">
              <TextArea
                label="Alamat"
                changeEvent={val => this.changeValueHandler("address", val)}
                value={String(form.address)}
                errorText={errorMsg.address}
              />
            </div>
          </div>
          <div className="row mb-sm">
            <div className="col-sm-12">
              <InputText
                label="Telepon *"
                changeEvent={val => this.changeValueHandler("phone", val)}
                value={form.phone}
                errorText={errorMsg.phone}
              />
            </div>
          </div>
          <div className="row mb-sm">
            <div className="col-sm-12">
              <Select2
                label="Provinsi"
                url="/api/v2/provinces"
                placeholder="- Pilih Provinsi -"
                value={form.provinceId}
                changeEvent={this.changeIdProvinceHandler}
                errorText={errorMsg.provinceId}
              />
            </div>
          </div>
          <div className="row mb-sm">
            <div className="col-sm-12">
              <Select2
                label="Kab./Kota"
                url="/api/v2/cities"
                additionalParam={{ idProvince: form.provinceId }}
                placeholder="- Pilih Kab./Kota -"
                value={form.cityId}
                changeEvent={this.changeIdCityHandler}
                errorText={errorMsg.cityId}
              />
            </div>
          </div>
          <div className="row mb-sm">
            <div className="col-sm-12">
              <Select2
                label="Kecamatan"
                url="/api/sub_districts"
                additionalParam={{ idCity: form.cityId }}
                placeholder="- Pilih Kecamatan -"
                value={form.sub_district}
                changeEvent={this.changeSubDistrictHandler}
                errorText={errorMsg.sub_district}
              />
            </div>
          </div>
          <div className="row mb-sm">
            <div className="col-sm-12">
              <Select2
                label="Kelurahan"
                url="/api/urbans"
                additionalParam={{ idCity: form.cityId, subDistrict: form.sub_district }}
                placeholder="- Pilih Kelurahan -"
                value={form.urban}
                changeEvent={this.changeUrbanHandler}
                errorText={errorMsg.urban}
              />
            </div>
          </div>
          <div className="row mb-sm">
            <div className="col-sm-12">
              <InputText
                label="Kode Pos"
                changeEvent={() => {}}
                value={form.postcode}
                errorText={errorMsg.postcode}
              />
            </div>
          </div>
        </SidePopup>}
        {(deleteIds.length > 0) && <div className="bulk-delete-wrapper">
          <div className="row">
            <div className="col-sm-6">
              <h5 className="text-muted"><b>{deleteIds.length} DATA AKAN DIHAPUS ?</b></h5>
            </div>
            <div className="col-sm-6 text-right">
              <button style={{ width: "100px" }} type="button" className="btn btn-danger btn-md btn-flat" onClick={this.bulkDeleteHandler}><i className="fa fa-trash-o" /> Hapus</button>
            </div>
          </div>
        </div>
        }
      </div>
    );
  }
}

export default CoreHoC(Address);
