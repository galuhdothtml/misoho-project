/* eslint prop-types: 0 */
import React from "react";
import CoreHoC from "./CoreHoC";

class ChangePassword extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      title: "Ganti Password",
    };
  }


  render() {
    const { title } = this.state;

    return (
      <div>
        <h1>{title}</h1>
      </div>
    );
  }
}

export default CoreHoC(ChangePassword);
