/* eslint prop-types: 0 */
import React from "react";
import update from "immutability-helper";
import {
  getCustomer,
  updateCustomer,
  updateCustomerPassword,
} from "../../data";
import CoreHoC from "../CoreHoC";
import InputText from "../form/InputText";
import ModalPopup from "../form/ModalPopup";
import InputNumber from "../form/InputNumber";
import InputPassword from "../form/InputPassword";
import Util from "../../utils";

const errorContent = (type, val) => {
  let retval = null;

  if (type === "name") {
    if (String(val).trim().length === 0) {
      retval = "* Nama wajib diisi";
    }
  } else if (type === "email") {
    if (String(val).trim().length === 0) {
      retval = "* Email wajib diisi";
    } else if (!Util.validateEmail(val)) {
      retval = "* Format email masih belum benar";
    }
  }

  return retval;
};
const errorPasswordContent = (type, val, toEqualVal = "") => {
  let retval = null;

  if (type === "password") {
    if (String(val).trim().length === 0) {
      retval = "* Password wajib diisi";
    }
  } else if (type === "confirmPassword") {
    if (String(val).trim().length === 0) {
      retval = "* Konfirmasi password wajib diisi";
    } else if (String(val) !== String(toEqualVal)) {
      retval = "* Konfirmasi password masih belum benar";
    }
  }

  return retval;
};
class CustomerDetail extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      isValidated: false,
      form: {
        id: "",
        name: "",
        email: "",
      },
      formPassword: {
        password: "",
        confirmPassword: "",
      },
      files: [],
      errorMsg: {
        name: "",
        email: "",
      },
      errorPasswordMsg: {
        password: "",
        confirmPassword: "",
      },
      isModalShowing: false,
    };
  }

  componentWillMount = () => {
    const {
      match: {
        params
      },
    } = this.props;

    this.setupCustomerData(params.id);
  }

  bulkCreateErrorMessage = () => {
    const { form, errorMsg } = this.state;
    const keys = Object.keys(errorMsg);
    let newErrorMsg = errorMsg;
    let isError = false;

    keys.forEach((x) => {
      let error = "";

      const ERROR_TEXT = errorContent(x, form[x]);

      if (ERROR_TEXT) {
        error = ERROR_TEXT;
        isError = true;
      }

      newErrorMsg = update(newErrorMsg, {
        [x]: { $set: error },
      });
    });

    return { msg: newErrorMsg, isError };
  }

  createErrorMessage = (type, val) => {
    const { errorMsg, isValidated } = this.state;
    let error = "";

    if (!isValidated) {
      return errorMsg;
    }

    const ERROR_TEXT = errorContent(type, val);

    if (ERROR_TEXT) {
      error = ERROR_TEXT;
    }

    const newValue = update(errorMsg, {
      [type]: { $set: error },
    });

    return newValue;
  }

  bulkCreateErrorPasswordMessage = () => {
    const { formPassword: form, errorPasswordMsg: errorMsg } = this.state;
    const keys = Object.keys(errorMsg);
    let newErrorMsg = errorMsg;
    let isError = false;

    keys.forEach((x) => {
      let error = "";

      const ERROR_TEXT = errorPasswordContent(x, form[x], form.password);

      if (ERROR_TEXT) {
        error = ERROR_TEXT;
        isError = true;
      }

      newErrorMsg = update(newErrorMsg, {
        [x]: { $set: error },
      });
    });

    return { msg: newErrorMsg, isError };
  }

  createErrorPasswordMessage = (type, val) => {
    const { errorPasswordMsg: errorMsg, isValidated, formPassword } = this.state;
    let error = "";

    if (!isValidated) {
      return errorMsg;
    }

    const ERROR_TEXT = errorPasswordContent(type, val, formPassword.password);

    if (ERROR_TEXT) {
      error = ERROR_TEXT;
    }

    const newValue = update(errorMsg, {
      [type]: { $set: error },
    });

    return newValue;
  }

  changePasswordValueHandler = (type, val) => {
    const { formPassword: form } = this.state;
    const newData = update(form, {
      [type]: { $set: val },
    });

    const errorMsg = this.createErrorPasswordMessage(type, val);
    this.setState({ formPassword: newData, errorPasswordMsg: errorMsg });
  }

  changeValueHandler = (type, val) => {
    const { form } = this.state;
    const newData = update(form, {
      [type]: { $set: val },
    });

    const errorMsg = this.createErrorMessage(type, val);
    this.setState({ form: newData, errorMsg });
  }

  setupCustomerData = async (id) => {
    const res = await getCustomer(id);

    if (res.status) {
      const { data } = res;
      const form = {
        id: data.id,
        name: data.name,
        email: data.email,
      };

      this.setState({ form, type: "edit" });
    }
  }

  saveHandler = async () => {
    const { history } = this.props;
    const { form } = this.state;
    const error = this.bulkCreateErrorMessage();
    const isValid = !error.isError;
    const payload = {
      id: form.id,
      name: form.name,
      email: form.email,
    };

    this.setState({ isValidated: true, errorMsg: error.msg });

    if (!isValid) {
      return;
    }

    const res = await updateCustomer(payload);

    if (res.status) {
      history.push("/pelanggan-pelanggan");
    }
  }

  saveChangePassword = async (callback) => {
    const { showNotification } = this.props;
    const { form, formPassword } = this.state;
    const error = this.bulkCreateErrorPasswordMessage();
    const isValid = !error.isError;
    const payload = {
      id: form.id,
      new_password: formPassword.password,
      confirm_new_password: formPassword.confirmPassword,
    };

    this.setState({ isValidated: true, errorPasswordMsg: error.msg });

    if (!isValid) {
      callback();
      return;
    }

    const res = await updateCustomerPassword(payload);
    if (res.status) {
      showNotification({
        type: "success",
        msg: "Berhasil mengganti password!",
      });
      this.hideModal();
      this.setState({
        isValidated: false,
        errorMsg: {
          name: "",
          email: "",
        },
      });
    } else {
      callback();
    }
  }

  showModal = () => {
    this.setState({ isModalShowing: true });
  }

  hideModal = () => {
    this.setState({ isModalShowing: false });
  }

  render() {
    const {
      form, errorMsg, errorPasswordMsg, isModalShowing, formPassword,
    } = this.state;

    return (
      <div>
        <div className="box box-solid">
          <div className="box-header with-border">
            <div className="row mb-0">
              <div className="col-sm-12">
                <h3 className="box-title">Edit Pelanggan</h3>
              </div>
            </div>
          </div>
          <div className="box-body">
            <div className="row mb-0">
              <div className="col-sm-5">
                <div className="row mb-sm">
                  <div className="col-sm-12">
                    <InputText
                      label="Nama"
                      changeEvent={val => this.changeValueHandler("name", val)}
                      value={String(form.name)}
                      errorText={errorMsg.name}
                    />
                  </div>
                </div>
                <div className="row mb-sm">
                  <div className="col-sm-12">
                    <InputText
                      label="Email"
                      changeEvent={val => this.changeValueHandler("email", val)}
                      value={String(form.email)}
                      errorText={errorMsg.email}
                    />
                  </div>
                </div>
                <div className="row mb-sm">
                  <div className="col-sm-12">
                    <button type="button" className="btn btn-default" onClick={this.showModal}>Ganti Password</button>
                  </div>
                </div>
              </div>
              <div className="col-sm-7">

              </div>
            </div>
          </div>
          <div className="box-footer text-right">
            <div>
              <button type="button" className="btn btn-primary" onClick={this.saveHandler}>Simpan</button>
            </div>
          </div>
        </div>
        {isModalShowing
          && (
            <ModalPopup width={340} title="Ganti Password" hideModal={this.hideModal} saveHandler={this.saveChangePassword}>
              <div>
                <div className="row mb-sm">
                  <div className="col-sm-12">
                    <InputPassword
                      label="Password"
                      changeEvent={val => this.changePasswordValueHandler("password", val)}
                      value={formPassword.password}
                      errorText={errorPasswordMsg.password}
                    />
                  </div>
                </div>
                <div className="row mb-sm">
                  <div className="col-sm-12">
                    <InputPassword
                      label="Konfirmasi Password"
                      changeEvent={val => this.changePasswordValueHandler("confirmPassword", val)}
                      value={formPassword.confirmPassword}
                      errorText={errorPasswordMsg.confirmPassword}
                    />
                  </div>
                </div>
              </div>
            </ModalPopup>
          )
        }
      </div>
    );
  }
}

export default CoreHoC(CustomerDetail);
