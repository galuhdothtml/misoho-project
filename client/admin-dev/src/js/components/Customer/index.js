import React from "react";
import PropTypes from "prop-types";
import moment from "moment";
import update from "immutability-helper";
import CoreHoC from "../CoreHoC";
import RawTable from "../form/Table";
import {
  getCustomers,
  createCustomer,
  deleteCustomer,
  bulkDeleteCustomer,
} from "../../data";
import Util from "../../utils";
import withEditDelete from "../form/Table/withEditDelete";
import withCheckbox from "../form/Table/withCheckbox";
import InputText from "../form/InputText";
import InputPassword from "../form/InputPassword";
import Select from "../form/Select";
import SidePopup from "../form/SidePopup";

const errorContent = (type, val, toEqualVal = "") => {
  let retval = null;

  if (type === "name") {
    if (String(val).trim().length === 0) {
      retval = "* Nama wajib diisi";
    }
  } else if (type === "email") {
    if (String(val).trim().length === 0) {
      retval = "* Email wajib diisi";
    } else if (!Util.validateEmail(val)) {
      retval = "* Format email masih belum benar";
    }
  } else if (type === "password") {
    if (String(val).trim().length === 0) {
      retval = "* Password wajib diisi";
    }
  } else if (type === "confirmPassword") {
    if (String(val).trim().length === 0) {
      retval = "* Konfirmasi password wajib diisi";
    } else if (String(val) !== String(toEqualVal)) {
      retval = "* Konfirmasi password masih belum benar";
    }
  }

  return retval;
};
const Table = withCheckbox(withEditDelete(RawTable));
const columns = [
  {
    Header: "Kode",
    accessor: "id",
    resizable: false,
    width: 80,
  },
  {
    Header: "Nama",
    accessor: "name",
    resizable: false,
    width: 280,
  },
  {
    Header: "Email",
    accessor: "email",
    resizable: false,
    width: 280,
  },
  {
    Header: "Update Terakhir",
    accessor: "updatedAt",
    resizable: false,
  },
];
class Customer extends React.Component {
  static propTypes = {
    assignCalendar: PropTypes.func.isRequired,
    assignButtons: PropTypes.func.isRequired,
    showNotification: PropTypes.func,
  }

  static defaultProps = {
    showNotification: () => { },
  }

  constructor(props) {
    super(props);

    this.state = {
      data: [],
      deleteIds: [],
      filterText: "",
      limitValue: "5",
      limitData: [
        { id: "5", name: "5" },
        { id: "10", name: "10" },
        { id: "20", name: "20" },
        { id: "50", name: "50" },
        { id: "100", name: "100" },
      ],
      form: {
        id: "",
        name: "",
        email: "",
        password: "",
        confirmPassword: "",
      },
      errorMsg: {
        name: "",
        email: "",
        password: "",
        confirmPassword: "",
      },
      isValidated: false,
      showPopup: false,
    };
  }

  componentWillMount = () => {
    const { assignButtons } = this.props;

    assignButtons([{
      id: "1", title: "Tambah Data", icon: "fa fa-plus-square", clickEvent: () => this.callCreateHandler(),
    }]);
  }

  checkedHandler = (values) => {
    this.setState({ deleteIds: values }, () => {
      this.table.refreshData();
    });
  }

  bulkDeleteHandler = async () => {
    const { showNotification } = this.props;

    try {
      const { deleteIds } = this.state;
      await bulkDeleteCustomer({ ids: JSON.stringify(deleteIds) });
      this.table.refreshData();
      this.setState({ deleteIds: [] });
      showNotification({
        type: "success",
        msg: "Berhasil menghapus data!",
      });
    } catch (err) {
      console.log(err);
    }
  }

  editDeleteHandler = async (id, type) => {
    const { showNotification } = this.props;
    if (type === "edit") {
      const { history } = this.props;

      history.push(`/pelanggan-pelanggan/detail/${id}`);
    } else {
      const { deleteIds } = this.state;
      const res = await deleteCustomer({ id });
      if (res.status) {
        this.setState({ deleteIds: deleteIds.filter(x => (String(x) !== String(id))) });
        showNotification({
          type: "success",
          msg: "Berhasil menghapus data!",
        });
        this.table.refreshData();
      }
    }
  }

  callCreateHandler = () => {
    this.setState({
      form: {
        id: "",
        name: "",
        email: "",
        password: "",
        confirmPassword: "",
      },
      errorMsg: {
        name: "",
        email: "",
        password: "",
        confirmPassword: "",
      },
      isValidated: false,
    }, () => {
      this.showPopup();
    });
  }

  fetchData = async (state) => {
    const { filterText, limitValue } = this.state;
    const payload = {
      page: state.page + 1,
      limit: limitValue,
    };

    if (state.sorted) {
      Object.assign(payload, {
        sorted: JSON.stringify(state.sorted),
      });
    }

    if (filterText && filterText.length > 0) {
      Object.assign(payload, {
        filterText,
      });
    }

    const res = await getCustomers(payload);

    if (res.status) {
      const newData = res.data.map(x => (Object.assign({}, x, {
        price: Util.currency(x.price),
        updatedAt: moment(x.updatedAt).format("DD MMM YYYY HH:mm:ss"),
      })));

      return { data: newData, pageTotal: res.page_total };
    }

    return { data: [], pageTotal: 0 };
  }

  changeFilterTextHandler = (val) => {
    this.setState({ filterText: val });

    clearTimeout(this.filterIdle);
    this.filterIdle = setTimeout(() => {
      this.table.resetCurrentPage(() => {
        this.setState({ filterText: val }, () => {
          this.table.refreshData();
        });
      });
    }, 500);
  }

  changeLimitValueHandler = (val) => {
    this.table.resetCurrentPage(() => {
      this.setState({ limitValue: val }, () => {
        this.table.refreshData();
      });
    });
  }


  showPopup = () => {
    this.setState({ showPopup: true });
  }

  hidePopup = () => {
    this.setState({ showPopup: false });
  }

  changeValueHandler = (type, val) => {
    const { form } = this.state;
    const newValue = update(form, {
      [type]: { $set: val },
    });

    const errorMsg = this.createErrorMessage(type, val);
    this.setState({ form: newValue, errorMsg });
  }

  bulkCreateErrorMessage = () => {
    const { errorMsg, form } = this.state;
    const keys = Object.keys(errorMsg);
    let newErrorMsg = errorMsg;
    let isError = false;

    keys.forEach((x) => {
      let error = "";
      const ERROR_TEXT = errorContent(x, form[x], form.password);

      if (ERROR_TEXT) {
        error = ERROR_TEXT;
        isError = true;
      }

      newErrorMsg = update(newErrorMsg, {
        [x]: { $set: error },
      });
    });

    return { msg: newErrorMsg, isError };
  }

  createErrorMessage = (type, val) => {
    const { errorMsg, isValidated, form } = this.state;
    let error = "";

    if (!isValidated) {
      return errorMsg;
    }

    const ERROR_TEXT = errorContent(type, val, form.password);

    if (ERROR_TEXT) {
      error = ERROR_TEXT;
    }

    const newValue = update(errorMsg, {
      [type]: { $set: error },
    });

    return newValue;
  }

  saveHandler = async (hide) => {
    const { showNotification } = this.props;
    const { form } = this.state;
    const error = this.bulkCreateErrorMessage();
    const isValid = !error.isError;

    this.setState({ isValidated: true, errorMsg: error.msg });

    if (isValid) {
      const payload = Object.assign({}, form, { passwd: form.password });
      await createCustomer(payload);
      showNotification({
        type: "success",
        msg: "Berhasil menambah data!",
      });

      this.table.refreshData();
      hide();
    }
  }

  render = () => {
    const {
      deleteIds,
      filterText,
      limitValue,
      limitData,
      showPopup,
      form,
      errorMsg,
    } = this.state;

    return (
      <div>
        <div className="box box-solid">
          <div className="box-header with-border">
            <div className="row mb-0">
              <div className="col-sm-8">
                <h3 className="box-title with-title">Pelanggan</h3>
              </div>
              <div className="col-sm-4">
                <div className="row">
                  <div className="col-sm-9">
                    <InputText
                      placeholder="Pencarian ..."
                      changeEvent={this.changeFilterTextHandler}
                      value={filterText}
                    />
                  </div>
                  <div className="col-sm-3 pl-0">
                    <Select
                      data={limitData}
                      value={limitValue}
                      changeEvent={this.changeLimitValueHandler}
                    />
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div className="box-body">
            <Table
              ref={(c) => { this.table = c; }}
              columns={columns}
              onFetch={this.fetchData}
              showPagination={true}
              checkedValues={deleteIds}
              checkedEvent={this.checkedHandler}
              editDeleteEvent={this.editDeleteHandler}
            />
          </div>
        </div>
        {showPopup && <SidePopup
          onHide={this.hidePopup}
          title="Tambah Data"
          saveEvent={this.saveHandler}
        >
          <div className="row mb-sm">
            <div className="col-sm-12">
              <InputText
                label="Nama *"
                changeEvent={val => this.changeValueHandler("name", val)}
                value={form.name}
                errorText={errorMsg.name}
              />
            </div>
          </div>
          <div className="row mb-sm">
            <div className="col-sm-12">
              <InputText
                label="Email *"
                changeEvent={val => this.changeValueHandler("email", val)}
                value={form.email}
                errorText={errorMsg.email}
              />
            </div>
          </div>
          <div className="row mb-sm">
            <div className="col-sm-12">
              <InputPassword
                label="Password *"
                changeEvent={val => this.changeValueHandler("password", val)}
                value={form.password}
                errorText={errorMsg.password}
              />
            </div>
          </div>
          <div className="row mb-sm">
            <div className="col-sm-12">
              <InputPassword
                label="Konfirmasi Password *"
                changeEvent={val => this.changeValueHandler("confirmPassword", val)}
                value={form.confirmPassword}
                errorText={errorMsg.confirmPassword}
              />
            </div>
          </div>
        </SidePopup>}
        {(deleteIds.length > 0) && <div className="bulk-delete-wrapper">
          <div className="row">
            <div className="col-sm-6">
              <h5 className="text-muted"><b>{deleteIds.length} DATA AKAN DIHAPUS ?</b></h5>
            </div>
            <div className="col-sm-6 text-right">
              <button style={{ width: "100px" }} type="button" className="btn btn-danger btn-md btn-flat" onClick={this.bulkDeleteHandler}><i className="fa fa-trash-o" /> Hapus</button>
            </div>
          </div>
        </div>
        }
      </div>
    );
  }
}

export default CoreHoC(Customer);
