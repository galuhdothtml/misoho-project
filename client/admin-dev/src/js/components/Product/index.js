import React from "react";
import PropTypes from "prop-types";
import moment from "moment";
import CoreHoC from "../CoreHoC";
import RawTable from "../form/Table";
import {
  getProducts,
  deleteProduct,
  bulkDeleteProduct,
} from "../../data";
import Util from "../../utils";
import withEditDelete from "../form/Table/withEditDelete";
import withCheckbox from "../form/Table/withCheckbox";
import InputText from "../form/InputText";
import Select from "../form/Select";

const Table = withCheckbox(withEditDelete(RawTable));
const columns = [
  {
    Header: "Kode",
    accessor: "id",
    resizable: false,
    width: 80,
  },
  {
    Header: "Kategori",
    id: "Category.name",
    accessor: "category_name",
    resizable: false,
    width: 130,
  },
  {
    Header: "Nama Produk",
    accessor: "name",
    resizable: false,
    width: 310,
  },
  {
    Header: "Harga",
    accessor: "price",
    resizable: false,
  },
  {
    Header: "Update Terakhir",
    accessor: "updatedAt",
    resizable: false,
  },
];
class Product extends React.Component {
  static propTypes = {
    assignCalendar: PropTypes.func.isRequired,
    assignButtons: PropTypes.func.isRequired,
    showNotification: PropTypes.func,
  }

  static defaultProps = {
    showNotification: () => { },
  }

  constructor(props) {
    super(props);

    this.state = {
      data: [],
      deleteIds: [],
      filterText: "",
      limitValue: "5",
      limitData: [
        { id: "5", name: "5" },
        { id: "10", name: "10" },
        { id: "20", name: "20" },
        { id: "50", name: "50" },
        { id: "100", name: "100" },
      ],
    };
  }

  componentWillMount = () => {
    const { assignButtons, assignCalendar } = this.props;
    // assignCalendar((val) => {
    //   console.log("DEBUG-CALENDAR: ", val);
    // });
    assignButtons([{
      id: "1", title: "Tambah Data", icon: "fa fa-plus-square", clickEvent: () => this.callCreateHandler(),
    }]);
  }

  checkedHandler = (values) => {
    this.setState({ deleteIds: values }, () => {
      this.table.refreshData();
    });
  }

  bulkDeleteHandler = async () => {
    const { showNotification } = this.props;

    try {
      const { deleteIds } = this.state;
      await bulkDeleteProduct({ ids: JSON.stringify(deleteIds) });
      this.table.refreshData();
      this.setState({ deleteIds: [] });
      showNotification({
        type: "success",
        msg: "Berhasil menghapus data!",
      });
    } catch (err) {
      console.log(err);
    }
  }

  editDeleteHandler = async (id, type) => {
    const { showNotification } = this.props;
    if (type === "edit") {
      const { history } = this.props;

      history.push(`/produk-produk/detail/edit/${id}`);
    } else {
      const { deleteIds } = this.state;
      const res = await deleteProduct({ id });
      if (res.status) {
        this.setState({ deleteIds: deleteIds.filter(x => (String(x) !== String(id))) });
        showNotification({
          type: "success",
          msg: "Berhasil menghapus data!",
        });
        this.table.refreshData();
      }
    }
  }

  callCreateHandler = () => {
    const { history } = this.props;

    history.push("/produk-produk/detail/create");
  }

  fetchData = async (state) => {
    const { filterText, limitValue } = this.state;
    const payload = {
      page: state.page + 1,
      limit: limitValue,
    };

    if (state.sorted) {
      Object.assign(payload, {
        sorted: JSON.stringify(state.sorted),
      });
    }

    if (filterText && filterText.length > 0) {
      Object.assign(payload, {
        filterText,
      });
    }

    const res = await getProducts(payload);

    if (res.status) {
      const newData = res.data.map(x => (Object.assign({}, x, {
        price: Util.currency(x.price),
        updatedAt: moment(x.updatedAt).format("DD MMM YYYY HH:mm:ss"),
      })));

      return { data: newData, pageTotal: res.page_total };
    }

    return { data: [], pageTotal: 0 };
  }

  changeFilterTextHandler = (val) => {
    this.setState({ filterText: val });

    clearTimeout(this.filterIdle);
    this.filterIdle = setTimeout(() => {
      this.table.resetCurrentPage(() => {
        this.setState({ filterText: val }, () => {
          this.table.refreshData();
        });
      });
    }, 500);
  }

  changeLimitValueHandler = (val) => {
    this.table.resetCurrentPage(() => {
      this.setState({ limitValue: val }, () => {
        this.table.refreshData();
      });
    });
  }

  render = () => {
    const {
      deleteIds,
      filterText,
      limitValue,
      limitData,
    } = this.state;

    return (
      <div>
        <div className="box box-solid">
          <div className="box-header with-border">
            <div className="row mb-0">
              <div className="col-sm-8">
                <h3 className="box-title with-title">Produk</h3>
              </div>
              <div className="col-sm-4">
                <div className="row">
                  <div className="col-sm-9">
                    <InputText
                      placeholder="Pencarian ..."
                      changeEvent={this.changeFilterTextHandler}
                      value={filterText}
                    />
                  </div>
                  <div className="col-sm-3 pl-0">
                    <Select
                      data={limitData}
                      value={limitValue}
                      changeEvent={this.changeLimitValueHandler}
                    />
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div className="box-body">
            <Table
              ref={(c) => { this.table = c; }}
              columns={columns}
              onFetch={this.fetchData}
              showPagination={true}
              checkedValues={deleteIds}
              checkedEvent={this.checkedHandler}
              editDeleteEvent={this.editDeleteHandler}
            />
          </div>
        </div>
        {(deleteIds.length > 0) && <div className="bulk-delete-wrapper">
          <div className="row">
            <div className="col-sm-6">
              <h5 className="text-muted"><b>{deleteIds.length} DATA AKAN DIHAPUS ?</b></h5>
            </div>
            <div className="col-sm-6 text-right">
              <button style={{ width: "100px" }} type="button" className="btn btn-danger btn-md btn-flat" onClick={this.bulkDeleteHandler}><i className="fa fa-trash-o" /> Hapus</button>
            </div>
          </div>
        </div>
        }
      </div>
    );
  }
}

export default CoreHoC(Product);
