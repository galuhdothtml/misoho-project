/* eslint prop-types: 0 */
import React from "react";
import update from "immutability-helper";
import {
  getCategories,
  getProduct,
  createProduct,
  updateProduct,
} from "../../data";
import CoreHoC from "../CoreHoC";
import InputText from "../form/InputText";
import InputCurrency from "../form/InputCurrency";
import InputNumber from "../form/InputNumber";
import TextArea from "../form/TextArea";
import Select from "../form/Select";
import Select2 from "../form/Select2";
import ImageDropzone from "../form/ImageDropzoneRaw";

const errorContent = (type, val) => {
  let retval = null;

  if (type === "id_category") {
    if (String(val).trim().length === 0) {
      retval = "* Kategori wajib diisi";
    }
  } else if (type === "name") {
    if (String(val).trim().length === 0) {
      retval = "* Nama produk wajib diisi";
    }
  } else if (type === "price") {
    if (String(val).trim().length === 0) {
      retval = "* Harga wajib diisi";
    } else if (parseFloat(val, 10) === 0) {
      retval = "* Harga tidak boleh 0";
    }
  } else if (type === "weight") {
    if (String(val).trim().length === 0) {
      retval = "* Berat wajib diisi";
    } else if (parseFloat(val, 10) === 0) {
      retval = "* Berat tidak boleh 0";
    }
  } else if (type === "cityId") {
    if (String(val).trim().length === 0) {
      retval = "* Kota wajib diisi";
    }
  } else if (type === "provinceId") {
    if (String(val).trim().length === 0) {
      retval = "* Provinsi wajib diisi";
    }
  }

  return retval;
};
class ProductDetail extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      type: "",
      categoryList: [],
      isValidated: false,
      form: {
        id: "",
        id_category: "",
        name: "",
        price: "",
        description: "",
        weight: "",
        available_qty: "",
        min_qty: "",
        cityId: "",
        provinceId: "",
      },
      files: [],
      errorMsg: {
        id_category: "",
        name: "",
        price: "",
        weight: "",
        cityId: "",
        provinceId: "",
      },
    };
  }

  componentWillMount = () => {
    const {
      match: {
        params
      },
    } = this.props;
    this.fetchCategories();

    if (params.type === "edit") {
      this.setupProductData(params.id);
    } else {
      this.setState({ type: "create" });
    }
  }

  bulkCreateErrorMessage = () => {
    const { form, errorMsg } = this.state;
    const keys = Object.keys(errorMsg);
    let newErrorMsg = errorMsg;
    let isError = false;

    keys.forEach((x) => {
      let error = "";

      const ERROR_TEXT = errorContent(x, form[x]);

      if (ERROR_TEXT) {
        error = ERROR_TEXT;
        isError = true;
      }

      newErrorMsg = update(newErrorMsg, {
        [x]: { $set: error },
      });
    });

    return { msg: newErrorMsg, isError };
  }

  createErrorMessage = (type, val) => {
    const { errorMsg, isValidated } = this.state;
    let error = "";

    if (!isValidated) {
      return errorMsg;
    }

    const ERROR_TEXT = errorContent(type, val);

    if (ERROR_TEXT) {
      error = ERROR_TEXT;
    }

    const newValue = update(errorMsg, {
      [type]: { $set: error },
    });

    return newValue;
  }

  changeValueHandler = (type, val) => {
    const { form } = this.state;
    const newData = update(form, {
      [type]: { $set: val },
    });

    const errorMsg = this.createErrorMessage(type, val);
    this.setState({ form: newData, errorMsg });
  }

  setupProductData = async (id) => {
    const res = await getProduct(id);

    if (res.status) {
      const { data } = res;
      const form = {
        id: data.id,
        id_category: data.id_category,
        name: data.name,
        price: parseInt(data.price, 10),
        description: data.description,
        weight: parseFloat(data.weight, 10),
        available_qty: parseInt(data.available_qty, 10),
        min_qty: parseInt(data.min_qty, 10),
        cityId: String(data.origin),
        provinceId: String(data.provinceId),
      };

      const files = data.images.map(x => (x.filename));

      this.setState({ form, files, type: "edit" });
    }
  }

  fetchCategories = async () => {
    const res = await getCategories();
    let data = [];

    if (res.status) {
      ({ data } = res);
    }

    data = [{ id: "", name: "- Pilih Kategori -" }, ...data];

    this.setState({ categoryList: data });
  }

  changeIdProvinceHandler = (val) => {
    const { form } = this.state;
    const newValue = update(form, {
      provinceId: { $set: val },
      cityId: { $set: "" },
    });

    const errorMsg = this.createErrorMessage("provinceId", val);
    this.setState({ form: newValue, errorMsg });
  }

  changeIdCityHandler = (val) => {
    const { form } = this.state;
    const newValue = update(form, {
      cityId: { $set: val },
    });

    const errorMsg = this.createErrorMessage("cityId", val);
    this.setState({ form: newValue, errorMsg });
  }

  saveHandler = async () => {
    const { history } = this.props;
    const { form, files, type } = this.state;
    const error = this.bulkCreateErrorMessage();
    const isValid = !error.isError;
    const payload = {
      id: form.id,
      id_category: form.id_category,
      name: form.name,
      price: form.price,
      description: form.description,
      origin: form.cityId,
      weight: form.weight,
      available_qty: form.available_qty ? form.available_qty : 0,
      min_qty: form.min_qty ? form.min_qty : 0,
      photos: files,
    };

    this.setState({ isValidated: true, errorMsg: error.msg });

    if (!isValid) {
      return;
    }

    if (type === "create") {
      const res = await createProduct(payload);

      if (res.status) {
        history.push("/produk-produk");
      }
    } else {
      const res = await updateProduct(payload);

      if (res.status) {
        history.push("/produk-produk");
      }
    }
  }

  changeFilesHandler = (files) => {
    this.setState({ files });
  }

  render() {
    const {
      type, form, errorMsg, categoryList, files,
    } = this.state;

    return (
      <div>
        <div className="box box-solid">
          <div className="box-header with-border">
            <div className="row mb-0">
              <div className="col-sm-12">
                <h3 className="box-title">{type === "create" ? "Tambah" : "Edit"} Produk</h3>
              </div>
            </div>
          </div>
          <div className="box-body">
            <div className="row mb-0">
              <div className="col-sm-5">
                <div className="row mb-sm">
                  <div className="col-sm-12">
                    <Select
                      label="Kategori"
                      data={categoryList}
                      value={String(form.id_category)}
                      changeEvent={val => this.changeValueHandler("id_category", val)}
                      errorText={errorMsg.id_category}
                    />
                  </div>
                </div>
                <div className="row mb-sm">
                  <div className="col-sm-12">
                    <InputText
                      label="Nama Produk"
                      changeEvent={val => this.changeValueHandler("name", val)}
                      value={String(form.name)}
                      errorText={errorMsg.name}
                    />
                  </div>
                </div>
                <div className="row mb-sm">
                  <div className="col-sm-12">
                    <InputCurrency
                      label="Harga"
                      changeEvent={val => this.changeValueHandler("price", val)}
                      value={String(form.price)}
                      errorText={errorMsg.price}
                    />
                  </div>
                </div>
                <div className="row mb-sm">
                  <div className="col-sm-12">
                    <InputNumber
                      label="Berat"
                      changeEvent={val => this.changeValueHandler("weight", val)}
                      value={form.weight}
                      errorText={errorMsg.weight}
                    />
                  </div>
                </div>
                <div className="row mb-sm">
                  <div className="col-sm-12">
                    <InputText
                      label="Stok Tersedia"
                      changeEvent={val => this.changeValueHandler("available_qty", val)}
                      value={String(form.available_qty)}
                      disabled={type === "edit"}
                    />
                  </div>
                </div>
                <div className="row mb-sm">
                  <div className="col-sm-12">
                    <InputText
                      label="Stok Minimum"
                      changeEvent={val => this.changeValueHandler("min_qty", val)}
                      value={String(form.min_qty)}
                    />
                  </div>
                </div>
                <div className="row mb-sm">
                  <div className="col-sm-12">
                    <Select2
                      label="Provinsi"
                      url="/api/v2/provinces"
                      placeholder="- Pilih Provinsi -"
                      value={form.provinceId}
                      changeEvent={this.changeIdProvinceHandler}
                      errorText={errorMsg.provinceId}
                    />
                  </div>
                </div>
                <div className="row mb-sm">
                  <div className="col-sm-12">
                    <Select2
                      label="Kab./Kota"
                      url="/api/v2/cities"
                      additionalParam={{ idProvince: form.provinceId }}
                      placeholder="- Pilih Kab./Kota -"
                      value={form.cityId}
                      changeEvent={this.changeIdCityHandler}
                      errorText={errorMsg.cityId}
                    />
                  </div>
                </div>
                <div className="row mb-sm">
                  <div className="col-sm-12">
                    <TextArea
                      label="Deskripsi"
                      changeEvent={val => this.changeValueHandler("description", val)}
                      value={String(form.description)}
                    />
                  </div>
                </div>
              </div>
              <div className="col-sm-7">
                <ImageDropzone value={files} changeEvent={this.changeFilesHandler} />
              </div>
            </div>
          </div>
          <div className="box-footer text-right">
            <div>
              <button type="button" className="btn btn-primary" onClick={this.saveHandler}>Simpan</button>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default CoreHoC(ProductDetail);
