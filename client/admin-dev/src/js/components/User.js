/* eslint prop-types: 0 */
import React from "react";
import CoreHoC from "./CoreHoC";

class User extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      title: "Pengguna",
    };
  }


  render() {
    const { title } = this.state;

    return (
      <div>
        <h1>{title}</h1>
      </div>
    );
  }
}

export default CoreHoC(User);
