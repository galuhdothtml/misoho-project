/* eslint prop-types: 0 */
// eslint-disable-next-line no-unused-vars
import React from "react";
import { useDropzone } from "react-dropzone";

const createId = () => (Math.random().toString(36).substring(2, 15) + Math.random().toString(36).substring(2, 15));
const ImageDropzone = ({ value, changeEvent }) => {
  const onDrop = (acceptedFiles) => {
    let newData = acceptedFiles.map(x => (Object.assign(x, { id: createId(), preview: URL.createObjectURL(x) })));
    newData = newData.filter(x => (!value.find(v => (String(x.path) === String(v.path)))));

    changeEvent([...value, ...newData]);
  };

  const removeImage = (id) => {
    const newData = value.filter(x => (String(x.id) !== String(id)));

    changeEvent(newData);
  };

  const { getRootProps, getInputProps, isDragActive } = useDropzone({ accept: "image/*", onDrop });

  const thumbs = value.map(file => (
    <div key={file.id} className="thumb-img-wrapper">
      <div className="thumb-img-inner">
        <img
          src={file.preview}
          className="thumb-img"
        />
        <button type="button" className="thumb-img-remove" onClick={() => removeImage(file.id)}><i className="fa fa-close" /></button>
      </div>
    </div>
  ));

  return (
    <div className="dropzone-container">
      <div className="dropzone-wrapper" {...getRootProps()}>
        <input {...getInputProps()} />
        {
          isDragActive
            ? <p>Drop the files here ...</p>
            : <p>Drag 'n' drop some files here, or click to select files</p>
        }
      </div>
      <div className="thumbs-container">
        {thumbs}
      </div>
    </div>
  );
};

export default ImageDropzone;
