/* eslint prop-types: 0 */
import React from "react";
import PropTypes from "prop-types";

class Select extends React.Component {
  static propTypes = {
    label: PropTypes.string,
    value: PropTypes.string,
    data: PropTypes.arrayOf(PropTypes.shape({})),
    changeEvent: PropTypes.func,
    errorText: PropTypes.string,
  }

  static defaultProps = {
    label: undefined,
    value: "",
    data: [],
    changeEvent: () => {},
    errorText: undefined,
  }

  changeHandler = (e) => {
    const { changeEvent } = this.props;

    changeEvent(e.target.value);
  }

  render() {
    const {
      label, value, errorText, data,
    } = this.props;

    return (
      <div className={`form-group mb-0 ${(errorText && errorText.length > 0) ? "has-error" : ""}`}>
        {label && <label>{label}</label>}
        <select value={value} className="form-control" onChange={this.changeHandler}>
            { data.map(x => (<option key={x.id} value={x.id}>{x.name}</option>)) }
        </select>
        { (errorText && errorText.length > 0) && <span className="help-block">{errorText}</span> }
      </div>
    );
  }
}

export default Select;
