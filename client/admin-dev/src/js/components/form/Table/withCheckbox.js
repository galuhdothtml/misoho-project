import React from "react";
import PropTypes from "prop-types";
import _ from "lodash";
import InputCheckbox from "../InputCheckbox";

const withCheckbox = WrappedComponent => class extends React.Component {
  static propTypes = {
    columns: PropTypes.arrayOf(PropTypes.shape({})),
    onFetch: PropTypes.func.isRequired,
    checkedEvent: PropTypes.func.isRequired,
    checkedValues: PropTypes.array.isRequired,
  }

  static defaultProps = {
    columns: [],
  }

  constructor(props) {
    super(props);

    this.state = {
      columns: [],
    };
  }

  onFetch = async (state) => {
    const { onFetch, checkedValues } = this.props;

    const { data: dt, pageTotal } = await onFetch(state);
    const newData = dt.map((x) => {
      const checked = !!(checkedValues.find(id => (String(x.id) === String(id))));

      return Object.assign({}, x, { checked });
    });

    const columns = this.setupColumns(newData);

    this.setState({ columns });

    return { data: newData, pageTotal };
  }

  changeCheckedHandler = (id, val) => {
    const { checkedEvent, checkedValues } = this.props;
    let checkedData = [];

    if (val) {
      checkedData.push(id);
    }

    const newCheckedValues = checkedValues.filter(x => (String(x) !== String(id)));
    checkedData = [...newCheckedValues, ...checkedData];
    checkedEvent(checkedData);
  }

  checkAllHandler = () => {
    const { checkedEvent, checkedValues } = this.props;
    const data = this.getData();
    const isCheckAll = !(data.find(x => (!x.checked)));
    let checkedData = [];
    const newCheckedValues = checkedValues.filter(x => !(data.find(dt => (String(dt.id) === String(x)))));
    if (!isCheckAll) {
      checkedData = data.map(x => (x.id));
    }

    checkedData = [...newCheckedValues, ...checkedData];

    checkedEvent(checkedData);
  }

  setupColumns = (data) => {
    const { columns: cols } = this.props;
    let isCheckAll = false;

    if (data.length > 0) {
      isCheckAll = !(data.find(x => (!x.checked)));
    }

    const newColumns = _.concat({
      Header: () => <InputCheckbox checked={isCheckAll} changeEvent={this.checkAllHandler} />,
      accessor: "checked",
      resizable: false,
      sortable: false,
      width: 40,
      Cell: ({ value, original: { id } }) => (<InputCheckbox checked={value} changeEvent={val => this.changeCheckedHandler(id, val)} />),
    }, cols);

    return newColumns;
  }

  refreshData = () => {
    this.table.refreshData();
  }

  resetCurrentPage = (callback) => {
    this.table.resetCurrentPage(callback);
  }

  getData = () => {
    if (this.table) {
      return this.table.getData();
    }

    return [];
  };

  setData = (data) => {
    this.table.setData(data);
  }

  render() {
    const { columns } = this.state;
    const props = Object.assign({}, this.props, { columns, onFetch: this.onFetch });

    return (
      <WrappedComponent ref={(c) => { this.table = c; }} {...props} />
    );
  }
};


export default withCheckbox;
