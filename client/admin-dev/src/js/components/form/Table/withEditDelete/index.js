import React from "react";
import PropTypes from "prop-types";
import _ from "lodash";
import EditDeleteComponent from "./EditDeleteComponent";

const withEditDelete = WrappedComponent => class extends React.Component {
  static propTypes = {
    columns: PropTypes.arrayOf(PropTypes.shape({})),
    editDeleteEvent: PropTypes.func.isRequired,
    onFetch: PropTypes.func.isRequired,
  }

  static defaultProps = {
    columns: [],
  }

  onFetch = async (state) => {
    const { onFetch } = this.props;

    const { data, pageTotal } = await onFetch(state);

    return { data: data.map(x => Object.assign(x, { option_column: x.id })), pageTotal };
  }

  setupColumns = () => {
    const { columns: cols, editDeleteEvent } = this.props;
    const columns = _.concat(cols, {
      Header: "",
      accessor: "option_column",
      sortable: false,
      resizable: false,
      width: 190,
      className: "option-column",
      Cell: ({ value }) => (<EditDeleteComponent id={value} editDeleteEvent={editDeleteEvent} />),
    });

    return columns;
  }

  refreshData = () => {
    this.table.refreshData();
  }

  resetCurrentPage = (callback) => {
    this.table.resetCurrentPage(callback);
  }

  getData = () => (this.table.getData())

  setData = (data, callback) => {
    this.table.setData(data, callback);
  }

  render() {
    const columns = this.setupColumns();
    const props = Object.assign({}, this.props, { columns, onFetch: this.onFetch });

    return (
      <WrappedComponent ref={(c) => { this.table = c; }} {...props} />
    );
  }
};


export default withEditDelete;
