/* eslint prop-types: 0 */
import React from "react";
import update from "immutability-helper";
import PropTypes from "prop-types";
import ReactTable from "react-table";
import ReactPaginate from "react-paginate";

require("react-table/react-table.css");

class Table extends React.Component {
  static propTypes = {
    onFetch: PropTypes.func.isRequired,
    columns: PropTypes.arrayOf(PropTypes.shape({})),
    showPagination: PropTypes.bool,
  }

  static defaultProps = {
    columns: [],
    showPagination: true,
  }

  constructor(props) {
    super(props);

    this.state = {
      data: [],
      isLoading: false,
      currentPayload: {},
      pageTotal: 0,
      currentPage: 0,
    };
  }

  componentWillMount = () => {
    this.refreshData();
  }

  refreshData = async () => {
    const { onFetch } = this.props;
    const { currentPage } = this.state;
    let { currentPayload } = this.state;
    currentPayload = update(currentPayload, {
      page: { $set: currentPage },
    });

    const { data, pageTotal } = await onFetch(currentPayload);

    this.setState({
      data, pageTotal, isLoading: false, currentPayload,
    });
  }

  resetCurrentPage = (callback) => {
    this.setState({ currentPage: 0 }, () => {
      callback();
    });
  }

  fetchDataHandler = async (state) => {
    const { currentPage } = this.state;
    const { onFetch } = this.props;
    const { sorted } = state;

    this.setState({ isLoading: true });

    const payload = {
      page: currentPage,
      sorted: (sorted.length > 0 ? sorted[0] : null),
    };

    const { data, pageTotal } = await onFetch(payload);

    this.setState({
      data, pageTotal, isLoading: false, currentPayload: payload,
    });
  }

  pageChangeHandler = async ({ selected }) => {
    const { onFetch } = this.props;
    const { currentPayload } = this.state;

    const payload = update(currentPayload, {
      page: { $set: selected },
    });

    const { data, pageTotal } = await onFetch(payload);

    this.setState({
      data, pageTotal, currentPage: selected, currentPayload: payload,
    });
  }

  getData = () => {
    const { data } = this.state;

    return data;
  }

  setData = (data, callback) => {
    this.setState({ data }, () => {
      if (callback) {
        callback();
      }
    });
  }

  render() {
    const { showPagination, columns } = this.props;
    const {
      data, isLoading, pageTotal, currentPage,
    } = this.state;

    return (
      <div>
        <ReactTable
          loading={isLoading}
          defaultPageSize={5}
          showPagination={false}
          data={data}
          columns={columns}
          manual
          onFetchData={this.fetchDataHandler}
        />
        {showPagination && <div className="row">
          <div className="col-sm-12 text-right">
            <ReactPaginate
              containerClassName="pagination"
              forcePage={currentPage}
              pageCount={pageTotal}
              onPageChange={this.pageChangeHandler}
              activeClassName="active"
              nextLabel="»"
              previousLabel="«"
            />
          </div>
        </div>}
      </div>
    );
  }
}

export default Table;
