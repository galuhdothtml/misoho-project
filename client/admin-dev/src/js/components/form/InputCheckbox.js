/* eslint prop-types: 0 */
import React from "react";
import PropTypes from "prop-types";

export default class InputCheckbox extends React.Component {
  static propTypes = {
    label: PropTypes.string,
    checked: PropTypes.bool,
    changeEvent: PropTypes.func,
  }

  static defaultProps = {
    label: "",
    checked: false,
    changeEvent: () => { },
  }

  changeHandler = (e) => {
    const { changeEvent } = this.props;

    changeEvent(e.target.checked);
  }

  render() {
    const { label, checked } = this.props;

    return (
      <div className="checkbox-wrap">
        <label className="checkbox-container">{label}&nbsp;
          <input
            type="checkbox"
            checked={checked}
            onChange={this.changeHandler}
          />
          <span className="checkmark"></span>
        </label>
      </div>
    );
  }
}
