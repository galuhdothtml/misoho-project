/* eslint prop-types: 0 */
import React from "react";
import PropTypes from "prop-types";

class DaterangePicker extends React.Component {
  static propTypes = {
    id: PropTypes.string.isRequired,
    label: PropTypes.string,
    errorText: PropTypes.string,
    value: PropTypes.shape({
      start: PropTypes.string,
      end: PropTypes.string,
    }).isRequired,
  }

  static defaultProps = {
    label: undefined,
    errorText: undefined,
  }

  componentDidMount = () => {
    const { id, value, changeEvent } = this.props;
    const $daterangepicker = $(`#${id}`);
    $daterangepicker.daterangepicker({
      startDate: moment(value.start, "YYYY-MM-DD"),
      endDate: moment(value.end, "YYYY-MM-DD"),
      locale: {
        format: "DD MMM YYYY",
      },
    });
    $daterangepicker.on("apply.daterangepicker", (ev, picker) => {
      const payload = {
        start: picker.startDate.format("YYYY-MM-DD"),
        end: picker.endDate.format("YYYY-MM-DD"),
      };
      changeEvent(payload);
    });
  }

  render() {
    const {
      label, errorText, id,
    } = this.props;

    return (
      <div className={`form-group mb-0 ${(errorText && errorText.length > 0) ? "has-error" : ""}`}>
        {label && <label>{label}</label>}
        <div className="input-group">
          <div className="input-group-addon">
            <i className="fa fa-calendar"></i>
          </div>
          <input type="text" className="form-control pull-right" id={id} />
        </div>
        {(errorText && errorText.length > 0) && <span className="help-block">{errorText}</span>}
      </div>
    );
  }
}

export default DaterangePicker;
