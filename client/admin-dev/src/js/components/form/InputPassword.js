/* eslint prop-types: 0 */
import React from "react";
import PropTypes from "prop-types";

class InputText extends React.Component {
  static propTypes = {
    label: PropTypes.string,
    value: PropTypes.string,
    placeholder: PropTypes.string,
    changeEvent: PropTypes.func,
    errorText: PropTypes.string,
    disabled: PropTypes.bool,
  }

  static defaultProps = {
    label: undefined,
    value: "",
    placeholder: "",
    changeEvent: () => {},
    errorText: undefined,
    disabled: false,
  }

  changeHandler = (e) => {
    const { changeEvent } = this.props;

    changeEvent(e.target.value);
  }

  render() {
    const {
      label, value, placeholder, errorText, disabled,
    } = this.props;

    return (
      <div className={`form-group mb-0 ${(errorText && errorText.length > 0) ? "has-error" : ""}`}>
        {label && <label>{label}</label>}
        <input
          type="password"
          className="form-control"
          placeholder={placeholder}
          value={value}
          onChange={this.changeHandler}
          disabled={disabled}
        />
        { (errorText && errorText.length > 0) && <span className="help-block">{errorText}</span> }
      </div>
    );
  }
}

export default InputText;
