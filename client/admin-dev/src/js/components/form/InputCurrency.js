/* eslint prop-types: 0 */
import React from "react";
import numeral from "numeral";
import PropTypes from "prop-types";

class InputCurrency extends React.Component {
  static propTypes = {
    label: PropTypes.string,
    value: PropTypes.string,
    placeholder: PropTypes.string,
    changeEvent: PropTypes.func,
    errorText: PropTypes.string,
    disabled: PropTypes.bool,
  }

  static defaultProps = {
    label: undefined,
    value: "",
    placeholder: "",
    changeEvent: () => { },
    errorText: undefined,
    disabled: false,
  }

  constructor(props) {
    super(props);

    this.state = {
      isFocusing: false,
    };
  }

  changeHandler = (e) => {
    const { changeEvent } = this.props;
    const { value } = e.target;
    const isNumber = /^\d*$/.test(value);

    if (isNumber) {
      changeEvent(e.target.value);
    }
  }

  onFocusHandler = () => {
    this.setState({ isFocusing: true });
  }

  onBlurHandler = () => {
    this.setState({ isFocusing: false });
  }

  createInputComponent = () => {
    const {
      value, placeholder, disabled,
    } = this.props;
    const { isFocusing } = this.state;

    if (isFocusing) {
      return (
        <input
          type="text"
          className="form-control"
          placeholder={placeholder}
          value={value}
          onChange={this.changeHandler}
          disabled={disabled}
          onBlur={this.onBlurHandler}
        />
      );
    }

    let defaultValue = "";

    if (String(value).length > 0) {
      defaultValue = numeral(value).format("0,0");
    }

    return (
      <input
        type="text"
        className="form-control"
        placeholder={placeholder}
        value={defaultValue}
        onChange={() => {}}
        disabled={disabled}
        onFocus={this.onFocusHandler}
      />
    );
  }

  render() {
    const {
      label, errorText,
    } = this.props;

    return (
      <div className={`form-group mb-0 ${(errorText && errorText.length > 0) ? "has-error" : ""}`}>
        {label && <label>{label}</label>}
        {this.createInputComponent()}
        {(errorText && errorText.length > 0) && <span className="help-block">{errorText}</span>}
      </div>
    );
  }
}

export default InputCurrency;
