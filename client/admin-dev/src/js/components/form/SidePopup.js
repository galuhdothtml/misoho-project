/* eslint prop-types: 0 */
import React from "react";
import PropTypes from "prop-types";
import posed from "react-pose";
import ScrollArea from "react-scrollbar";

const style = {
  modalBody: {
    height: `${(window.innerHeight - 120)}px`,
    padding: "0px",
  },
  modalScrollArea: {
    height: "100%",
    width: "auto",
    padding: "0px",
  },
  modalScrollAreaContent: {
    padding: "16px",
  },
};
const Box = posed.div({
  hidden: {
    opacity: 0,
    transition: { duration: 300 },
  },
  visible: {
    opacity: 1,
    transition: { duration: 300 },
  },
});
const InnerBox = posed.div({
  hidden: {
    right: "-500px",
    transition: { duration: 300 },
  },
  visible: {
    right: "0px",
    transition: { duration: 300 },
  },
});
class SidePopup extends React.Component {
  static propTypes = {
    saveEvent: PropTypes.func,
    onHide: PropTypes.func,
    children: PropTypes.node,
    title: PropTypes.string,
  }

  static defaultProps = {
    saveEvent: () => {},
    onHide: () => {},
    children: null,
    title: undefined,
  }

  constructor(props) {
    super(props);

    this.state = {
      isVisible: false,
    };
  }

  componentDidMount() {
    this.setState({ isVisible: true });
  }

  saveHandler = (e) => {
    const { saveEvent } = this.props;

    if (e) {
      e.preventDefault();
    }

    saveEvent(this.hidePopup);
  }

  hidePopup = () => {
    const { onHide } = this.props;
    this.setState({ isVisible: false }, () => {
      setTimeout(() => {
        onHide();
      }, 300);
    });
  }

  render() {
    const { isVisible } = this.state;
    const { children, title } = this.props;

    return (
      <div className="sidepopup-container">
        <Box className="modal" pose={isVisible ? "visible" : "hidden"} style={{ display: "block" }}>
          <InnerBox className="modal-dialog" pose={isVisible ? "visible" : "hidden"} style={{ width: "500px" }}>
            <div className="modal-content">
              { title && <div className="modal-header">
                  <h4 className="modal-title">{title}</h4>
                </div>
              }
              <div className="modal-body" style={style.modalBody}>
                <ScrollArea
                  smoothScrolling={false}
                  className="modal-scrollarea"
                  style={style.modalScrollArea}
                  contentStyle={style.modalScrollAreaContent}
                  horizontal={false}
                  stopScrollPropagation={true}
                >
                  <form onSubmit={this.saveHandler}>
                  {children}
                  </form>
                </ScrollArea>
              </div>
              <div className="modal-footer">
                <div className="btn pull-left sidepopup-note">
                  <div className="text-muted">Kolom bertanda <span className="text-red">*</span> wajib diisi</div>
                </div>
                <button
                  type="button"
                  className="btn btn-default"
                  onClick={this.hidePopup}>Batal</button>
                <button
                  type="button"
                  className="btn btn-primary"
                  onClick={this.saveHandler}>Simpan</button>
              </div>
            </div>
          </InnerBox>
        </Box>
      </div>
    );
  }
}

export default SidePopup;
