import React from "react";
import update from "immutability-helper";
import { uploadFiles } from "../../data";
import Util from "../../utils";
import LoadingGif from "../../images/loading.gif";

const createId = () => (Math.random().toString(36).substring(2, 4) + Math.random().toString(36).substring(2, 4));
class ImageDropzone extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      uploadLength: 12,
      data: [],
    };
  }

  componentDidMount = () => {
    this.setDataFromProps(this.props);
  }

  componentWillReceiveProps = (nextProps) => {
    const { value } = this.props;

    if (value !== nextProps.value) {
      this.setDataFromProps(nextProps);
    }
  }

  setDataFromProps = (props) => {
    const { data } = this.state;

    if (data.length > 0) {
      this.doSettingData(props);
    } else {
      this.prepareDefaultData();
    }
  }

  doSettingData = (props) => {
    const { data } = this.state;
    let newData = data;

    for (let i = 0; i < props.value.length; i += 1) {
      const filename = props.value[i];
      const findIndex = data.findIndex(x => (x.filename === filename));

      if (findIndex > -1) {
        newData = update(newData, {
          [findIndex]: {
            $set: {
              id: newData[findIndex].id,
              value: Util.createPathPreview(newData[findIndex].value),
              filename,
            },
          },
        });
      } else {
        newData = update(newData, {
          [i]: {
            $set: {
              id: createId(),
              value: Util.createPathPreview(`public/uploads/${filename}`),
              filename,
            },
          },
        });
      }
    }

    this.setState({ data: newData });
  }

  prepareDefaultData = () => {
    const { uploadLength } = this.state;
    const newData = [];

    for (let i = 0; i < uploadLength; i += 1) {
      newData.push({ id: createId(), value: "" });
    }

    this.setState({ data: newData });
  }

  renderUploadComponent = () => {
    const { data } = this.state;
    const retval = data.map((x, i) => {
      let customStyle = {};
      let content = (<i className="fa fa-camera" />);

      if (Util.ishasProperty(x, "loading")) {
        content = (<div><img src={LoadingGif} className="upload-file-box__loading" /><div>Uploading ...</div></div>);
      } else if (x.value.length > 0) {
        customStyle = { border: "0px" };
        const style = {
          backgroundImage: `url(${x.value})`,
        };
        content = (
          <div style={style} className="upload-file-box__image">
            <div className="upload-file-box__image-delete" onClick={(e) => { this.removeFileData(e, i); }}><i className="fa fa-close" /></div>
          </div>
        );
      }

      return (
        <div key={x.id} onClick={this.chooseFile} style={customStyle} className="upload-file-box">
            {content}
        </div>
      );
    });

    return retval;
  }

  preventDefaultHandler = (e) => {
    e.preventDefault();
    e.stopPropagation();
  }

  removeFileData = (e, index) => {
    e.preventDefault();
    e.stopPropagation();
    const { changeEvent } = this.props;
    const { data } = this.state;
    let newData = data.filter((x, i) => (i !== index));
    newData = [...newData, { id: createId(), value: "" }];

    this.setState({ data: newData }, () => {
      const filteredData = newData.filter(x => (Util.ishasProperty(x, "filename"))).map(x => (x.filename));
      changeEvent(filteredData);
    });
  }

  handleDrop = (e) => {
    e.preventDefault();
    e.stopPropagation();
    let files = [...e.dataTransfer.files];
    files = files.filter(x => (x.type.includes("image/")));

    this.startLoading(files, () => {
      this.doUploadingFiles(files);
    });

    e.dataTransfer.clearData();
  }

  startLoading = (files, callback) => {
    const { data } = this.state;
    let newData = data;

    files.forEach(() => {
      const findIndex = newData.findIndex(d => (!Util.ishasProperty(d, "loading") && (d.value.length === 0)));

      if (findIndex > -1) {
        newData = update(newData, {
          [findIndex]: {
            $set: {
              id: newData[findIndex].id,
              loading: true,
            },
          },
        });
      }
    });

    this.setState({ data: newData }, () => {
      if (callback) {
        callback();
      }
    });
  }

  finishLoading = (files) => {
    const { changeEvent } = this.props;
    const { data } = this.state;
    let newData = data;

    files.forEach((x) => {
      const findIndex = newData.findIndex(d => (Util.ishasProperty(d, "loading")));

      if (findIndex > -1) {
        newData = update(newData, {
          [findIndex]: {
            $set: {
              id: newData[findIndex].id,
              value: Util.createPathPreview(x.path),
              filename: x.filename,
            },
          },
        });
      }
    });

    this.setState({ data: newData }, () => {
      const filteredData = newData.filter(x => (Util.ishasProperty(x, "filename"))).map(x => (x.filename));
      changeEvent(filteredData);
    });
  }

  doUploadingFiles = async (files) => {
    const res = await uploadFiles(files);
    if (res.status) {
      this.finishLoading(res.data);
    }
  }

  chooseFile = () => {
    this.fileUpload.click();
  }

  changeFileHandler = () => {
    const filesData = [];
    const { files } = this.fileUpload;

    Array.from(files).forEach((file) => {
      filesData.push(file);
    });

    this.startLoading(filesData, () => {
      this.doUploadingFiles(filesData);
    });

    this.fileUpload.value = null;
  }

  render() {
    return (
      <div>
        <label style={{ marginLeft: "5px" }}>Unggah Hingga 12 Foto</label>
        <div
          style={{ width: "32em" }}
          onDrop={this.handleDrop}
          onDragOver={this.preventDefaultHandler}
          onDragEnter={this.preventDefaultHandler}
          onDragLeave={this.preventDefaultHandler}
        >{this.renderUploadComponent()}
        <input ref={(c) => { this.fileUpload = c; }} onChange={this.changeFileHandler} className="hidden" type="file" multiple />
        </div>
      </div>
    );
  }
}

export default ImageDropzone;
