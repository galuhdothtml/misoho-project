/* eslint prop-types: 0 */
import React from "react";
import PropTypes from "prop-types";
import ImgPlaceholder from "../../images/img-placeholder.png";

class UploadSingleImage extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      previewImg: ImgPlaceholder,
    };
  }

  componentWillMount = () => {
    const { value } = this.props;

    if (value) {
      this.setPreviewImg(value);
    }
  }

  componentWillReceiveProps = (nextProps) => {
    const { value: pValue } = this.props;

    if (pValue !== nextProps.value) {
      this.setPreviewImg(nextProps.value);
    }
  }

  setPreviewImg = (file) => {
    const reader = new FileReader();
    reader.addEventListener("load", () => {
      this.setState({ previewImg: reader.result });
    }, false);

    if (file) {
      reader.readAsDataURL(file);
    }
  }

  buttonClickHandler = () => {
    this.fileUpload.click();
  }

  changeFileHandler = () => {
    const { changeEvent } = this.props;
    const file = this.fileUpload.files[0];
    const reader = new FileReader();
    reader.addEventListener("load", () => {
      this.setState({ previewImg: reader.result }, () => {
        changeEvent(file);
      });
    }, false);

    if (file) {
      reader.readAsDataURL(file);
    }
  }

  render() {
    const { previewImg } = this.state;

    return (
      <div className="upload-single-image">
        <input ref={(c) => { this.fileUpload = c; }} onChange={this.changeFileHandler} className="hidden" type="file" accept="image/*" />
        <div>
          <img src={previewImg} className="upload-single-image__img" />
        </div>
        <button type="button" className="btn btn-default btn-block upload-single-image__button" onClick={this.buttonClickHandler}>Pilih Gambar</button>
      </div>
    );
  }
}

export default UploadSingleImage;
