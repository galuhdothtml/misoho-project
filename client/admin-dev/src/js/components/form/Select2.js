/* eslint prop-types: 0 */
import React from "react";
import PropTypes from "prop-types";
import Util from "../../utils";

const createId = () => (Math.random().toString(36).substring(2, 15) + Math.random().toString(36).substring(2, 15));
class Select2 extends React.Component {
  static propTypes = {
    label: PropTypes.string,
    value: PropTypes.string,
    url: PropTypes.string.isRequired,
    additionalParam: PropTypes.shape({}),
    changeEvent: PropTypes.func,
    errorText: PropTypes.string,
    placeholder: PropTypes.string,
  }

  static defaultProps = {
    label: undefined,
    value: "",
    changeEvent: () => { },
    errorText: undefined,
    placeholder: "",
    additionalParam: null,
  }

  constructor(props) {
    super(props);

    this.state = {
      id: createId(),
      $eventSelect: null,
      ajaxOptions: null,
      selectData: [],
    };
  }

  componentDidMount = () => {
    this.createSelect(this.props, () => {
      this.setupComponent(this.props);
    });
  }

  componentWillReceiveProps = (nextProps) => {
    const { value, additionalParam } = this.props;

    if (nextProps.additionalParam && JSON.stringify(additionalParam) !== JSON.stringify(nextProps.additionalParam)) {
      this.createSelect(nextProps, () => {
        this.setupComponent(nextProps);
      });
    }

    if (String(value) !== String(nextProps.value)) {
      this.createSelect(nextProps, () => {
        this.setupComponent(nextProps);
      });
    }
  }

  createSelect = ({ placeholder, url, additionalParam }, callback) => {
    const {
      id,
    } = this.state;
    const ajaxOptions = Util.createAjax(url, additionalParam);
    // eslint-disable-next-line no-undef
    const $eventSelect = $(`#${id}.select2`);
    $eventSelect.select2({
      placeholder,
      allowClear: true,
      ajax: ajaxOptions,
      minimumInputLength: 3,
    });

    this.setState({ $eventSelect, ajaxOptions }, () => {
      if (callback) {
        callback();
      }
    });
  }

  setupComponent = (props) => {
    const {
      value, changeEvent,
    } = props;
    const { $eventSelect, ajaxOptions, selectData } = this.state;
    $eventSelect.val("");
    $eventSelect.trigger("change");
    $eventSelect.on("change", (e) => {
      setTimeout(() => {
        if (e.target.value) {
          const newSelectData = $eventSelect.select2("data");

          this.setState({ selectData: newSelectData });

          changeEvent(e.target.value);
        }
      });
    });
    $eventSelect.on("select2:unselect", () => {
      setTimeout(() => {
        changeEvent("");
      });
    });

    if (value && String(value).length > 0) {
      if (selectData.length > 0) {
        const newOption = new Option(selectData[0].text, selectData[0].id, true, true);
        $eventSelect.append(newOption);
      } else {
        // eslint-disable-next-line no-undef
        $.ajax({ // make the request for the selected data object
          headers: ajaxOptions.headers,
          type: "GET",
          url: ajaxOptions.url,
          dataType: "json",
          data: {
            page: 1,
            limit: 5,
            orId: value,
          },
        }).then((res) => {
          if (res.status) {
            const { data } = res;
            if (data.length > 0) {
              const newOption = new Option(data[0].name, data[0].id, true, true);
              $eventSelect.append(newOption);
            }
          }
        });
      }
    }
  }

  render() {
    const {
      label, value, errorText,
    } = this.props;
    const { id } = this.state;

    return (
      <div className={`form-group mb-0 ${(errorText && errorText.length > 0) ? "has-error" : ""}`}>
        {label && <label>{label}</label>}
        <select id={id} defaultValue={value} className="form-control select2" />
        {(errorText && errorText.length > 0) && <span className="help-block">{errorText}</span>}
      </div>
    );
  }
}

export default Select2;
