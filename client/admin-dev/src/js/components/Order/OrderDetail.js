/* eslint prop-types: 0 */
import React from "react";
import update from "immutability-helper";
import CoreHoC from "../CoreHoC";
import {
  getOrderDetail,
  processOrder,
  shipOrder,
} from "../../data";
import Util from "../../utils";
import ModalPopup from "../form/ModalPopup";
import InputText from "../form/InputText";

const errorContent = (type, val) => {
  let retval = null;

  if (type === "shipmentNumber") {
    if (String(val).trim().length === 0) {
      retval = "* Nomor resi pengiriman wajib diisi";
    }
  }

  return retval;
};
class OrderDetail extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      id: "",
      reference: "",
      receiver_name: "",
      address_delivery: "",
      postcode: "",
      order_state: "",
      orderStateText: "",
      orderHistoryData: [],
      orderDetailData: [],
      shipping: {
        courier: "",
        service: "",
        shipment_number: "",
      },
      subtotal: 0,
      totalShipping: 0,
      grandTotal: 0,
      isModalShowing: false,
      formShipment: {
        shipmentNumber: "",
      },
      errorShipmentMsg: {
        shipmentNumber: "",
      },
      isValidated: false,
    };
  }

  componentWillMount = () => {
    const {
      match: {
        params
      },
    } = this.props;

    this.setupData(params.id);
  }

  setupData = async (id) => {
    const res = await getOrderDetail(id);

    if (res.status) {
      const { data } = res;

      // eslint-disable-next-line no-undef
      const newOrderHistory = data.order_history.map(x => Object.assign({}, x, { createdAt: moment(x.createdAt).format("DD MMMM YYYY HH:mm:ss") }));

      this.setState({
        id: data.id,
        reference: data.reference,
        receiver_name: data.receiver_name,
        address_delivery: data.address_delivery_text,
        postcode: data.postcode ? data.postcode : "-",
        order_state: data.order_state,
        orderStateText: data.order_state_text,
        orderHistoryData: newOrderHistory,
        orderDetailData: data.orderDetailData,
        shipping: JSON.parse(data.shipping),
        subtotal: data.subtotal,
        totalShipping: data.total_shipping,
        grandTotal: data.grand_total,
        shipmentNumber: "",
      });
    }
  }

  changeShipmentNumber = (val) => {
    const { formShipment: form } = this.state;
    const newData = update(form, {
      shipmentNumber: { $set: val },
    });

    const errorMsg = this.createErrorMessage("shipmentNumber", val);
    this.setState({ formShipment: newData, errorShipmentMsg: errorMsg });
  }

  bulkCreateErrorMessage = () => {
    const { formShipment: form, errorShipmentMsg: errorMsg } = this.state;
    const keys = Object.keys(errorMsg);
    let newErrorMsg = errorMsg;
    let isError = false;

    keys.forEach((x) => {
      let error = "";

      const ERROR_TEXT = errorContent(x, form[x]);

      if (ERROR_TEXT) {
        error = ERROR_TEXT;
        isError = true;
      }

      newErrorMsg = update(newErrorMsg, {
        [x]: { $set: error },
      });
    });

    return { msg: newErrorMsg, isError };
  }

  createErrorMessage = (type, val) => {
    const { errorShipmentMsg: errorMsg, isValidated } = this.state;
    let error = "";

    if (!isValidated) {
      return errorMsg;
    }

    const ERROR_TEXT = errorContent(type, val);

    if (ERROR_TEXT) {
      error = ERROR_TEXT;
    }

    const newValue = update(errorMsg, {
      [type]: { $set: error },
    });

    return newValue;
  }

  processOrderHandler = async (callback) => {
    const { id, order_state: orderState, formShipment } = this.state;
    const payload = {
      id,
    };

    if (String(orderState) === "2") { // sudah dibayar
      const res = await processOrder(payload);

      if (res.status) {
        // eslint-disable-next-line no-restricted-globals
        location.reload();
      } else {
        callback();
      }
    } else if (String(orderState) === "3") { // sedang di proses
      const error = this.bulkCreateErrorMessage();
      const isValid = !error.isError;

      this.setState({ isValidated: true, errorShipmentMsg: error.msg });

      if (!isValid) {
        callback();
        return;
      }

      Object.assign(payload, { shipment_number: formShipment.shipmentNumber });
      const res = await shipOrder(payload);

      if (res.status) {
        // eslint-disable-next-line no-restricted-globals
        location.reload();
      } else {
        callback();
      }
    }
  }

  createButton = () => {
    const { order_state: val } = this.state;
    let retval = <button style={{ width: "100px" }} type="button" className="btn btn-default">OK</button>;

    if (String(val) === "2") {
      retval = <button type="button" className="btn btn-primary" onClick={this.showModal}>Proses</button>;
    } else if (String(val) === "3") {
      retval = <button type="button" className="btn btn-primary" onClick={this.showModal}>Kirim</button>;
    }

    return retval;
  }

  showModal = () => {
    this.setState({ isModalShowing: true });
  }

  hideModal = () => {
    this.setState({ isModalShowing: false });
  }

  modaPopupContent = () => {
    const {
      order_state: orderState, reference, shipping, formShipment,
      errorShipmentMsg,
    } = this.state;
    let retval = (<div></div>);

    if (String(orderState) === "2") {
      retval = (<div>Anda yakin ingin memproses order <b>{reference}</b></div>);
    } else if (String(orderState) === "3") {
      retval = (
        <div>
          <div className="row mb-sm" style={{ fontSize: "16px" }}>
            <div className="col-sm-12">Pengiriman :</div>
            <div className="col-sm-12">{`${shipping.courier} - ${shipping.service}`}</div>
          </div>
          <div className="row mb-sm">
            <div className="col-sm-12">
              <InputText
                label="Nomor Resi Pengiriman"
                changeEvent={this.changeShipmentNumber}
                value={formShipment.shipmentNumber}
                errorText={errorShipmentMsg.shipmentNumber}
              />
            </div>
          </div>
        </div>
      );
    }

    return retval;
  }

  render() {
    const {
      reference,
      receiver_name: receiverName,
      address_delivery: addressDelivery,
      postcode, orderHistoryData, orderDetailData,
      shipping, orderStateText,
      subtotal, totalShipping, grandTotal,
      isModalShowing, order_state: orderState,
    } = this.state;

    return (
      <div>
        <div className="box box-solid">
          <div className="box-header with-border">
            <div className="row mb-0">
              <div className="col-sm-12">
                <h3 className="box-title">Pesanan #{reference}</h3>
              </div>
            </div>
          </div>
          <div className="box-body">
            <div className="row mb-0">
              <div className="col-sm-6">
                <div className="form-group">
                  <label className="bold">Nama Penerima :</label>
                  <div>{receiverName}</div>
                </div>
                <div className="form-group">
                  <label className="bold">Alamat Pengiriman :</label>
                  <div>{addressDelivery}</div>
                </div>
                <div className="form-group">
                  <label className="bold">Kode Pos :</label>
                  <div>{postcode}</div>
                </div>
              </div>
              <div className="col-sm-6">
                <div className="row">
                  <div className="col-sm-6">
                    <div className="form-group">
                      <label className="bold">Status Pesanan :</label>
                      <div>{orderStateText}</div>
                    </div>
                  </div>
                  <div className="col-sm-6 text-right">
                    <div className="form-group">
                      <label className="bold">Nomor Pesanan :</label>
                      <div>{reference}</div>
                    </div>
                  </div>
                </div>
                <hr className="divider" />
                <div className="form-group">
                  <label className="bold">Histori Pesanan :</label>
                  <div>
                    <table className="table table-bordered">
                      <thead>
                        <tr>
                          <th>Waktu</th>
                          <th>Status Pesanan</th>
                        </tr>
                      </thead>
                      <tbody>
                        {orderHistoryData.map(x => (
                          <tr key={x.id}>
                            <td>{x.createdAt}</td>
                            <td>{x.order_state_text}</td>
                          </tr>
                        ))}
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
            <div>
              <div className="row">
                <div className="col-sm-12">
                  <div className="form-group">
                    <label className="bold">Produk :</label>
                    <div>
                      <table className="table table-bordered">
                        <thead>
                          <tr>
                            <th>Kode</th>
                            <th>Nama Produk</th>
                            <th>Harga</th>
                            <th>Jumlah</th>
                            <th>Subtotal</th>
                          </tr>
                        </thead>
                        <tbody>
                          {orderDetailData.map(x => (
                            <tr key={x.id}>
                              <td>{x.id}</td>
                              <td>{x.product_name}</td>
                              <td>{Util.currency(x.product_price)}</td>
                              <td>{Util.currency(x.qty)}</td>
                              <td>{Util.currency(x.product_price * x.qty)}</td>
                            </tr>
                          ))}
                        </tbody>
                      </table>
                    </div>
                  </div>
                </div>
              </div>
              <div className="row" style={{ fontSize: "16px" }}>
                <div className="col-sm-8">
                  <div className="row mb-sm">
                    <div className="col-sm-12">Pengiriman :</div>
                    <div className="col-sm-12">{`${shipping.courier} - ${shipping.service}`}</div>
                  </div>
                  {(String(orderState) === "4") && (
                    <div className="row mb-sm">
                      <div className="col-sm-12">Nomor Resi Pengiriman :</div>
                      <div className="col-sm-12">{shipping.shipment_number}</div>
                    </div>
                  )}
                </div>
                <div className="col-sm-4 text-right">
                  <div className="row mb-sm">
                    <div className="col-sm-6">Total Belanja :</div>
                    <div className="col-sm-6">{Util.currency(subtotal)}</div>
                  </div>
                  <div className="row mb-sm">
                    <div className="col-sm-6">Biaya Pengiriman :</div>
                    <div className="col-sm-6">{Util.currency(totalShipping)}</div>
                  </div>
                  <div className="row mb-sm">
                    <div className="col-sm-6">Total Terbayar :</div>
                    <div className="col-sm-6">{Util.currency(grandTotal)}</div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div className="box-footer text-right">
            <div>
              {this.createButton()}
            </div>
          </div>
        </div>
        {isModalShowing && (
          <ModalPopup
            width={340}
            title="Proses Order"
            hideModal={this.hideModal}
            saveHandler={this.processOrderHandler}>
            {this.modaPopupContent()}
          </ModalPopup>
        )}
      </div>
    );
  }
}

export default CoreHoC(OrderDetail);
