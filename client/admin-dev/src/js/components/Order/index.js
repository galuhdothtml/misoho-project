import React from "react";
import PropTypes from "prop-types";
import moment from "moment";
import CoreHoC from "../CoreHoC";
import Table from "../form/Table";
import {
  getOrders,
} from "../../data";
import Util from "../../utils";
import InputText from "../form/InputText";
import Select from "../form/Select";

const getOrderStateName = (val) => {
  let retval = "";

  if (String(val) === "1") {
    retval = "Menunggu Pembayaran";
  } else if (String(val) === "2") {
    retval = "Terbayar";
  } else if (String(val) === "3") {
    retval = "Sedang Diproses";
  } else if (String(val) === "4") {
    retval = "Sudah Dikirim";
  } else if (String(val) === "5") {
    retval = "Sudah Diterima";
  }

  return retval;
};
const columns = editEvent => [
  {
    Header: "Kode",
    accessor: "id",
    resizable: false,
    width: 80,
  },
  {
    Header: "Nomor Order",
    accessor: "reference",
    resizable: false,
  },
  {
    Header: "Pelanggan",
    accessor: "Customer.name",
    resizable: false,
  },
  {
    Header: "Total Pesanan",
    accessor: "grand_total",
    resizable: false,
  },
  {
    Header: "Status Pesanan",
    accessor: "order_state_name",
    resizable: false,
  },
  {
    Header: "Tanggal Dibuat",
    accessor: "createdAt",
    resizable: false,
  },
  {
    Header: "",
    accessor: "edit_column",
    sortable: false,
    resizable: false,
    width: 150,
    className: "option-column",
    // eslint-disable-next-line react/prop-types
    Cell: ({ value }) => (
      <button
            onClick={() => editEvent(value)}
            type="button"
            className="btn btn-default btn-sm text-center table-option"><i className="fa fa-edit" /> Detail Pesanan</button>
    ),
  },
];
class Product extends React.Component {
  static propTypes = {
    assignCalendar: PropTypes.func.isRequired,
    assignButtons: PropTypes.func.isRequired,
    showNotification: PropTypes.func,
  }

  static defaultProps = {
    showNotification: () => { },
  }

  constructor(props) {
    super(props);

    this.state = {
      data: [],
      filterText: "",
      limitValue: "5",
      limitData: [
        { id: "5", name: "5" },
        { id: "10", name: "10" },
        { id: "20", name: "20" },
        { id: "50", name: "50" },
        { id: "100", name: "100" },
      ],
    };
  }

  fetchData = async (state) => {
    const { filterText, limitValue } = this.state;
    const payload = {
      page: state.page + 1,
      limit: limitValue,
    };

    if (state.sorted) {
      Object.assign(payload, {
        sorted: JSON.stringify(state.sorted),
      });
    }

    if (filterText && filterText.length > 0) {
      Object.assign(payload, {
        filterText,
      });
    }

    const res = await getOrders(payload);

    if (res.status) {
      const newData = res.data.map(x => (Object.assign({}, x, {
        edit_column: x.id,
        order_state_name: getOrderStateName(x.order_state),
        grand_total: Util.currency(x.grand_total),
        createdAt: moment(x.createdAt).format("DD MMM YYYY HH:mm:ss"),
      })));

      return { data: newData, pageTotal: res.page_total };
    }

    return { data: [], pageTotal: 0 };
  }

  changeFilterTextHandler = (val) => {
    this.setState({ filterText: val });

    clearTimeout(this.filterIdle);
    this.filterIdle = setTimeout(() => {
      this.table.resetCurrentPage(() => {
        this.setState({ filterText: val }, () => {
          this.table.refreshData();
        });
      });
    }, 500);
  }

  changeLimitValueHandler = (val) => {
    this.table.resetCurrentPage(() => {
      this.setState({ limitValue: val }, () => {
        this.table.refreshData();
      });
    });
  }

  editValueHandler = (id) => {
    const { history } = this.props;

    history.push(`pesanan-pesanan/detail/${id}`);
  }

  render = () => {
    const {
      filterText,
      limitValue,
      limitData,
    } = this.state;

    return (
      <div>
        <div className="box box-solid">
          <div className="box-header with-border">
            <div className="row mb-0">
              <div className="col-sm-8">
                <h3 className="box-title with-title">Pesanan</h3>
              </div>
              <div className="col-sm-4">
                <div className="row">
                  <div className="col-sm-9">
                    <InputText
                      placeholder="Pencarian ..."
                      changeEvent={this.changeFilterTextHandler}
                      value={filterText}
                    />
                  </div>
                  <div className="col-sm-3 pl-0">
                    <Select
                      data={limitData}
                      value={limitValue}
                      changeEvent={this.changeLimitValueHandler}
                    />
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div className="box-body">
            <Table
              ref={(c) => { this.table = c; }}
              columns={columns(this.editValueHandler)}
              onFetch={this.fetchData}
              showPagination={true}
            />
          </div>
        </div>
      </div>
    );
  }
}

export default CoreHoC(Product);
