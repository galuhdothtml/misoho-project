import React from "react";
import { Link } from "react-router-dom";
import ProfilePhoto from "../../../assets/template/img/user2-160x160.jpg";
import Logo from "../../images/logo.png";

const style = {
  profile_photo: {
    marginLeft: "10px",
  },
};
class Header extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      fullname: "Administrator",
      showUserMenu: false,
    };
  }

  toggleUserMenu = (e) => {
    const { showUserMenu } = this.state;

    e.preventDefault();

    this.setState({ showUserMenu: !showUserMenu });
  }

  render() {
    const { fullname, showUserMenu } = this.state;

    return (
      <header className="main-header">
        <a href="#" className="logo">
          <span className="logo-lg"><img src={Logo} /></span>
        </a>
        <nav className="navbar navbar-static-top">
          <div className="navbar-custom-menu" style={{ float: "none" }}></div>
          <div className="navbar-custom-menu">
            <ul className="nav navbar-nav">
              <li className={`dropdown user user-menu ${showUserMenu ? "open" : ""}`}>
                <a onClick={this.toggleUserMenu} href="#" className="dropdown-toggle">
                  <span className="hidden-xs">Hi, {fullname}</span>
                  <span className="pull-right">
                    <i className="fa fa-angle-down" />
                    <img src={ProfilePhoto} style={style.profile_photo} className="user-image" alt="User Image" />
                  </span>
                </a>
                <ul className="dropdown-menu">
                  <li className="user-body">
                    <div className="item">
                      <a href="#"><i className="fa fa-user" />Profil Saya</a>
                    </div>
                    <div className="item">
                      <a href="#"><i className="fa fa-lock" />Ganti Password</a>
                    </div>
                  </li>
                  <li className="user-footer">
                    <div className="item">
                      <Link to='/auth/logout'><i className="fa fa-sign-out" />Logout</Link>
                    </div>
                  </li>
                </ul>
              </li>
            </ul>
          </div>
        </nav>
      </header>
    );
  }
}

export default Header;
