/* eslint prop-types: 0 */
import React from "react";
import { connect } from "react-redux";
import Header from "./Header";
import Aside from "./Aside";
import Util from "../../utils";
import {
  fetchAppMenu,
} from "../../actions/api_actions";

const isAuthenticated = (Util.getToken() && Util.getToken().length > 0);
class Menu extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      currentSideMenuId: {
        noContent: "",
        withContent: "",
      },
      currentSideMenuChildrenId: "",
      sideMenu: [],
      rawMenu: [],
    };
  }

  componentWillMount = () => {
    this.fetchData();
  }

  componentWillReceiveProps = (nextProps) => {
    const { appMenu } = this.props;

    if (appMenu !== nextProps.appMenu) {
      const result = nextProps.appMenu;
      if (!result.isFetching && result.isSuccess) {
        this.setupData(result.data);
      }
    }
  }

  fetchData = () => {
    if (isAuthenticated) {
      const { fetchAppMenu } = this.props;

      fetchAppMenu();
    }
  }

  setupData = (menu) => {
    let currentLink = String(window.location.pathname).substring(1);

    if (currentLink.split("/").length > 0) {
      [currentLink] = currentLink.split("/");
    }

    const currentSideMenuId = Util.findSideMenuIdByLink(currentLink, menu);
    const currentSideMenuChildrenId = Util.findSideMenuChildrenId(menu, currentSideMenuId.withContent, currentLink);

    this.setState({
      rawMenu: menu, currentSideMenuId, sideMenu: menu, currentSideMenuChildrenId,
    });
  }

  changeSideMenu = (val, childrenId) => {
    const { sideMenu } = this.state;
    const data = sideMenu.map(x => (Object.assign(x, { isActive: String(val) === String(x.id) })));
    const newData = val;

    this.setState({ sideMenu: data, currentSideMenuId: newData, currentSideMenuChildrenId: String(childrenId) });
  }

  render() {
    const {
      currentSideMenuId, sideMenu, currentSideMenuChildrenId,
    } = this.state;

    return (
      <React.Fragment>
        <Header />
        <Aside
          changeEvent={this.changeSideMenu}
          id={currentSideMenuId}
          childrenId={currentSideMenuChildrenId}
          menu={sideMenu}
        />
      </React.Fragment>
    );
  }
}

const mapStateToProps = state => ({
  appMenu: state.appMenu,
});

const mapDispatchToProps = dispatch => ({
  fetchAppMenu: () => dispatch(fetchAppMenu()),
});

export default connect(mapStateToProps, mapDispatchToProps)(Menu);
