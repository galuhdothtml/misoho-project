/* eslint prop-types: 0 */
import React from "react"; // eslint-disable-line no-unused-vars
import {
  BrowserRouter as Router, Route, Switch, Redirect,
} from "react-router-dom";
import Util from "../utils";
import Menu from "./components/Menu";
import Dashboard from "../components/Dashboard";
import Address from "../components/Address";
import Category from "../components/Category";
import ChangePassword from "../components/ChangePassword";
import Customer from "../components/Customer";
import CustomerDetail from "../components/Customer/CustomerDetail";
import Order from "../components/Order";
import OrderDetail from "../components/Order/OrderDetail";
import OrderHistory from "../components/OrderHistory";
import PaymentConfirmation from "../components/PaymentConfirmation";
import Product from "../components/Product";
import ProductDetail from "../components/Product/ProductDetail";
import User from "../components/User";
import UserGroup from "../components/UserGroup";
import Login from "../components/Login";

require("../sass/styles.scss");

const style = {
  container: {
    height: "auto",
    minHeight: "100%",
  },
  wrapper: {
    height: "auto",
    minHeight: "100%",
  },
};
const isAuthenticated = (Util.getToken() && Util.getToken().length > 0);
// eslint-disable-next-line react/prop-types
const PrivateRoute = ({ component: Component, ...rest }) => (
    <Route
      {...rest}
      render={props => (isAuthenticated ? (
          <Component {...props} />
      ) : (
          <Redirect
            to={{
              pathname: "/auth/login",
              state: { from: props.location }, // eslint-disable-line react/prop-types
            }}
          />
      ))
      }
    />
);
// eslint-disable-next-line react/prop-types
const AuthRoute = ({ component: Component, ...rest }) => (
  <Route
    {...rest}
    render={props => (!isAuthenticated ? (
        <Component {...props} />
    ) : (
        <Redirect
          to={{
            pathname: "/",
          }}
        />
    ))
    }
  />
);
const Logout = () => {
  Util.removeToken();
  location.href = "/auth/login"; // eslint-disable-line no-restricted-globals

  return (<div />);
};
const RedirectToDashboard = () => (
  <Redirect
    to={{
      pathname: "/dashboard",
    }}
  />
);
class App extends React.Component {
  constructor(props) {
    super(props);

    moment.locale("id");
  }

  render = () => (
    <Router>
      <Switch>
        <AuthRoute exact path="/auth/login" component={Login} />
        <Route exact path="/auth/logout" component={Logout} />
        <Route path="/">
          <div className="sidebar-mini fixed skin-purple" style={style.container}>
            <div className="wrapper" style={style.wrapper}>
              <Menu />
              <div>
                <Route exact path="/" component={RedirectToDashboard} />
                  <PrivateRoute path="/dashboard" component={Dashboard} />
                  <PrivateRoute path="/alamat-pelanggan" component={Address} />
                  <PrivateRoute path="/produk-kategori" component={Category} />
                  <PrivateRoute path="/ganti-password" component={ChangePassword} />
                  <PrivateRoute exact path="/pelanggan-pelanggan" component={Customer} />
                  <PrivateRoute path="/pelanggan-pelanggan/detail/:id" component={CustomerDetail} />
                  <PrivateRoute exact path="/pesanan-pesanan" component={Order} />
                  <PrivateRoute path="/pesanan-pesanan/detail/:id" component={OrderDetail} />
                  <PrivateRoute path="/pesanan-histori-pesanan" component={OrderHistory} />
                  <PrivateRoute path="/konfirmasi-pembayaran" component={PaymentConfirmation} />
                  <PrivateRoute exact path="/produk-produk" component={Product} />
                  <PrivateRoute path="/produk-produk/detail/:type/:id?" component={ProductDetail} />
                  <PrivateRoute path="/pengguna" component={User} />
                  <PrivateRoute path="/grup-pengguna" component={UserGroup} />
              </div>
              <footer className="main-footer">
                <div className="pull-right hidden-xs">
                  <b>Version</b> 2.4.0
                </div>
                <b>Created by: </b> <a href="#">@galuhrmdh</a>
              </footer>
            </div>
          </div>
        </Route>
      </Switch>
    </Router>
  )
}

export default App;
