-- jumlah produk per daerah
SELECT p.origin, c.name, 
COUNT(p.origin) c FROM Products p 
JOIN Cities c ON c.id=p.origin 
GROUP BY p.origin 
HAVING c > 1 
ORDER BY c DESC;