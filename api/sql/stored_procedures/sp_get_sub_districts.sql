DELIMITER ;;

DROP PROCEDURE IF EXISTS sp_get_sub_districts;

CREATE PROCEDURE `sp_get_sub_districts`(
 IN __id_city INT(11),
 IN __q TEXT
)
BEGIN
    DECLARE __name VARCHAR(255);
    DECLARE __province_code INT(2);

    IF __id_city <> 0 THEN
        SELECT name FROM Cities WHERE id = __id_city INTO __name;

        SET @replaced_name = REPLACE(__name, 'Kabupaten ', '');
        SET @replaced_name = REPLACE(@replaced_name, 'Kota ', '');
        SET @replaced_name = LOWER(@replaced_name);
        
        SELECT province_code FROM PostalCodes 
        WHERE LOWER(city) LIKE CONCAT('%', @replaced_name, '%') 
        GROUP BY city LIMIT 1 INTO __province_code;

        SELECT sub_district FROM PostalCodes 
        WHERE LOWER(city) LIKE CONCAT('%', @replaced_name, '%') 
        AND LOWER(sub_district) LIKE CONCAT('%', __q, '%') 
        AND province_code = __province_code 
        GROUP BY sub_district ORDER BY sub_district ASC LIMIT 5;
    ELSE
        SELECT sub_district FROM PostalCodes 
        WHERE LOWER(sub_district) = LOWER(__q) 
        GROUP BY sub_district ORDER BY sub_district ASC LIMIT 5;
    END IF;    
END ;;
DELIMITER ;