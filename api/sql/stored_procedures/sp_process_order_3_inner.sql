DELIMITER ;;

DROP PROCEDURE IF EXISTS sp_process_order_3_inner;

CREATE PROCEDURE `sp_process_order_3_inner`(
 IN __id_order INT(11)
)
BEGIN
  DECLARE __id_product INT(11);
  DECLARE __qty DECIMAL(19,4);

  DECLARE done INT DEFAULT FALSE;

  DECLARE cur1 CURSOR FOR SELECT id_product, qty FROM OrderDetails WHERE id_order = __id_order;
  
  DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;

  OPEN cur1;

  read_loop: LOOP
    FETCH FROM cur1 INTO __id_product, __qty;

    IF done THEN
       LEAVE read_loop;
    END IF;

    UPDATE Stocks SET qty = (qty - __qty), updatedAt = NOW() WHERE id_product = __id_product;
  END LOOP;

  CLOSE cur1;

END ;;
DELIMITER ;