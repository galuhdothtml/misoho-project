-- kirim pesanan
DELIMITER ;;

DROP PROCEDURE IF EXISTS sp_process_order_3;

CREATE PROCEDURE `sp_process_order_3`(
 IN __id INT(11), -- id order
 IN __shipping TEXT,
 IN __id_employee INT(11)
)
BEGIN
  UPDATE Orders SET order_state = '4', shipping=__shipping WHERE id = __id;

  INSERT INTO OrderHistories(id_order, id_employee, order_state, createdAt, updatedAt) 
  VALUES(__id, __id_employee, '4', NOW(), NOW());

  CALL sp_process_order_3_inner(__id);

  SELECT * FROM Orders WHERE id = __id;
END ;;
DELIMITER ;