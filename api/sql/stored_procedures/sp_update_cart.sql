DELIMITER ;;

DROP PROCEDURE IF EXISTS sp_update_cart;

CREATE PROCEDURE `sp_update_cart`(
 IN __id_customer INT(11),
 IN __id_product INT(11),
 IN __qty INT(11)
)
BEGIN
  DECLARE __id_cart INT(11);
  
  SELECT id FROM Carts 
  WHERE id_customer = __id_customer
  INTO __id_cart;

  UPDATE CartDetails SET qty = __qty WHERE id_cart = __id_cart AND id_product = __id_product;

  SET @id_product = __id_product;
  SET @message = 'Cart has been updated!';

  SELECT @id_product, @message;
END ;;
DELIMITER ;