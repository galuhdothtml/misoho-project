DELIMITER ;;

DROP PROCEDURE IF EXISTS sp_inner_checkout_order;

CREATE PROCEDURE `sp_inner_checkout_order`(
 IN __id_order INT(11),
 IN __id_cart INT(11)
)
BEGIN
  DECLARE __id_product INT(11);
  DECLARE __qty DECIMAL(19,4);
  DECLARE __product_name VARCHAR(255);
  DECLARE __product_price DECIMAL(19,4);

  DECLARE done INT DEFAULT FALSE;

  DECLARE cur1 CURSOR FOR SELECT cd.id_product, cd.qty, p.name, p.price FROM CartDetails cd 
  JOIN Carts c ON c.id = cd.id_cart 
  JOIN Products p ON p.id=cd.id_product 
  WHERE c.id = __id_cart;
  
  DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;

  OPEN cur1;

  read_loop: LOOP
    FETCH FROM cur1 INTO __id_product, __qty, __product_name, __product_price;

    IF done THEN
       LEAVE read_loop;
    END IF;

    INSERT INTO OrderDetails(id_order, id_product, product_name, product_price, qty, createdAt, updatedAt) 
    VALUES(__id_order, __id_product, __product_name, __product_price, __qty, NOW(), NOW());
  END LOOP;

  CLOSE cur1;

END ;;
DELIMITER ;