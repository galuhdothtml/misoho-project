DELIMITER ;;

DROP PROCEDURE IF EXISTS sp_save_wishlist;

CREATE PROCEDURE `sp_save_wishlist`(
 IN __id_customer INT(11),
 IN __id_product INT(11)
)
BEGIN
  DECLARE __id_wishlist INT(11);
  DECLARE __product_name VARCHAR(255);

  SET @id_product = __id_product;
  SET @product_name = '';
  SET @message = 'Berhasil! Silahkan lihat wishlist Anda.';

  SELECT name FROM Products
  WHERE id = __id_product
  LIMIT 1 INTO __product_name;

  SET @product_name = __product_name;

  SELECT id FROM Wishlists
  WHERE id_customer = __id_customer
  AND id_product = __id_product 
  LIMIT 1 
  INTO __id_wishlist;

  IF __id_wishlist IS NULL THEN
    INSERT INTO Wishlists(id_product, id_customer, createdAt, updatedAt) VALUES(__id_product, __id_customer, NOW(), NOW());
  END IF;


  SELECT @id_product, @product_name, @message;
END ;;
DELIMITER ;