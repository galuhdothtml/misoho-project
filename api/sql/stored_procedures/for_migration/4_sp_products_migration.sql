DELIMITER ;;

DROP PROCEDURE IF EXISTS 4_sp_products_migration;

CREATE PROCEDURE `4_sp_products_migration`()
BEGIN
    DECLARE __id INT(11);
    DECLARE __id_category INT(11);
    DECLARE __name VARCHAR(255);
    DECLARE __link_rewrite VARCHAR(255);
    DECLARE __price DECIMAL(19,4);
    DECLARE __description TEXT;
    DECLARE __primary_img TEXT;
    DECLARE __is_active TINYINT(1);
    DECLARE __createdAt DATETIME;
    DECLARE __updatedAt DATETIME;
    DECLARE __id_city INT(11);

    DECLARE done INT DEFAULT FALSE;

    DECLARE cur1 CURSOR FOR SELECT id, id_category, name, link_rewrite, 
    price, description, primary_img, createdAt, updatedAt, is_active
    FROM db_misoho_backup.Products WHERE is_deleted = '0';

    DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;

    OPEN cur1;

    read_loop: LOOP
        FETCH FROM cur1 INTO __id, __id_category, 
        __name, __link_rewrite, 
        __price, __description, __primary_img,
        __createdAt, __updatedAt, __is_active;

        IF done THEN
          LEAVE read_loop;
        END IF;

        SELECT id FROM Cities ORDER BY RAND() LIMIT 1 INTO __id_city;

        INSERT INTO Products(
          id,
          id_category,
          name,
          slug,
          price,
          description,
          origin,
          weight,
          popular_score,
          available_qty,
          is_active,
          primary_img,
          createdAt, 
          updatedAt
        ) VALUES(
          __id,
          __id_category,
          __name,
          __link_rewrite,
          __price,
          __description,
          __id_city,
          250,
          0,
          100,
          __is_active,
          __primary_img,
          __createdAt,
          __updatedAt
        );
      END LOOP;

    CLOSE cur1;

    SELECT id, name, slug, origin FROM Products;

END ;;
DELIMITER ;