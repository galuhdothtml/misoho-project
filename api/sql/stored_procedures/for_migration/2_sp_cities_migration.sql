DELIMITER ;;

DROP PROCEDURE IF EXISTS 2_sp_cities_migration;

CREATE PROCEDURE `2_sp_cities_migration`()
BEGIN
    DECLARE __id INT(11);
    DECLARE __name VARCHAR(255);
    DECLARE __id_province INT(11);
    DECLARE __createdAt DATETIME;
    DECLARE __updatedAt DATETIME;

    DECLARE done INT DEFAULT FALSE;

    DECLARE cur1 CURSOR FOR SELECT id, name, id_province, createdAt, updatedAt
    FROM db_misoho_backup.Cities;

    DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;

    OPEN cur1;

    read_loop: LOOP
        FETCH FROM cur1 INTO __id, __name, __id_province, __createdAt, __updatedAt;

        IF done THEN
          LEAVE read_loop;
        END IF;

        INSERT INTO Cities(
          id,
          name,
          id_province, 
          createdAt, 
          updatedAt
        ) VALUES(
          __id,
          __name,
          __id_province,
          __createdAt,
          __updatedAt
        );
    END LOOP;

    CLOSE cur1;

    SELECT * FROM Cities;

END ;;
DELIMITER ;