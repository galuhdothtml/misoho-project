DELIMITER ;;

DROP PROCEDURE IF EXISTS 1_sp_provinces_migration;

CREATE PROCEDURE `1_sp_provinces_migration`()
BEGIN
    DECLARE __id INT(11);
    DECLARE __name VARCHAR(255);
    DECLARE __createdAt DATETIME;
    DECLARE __updatedAt DATETIME;

    DECLARE done INT DEFAULT FALSE;

    DECLARE cur1 CURSOR FOR SELECT id, name, createdAt, updatedAt
    FROM db_misoho_backup.Provinces;

    DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;

    OPEN cur1;

    read_loop: LOOP
        FETCH FROM cur1 INTO __id, __name, __createdAt, __updatedAt;

        IF done THEN
          LEAVE read_loop;
        END IF;

        INSERT INTO Provinces(
          id,
          name, 
          createdAt, 
          updatedAt
        ) VALUES(
          __id,
          __name,
          __createdAt,
          __updatedAt
        );
      END LOOP;

    CLOSE cur1;

    SELECT * FROM Provinces;

END ;;
DELIMITER ;
DELIMITER ;;

DROP PROCEDURE IF EXISTS 2_sp_cities_migration;

CREATE PROCEDURE `2_sp_cities_migration`()
BEGIN
    DECLARE __id INT(11);
    DECLARE __name VARCHAR(255);
    DECLARE __id_province INT(11);
    DECLARE __createdAt DATETIME;
    DECLARE __updatedAt DATETIME;

    DECLARE done INT DEFAULT FALSE;

    DECLARE cur1 CURSOR FOR SELECT id, name, id_province, createdAt, updatedAt
    FROM db_misoho_backup.Cities;

    DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;

    OPEN cur1;

    read_loop: LOOP
        FETCH FROM cur1 INTO __id, __name, __id_province, __createdAt, __updatedAt;

        IF done THEN
          LEAVE read_loop;
        END IF;

        INSERT INTO Cities(
          id,
          name,
          id_province, 
          createdAt, 
          updatedAt
        ) VALUES(
          __id,
          __name,
          __id_province,
          __createdAt,
          __updatedAt
        );
    END LOOP;

    CLOSE cur1;

    SELECT * FROM Cities;

END ;;
DELIMITER ;
DELIMITER ;;

DROP PROCEDURE IF EXISTS 3_sp_categories_migration;

CREATE PROCEDURE `3_sp_categories_migration`()
BEGIN
    DECLARE __id INT(11);
    DECLARE __name VARCHAR(255);
    DECLARE __link_rewrite VARCHAR(255);
    DECLARE __description TEXT;
    DECLARE __createdAt DATETIME;
    DECLARE __updatedAt DATETIME;

    DECLARE done INT DEFAULT FALSE;

    DECLARE cur1 CURSOR FOR SELECT id, name, link_rewrite, description, createdAt, updatedAt
    FROM db_misoho_backup.Categories WHERE is_deleted = '0';

    DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;

    OPEN cur1;

    read_loop: LOOP
        FETCH FROM cur1 INTO __id, __name, __link_rewrite, __description, __createdAt, __updatedAt;

        IF done THEN
          LEAVE read_loop;
        END IF;

        INSERT INTO Categories(
          id,
          name,
          slug,
          description, 
          createdAt, 
          updatedAt
        ) VALUES(
          __id,
          __name,
          __link_rewrite,
          __description,
          __createdAt,
          __updatedAt
        );
    END LOOP;

    CLOSE cur1;

    SELECT * FROM Categories;

END ;;
DELIMITER ;
DELIMITER ;;

DROP PROCEDURE IF EXISTS 4_sp_products_migration;

CREATE PROCEDURE `4_sp_products_migration`()
BEGIN
    DECLARE __id INT(11);
    DECLARE __id_category INT(11);
    DECLARE __name VARCHAR(255);
    DECLARE __link_rewrite VARCHAR(255);
    DECLARE __price DECIMAL(19,4);
    DECLARE __description TEXT;
    DECLARE __primary_img TEXT;
    DECLARE __is_active TINYINT(1);
    DECLARE __createdAt DATETIME;
    DECLARE __updatedAt DATETIME;
    DECLARE __id_city INT(11);

    DECLARE done INT DEFAULT FALSE;

    DECLARE cur1 CURSOR FOR SELECT id, id_category, name, link_rewrite, 
    price, description, primary_img, createdAt, updatedAt, is_active
    FROM db_misoho_backup.Products WHERE is_deleted = '0';

    DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;

    OPEN cur1;

    read_loop: LOOP
        FETCH FROM cur1 INTO __id, __id_category, 
        __name, __link_rewrite, 
        __price, __description, __primary_img,
        __createdAt, __updatedAt, __is_active;

        IF done THEN
          LEAVE read_loop;
        END IF;

        SELECT id FROM Cities ORDER BY RAND() LIMIT 1 INTO __id_city;

        INSERT INTO Products(
          id,
          id_category,
          name,
          slug,
          price,
          description,
          origin,
          weight,
          popular_score,
          available_qty,
          is_active,
          primary_img,
          createdAt, 
          updatedAt
        ) VALUES(
          __id,
          __id_category,
          __name,
          __link_rewrite,
          __price,
          __description,
          __id_city,
          250,
          0,
          100,
          __is_active,
          __primary_img,
          __createdAt,
          __updatedAt
        );
      END LOOP;

    CLOSE cur1;

    SELECT id, name, slug, origin FROM Products;

END ;;
DELIMITER ;
DELIMITER ;;

DROP PROCEDURE IF EXISTS 5_sp_file_storages_migration;

CREATE PROCEDURE `5_sp_file_storages_migration`()
BEGIN
    DECLARE __id INT(11);
    DECLARE __tablename VARCHAR(255);
    DECLARE __filename TEXT;
    DECLARE __id_reference INT(11);
    DECLARE __createdAt DATETIME;
    DECLARE __updatedAt DATETIME;

    DECLARE done INT DEFAULT FALSE;

    DECLARE cur1 CURSOR FOR SELECT id, tablename, filename, id_reference, createdAt, updatedAt
    FROM db_misoho_backup.FileStorages;

    DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;

    OPEN cur1;

    read_loop: LOOP
        FETCH FROM cur1 INTO __id, __tablename, __filename, __id_reference, __createdAt, __updatedAt;

        IF done THEN
          LEAVE read_loop;
        END IF;

        INSERT INTO FileStorages(
          id,
          tablename,
          filename,
          id_reference, 
          createdAt, 
          updatedAt
        ) VALUES(
          __id, 
          __tablename, 
          __filename, 
          __id_reference,
          __createdAt,
          __updatedAt
        );
    END LOOP;

    CLOSE cur1;

    SELECT * FROM FileStorages;

END ;;
DELIMITER ;
DELIMITER ;;

DROP PROCEDURE IF EXISTS 6_sp_customers_migration;

CREATE PROCEDURE `6_sp_customers_migration`()
BEGIN
    DECLARE __id INT(11);
    DECLARE __name VARCHAR(255);
    DECLARE __email VARCHAR(255);
    DECLARE __passwd VARCHAR(255);
    DECLARE __newsletter TINYINT(1);
    DECLARE __active TINYINT(1);
    DECLARE __is_confirmed TINYINT(1);
    DECLARE __createdAt DATETIME;
    DECLARE __updatedAt DATETIME;

    DECLARE done INT DEFAULT FALSE;

    DECLARE cur1 CURSOR FOR SELECT id, name, email, passwd, newsletter, 
    active, is_confirmed, createdAt, updatedAt
    FROM db_misoho_backup.Customers WHERE is_deleted = '0';

    DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;

    OPEN cur1;

    read_loop: LOOP
        FETCH FROM cur1 INTO __id, __name, __email, __passwd,
        __newsletter, __active, __is_confirmed,
        __createdAt, __updatedAt;

        IF done THEN
          LEAVE read_loop;
        END IF;

        INSERT INTO Customers(
          id, 
          name, 
          email, 
          passwd, 
          newsletter, 
          is_active, 
          is_confirmed,
          createdAt, 
          updatedAt
        ) VALUES(
          __id, 
          __name, 
          __email, 
          __passwd,
          __newsletter, 
          __active, 
          __is_confirmed,
          __createdAt,
          __updatedAt
        );
    END LOOP;

    CLOSE cur1;

    SELECT * FROM Customers;

END ;;
DELIMITER ;
DELIMITER ;;

DROP PROCEDURE IF EXISTS 7_sp_employees_migration;

CREATE PROCEDURE `7_sp_employees_migration`()
BEGIN
    DECLARE __id INT(11);
    DECLARE __name VARCHAR(255);
    DECLARE __username VARCHAR(255);
    DECLARE __email VARCHAR(255);
    DECLARE __passwd VARCHAR(255);
    DECLARE __createdAt DATETIME;
    DECLARE __updatedAt DATETIME;

    DECLARE done INT DEFAULT FALSE;

    DECLARE cur1 CURSOR FOR SELECT id, name, username, email, passwd, 
    createdAt, updatedAt
    FROM db_misoho_backup.Employees;

    DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;

    OPEN cur1;

    read_loop: LOOP
        FETCH FROM cur1 INTO __id, __name, __username, __email, __passwd,
        __createdAt, __updatedAt;

        IF done THEN
          LEAVE read_loop;
        END IF;

        INSERT INTO Employees(
          id, 
          name, 
          username, 
          email, 
          passwd, 
          type,
          privileges,
          createdAt, 
          updatedAt
        ) VALUES(
          __id, 
          __name, 
          __username, 
          __email, 
          __passwd,
          1,
          '-',
          __createdAt,
          __updatedAt
        );
    END LOOP;

    CLOSE cur1;

    SELECT * FROM Employees;

END ;;
DELIMITER ;
