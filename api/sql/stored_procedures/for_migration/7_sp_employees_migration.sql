DELIMITER ;;

DROP PROCEDURE IF EXISTS 7_sp_employees_migration;

CREATE PROCEDURE `7_sp_employees_migration`()
BEGIN
    DECLARE __id INT(11);
    DECLARE __name VARCHAR(255);
    DECLARE __username VARCHAR(255);
    DECLARE __email VARCHAR(255);
    DECLARE __passwd VARCHAR(255);
    DECLARE __createdAt DATETIME;
    DECLARE __updatedAt DATETIME;

    DECLARE done INT DEFAULT FALSE;

    DECLARE cur1 CURSOR FOR SELECT id, name, username, email, passwd, 
    createdAt, updatedAt
    FROM db_misoho_backup.Employees;

    DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;

    OPEN cur1;

    read_loop: LOOP
        FETCH FROM cur1 INTO __id, __name, __username, __email, __passwd,
        __createdAt, __updatedAt;

        IF done THEN
          LEAVE read_loop;
        END IF;

        INSERT INTO Employees(
          id, 
          name, 
          username, 
          email, 
          passwd, 
          type,
          privileges,
          createdAt, 
          updatedAt
        ) VALUES(
          __id, 
          __name, 
          __username, 
          __email, 
          __passwd,
          1,
          '-',
          __createdAt,
          __updatedAt
        );
    END LOOP;

    CLOSE cur1;

    SELECT * FROM Employees;

END ;;
DELIMITER ;