DELIMITER ;;

DROP PROCEDURE IF EXISTS 3_sp_categories_migration;

CREATE PROCEDURE `3_sp_categories_migration`()
BEGIN
    DECLARE __id INT(11);
    DECLARE __name VARCHAR(255);
    DECLARE __link_rewrite VARCHAR(255);
    DECLARE __description TEXT;
    DECLARE __createdAt DATETIME;
    DECLARE __updatedAt DATETIME;

    DECLARE done INT DEFAULT FALSE;

    DECLARE cur1 CURSOR FOR SELECT id, name, link_rewrite, description, createdAt, updatedAt
    FROM db_misoho_backup.Categories WHERE is_deleted = '0';

    DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;

    OPEN cur1;

    read_loop: LOOP
        FETCH FROM cur1 INTO __id, __name, __link_rewrite, __description, __createdAt, __updatedAt;

        IF done THEN
          LEAVE read_loop;
        END IF;

        INSERT INTO Categories(
          id,
          name,
          slug,
          description, 
          createdAt, 
          updatedAt
        ) VALUES(
          __id,
          __name,
          __link_rewrite,
          __description,
          __createdAt,
          __updatedAt
        );
    END LOOP;

    CLOSE cur1;

    SELECT * FROM Categories;

END ;;
DELIMITER ;