DELIMITER ;;

DROP PROCEDURE IF EXISTS 6_sp_customers_migration;

CREATE PROCEDURE `6_sp_customers_migration`()
BEGIN
    DECLARE __id INT(11);
    DECLARE __name VARCHAR(255);
    DECLARE __email VARCHAR(255);
    DECLARE __passwd VARCHAR(255);
    DECLARE __newsletter TINYINT(1);
    DECLARE __active TINYINT(1);
    DECLARE __is_confirmed TINYINT(1);
    DECLARE __createdAt DATETIME;
    DECLARE __updatedAt DATETIME;

    DECLARE done INT DEFAULT FALSE;

    DECLARE cur1 CURSOR FOR SELECT id, name, email, passwd, newsletter, 
    active, is_confirmed, createdAt, updatedAt
    FROM db_misoho_backup.Customers WHERE is_deleted = '0';

    DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;

    OPEN cur1;

    read_loop: LOOP
        FETCH FROM cur1 INTO __id, __name, __email, __passwd,
        __newsletter, __active, __is_confirmed,
        __createdAt, __updatedAt;

        IF done THEN
          LEAVE read_loop;
        END IF;

        INSERT INTO Customers(
          id, 
          name, 
          email, 
          passwd, 
          newsletter, 
          is_active, 
          is_confirmed,
          createdAt, 
          updatedAt
        ) VALUES(
          __id, 
          __name, 
          __email, 
          __passwd,
          __newsletter, 
          __active, 
          __is_confirmed,
          __createdAt,
          __updatedAt
        );
    END LOOP;

    CLOSE cur1;

    SELECT * FROM Customers;

END ;;
DELIMITER ;