DELIMITER ;;

DROP PROCEDURE IF EXISTS 1_sp_provinces_migration;

CREATE PROCEDURE `1_sp_provinces_migration`()
BEGIN
    DECLARE __id INT(11);
    DECLARE __name VARCHAR(255);
    DECLARE __createdAt DATETIME;
    DECLARE __updatedAt DATETIME;

    DECLARE done INT DEFAULT FALSE;

    DECLARE cur1 CURSOR FOR SELECT id, name, createdAt, updatedAt
    FROM db_misoho_backup.Provinces;

    DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;

    OPEN cur1;

    read_loop: LOOP
        FETCH FROM cur1 INTO __id, __name, __createdAt, __updatedAt;

        IF done THEN
          LEAVE read_loop;
        END IF;

        INSERT INTO Provinces(
          id,
          name, 
          createdAt, 
          updatedAt
        ) VALUES(
          __id,
          __name,
          __createdAt,
          __updatedAt
        );
      END LOOP;

    CLOSE cur1;

    SELECT * FROM Provinces;

END ;;
DELIMITER ;