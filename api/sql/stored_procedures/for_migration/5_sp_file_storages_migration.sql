DELIMITER ;;

DROP PROCEDURE IF EXISTS 5_sp_file_storages_migration;

CREATE PROCEDURE `5_sp_file_storages_migration`()
BEGIN
    DECLARE __id INT(11);
    DECLARE __tablename VARCHAR(255);
    DECLARE __filename TEXT;
    DECLARE __id_reference INT(11);
    DECLARE __createdAt DATETIME;
    DECLARE __updatedAt DATETIME;

    DECLARE done INT DEFAULT FALSE;

    DECLARE cur1 CURSOR FOR SELECT id, tablename, filename, id_reference, createdAt, updatedAt
    FROM db_misoho_backup.FileStorages;

    DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;

    OPEN cur1;

    read_loop: LOOP
        FETCH FROM cur1 INTO __id, __tablename, __filename, __id_reference, __createdAt, __updatedAt;

        IF done THEN
          LEAVE read_loop;
        END IF;

        INSERT INTO FileStorages(
          id,
          tablename,
          filename,
          id_reference, 
          createdAt, 
          updatedAt
        ) VALUES(
          __id, 
          __tablename, 
          __filename, 
          __id_reference,
          __createdAt,
          __updatedAt
        );
    END LOOP;

    CLOSE cur1;

    SELECT * FROM FileStorages;

END ;;
DELIMITER ;