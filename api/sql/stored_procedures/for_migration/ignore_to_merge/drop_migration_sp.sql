DROP PROCEDURE IF EXISTS 1_sp_provinces_migration;
DROP PROCEDURE IF EXISTS 2_sp_cities_migration;
DROP PROCEDURE IF EXISTS 3_sp_categories_migration;
DROP PROCEDURE IF EXISTS 4_sp_products_migration;
DROP PROCEDURE IF EXISTS 5_sp_file_storages_migration;
DROP PROCEDURE IF EXISTS 6_sp_customers_migration;
DROP PROCEDURE IF EXISTS 7_sp_employees_migration;