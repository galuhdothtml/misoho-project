DELIMITER ;;

DROP PROCEDURE IF EXISTS sp_temporary;

CREATE PROCEDURE `sp_temporary`()
BEGIN
    DECLARE __id_product INT(11);
    DECLARE __slug VARCHAR(255);

    DECLARE done INT DEFAULT FALSE;

    DECLARE cur1 CURSOR FOR SELECT id, REPLACE(slug, '.html', '') FROM Products;

    DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;

    OPEN cur1;

    read_loop: LOOP
        FETCH FROM cur1 INTO __id_product, __slug;

        IF done THEN
            LEAVE read_loop;
        END IF;

        UPDATE Products SET slug = __slug WHERE id = __id_product;
    END LOOP;

    CLOSE cur1;

    SELECT id, name, slug FROM Products;
END ;;
DELIMITER ;