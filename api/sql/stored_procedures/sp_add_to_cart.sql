DELIMITER ;;

DROP PROCEDURE IF EXISTS sp_add_to_cart;

CREATE PROCEDURE `sp_add_to_cart`(
 IN __id_customer INT(11),
 IN __id_product INT(11),
 IN __qty INT(11)
)
BEGIN
  DECLARE __id_cart INT(11);
  DECLARE __cart_detail_count INT;
  DECLARE __product_name VARCHAR(255);

  SET @id_product = __id_product;
  SET @product_name = '';
  SET @qty = __qty;
  SET @message = 'PRODUCT_IS_NOT_EXIST';

  SELECT name FROM Products
  WHERE id = __id_product
  LIMIT 1 INTO __product_name;

  SET @product_name = __product_name;

  IF @product_name IS NOT NULL AND @qty > 0 THEN
    SET @message = 'Berhasil! Silahkan lihat keranjang belanja Anda.';

    SELECT id FROM Carts
    WHERE id_customer = __id_customer
    LIMIT 1
    INTO __id_cart;

    IF __id_cart IS NULL THEN
      INSERT INTO Carts(id_customer, createdAt, updatedAt) VALUES(__id_customer, NOW(), NOW());

      SELECT id FROM Carts
      WHERE id_customer = __id_customer
      INTO __id_cart;
    END IF;

    
    SELECT COUNT(*) FROM CartDetails
    WHERE id_cart = __id_cart
    AND id_product = __id_product
    INTO __cart_detail_count;

    IF __cart_detail_count > 0 THEN
      UPDATE CartDetails SET qty = (qty + __qty)
      WHERE id_cart = __id_cart
      AND id_product = __id_product;
    ELSE
      INSERT INTO CartDetails(id_cart, id_product, qty, createdAt, updatedAt)
      VALUES(__id_cart, __id_product, __qty, NOW(), NOW());
    END IF;
  ELSEIF @qty <= 0 THEN
    SET @message = 'QTY_IS_NOT_VALID';
  END IF;

  SELECT @id_product, @product_name, @qty, @message;
END ;;
DELIMITER ;