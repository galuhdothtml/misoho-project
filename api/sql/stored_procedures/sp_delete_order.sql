DELIMITER ;;

DROP PROCEDURE IF EXISTS sp_delete_order;

CREATE PROCEDURE `sp_delete_order`(
 IN __id INT(11)
)
BEGIN
  DELETE FROM OrderHistories WHERE id_order = __id;
  DELETE FROM OrderDetails WHERE id_order = __id;
  DELETE FROM Orders WHERE id = __id;
  
  SELECT * FROM Orders WHERE id = __id;
END ;;
DELIMITER ;