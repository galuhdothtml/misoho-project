DELIMITER ;;

DROP PROCEDURE IF EXISTS sp_add_to_cart;

CREATE PROCEDURE `sp_add_to_cart`(
 IN __id_customer INT(11),
 IN __id_product INT(11),
 IN __qty INT(11)
)
BEGIN
  DECLARE __id_cart INT(11);
  DECLARE __cart_detail_count INT;
  DECLARE __product_name VARCHAR(255);

  SET @id_product = __id_product;
  SET @product_name = '';
  SET @qty = __qty;
  SET @message = 'PRODUCT_IS_NOT_EXIST';

  SELECT name FROM Products
  WHERE id = __id_product
  LIMIT 1 INTO __product_name;

  SET @product_name = __product_name;

  IF @product_name IS NOT NULL AND @qty > 0 THEN
    SET @message = 'Berhasil! Silahkan lihat keranjang belanja Anda.';

    SELECT id FROM Carts
    WHERE id_customer = __id_customer
    LIMIT 1
    INTO __id_cart;

    IF __id_cart IS NULL THEN
      INSERT INTO Carts(id_customer, createdAt, updatedAt) VALUES(__id_customer, NOW(), NOW());

      SELECT id FROM Carts
      WHERE id_customer = __id_customer
      INTO __id_cart;
    END IF;

    
    SELECT COUNT(*) FROM CartDetails
    WHERE id_cart = __id_cart
    AND id_product = __id_product
    INTO __cart_detail_count;

    IF __cart_detail_count > 0 THEN
      UPDATE CartDetails SET qty = (qty + __qty)
      WHERE id_cart = __id_cart
      AND id_product = __id_product;
    ELSE
      INSERT INTO CartDetails(id_cart, id_product, qty, createdAt, updatedAt)
      VALUES(__id_cart, __id_product, __qty, NOW(), NOW());
    END IF;
  ELSEIF @qty <= 0 THEN
    SET @message = 'QTY_IS_NOT_VALID';
  END IF;

  SELECT @id_product, @product_name, @qty, @message;
END ;;
DELIMITER ;
DELIMITER ;;

DROP PROCEDURE IF EXISTS sp_checkout_order;

CREATE PROCEDURE `sp_checkout_order`(
 IN __id_customer INT(11),
 IN __reference VARCHAR(255),
 IN __address_delivery TEXT,
 IN __shipping TEXT,
 IN __total_shipping DECIMAL(19,4),
 IN __id_employee INT(11)
)
BEGIN
  DECLARE __subtotal DECIMAL(19,4);
  DECLARE __id_order INT(11);
  
  DECLARE __id_cart INT(11);

  SELECT SUM(p.price * cd.qty) FROM CartDetails cd 
  JOIN Carts c ON c.id = cd.id_cart 
  JOIN Products p ON p.id = cd.id_product 
  WHERE id_customer = __id_customer INTO __subtotal;

  INSERT INTO Orders(
        reference,
        id_customer,
        address_delivery,
        order_state,
        payment,
        shipping,
        subtotal,
        total_shipping,
        grand_total,
        createdAt,
        updatedAt
  ) VALUES(
        __reference,
        __id_customer,
        address_delivery,
        '1',
        '1',
        __shipping,
        __subtotal,
        __total_shipping,
        (__subtotal + __total_shipping),
        NOW(),
        NOW()
  );

  SELECT LAST_INSERT_ID() INTO __id_order;

  SELECT id FROM Carts WHERE id_customer = __id_customer LIMIT 1 INTO __id_cart;

  CALL sp_inner_checkout_order(__id_order, __id_cart);

  INSERT INTO OrderHistories(id_order, id_employee, order_state, createdAt, updatedAt) 
  VALUES(__id_order, __id_employee, '1', NOW(), NOW());
  
  DELETE FROM CartDetails WHERE id_cart = __id_cart;
  DELETE FROM Carts WHERE id = __id_cart;
  
  SELECT * FROM Orders WHERE id = __id_order;
END ;;
DELIMITER ;
DELIMITER ;;

DROP PROCEDURE IF EXISTS sp_inner_checkout_order;

CREATE PROCEDURE `sp_inner_checkout_order`(
 IN __id_order INT(11),
 IN __id_cart INT(11)
)
BEGIN
  DECLARE __id_product INT(11);
  DECLARE __qty DECIMAL(19,4);
  DECLARE __product_name VARCHAR(255);
  DECLARE __product_price DECIMAL(19,4);

  DECLARE done INT DEFAULT FALSE;

  DECLARE cur1 CURSOR FOR SELECT cd.id_product, cd.qty, p.name, p.price FROM CartDetails cd 
  JOIN Carts c ON c.id = cd.id_cart 
  JOIN Products p ON p.id=cd.id_product 
  WHERE c.id = __id_cart;
  
  DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;

  OPEN cur1;

  read_loop: LOOP
    FETCH FROM cur1 INTO __id_product, __qty, __product_name, __product_price;

    IF done THEN
       LEAVE read_loop;
    END IF;

    INSERT INTO OrderDetails(id_order, id_product, product_name, product_price, qty, createdAt, updatedAt) 
    VALUES(__id_order, __id_product, __product_name, __product_price, __qty, NOW(), NOW());
  END LOOP;

  CLOSE cur1;

END ;;
DELIMITER ;
DELIMITER ;;

DROP PROCEDURE IF EXISTS sp_save_wishlist;

CREATE PROCEDURE `sp_save_wishlist`(
 IN __id_customer INT(11),
 IN __id_product INT(11)
)
BEGIN
  DECLARE __id_wishlist INT(11);
  DECLARE __product_name VARCHAR(255);

  SET @id_product = __id_product;
  SET @product_name = '';
  SET @message = 'Berhasil! Silahkan lihat wishlist Anda.';

  SELECT name FROM Products
  WHERE id = __id_product
  LIMIT 1 INTO __product_name;

  SET @product_name = __product_name;

  SELECT id FROM Wishlists
  WHERE id_customer = __id_customer
  AND id_product = __id_product 
  LIMIT 1 
  INTO __id_wishlist;

  IF __id_wishlist IS NULL THEN
    INSERT INTO Wishlists(id_product, id_customer, createdAt, updatedAt) VALUES(__id_product, __id_customer, NOW(), NOW());
  END IF;


  SELECT @id_product, @product_name, @message;
END ;;
DELIMITER ;
DELIMITER ;;

DROP PROCEDURE IF EXISTS sp_set_default_address;

CREATE PROCEDURE `sp_set_default_address`(
 IN __id_customer INT(11),
 IN __id_address INT(11)
)
BEGIN
    DECLARE __id INT(11);

    UPDATE Addresses SET is_default = 0 WHERE id_customer = __id_customer;

    UPDATE Addresses SET is_default = 1 WHERE id = __id_address;

    SELECT * FROM Addresses WHERE id = __id_address;
END ;;
DELIMITER ;
