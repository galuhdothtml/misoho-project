-- proses konfirmasi pembayaran
DELIMITER ;;

DROP PROCEDURE IF EXISTS sp_process_order_1;

CREATE PROCEDURE `sp_process_order_1`(
 IN __id INT(11), -- id konfirmasi pembayaran
 IN __id_employee INT(11),
 IN __is_accepted TINYINT(1)
)
BEGIN
  DECLARE __id_order INT(11);
  DECLARE __reference VARCHAR(255);

  SELECT reference_number FROM PaymentConfirmations WHERE id = __id INTO __reference;

  IF __is_accepted = 1 THEN
    SELECT id FROM Orders WHERE reference = __reference INTO __id_order;

    UPDATE Orders SET order_state = '2' WHERE id = __id_order;

    INSERT INTO OrderHistories(id_order, id_employee, order_state, createdAt, updatedAt) 
    VALUES(__id_order, __id_employee, '2', NOW(), NOW());

    DELETE FROM PaymentConfirmations WHERE id = __id;
  ELSE
    DELETE FROM PaymentConfirmations WHERE id = __id;
  END IF; 

  SELECT * FROM Orders WHERE id = __id_order;
END ;;
DELIMITER ;