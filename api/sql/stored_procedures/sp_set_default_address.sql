DELIMITER ;;

DROP PROCEDURE IF EXISTS sp_set_default_address;

CREATE PROCEDURE `sp_set_default_address`(
 IN __id_customer INT(11),
 IN __id_address INT(11)
)
BEGIN
    DECLARE __id INT(11);

    UPDATE Addresses SET is_default = 0 WHERE id_customer = __id_customer;

    UPDATE Addresses SET is_default = 1 WHERE id = __id_address;

    SELECT * FROM Addresses WHERE id = __id_address;
END ;;
DELIMITER ;