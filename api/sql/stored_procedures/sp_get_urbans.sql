DELIMITER ;;

DROP PROCEDURE IF EXISTS sp_get_urbans;

CREATE PROCEDURE `sp_get_urbans`(
 IN __id_city INT(11),
 IN __sub_district TEXT,
 IN __q TEXT,
 IN __id BIGINT(11)
)
BEGIN
    DECLARE __name VARCHAR(255);
    DECLARE __province_code INT(2);

    IF __id_city <> 0 THEN
        SELECT name FROM Cities WHERE id = __id_city INTO __name;

        SET @replaced_name = REPLACE(__name, 'Kabupaten ', '');
        SET @replaced_name = REPLACE(@replaced_name, 'Kota ', '');
        SET @replaced_name = LOWER(@replaced_name);
        
        SELECT province_code FROM PostalCodes 
        WHERE LOWER(city) LIKE CONCAT('%', @replaced_name, '%') 
        GROUP BY city LIMIT 1 INTO __province_code;

        SELECT id, urban, postal_code FROM PostalCodes 
        WHERE LOWER(city) LIKE CONCAT('%', @replaced_name, '%') 
        AND LOWER(sub_district) LIKE LOWER(CONCAT('%', __sub_district, '%')) 
        AND province_code = __province_code 
        AND LOWER(urban) LIKE LOWER(CONCAT('%', __q, '%'))
        ORDER BY urban ASC LIMIT 5;
    ELSE
        SELECT id, urban, postal_code FROM PostalCodes 
        WHERE id = __id
        ORDER BY urban ASC LIMIT 5;
    END IF;    
END ;;
DELIMITER ;