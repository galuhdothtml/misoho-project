DELIMITER ;;

DROP PROCEDURE IF EXISTS sp_delete_cart;

CREATE PROCEDURE `sp_delete_cart`(
 IN __id_customer INT(11),
 IN __id_product INT(11)
)
BEGIN
  DECLARE __id_cart INT(11);
  DECLARE __cart_product_amount INT(11);
  
  SELECT id FROM Carts 
  WHERE id_customer = __id_customer
  INTO __id_cart;

  DELETE FROM CartDetails WHERE id_product = __id_product AND id_cart = __id_cart;

  SELECT COUNT(id) AS cart_product_amount 
  FROM CartDetails
  WHERE id_cart = __id_cart
  INTO __cart_product_amount;

  IF __cart_product_amount = 0 THEN
      DELETE FROM Carts WHERE id = __id_cart;
  END IF;

  SET @id_product = __id_product;
  SET @message = 'Cart has been deleted!';

  SELECT @id_product, @message;
END ;;
DELIMITER ;