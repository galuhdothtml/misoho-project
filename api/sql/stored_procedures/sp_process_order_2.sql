-- proses pesanan
DELIMITER ;;

DROP PROCEDURE IF EXISTS sp_process_order_2;

CREATE PROCEDURE `sp_process_order_2`(
 IN __id INT(11), -- id order
 IN __id_employee INT(11)
)
BEGIN
  UPDATE Orders SET order_state = '3' WHERE id = __id;

  INSERT INTO OrderHistories(id_order, id_employee, order_state, createdAt, updatedAt) 
  VALUES(__id, __id_employee, '3', NOW(), NOW());

  SELECT * FROM Orders WHERE id = __id;
END ;;
DELIMITER ;