DELIMITER ;;

DROP PROCEDURE IF EXISTS sp_checkout_order;

CREATE PROCEDURE `sp_checkout_order`(
 IN __id_customer INT(11),
 IN __reference VARCHAR(255),
 IN __address_delivery TEXT,
 IN __shipping TEXT,
 IN __total_shipping DECIMAL(19,4),
 IN __id_employee INT(11)
)
BEGIN
  DECLARE __subtotal DECIMAL(19,4);
  DECLARE __id_order INT(11);
  
  DECLARE __id_cart INT(11);

  SELECT SUM(p.price * cd.qty) FROM CartDetails cd 
  JOIN Carts c ON c.id = cd.id_cart 
  JOIN Products p ON p.id = cd.id_product 
  WHERE id_customer = __id_customer INTO __subtotal;

  INSERT INTO Orders(
        reference,
        id_customer,
        address_delivery,
        order_state,
        payment,
        shipping,
        subtotal,
        total_shipping,
        grand_total,
        createdAt,
        updatedAt
  ) VALUES(
        __reference,
        __id_customer,
        __address_delivery,
        '1', -- menunggu pembayaran
        '1', -- bank transfer
        __shipping,
        __subtotal,
        __total_shipping,
        (__subtotal + __total_shipping),
        NOW(),
        NOW()
  );

  SELECT LAST_INSERT_ID() INTO __id_order;

  SELECT id FROM Carts WHERE id_customer = __id_customer LIMIT 1 INTO __id_cart;

  CALL sp_inner_checkout_order(__id_order, __id_cart);

  INSERT INTO OrderHistories(id_order, id_employee, order_state, createdAt, updatedAt) 
  VALUES(__id_order, __id_employee, '1', NOW(), NOW());
  
  DELETE FROM CartDetails WHERE id_cart = __id_cart;
  DELETE FROM Carts WHERE id = __id_cart;
  
  SELECT * FROM Orders WHERE id = __id_order;
END ;;
DELIMITER ;