'use strict';
import Util from '../utils';

module.exports = (sequelize, DataTypes) => {
  const OrderHistory = sequelize.define('OrderHistory', Util.formatModelDateTime({
    id_order: DataTypes.INTEGER,
    id_employee: DataTypes.INTEGER,
    order_state: DataTypes.STRING(2)
  }, DataTypes), {});
  OrderHistory.associate = function(models) {
    // associations can be defined here
  };
  return OrderHistory;
};