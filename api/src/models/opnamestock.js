'use strict';
import Util from '../utils';

module.exports = (sequelize, DataTypes) => {
  const OpnameStock = sequelize.define('OpnameStock', Util.formatModelDateTime({
    id_product: DataTypes.INTEGER,
    system_qty: DataTypes.DECIMAL(19, 4),
    actual_qty: DataTypes.DECIMAL(19, 4),
    is_draft: DataTypes.BOOLEAN,
  }, DataTypes), {});
  OpnameStock.associate = function(models) {
    // associations can be defined here
  };
  return OpnameStock;
};