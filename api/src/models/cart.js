'use strict';
import Util from '../utils';

module.exports = (sequelize, DataTypes) => {
  const Cart = sequelize.define('Cart', Util.formatModelDateTime({
    id_customer: DataTypes.STRING,
  }, DataTypes), {});
  Cart.associate = function(models) {
    // associations can be defined here
  };
  return Cart;
};