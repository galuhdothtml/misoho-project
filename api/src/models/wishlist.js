'use strict';
import Util from '../utils';

module.exports = (sequelize, DataTypes) => {
  const Wishlist = sequelize.define('Wishlist', Util.formatModelDateTime({
    id_product: DataTypes.STRING,
    id_customer: DataTypes.STRING
  }, DataTypes), {});
  Wishlist.associate = function(models) {
    // associations can be defined here
  };
  return Wishlist;
};