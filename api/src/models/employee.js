'use strict';
import Util from '../utils';

module.exports = (sequelize, DataTypes) => {
  const Employee = sequelize.define('Employee', Util.formatModelDateTime({
    name: DataTypes.STRING,
    username: DataTypes.STRING,
    email: DataTypes.STRING,
    passwd: DataTypes.STRING,
    type: DataTypes.STRING(2),
    privileges: DataTypes.TEXT,
    is_active: DataTypes.BOOLEAN,
  }, DataTypes), {});
  Employee.associate = function(models) {
    // associations can be defined here
  };
  return Employee;
};