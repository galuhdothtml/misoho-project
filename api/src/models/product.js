'use strict';
import Util from '../utils';

module.exports = (sequelize, DataTypes) => {
  const Product = sequelize.define('Product', Util.formatModelDateTime({
    id_category: DataTypes.INTEGER,
    name: DataTypes.STRING,
    slug: DataTypes.STRING,
    price: DataTypes.DECIMAL(19,4),
    description: DataTypes.TEXT,
    origin: DataTypes.INTEGER,
    weight: DataTypes.DECIMAL(19,4),
    popular_score: DataTypes.DECIMAL(19,4),
    is_active: DataTypes.BOOLEAN,
    primary_img: DataTypes.TEXT,
  }, DataTypes), {});
  Product.associate = function(models) {
    // associations can be defined here
  };
  return Product;
};