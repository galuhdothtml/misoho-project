'use strict';
import Util from '../utils';

module.exports = (sequelize, DataTypes) => {
  const Customer = sequelize.define('Customer', Util.formatModelDateTime({
    name: DataTypes.STRING,
    email: DataTypes.STRING,
    passwd: DataTypes.STRING,
    newsletter: DataTypes.BOOLEAN,
    is_active: DataTypes.BOOLEAN,
    is_confirmed: DataTypes.BOOLEAN,
  }, DataTypes), {});
  Customer.associate = function(models) {
    // associations can be defined here
  };
  return Customer;
};