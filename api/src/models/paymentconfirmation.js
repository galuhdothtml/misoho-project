'use strict';
import moment from 'moment';
import Util from '../utils';

module.exports = (sequelize, DataTypes) => {
  const PaymentConfirmation = sequelize.define('PaymentConfirmation', Util.formatModelDateTime({
    reference_number: DataTypes.STRING,
    is_auto: DataTypes.BOOLEAN,
    transfer_date: {
      type: DataTypes.DATE,
      get: function () {
          return moment.utc(this.getDataValue('transfer_date')).format('YYYY-MM-DD HH:mm:ss')
      }
    },
  }, DataTypes), {});
  PaymentConfirmation.associate = function(models) {
    // associations can be defined here
  };
  return PaymentConfirmation;
};