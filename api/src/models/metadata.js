'use strict';
import Util from '../utils';

module.exports = (sequelize, DataTypes) => {
  const MetaData = sequelize.define('MetaData', Util.formatModelDateTime({
    id_reference: DataTypes.INTEGER,
    table_name: DataTypes.STRING,
    meta_title: DataTypes.TEXT,
    meta_keywords: DataTypes.TEXT,
    meta_description: DataTypes.TEXT,
  }, DataTypes), {});
  MetaData.associate = function(models) {
    // associations can be defined here
  };
  return MetaData;
};