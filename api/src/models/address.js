'use strict';
import Util from '../utils';

module.exports = (sequelize, DataTypes) => {
  const Address = sequelize.define('Address', Util.formatModelDateTime({
    id_city: DataTypes.INTEGER,
    id_customer: DataTypes.INTEGER,
    alias: DataTypes.STRING,
    receiver_name: DataTypes.STRING,
    address: DataTypes.TEXT,
    sub_district: DataTypes.STRING,
    id_postal_code: DataTypes.INTEGER,
    postcode: DataTypes.STRING,
    phone: DataTypes.STRING,
    is_default: DataTypes.BOOLEAN
  }, DataTypes), {});
  Address.associate = function(models) {
    // associations can be defined here
  };
  return Address;
};