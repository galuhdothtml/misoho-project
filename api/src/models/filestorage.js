'use strict';
import Util from '../utils';

module.exports = (sequelize, DataTypes) => {
  const FileStorage = sequelize.define('FileStorage', Util.formatModelDateTime({
    tablename: DataTypes.STRING,
    filename: DataTypes.TEXT,
    id_reference: DataTypes.INTEGER
  }, DataTypes), {});
  FileStorage.associate = function(models) {
    // associations can be defined here
  };
  return FileStorage;
};