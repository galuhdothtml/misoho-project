'use strict';
import Util from '../utils';

module.exports = (sequelize, DataTypes) => {
  const CartDetail = sequelize.define('CartDetail', Util.formatModelDateTime({
    id_cart: DataTypes.INTEGER,
    id_product: DataTypes.INTEGER,
    qty: DataTypes.DECIMAL(19,4)
  }, DataTypes), {});
  CartDetail.associate = function(models) {
    // associations can be defined here
  };
  return CartDetail;
};