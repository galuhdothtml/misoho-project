'use strict';
import Util from '../utils';

module.exports = (sequelize, DataTypes) => {
  const Stock = sequelize.define('Stock', Util.formatModelDateTime({
    id_product: DataTypes.INTEGER,
    qty: DataTypes.DECIMAL(19, 4),
    min_qty: DataTypes.DECIMAL(19, 4),
  }, DataTypes), {});
  Stock.associate = function(models) {
    // associations can be defined here
  };
  return Stock;
};