'use strict';
import Util from '../utils';

module.exports = (sequelize, DataTypes) => {
  const Category = sequelize.define('Category', Util.formatModelDateTime({
    name: DataTypes.STRING,
    slug: DataTypes.STRING,
    description: DataTypes.TEXT,
    is_active: DataTypes.BOOLEAN,
  }, DataTypes), {});
  Category.associate = function(models) {
    // associations can be defined here
  };
  return Category;
};