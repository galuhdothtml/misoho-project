'use strict';
import Util from '../utils';

module.exports = (sequelize, DataTypes) => {
  const BackofficeMenu = sequelize.define('BackofficeMenu', Util.formatModelDateTime({
    title: DataTypes.STRING,
    content: DataTypes.TEXT,
    icon: DataTypes.STRING,
  }, DataTypes), {});
  BackofficeMenu.associate = function(models) {
    // associations can be defined here
  };
  return BackofficeMenu;
};