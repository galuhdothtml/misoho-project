'use strict';
import Util from '../utils';

module.exports = (sequelize, DataTypes) => {
  const ProductRating = sequelize.define('ProductRating', Util.formatModelDateTime({
    id_product: DataTypes.INTEGER,
    id_user: DataTypes.INTEGER,
    id_order: DataTypes.INTEGER,
    rating: DataTypes.DECIMAL(19,4),
    description: DataTypes.TEXT,
  }, DataTypes), {});
  ProductRating.associate = function(models) {
    // associations can be defined here
  };
  return ProductRating;
};