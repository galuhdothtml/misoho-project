'use strict';
import Util from '../utils';

module.exports = (sequelize, DataTypes) => {
  const Order = sequelize.define('Order', Util.formatModelDateTime({
    reference: DataTypes.STRING,
    id_customer: DataTypes.INTEGER,
    address_delivery: DataTypes.TEXT,
    order_state: DataTypes.STRING(2),
    payment: DataTypes.STRING(2),
    shipping: DataTypes.STRING(2),
    subtotal: DataTypes.DECIMAL(19,4),
    total_shipping: DataTypes.DECIMAL(19,4),
    grand_total: DataTypes.DECIMAL(19,4),
  }, DataTypes), {});
  Order.associate = function(models) {
    // associations can be defined here
  };
  return Order;
};