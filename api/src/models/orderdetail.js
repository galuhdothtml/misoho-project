'use strict';
import Util from '../utils';

module.exports = (sequelize, DataTypes) => {
  const OrderDetail = sequelize.define('OrderDetail', Util.formatModelDateTime({
    id_order: DataTypes.INTEGER,
    id_product: DataTypes.INTEGER,
    product_name: DataTypes.STRING,
    product_price: DataTypes.DECIMAL(19, 4),
    qty: DataTypes.DECIMAL(19, 4),
  }, DataTypes), {});
  OrderDetail.associate = function(models) {
    // associations can be defined here
  };
  return OrderDetail;
};