'use strict';
import Util from '../utils';

module.exports = (sequelize, DataTypes) => {
  const City = sequelize.define('City', Util.formatModelDateTime({
    name: DataTypes.STRING,
    id_province: DataTypes.INTEGER
  }, DataTypes), {});
  City.associate = function(models) {
    // associations can be defined here
  };
  return City;
};