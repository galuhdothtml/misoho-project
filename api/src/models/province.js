'use strict';
import Util from '../utils';

module.exports = (sequelize, DataTypes) => {
  const Province = sequelize.define('Province', Util.formatModelDateTime({
    name: DataTypes.STRING
  }, DataTypes), {});
  Province.associate = function(models) {
    // associations can be defined here
  };
  return Province;
};