'use strict';
import Util from '../utils';

module.exports = (sequelize, DataTypes) => {
  const TopSearch = sequelize.define('TopSearch', Util.formatModelDateTime({
    keyword: DataTypes.STRING
  }, DataTypes), {});
  TopSearch.associate = function(models) {
    // associations can be defined here
  };
  return TopSearch;
};