import Sequelize from "sequelize";
import request from "superagent";
import moment from "moment";
import jwt from 'jsonwebtoken';
import path from 'path';
import bcrypt from "bcrypt";
import fs from "fs";
import sharp from "sharp";
import kue from 'kue';
import { SECRET_KEY, ModelName } from '../constants';
import StoredProcedure from '../utils/stored_procedure';
import Db from '../utils/db';
import ModelFactory from '../utils/model_factory';
import Util from '../utils';
import GoogleUtil from '../utils/google_util';

const SALT_ROUNDS = 10;
const SALT_PASSWD = bcrypt.genSaltSync(SALT_ROUNDS);
const RAJA_ONGKIR_API_KEY = '323fb606f44b43b4febb6470be051f2c';
const Op = Sequelize.Op
const Address = ModelFactory.createModel(ModelName.ADDRESS);
const BackofficeMenu = ModelFactory.createModel(ModelName.BACKOFFICE_MENU);
const Product = ModelFactory.createModel(ModelName.PRODUCT);
const Category = ModelFactory.createModel(ModelName.CATEGORY);
const Employee = ModelFactory.createModel(ModelName.EMPLOYEE);
const Customer = ModelFactory.createModel(ModelName.CUSTOMER);
const Cart = ModelFactory.createModel(ModelName.CART);
const CartDetail = ModelFactory.createModel(ModelName.CART_DETAIL);
const Wishlist = ModelFactory.createModel(ModelName.WISHLIST);
const OrderHistory = ModelFactory.createModel(ModelName.ORDER_HISTORY);
const OrderDetail = ModelFactory.createModel(ModelName.ORDER_DETAIL);
const FileStorage = ModelFactory.createModel(ModelName.FILE_STORAGE);
const Province = ModelFactory.createModel(ModelName.PROVINCE);
const City = ModelFactory.createModel(ModelName.CITY);
const Order = ModelFactory.createModel(ModelName.ORDER);
const Stock = ModelFactory.createModel(ModelName.STOCK);
const PaymentConfirmation = ModelFactory.createModel(ModelName.PAYMENT_CONFIRMATION);
const PostalCode = ModelFactory.createModel(ModelName.POSTAL_CODE);
const ProductRating = ModelFactory.createModel(ModelName.PRODUCT_RATING);
const QueryBuilder = ModelFactory.createModel('default');

const getOrderStateName = (val) => {
    let retval = "";
  
    if (String(val) === "1") {
      retval = "Menunggu Pembayaran";
    } else if (String(val) === "2") {
      retval = "Terbayar";
    } else if (String(val) === "3") {
      retval = "Sedang Diproses";
    } else if (String(val) === "4") {
      retval = "Sudah Dikirim";
    } else if (String(val) === "5") {
        retval = "Sudah Diterima";
    }
  
    return retval;
};

const getFileName = (length = 4) => {
    var result = '';
    var characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    var charactersLength = characters.length;
    for (var i = 0; i < length; i++) {
        result += characters.charAt(Math.floor(Math.random() * charactersLength));
    }
    return result;
}

const checkDuplicateEmail = ({ email, id }) => {
    if (String(id).trim().length > 0) {
        return Customer.findOne({
            where: {
                email,
                id: { $not: id }
            }
        })
    }

    return Customer.findOne({
        where: {
            email
        }
    })
}

const queue = kue.createQueue();

class Api {
    constructor() {
        moment.locale('id');
    }

    index = (req, res) => {
        const data = 'Mister Oleh Oleh REST API';
        res.json({ status: true, data });
    }

    uploadFile = (req, res) => {
        const api = {
            "status": true,
            "data": req.file,
            "msg": 'success',
        }

        res.json(api);
    }

    uploadFiles = (req, res) => {
        const api = {
            "status": true,
            "data": req.files,
            "msg": 'success',
        }

        res.json(api);
    }

    getGoogleUrl = async (req, res) => {
        let api = {
            "status": false,
            "msg": 'error',
        };

        api = {
            "status": true,
            "data": GoogleUtil.urlGoogle(),
            "msg": 'success',
        }
    
        res.json(api);
    }

    setupGoogleAccount = async (req, res) => {
        const { code } = req.body;
        let api = {
            "status": false,
            "msg": 'error',
        };
        let data = {};
        const result = await GoogleUtil.getGoogleAccountFromCode(code);

        const exist = await Customer.findOne({
            where: { email: result.email },
            raw: true
        });

        if (exist) {
            data = Object.assign({}, exist, {
                type: 'user'
            });

            delete data.passwd;

            api = {
                "status": true,
                "data": data,
                "token": jwt.sign({ data }, SECRET_KEY, { expiresIn: '17520h' }),
            }
        } else {
            const payload = {
                name: result.displayName,
                email: result.email,
                passwd: bcrypt.hashSync(String(Util.randChar(6)), SALT_PASSWD)
            }
            const savedData = await Customer.create(payload);
            if (savedData) {
                data = Object.assign({}, Util.stringifyThenParse(savedData), {
                    type: 'user'
                });

                delete data.passwd;
            
                api = {
                    "status": true,
                    "data": data,
                    "token": jwt.sign({ data }, SECRET_KEY, { expiresIn: '17520h' }),
                }
            }
        }
    
        res.json(api);
    }

    register = async (req, res) => {
        const { name, email, password, confirm_password } = req.body;

        const isPasswordValid = (String(password) === String(confirm_password));
        let api = {
            status: false,
            msg: "wrong_confirm_password"
        }

        if (isPasswordValid) {
            const isExist = await checkDuplicateEmail({ email, id: null });

            if (isExist) {
                api = {
                    status: false,
                    msg: "email_already_exist"
                }
            } else {
                const data = await Customer.create({
                    name,
                    email: email,
                    passwd: bcrypt.hashSync(password, SALT_PASSWD)
                });
                const newData = Object.assign({}, Util.stringifyThenParse(data), {
                    type: 'user'
                });
                delete newData.passwd;

                api = {
                    status: true,
                    data,
                    token: jwt.sign({ data: newData }, SECRET_KEY, { expiresIn: '17520h' }),
                    msg: "success"
                }
            }
        }

        res.json(api);
    }

    login = async (req, res) => {
        const { type } = req.params;

        if (type === 'user') {
            const { email, password } = req.body;
            let isPasswordValid = false;
            let data = {};
            let api = {
                "status": false,
                "data": null,
            };
            
            if ((email && String(email).length > 0) && (password && String(password).length > 0)) {
                const result = await Customer.findOne({
                    where: { email },
                    raw: true
                });
    
                if (result) {
                    isPasswordValid = bcrypt.compareSync(password, result.passwd);
                    data = Object.assign({}, result, {
                        type: 'user'
                    });
    
                    delete data.passwd;
                }
    
                if (isPasswordValid) {
                    api = {
                        "status": true,
                        "data": data,
                        "token": jwt.sign({ data }, SECRET_KEY, { expiresIn: '17520h' }),
                    }
                }
            }

            res.json(api);
        } else if (type === 'employee') {
            const { username, password } = req.body;
            let isPasswordValid = false;
            let data = {};
            const result = await Employee.findOne({
                where: { username },
                raw: true
            });

            if (result) {
                isPasswordValid = bcrypt.compareSync(password, result.passwd);
                data = Object.assign({}, result, {
                    type: 'employee'
                });

                delete data.passwd;
            }

            let api = {
                "status": false,
                "data": null,
            };

            if (isPasswordValid) {
                api = {
                    "status": true,
                    "data": data,
                    "token": jwt.sign({ data }, SECRET_KEY, { expiresIn: '17520h' }),
                }
            }

            res.json(api);
        }
    }

    appMenu = async (req, res) => {
        let api = {
            "status": false,
            "msg": 'error',
        };

        const results = await BackofficeMenu.findAll();

        if (results) {
            const data = results.map(x => {
                if (!Array.isArray(JSON.parse(x.content))) {
                    const { link } = JSON.parse(x.content);
                    const newObj = Object.assign(Util.stringifyThenParse(x), { link });

                    delete newObj.content;

                    return newObj;
                }

                return Object.assign(x, { content: JSON.parse(x.content) });
            });
            api = {
                "status": true,
                "data": data,
                "msg": 'success',
            }
        }

        res.json(api);
    }

    /**
     * [ADMIN] menampilkan produk 
     */
    getBackofficeProducts = async (req, res) => {
        let api = {
            "status": false,
            "msg": 'error',
        };
        const { sorted, limit: _limit, page: _page, filterText, } = req.query;

        let limit = 5,
            page = 1;

        if (_limit) {
            limit = _limit;
        }

        if (_page) {
            page = _page;
        }

        const options = {
            offset: ((page - 1) * limit),
            limit: Number.parseInt(limit, 10),
            order: [['updatedAt', 'DESC']],
            include: [{
                model: Category
            }]
        };
        const calculateOptions = {
            attributes: [[Db.fn('COUNT', Db.col('id')), 'total']],
            raw: true
        }

        if (sorted) {
            const { id, desc } = JSON.parse(sorted);
            Object.assign(options, {
                order: Db.literal(`${id} ${(desc ? 'DESC' : 'ASC')}`)
            });
        }

        if (filterText) {
            const filter = {
                name: {
                    [Op.regexp]: `(${filterText})`,
                }
            }
            Object.assign(options, {
                where: filter,
            });
            Object.assign(calculateOptions, {
                where: filter,
            });
        }

        Category.hasMany(Product, { foreignKey: 'id_category', sourceKey: 'id' });
        Product.belongsTo(Category, { foreignKey: 'id_category', sourceKey: 'id' });
        const results = await Product.findAll(options);
        const data = Util.stringifyThenParse(results).map((x) => {
            const category_name = x.Category.name;

            delete x.Category;

            return Object.assign({}, x, { category_name });
        });
        const calculateAmountOfData = await Product.findOne(calculateOptions);
        const amountOfData = calculateAmountOfData.total;

        if (data) {
            api = {
                "status": true,
                "data": data,
                "page_total": Math.ceil(amountOfData / limit),
                "msg": 'success',
            }
        }

        res.json(api);
    }

    uploadProfilePhoto = async (req, res) => {
        const token = Util.getTokenFromRequest(req);
        const { data: { id: idCustomer } } = Util.decodeToken(token);
        
        let fileStorageFound = await FileStorage.findOne({
            where: {
                id_reference: idCustomer,
                tablename: 'Customers',
            }
        });

        if (fileStorageFound) {
            try {
                fs.unlinkSync(`public/uploads/${fileStorageFound.filename}`);
            } catch (err) {
                console.log(err);
            }
        }

        await FileStorage.destroy({
            where: {
                id_reference: idCustomer,
                tablename: 'Customers',
            }
        });

        let fileName = null;

        if (req.file) {
            fileName = req.file.filename;
        }

        await FileStorage.create({
            tablename: "Customers",
            filename: fileName,
            id_reference: idCustomer,
        });

        const api = {
            "status": true,
            "data": fileName,
        };

        res.json(api);
    }

    /**
     * menampilkan produk
     */
    showProducts = async (req, res) => {
        const { limit: _limit, page: _page, id_category, sort, origin, search, min_price, max_price } = req.query;
        let limit = 5,
            page = 1;

        if (_limit) {
            limit = _limit;
        }

        if (_page) {
            page = _page;
        }

        let options = {
            offset: ((page - 1) * limit),
            limit: Number.parseInt(limit, 10),
            order: [['createdAt', 'DESC']],
            include: [{
                model: Category
            }]
        };
        let calculateOptions = {
            attributes: [[Db.fn('COUNT', Db.col('id')), 'total']],
            raw: true
        }

        if (id_category && String(id_category) !== '') {
            options = Object.assign({}, options, {
                where: { id_category },
            });
            calculateOptions = Object.assign({}, calculateOptions, {
                where: { id_category },
            });
        }

        if (origin) {
            options = Object.assign({}, options, {
                where: options.where ? Object.assign({}, options.where, { origin }) : { origin },
            });
            calculateOptions = Object.assign({}, calculateOptions, {
                where: calculateOptions.where ? Object.assign({}, calculateOptions.where, { origin }) : { origin },
            });
        }

        if (search) {
            const filter = {
                name: {
                    [Op.regexp]: `(${search})`,
                }
            }
            options = Object.assign({}, options, {
                where: options.where ? Object.assign({}, options.where, filter) : filter,
            });
            calculateOptions = Object.assign({}, calculateOptions, {
                where: calculateOptions.where ? Object.assign({}, calculateOptions.where, filter) : filter,
            });
        }

        let priceRangeFilter = {};

        if (min_price && max_price) {
            priceRangeFilter = {
                price: {
                    [Op.between]: [min_price, max_price],
                }
            }
        } else if (min_price) {
            priceRangeFilter = {
                price: {
                    [Op.gte]: min_price,
                }
            }
        } else if (max_price) {
            priceRangeFilter = {
                price: {
                    [Op.lte]: max_price,
                }
            }
        }

        options = Object.assign({}, options, {
            where: options.where ? Object.assign({}, options.where, priceRangeFilter) : priceRangeFilter,
        });
        calculateOptions = Object.assign({}, calculateOptions, {
            where: calculateOptions.where ? Object.assign({}, calculateOptions.where, priceRangeFilter) : priceRangeFilter,
        });

        if (sort) {
            switch (sort) {
                case 'TERBARU':
                    options = Object.assign({}, options, {
                        order: [['createdAt', 'DESC']],
                    });
                    break;
                case 'TERPOPULER':
                    options = Object.assign({}, options, {
                        order: [['popular_score', 'DESC']],
                    });
                    break;
                case 'HARGA_TERENDAH':
                    options = Object.assign({}, options, {
                        order: [['price', 'ASC']],
                    });
                    break;
                case 'HARGA_TERTINGGI':
                    options = Object.assign({}, options, {
                        order: [['price', 'DESC']],
                    });
                    break;
                default:
                    options = Object.assign({}, options, {
                        order: [['createdAt', 'DESC']],
                    });
            }
        }

        Category.hasMany(Product, { foreignKey: 'id_category', sourceKey: 'id' });
        Product.belongsTo(Category, { foreignKey: 'id_category', sourceKey: 'id' });
        const products = await Product.findAll(options);
        const data = Util.stringifyThenParse(products).map((x) => {
            const category_name = x.Category.name;

            delete x.Category;

            return Object.assign({}, x, { category_name });
        })

        const calculateAmountOfData = await Product.findOne(calculateOptions);

        const amountOfData = calculateAmountOfData.total;

        const api = {
            "status": true,
            "data": data,
            "page_total": Math.ceil(amountOfData / limit)
        };

        res.json(api);
    }

    /**
     * [ADMIN] menampilkan kategori 
     */
    getBackofficeCategories = async (req, res) => {
        let api = {
            "status": false,
            "msg": 'error',
        };
        const { sorted, limit: _limit, page: _page, filterText, } = req.query;

        let limit = 0,
            page = 1;

        const options = {
            order: [['id', 'ASC']],
        };

        if (_limit) {
            if (_page) {
                page = _page;
            }

            limit = _limit;
            Object.assign(options, {
                offset: ((page - 1) * limit),
                limit: Number.parseInt(limit, 10),
            });
        }

        const calculateOptions = {
            attributes: [[Db.fn('COUNT', Db.col('id')), 'total']],
            raw: true
        }

        if (sorted) {
            const { id, desc } = JSON.parse(sorted);
            Object.assign(options, {
                order: Db.literal(`${id} ${(desc ? 'DESC' : 'ASC')}`)
            });
        }

        if (filterText) {
            const filter = {
                name: {
                    [Op.regexp]: `(${filterText})`,
                }
            }
            Object.assign(options, {
                where: filter,
            });
            Object.assign(calculateOptions, {
                where: filter,
            });
        }

        const results = await Category.findAll(options);
        const calculateAmountOfData = await Category.findOne(calculateOptions);
        const amountOfData = calculateAmountOfData.total;

        if (results) {
            api = {
                "status": true,
                "data": results,
                "page_total": Math.ceil(amountOfData / limit),
                "msg": 'success',
            }
        }

        res.json(api);
    }

    /**
     * menampilkan kategori
     */
    showCategories = async (req, res) => {
        /**
         * mandatory param: limit, page, is_unlimited
        */
        const { limit: _limit, page: _page, search, is_unlimited, with_image } = req.query;
        const isUnlimited = is_unlimited && is_unlimited === 'Y';
        let limit = 5,
            page = 1;
        let options = {
            raw: true,
            order: [['createdAt', 'DESC']],
        };
        let calculateOptions = {
            attributes: [[Db.fn('COUNT', Db.col('id')), 'total']],
            raw: true
        }

        if (_limit) {
            limit = _limit;
        }

        if (_page) {
            page = _page;
        }

        if (!isUnlimited) {
            options = Object.assign({}, options, {
                offset: ((page - 1) * limit),
                limit: Number.parseInt(limit, 10),
            });
        }

        if (search) {
            const filter = {
                name: {
                    [Op.regexp]: `(${search})`,
                }
            }
            options = Object.assign({}, options, {
                where: options.where ? Object.assign({}, options.where, filter) : filter,
            });
            calculateOptions = Object.assign({}, calculateOptions, {
                where: calculateOptions.where ? Object.assign({}, calculateOptions.where, filter) : filter,
            });
        }

        const data = await Category.findAll(options);

        let newData = Util.stringifyThenParse(data);

        if (with_image) {
            const fileStorages = await FileStorage.findAll({
                where: {
                    tablename: 'Categories',
                }
            });
            const parsedImages = Util.stringifyThenParse(fileStorages);
            newData = newData.map((x) => {
                const foundImg = parsedImages.find((pImg) => (String(pImg.id_reference) === String(x.id)));
                
                if (foundImg) {
                    return Object.assign({}, x, { img: foundImg.filename });
                }

                return x;
            });
        }

        const calculateAmountOfData = await Category.findOne(calculateOptions);

        const amountOfData = calculateAmountOfData.total;

        const api = {
            "status": true,
            "data": newData,
            "page_total": !isUnlimited ? Math.ceil(amountOfData / limit) : 1
        };

        res.json(api);
    }

    /**
     * checkout order
     */
    checkoutOrder = async (req, res) => {
        try {
            const {
                id_address,
                shipping_courier,
                shipping_service,
            } = req.body;
            const token = Util.getTokenFromRequest(req);
            const { data: { id: idCustomer } } = Util.decodeToken(token);
            let reference = `MO-${Util.randChar(8)}`;
            let found = await Order.findOne({
                where: {
                    reference,
                }
            });

            while (found) {
                reference = `MO-${Util.randChar(8)}`;
                found = await Order.findOne({
                    where: {
                        reference,
                    }
                });
            }

            const address = await Address.findOne({
                where: {
                    id: id_address,
                    id_customer: idCustomer
                }
            });

            let api = {
                "status": false,
                "msg": "address_not_found",
            };

            if (address) {
                // hitung total berat produk
                const weightResult = await Db.query(`SELECT IFNULL(SUM(p.weight*cd.qty), 0) AS weight_total FROM CartDetails cd 
                JOIN Products p ON p.id = cd.id_product 
                JOIN Carts ca ON ca.id = cd.id_cart 
                JOIN Customers c ON c.id = ca.id_customer WHERE ca.id_customer = ${idCustomer}`, { type: Db.QueryTypes.SELECT });

                let weight = 0;

                if (weightResult.length > 0) {
                    weight = weightResult[0].weight_total;
                }

                api = {
                    "status": false,
                    "msg": "weight_zero_is_not_allowed",
                };

                if (weight > 0) {
                    const shippingParam = {
                        key: RAJA_ONGKIR_API_KEY,
                        origin: '256', // malang
                        destination: address.id_city,
                        weight,
                        courier: shipping_courier,
                    }

                    const shippingRes = await request.post(`https://api.rajaongkir.com/starter/cost`).send(shippingParam);
                    const shippingCosts = shippingRes.body.rajaongkir.results[0].costs;
                    const shippingCost = shippingCosts.find(x => (x.service === shipping_service)).cost[0];

                    const execStoredProcedure = await StoredProcedure.run('sp_checkout_order', [
                        idCustomer,
                        `'${reference}'`,
                        `'${JSON.stringify(address)}'`,
                        `'${JSON.stringify({ courier: shipping_courier, service: shipping_service })}'`,
                        shippingCost.value,
                        '1' // id_employee
                    ]);

                    const data = execStoredProcedure.result;

                    api = {
                        "status": true,
                        "data": data,
                    };

                    queue.create('confirmPayment', data).delay(180 * 1000).save(); // 3 menit
                }
            }

            res.json(api);
        } catch (err) {
            console.log(err);
            res.json({ status: false, error: err });
        }
    }

    /**
     * menampilkan detail kategori
     */
    showCategory = async (req, res) => {
        const { id } = req.params;

        const data = await Category.findOne({
            where: {
                id
            }
        });

        const rawCategoryImg = await FileStorage.findOne({
            where: {
                tablename: 'Categories',
                id_reference: id,
            }
        });

        let categoryImg = "";

        if (rawCategoryImg) {
            const imageDir = path.resolve(__dirname).replace("dist/controllers", "");
            const imageBuf = await sharp(`${imageDir}public/uploads/${rawCategoryImg.filename}`)
            .png()
            .toBuffer();
            categoryImg = `data:image/png;base64,${imageBuf.toString('base64')}`;
        }

        const results = Util.stringifyThenParse(data);

        const newData = Object.assign({}, results, {
            base64: categoryImg,
            imgData: rawCategoryImg,
        });

        const api = {
            "status": true,
            "data": newData
        };

        res.json(api);
    }

    /**
     * update profile user
     */
    updateMyProfile = async (req, res) => {
        const token = Util.getTokenFromRequest(req);
        const { data: { id: idCustomer } } = Util.decodeToken(token);
        const { name, email } = req.body;

        const customer = await Customer.findById(idCustomer);

        const data = await customer.update({
            name,
            email,
        });

        const newData = Util.stringifyThenParse(data);

        delete newData.passwd;

        const api = {
            "status": true,
            "data": newData
        };

        res.json(api);
    }

    /**
     * update password user
     */
    updateMyPassword = async (req, res) => {
        const token = Util.getTokenFromRequest(req);
        const { data: { id: idCustomer } } = Util.decodeToken(token);
        const { old_password, new_password, confirm_new_password } = req.body;
        let api = {
            "status": false,
            "msg": "Password lama Anda masih belum benar"
        };

        const customer = await Customer.findById(idCustomer);
        const isPasswordValid = bcrypt.compareSync(old_password, customer.passwd);
        if (isPasswordValid) {
            const isConfirmed = String(new_password) === String(confirm_new_password);
            if (isConfirmed) {
                const data = await customer.update({
                    passwd: bcrypt.hashSync(new_password, SALT_PASSWD),
                });

                const newData = Util.stringifyThenParse(data);

                delete newData.passwd;

                api = {
                    "status": true,
                    "data": newData
                };
            } else {
                api = {
                    "status": false,
                    "msg": "Konfirmasi password Anda masih belum benar",
                };
            }
        }

        res.json(api);
    }

    /**
     * [ADMIN] create kategori
     */
    createCategory = async (req, res) => {
        const { name, description } = req.body;

        const data = await Category.create({
            name,
            slug: Util.slugify(name),
            description
        });

        const api = {
            "status": true,
            "data": Util.stringifyThenParse(data)
        };

        res.json(api);
    }

    /**
     * [ADMIN] update kategori
     */
    updateCategory = async (req, res) => {
        const { id, name, description } = JSON.parse(req.body.data);

        let api = {
            "status": false,
            "data": "doesn't exist"
        };

        const category = await Category.findById(id);

        if (category) {
            let fileStorageFound = await FileStorage.findOne({
                where: {
                    id_reference: id,
                    tablename: 'Categories',
                }
            });
    
            if (fileStorageFound) {
                try {
                    fs.unlinkSync(`public/uploads/${fileStorageFound.filename}`);
                } catch (err) {
                    console.log(err);
                }
    
                await FileStorage.destroy({
                    where: {
                        id_reference: id,
                        tablename: 'Categories',
                    }
                });
            }
    
            if (req.file) {
                const fileName = req.file.filename;
                await FileStorage.create({
                    tablename: "Categories",
                    filename: fileName,
                    id_reference: id,
                });
            }

            const data = category.update({
                name,
                slug: Util.slugify(name),
                description
            });
    
            api = {
                "status": true,
                "data": Util.stringifyThenParse(data)
            };
        }

        res.json(api);
    }

    /**
     * [ADMIN] hapus kategori
     */
    deleteCategory = async (req, res) => {
        // const { id } = req.body;

        // let data = await Category.destroy({
        //     where: {
        //         id
        //     }
        // });

        // const api = {
        //     "status": true,
        //     "data": Util.stringifyThenParse(data)
        // };

        const api = {
            "status": true,
            "msg": "success"
        };

        res.json(api);
    }

    bulkDeleteCategory = async (req, res) => {
        const api = {
            "status": true,
            "msg": "success"
        };

        res.json(api);
    }

    /**
     * menampilkan detail produk
     */
    showProductDetail = async (req, res) => {
        const { id } = req.params;
        const imageDir = path.resolve(__dirname).replace("dist/controllers", "");
        
        City.belongsTo(Province, { foreignKey: 'id_province', sourceKey: 'id' });
        Province.hasMany(City, { foreignKey: 'id_province', sourceKey: 'id' });

        Product.belongsTo(City, { foreignKey: 'origin', sourceKey: 'id' });
        City.hasMany(Product, { foreignKey: 'origin', sourceKey: 'id' });

        ProductRating.belongsTo(Product, { foreignKey: 'id_product', sourceKey: 'id' });
        Product.hasMany(ProductRating, { foreignKey: 'id_product', sourceKey: 'id' });

        Product.belongsTo(Category, { foreignKey: 'id_category', sourceKey: 'id' });
        Category.hasMany(Product, { foreignKey: 'id_category', sourceKey: 'id' });
        const data = await Product.findOne({
            where: {
                id
            },
            include: [{
                model: Category,
            }, {
                model: ProductRating,
            }, {
                model: City,
                include: [{
                    model: Province,
                }]
            }],
        });

        let fileStorages = await FileStorage.findAll({
            where: {
                id_reference: id,
                tablename: 'Products',
            }
        });

        let productData = Util.stringifyThenParse(data);
        productData = Object.assign(productData, {
            cityName: productData.City.name,
            provinceId: productData.City.id_province,
            provinceName: productData.City.Province.name,
        });

        delete productData.City;

        const images = Util.stringifyThenParse(fileStorages);

        const stockFound = await Stock.findOne({
            where: {
                id_product: id,
            }
        });

        let availableQty = 0;
        let minQty = 0;

        if (stockFound) {
            availableQty = stockFound.qty;
            minQty = stockFound.min_qty;
        }

        const newProductRatingPromises = productData.ProductRatings.map(async pr => {
            const prImages = await FileStorage.findAll({
                where: {
                    id_reference: pr.id,
                    tablename: 'ProductRatings'
                }
            });

            const profilePhoto = await FileStorage.findOne({
                where: {
                    tablename: 'Customers',
                    id_reference: pr.id_user,
                }
            });

            const customer = await Customer.findOne({
                where: {
                    id: pr.id_user
                }
            });
            const parsedCustomer = Util.stringifyThenParse(customer);

            delete parsedCustomer.passwd;

            return Object.assign({}, pr, { customer: parsedCustomer, images: prImages, profilePhoto });
        });

        const newProductRatings = await Promise.all(newProductRatingPromises);

        const api = {
            "status": true,
            "data": Object.assign({}, productData, { available_qty: availableQty, min_qty: minQty, images, ProductRatings: newProductRatings })
        };

        res.json(api);
    }

    /**
     * menampilkan images produk
     */
    showProductImages = async (req, res) => {
        const { id } = req.params;

        const imageDir = path.resolve(__dirname).replace("dist/controllers", "");
        const data = await FileStorage.findAll({
            where: {
                id_reference: id,
                tablename: 'Products'
            }
        });

        const newImages = [];
        let loopIndex = 0;

        const images = Util.stringifyThenParse(data);

        while (loopIndex < images.length) {
            const x = images[loopIndex];
            const imageBuf = await sharp(`${imageDir}public/uploads/${x.filename}`)
                .resize(500)
                .png()
                .toBuffer();
            const base64Data = `data:image/png;base64,${imageBuf.toString('base64')}`;

            const newData = Object.assign({}, x, { base64Data });
            newImages.push(newData);
            loopIndex += 1;
        }

        const api = {
            "status": true,
            "data": newImages,
        };

        res.json(api);
    }

    /**
     * [ADMIN] create produk
     */
    createProduct = async (req, res) => {
        const paramData = req.body;
        const {
            id_category,
            name,
            price,
            description,
            origin,
            weight,
            available_qty,
            min_qty,
            photos: photosValue
        } = paramData;

        let newFiles = [];

        if (photosValue) {
            newFiles = photosValue;
        }

        const productCreated = await Product.create({
            id_category,
            name,
            slug: Util.slugify(name),
            price,
            description,
            origin,
            weight,
            popular_score: 0,
            is_active: 1,
            primary_img: newFiles.length > 0 ? newFiles[0] : ''
        });

        const product = Util.stringifyThenParse(productCreated);

        const photos = newFiles.map((x) => {
            const oldPath = `public/uploads/temp/${x}`;
            const newPath = `public/uploads/${x}`;
            fs.renameSync(oldPath, newPath);

            return {
                tablename: "Products",
                filename: x,
                id_reference: product.id
            };
        });

        await Stock.create({
            id_product: product.id,
            qty: available_qty,
            min_qty,
        });

        const fileStorage = await FileStorage.bulkCreate(photos);

        const data = Object.assign({}, product, { images: fileStorage });

        const api = {
            "status": true,
            "data": data
        };

        res.json(api);
    }

    /**
     * [ADMIN] update produk
     */
    updateProduct = async (req, res) => {
        const paramData = req.body;
        const {
            id,
            id_category,
            name,
            price,
            description,
            origin,
            weight,
            min_qty,
            photos: photosValue
        } = paramData;

        let newFiles = [];

        if (photosValue) {
            newFiles = photosValue;
        }

        let fileStorage = await FileStorage.findAll({
            where: {
                id_reference: id,
                tablename: 'Products',
            }
        });

        await FileStorage.destroy({
            where: {
                id_reference: id,
                tablename: 'Products',
            }
        });

        const product = await Product.findById(id);

        const updateData = await product.update({
            id_category,
            name,
            slug: Util.slugify(name),
            price,
            description,
            origin,
            weight,
            primary_img: newFiles.length > 0 ? newFiles[0] : ''
        });

        const resProduct = Util.stringifyThenParse(product);

        const stockFound = await Stock.findOne({
            where: {
                id_product: resProduct.id,
            }
        });
        await stockFound.update({
            min_qty,
        });

        const photos = newFiles.map((x) => {
            try {
                const oldPath = `public/uploads/temp/${x}`;
                if (fs.existsSync(oldPath)) {
                    const newPath = `public/uploads/${x}`;
                    fs.renameSync(oldPath, newPath);
                }
            } catch (err) {
                console.error(err)
            }

            return {
                tablename: "Products",
                filename: x,
                id_reference: resProduct.id
            };
        });

        fileStorage = await FileStorage.bulkCreate(photos);

        const data = Object.assign({}, Util.stringifyThenParse(updateData), { images: fileStorage });

        const api = {
            "status": true,
            "data": data
        };

        res.json(api);
    }

    /**
     * [ADMIN] hapus produk
     */
    deleteProduct = async (req, res) => {
        // const {
        //     id,
        // } = req.body;

        // let product = await Product.destroy({
        //     where: {
        //         id
        //     }
        // });

        // const fileStorage = await FileStorage.findAll({
        //     where: {
        //         id_reference: id,
        //         tablename: 'Products',
        //     }
        // });

        // Util.stringifyThenParse(fileStorage).forEach(x => {
        //     try {
        //         fs.unlinkSync(`public/uploads/${x.filename}`);
        //     } catch (err) {
        //         console.log(err);
        //     }
        // });

        // await FileStorage.destroy({
        //     where: {
        //         id_reference: id,
        //         tablename: 'Products',
        //     }
        // });

        // await CartDetail.destroy({
        //     where: {
        //         id_product: id,
        //     }
        // });

        // await Wishlist.destroy({
        //     where: {
        //         id_product: id,
        //     }
        // });

        // const api = {
        //     "status": true,
        //     "data": Util.stringifyThenParse(product)
        // };

        const api = {
            "status": true,
            "msg": "success"
        };

        res.json(api);
    }

    bulkDeleteProduct = async (req, res) => {
        // const { ids } = req.body;
        // const deleteIds = JSON.parse(ids);

        // let product = await Product.destroy({
        //     where: {
        //         id: deleteIds
        //     }
        // });

        // const fileStorage = await FileStorage.findAll({
        //     where: {
        //         id_reference: deleteIds,
        //         tablename: 'Products',
        //     }
        // });

        // Util.stringifyThenParse(fileStorage).forEach(x => {
        //     try {
        //         fs.unlinkSync(`public/uploads/${x.filename}`);
        //     } catch (err) {
        //         console.log(err);
        //     }
        // });

        // await FileStorage.destroy({
        //     where: {
        //         id_reference: deleteIds,
        //         tablename: 'Products',
        //     }
        // });

        // await CartDetail.destroy({
        //     where: {
        //         id_product: deleteIds,
        //     }
        // });

        // await Wishlist.destroy({
        //     where: {
        //         id_product: deleteIds,
        //     }
        // });

        // const api = {
        //     "status": true,
        //     "data": Util.stringifyThenParse(product)
        // };

        const api = {
            "status": true,
            "msg": "success"
        };

        res.json(api);
    }

    /**
     * menampilkan keranjang belanja customer
     */
    showMyCarts = async (req, res) => {
        const token = Util.getTokenFromRequest(req);
        const { data: { id: idCustomer } } = Util.decodeToken(token);

        Cart.hasMany(CartDetail, { foreignKey: 'id_cart', sourceKey: 'id' });
        CartDetail.belongsTo(Cart, { foreignKey: 'id_cart', sourceKey: 'id' });
        Product.hasMany(CartDetail, { foreignKey: 'id_product', sourceKey: 'id' });
        CartDetail.belongsTo(Product, { foreignKey: 'id_product', sourceKey: 'id' });
        const result = await Cart.findOne({
            where: {
                id_customer: idCustomer
            },
            include: [{
                model: CartDetail,
                include: [{
                    model: Product,
                }]
            }],
        });
        const resultData = Util.stringifyThenParse(result);
        let totalWeight = 0;
        let carts = [];

        if (resultData) {
            carts = resultData.CartDetails.map((x) => {
                totalWeight = totalWeight + (parseFloat(x.Product.weight, 10) * parseFloat(x.qty, 10));

                return {
                    id: x.id,
                    id_product: x.id_product,
                    qty: parseFloat(x.qty, 10),
                    product_name: x.Product.name,
                    product_slug: x.Product.slug,
                    product_price: x.Product.price,
                    product_weight: parseFloat(x.Product.weight, 10),
                    primary_img: x.Product.primary_img,
                };
            });
        }

        const data = Object.assign({}, resultData, { carts, total_weight: totalWeight });

        delete data.CartDetails;

        const api = {
            "status": true,
            "data": data
        };

        res.json(api);
    }

    /**
     * menampilkan dashboard customer
     */
    showMyDashboard = async (req, res) => {
        const api = {
            "status": true,
            "data": "coming soon"
        };

        res.json(api);
    }

    /**
     * menampilkan informasi akun customer
     */
    showMyAccountInformation = async (req, res) => {
        const token = Util.getTokenFromRequest(req);
        const { data: { id: idCustomer } } = Util.decodeToken(token);

        const data = await Customer.findOne({
            where: {
                id: idCustomer
            }
        });

        const rawProfilePhoto = await FileStorage.findOne({
            where: {
                tablename: 'Customers',
                id_reference: idCustomer,
            }
        });

        let profilePhoto = "";

        if (rawProfilePhoto) {
            const imageDir = path.resolve(__dirname).replace("dist/controllers", "");
            const imageBuf = await sharp(`${imageDir}public/uploads/${rawProfilePhoto.filename}`)
            .png()
            .toBuffer();
            profilePhoto = `data:image/png;base64,${imageBuf.toString('base64')}`;
        }

        const results = Util.stringifyThenParse(data);

        delete results.passwd;

        const newData = Object.assign({}, results, {
            profile_photo: profilePhoto,
        });

        const api = {
            "status": true,
            "data": newData
        };

        res.json(api);
    }

    /**
     * menambahkan produk ke dalam keranjang belanja
     */
    addToCart = async (req, res) => {
        const token = Util.getTokenFromRequest(req);
        const { data: { id: idCustomer } } = Util.decodeToken(token);
        const { id_product: idProduct, qty } = req.body;

        try {
            const execStoredProcedure = await StoredProcedure.run('sp_add_to_cart', [
                idCustomer,
                idProduct,
                qty
            ]);

            const data = execStoredProcedure.result;
            const api = {
                "status": true,
                "data": data
            };
            res.json(api);
        } catch (err) {
            res.json({ status: false, error: err });
        }
    }

    /**
     * mengubah qty produk keranjang belanja
     */
    updateCart = async (req, res) => {
        const token = Util.getTokenFromRequest(req);
        const { data: { id: idCustomer } } = Util.decodeToken(token);
        const { id_product: idProduct, qty } = req.body;

        try {
            const execStoredProcedure = await StoredProcedure.run('sp_update_cart', [
                idCustomer,
                idProduct,
                qty
            ]);

            const data = execStoredProcedure.result;
            const api = {
                "status": true,
                "data": data
            };
            res.json(api);
        } catch (err) {
            res.json({ status: false, error: err });
        }
    }

    /**
     * menghapus produk keranjang belanja
     */
    deleteCart = async (req, res) => {
        const token = Util.getTokenFromRequest(req);
        const { data: { id: idCustomer } } = Util.decodeToken(token);
        const { id_product: idProduct } = req.body;

        try {
            const execStoredProcedure = await StoredProcedure.run('sp_delete_cart', [
                idCustomer,
                idProduct,
            ]);

            const data = execStoredProcedure.result;
            const api = {
                "status": true,
                "data": data
            };
            res.json(api);
        } catch (err) {
            res.json({ status: false, error: err });
        }
    }

    /**
     * [ADMIN] menampilkan alamat semua customer
     */
    showAddresses = async (req, res) => {
        /**
         * mandatory param: limit, page
        */
        const { limit, page, filterText: search } = req.query;
        let options = {
            offset: ((page - 1) * limit),
            limit: Number.parseInt(limit, 10),
            order: [['createdAt', 'DESC']],
            include: [{
                model: Customer,
            }],
        };
        let calculateOptions = {
            attributes: [[Db.fn('COUNT', Db.col('id')), 'total']],
            raw: true
        }

        if (search) {
            const filter = {
                alias: {
                    [Op.regexp]: `(${search})`,
                }
            }
            options = Object.assign({}, options, {
                where: options.where ? Object.assign({}, options.where, filter) : filter,
            });
            calculateOptions = Object.assign({}, calculateOptions, {
                where: calculateOptions.where ? Object.assign({}, calculateOptions.where, filter) : filter,
            });
        }

        Customer.hasMany(Address, { foreignKey: 'id_customer', sourceKey: 'id' });
        Address.belongsTo(Customer, { foreignKey: 'id_customer', sourceKey: 'id' });
        const data = await Address.findAll(options);

        const calculateAmountOfData = await Address.findOne(calculateOptions);

        const amountOfData = calculateAmountOfData.total;

        const api = {
            "status": true,
            "data": data,
            "page_total": Math.ceil(amountOfData / limit)
        };

        res.json(api);
    }

    /**
     * [ADMIN] menampilkan alamat customer
     */
    showAddress = async (req, res) => {
        const { id } = req.params;

        Customer.hasMany(Address, { foreignKey: 'id_customer', sourceKey: 'id' });
        Address.belongsTo(Customer, { foreignKey: 'id_customer', sourceKey: 'id' });
        City.hasMany(Address, { foreignKey: 'id_city', sourceKey: 'id' });
        Address.belongsTo(City, { foreignKey: 'id_city', sourceKey: 'id' });
        const data = await Address.findOne({
            where: {
                id
            },
            include: [{
                model: City,
            }, {
                model: Customer,
            }],
        });

        const api = {
            "status": true,
            "data": data
        };

        res.json(api);
    }

    /**
     * create alamat customer
     */
    createAddress = async (req, res) => {
        const token = Util.getTokenFromRequest(req);
        const { data: { id: idCustomer, type } } = Util.decodeToken(token);
        const {
            cityId: id_city,
            id_customer,
            alias,
            receiver_name,
            address,
            sub_district,
            id_postal_code,
            postcode,
            phone
        } = req.body;
        let data;

        if (type === 'user') {
            data = await Address.create({
                id_city,
                id_customer: idCustomer,
                alias,
                receiver_name,
                address,
                sub_district,
                id_postal_code,
                postcode,
                phone,
                is_default: false
            });
        } else {
            data = await Address.create({
                id_city,
                id_customer,
                alias,
                receiver_name,
                address,
                sub_district,
                id_postal_code,
                postcode,
                phone,
                is_default: false
            });
        }

        const calculateOptions = {
            where: {
                id_customer: idCustomer
            },
            attributes: [[Db.fn('COUNT', Db.col('id')), 'total']],
            raw: true
        }
        const calculateAmountOfData = await Address.findOne(calculateOptions);
        const isFirstTime = calculateAmountOfData.total === 1;

        if (isFirstTime) {
            const options = {
                where: {
                    id_customer: idCustomer
                }
            }
            const addressData = await Address.findOne(options);
            data = await addressData.update({
                is_default: true
            });
        }

        const api = {
            "status": true,
            "data": Util.stringifyThenParse(data)
        };

        res.json(api);
    }

    /**
     * update alamat customer
     */
    updateAddress = async (req, res) => {
        const token = Util.getTokenFromRequest(req);
        const { data: { id: idCustomer, type } } = Util.decodeToken(token);
        const {
            id,
            cityId: id_city,
            id_customer,
            alias,
            receiver_name,
            address,
            sub_district,
            id_postal_code,
            postcode,
            phone
        } = req.body;
        let data = { message: 'data tidak ditemukan' }, addressData, options = {
            where: {
                id,
            }
        };

        if (type === 'user') {
            options = {
                where: {
                    id,
                    id_customer: idCustomer
                }
            }
            addressData = await Address.findOne(options);
            if (addressData) {
                data = await addressData.update({
                    id_city,
                    id_customer: idCustomer,
                    alias,
                    receiver_name,
                    address,
                    sub_district,
                    id_postal_code,
                    postcode,
                    phone,
                    is_default: false
                });
            }
        } else {
            addressData = await Address.findOne(options);
            data = await addressData.update({
                id_city,
                id_customer,
                alias,
                receiver_name,
                address,
                sub_district,
                id_postal_code,
                postcode,
                phone,
                is_default: false
            });
        }

        const api = {
            "status": true,
            "data": Util.stringifyThenParse(data)
        };

        res.json(api);
    }

    /**
     * hapus alamat customer
     */
    deleteAddress = async (req, res) => {
        const token = Util.getTokenFromRequest(req);
        const { data: { id: idCustomer, type } } = Util.decodeToken(token);
        const { id } = req.body;
        let data = null;

        if (type === 'user') {
            data = await Address.destroy({
                where: {
                    id,
                    id_customer: idCustomer
                }
            });
        } else {
            data = await Address.destroy({
                where: {
                    id
                }
            });
        }

        const api = {
            "status": true,
            data,
            "msg": "success"
        };

        res.json(api);
    }

    /**
     * [ADMIN] bulk hapus alamat customer
     */
    bulkDeleteAddress = async (req, res) => {
        const api = {
            "status": true,
            "msg": "success"
        };

        res.json(api);
    }

    /**
     * set default address
     */
    setDefaultAddress = async (req, res) => {
        const token = Util.getTokenFromRequest(req);
        const { data: { id: idCustomer } } = Util.decodeToken(token);
        const { id_address } = req.body;

        const execStoredProcedure = await StoredProcedure.run('sp_set_default_address', [
            idCustomer,
            id_address,
        ]);

        const data = execStoredProcedure.result;

        const api = {
            "status": true,
            "data": data
        };

        res.json(api);
    }

    /**
     * menampilkan alamat customer
     */
    showMyAddresses = async (req, res) => {
        /**
         * mandatory param: page
        */
        const token = Util.getTokenFromRequest(req);
        const { data: { id: idCustomer } } = Util.decodeToken(token);
        const { page, search } = req.query;
        const limit = 5;
        let options = {
            where: {
                id_customer: idCustomer
            },
            offset: ((page - 1) * limit),
            limit: Number.parseInt(limit, 10),
            order: [['is_default', 'DESC']],
            include: [
            {
                model: PostalCode,
            },
            {
                model: City,
                include: [{
                    model: Province,
                }]
            }]
        };
        let calculateOptions = {
            where: {
                id_customer: idCustomer
            },
            attributes: [[Db.fn('COUNT', Db.col('id')), 'total']],
            raw: true
        }

        if (search) {
            const filter = {
                address: {
                    [Op.regexp]: `(${search})`,
                }
            }
            options = Object.assign({}, options, {
                where: options.where ? Object.assign({}, options.where, filter) : filter,
            });
            calculateOptions = Object.assign({}, calculateOptions, {
                where: calculateOptions.where ? Object.assign({}, calculateOptions.where, filter) : filter,
            });
        }

        Address.belongsTo(PostalCode, { foreignKey: 'id_postal_code', sourceKey: 'id' });
        PostalCode.hasMany(Address, { foreignKey: 'id_postal_code', sourceKey: 'id' });
        City.belongsTo(Province, { foreignKey: 'id_province', sourceKey: 'id' });
        Province.hasMany(City, { foreignKey: 'id_province', sourceKey: 'id' });
        Address.belongsTo(City, { foreignKey: 'id_city', sourceKey: 'id' });
        City.hasMany(Address, { foreignKey: 'id_city', sourceKey: 'id' });
        const data = await Address.findAll(options);

        const calculateAmountOfData = await Address.findOne(calculateOptions);
        const amountOfData = calculateAmountOfData.total;

        const results = Util.stringifyThenParse(data).map((x) => {
            const retval = Object.assign({}, x, {
                city_name: x.City.name,
                province_name: x.City.Province.name,
                id_province: x.City.id_province,
            });

            delete retval.City;

            return retval;
        });

        const api = {
            "status": true,
            "data": results,
            "page_total": Math.ceil(amountOfData / limit)
        };

        res.json(api);
    }

    /**
     * menampilkan detail alamat customer
     */
    showMyAddress = async (req, res) => {
        const token = Util.getTokenFromRequest(req);
        const { data: { id: idCustomer } } = Util.decodeToken(token);
        const { id } = req.params;
        Address.belongsTo(PostalCode, { foreignKey: 'id_postal_code', sourceKey: 'id' });
        PostalCode.hasMany(Address, { foreignKey: 'id_postal_code', sourceKey: 'id' });
        City.belongsTo(Province, { foreignKey: 'id_province', sourceKey: 'id' });
        Province.hasMany(City, { foreignKey: 'id_province', sourceKey: 'id' });
        Address.belongsTo(City, { foreignKey: 'id_city', sourceKey: 'id' });
        City.hasMany(Address, { foreignKey: 'id_city', sourceKey: 'id' });
        const data = await Address.findOne({
            where: {
                id,
                id_customer: idCustomer
            },
            include: [{
               model: PostalCode,
            }, {
                model: City,
                include: [{
                    model: Province,
                }]
            }]
        });

        const x = Util.stringifyThenParse(data);
        const newData = Object.assign({}, x, {
            city_name: x.City.name,
            id_province: x.City.Province.id,
            province_name: x.City.Province.name
        });

        delete newData.City

        const api = {
            "status": true,
            "data": newData
        };

        res.json(api);
    }

    /**
     * menampilkan order customer
     */
    showMyOrders = async (req, res) => {
        const imageDir = path.resolve(__dirname).replace("dist/controllers", "");
        const token = Util.getTokenFromRequest(req);
        const { data: { id: idCustomer } } = Util.decodeToken(token);
        const { limit, page, filterText: search } = req.query;
        let options = {
           offset: ((page - 1) * limit),
           limit: Number.parseInt(limit, 10),
           order: [['createdAt', 'DESC']],
           include: [{
               model: Customer,
           }, {
               model: OrderDetail,
               include: [Product]
           }, {
            model: ProductRating,
           }],
           where: {
            id_customer: idCustomer,
           },
        };
        let calculateOptions = {
           attributes: [[Db.fn('COUNT', Db.col('id')), 'total']],
           raw: true,
           where: {
            id_customer: idCustomer,
           }
        }

       if (search) {
           const filter = {
               reference: {
                   [Op.regexp]: `(${search})`,
               }
           }
           options = Object.assign({}, options, {
               where: options.where ? Object.assign({}, options.where, filter) : filter,
           });
           calculateOptions = Object.assign({}, calculateOptions, {
               where: calculateOptions.where ? Object.assign({}, calculateOptions.where, filter) : filter,
           });
       }
       
       Order.hasMany(ProductRating, { foreignKey: 'id_order', sourceKey: 'id' });
       ProductRating.belongsTo(Order, { foreignKey: 'id_order', sourceKey: 'id' });
       Customer.hasMany(Order, { foreignKey: 'id_customer', sourceKey: 'id' });
       Order.belongsTo(Customer, { foreignKey: 'id_customer', sourceKey: 'id' });
       Order.hasMany(OrderDetail, { foreignKey: 'id_order', sourceKey: 'id' });
       OrderDetail.belongsTo(Order, { foreignKey: 'id_order', sourceKey: 'id' });
       Product.hasOne(OrderDetail, { foreignKey: 'id_product', sourceKey: 'id' });
       OrderDetail.belongsTo(Product, { foreignKey: 'id_product', sourceKey: 'id' });
       const data = await Order.findAll(options);
       
       const calculateAmountOfData = await Order.findOne(calculateOptions);

       const amountOfData = calculateAmountOfData.total;

       let newData = Util.stringifyThenParse(data);
       let loopIndex = 0;

       let formattedData = [];

       while (loopIndex < newData.length) {
            const x = newData[loopIndex];
            const newProductRatingPromises = x.ProductRatings.map(async pr => {
                const prImages = await FileStorage.findAll({
                    where: {
                        id_reference: pr.id,
                        tablename: 'ProductRatings'
                    }
                });

                const prImagePromises = prImages.map(async img => {
                    const imageBuf = await sharp(`${imageDir}public/uploads/${img.filename}`)
                        .resize(500)
                        .png()
                        .toBuffer();
                    const base64Data = `data:image/png;base64,${imageBuf.toString('base64')}`;
                    const newPrImageData = Object.assign({}, Util.stringifyThenParse(img), { base64Data });
                    return newPrImageData;
                });

                const prImageData = await Promise.all(prImagePromises);

                return Object.assign({}, pr, { images: prImageData });
            });

            const newProductRatings = await Promise.all(newProductRatingPromises);
            const formatData = Object.assign({}, x, {
                ProductRatings: newProductRatings,
            });

            formattedData.push(formatData);

            loopIndex += 1;
       }

       const api = {
           "status": true,
           "data": formattedData,
           "page_total": Math.ceil(amountOfData / limit)
       };

       res.json(api);
    }

    /**
     * menampilkan detail order customer
     */
    showMyOrderDetail = async (req, res) => {
        const token = Util.getTokenFromRequest(req);
        const { data: { id: idCustomer } } = Util.decodeToken(token);
        const { id } = req.params;

        const data = await Order.findOne({
            where: {
                id,
                id_customer: idCustomer,
            }
        });

        const x = Util.stringifyThenParse(data);
        const parseAddress = JSON.parse(x.address_delivery);
        const cityData = await City.findOne({
            where: {
                id: parseAddress.id_city
            }
        });
        const provinceData = await Province.findOne({
            where: {
                id: cityData.id_province
            }
        });
        const orderHistoryData = await OrderHistory.findAll({
            where: {
                id_order: id,
            },
            order: [['createdAt', 'DESC']],
        });
        const parsedOrderHistoryData = Util.stringifyThenParse(orderHistoryData);
        const newOrderHistory = parsedOrderHistoryData.map((oh) => (Object.assign({}, oh, { order_state_text: getOrderStateName(oh.order_state) })));

        Product.hasOne(OrderDetail, { foreignKey: 'id_product', sourceKey: 'id' });
        OrderDetail.belongsTo(Product, { foreignKey: 'id_product', sourceKey: 'id' });
        const orderDetailData = await OrderDetail.findAll({
            include: [Product],
            where: {
                id_order: id,
            },
            order: [['product_name', 'DESC']],
        });

        const newData = Object.assign({}, x, {
            address_delivery_text: `${parseAddress.address} ${cityData.name} ${provinceData.name}`,
            postcode: parseAddress.postcode,
            receiver_name: parseAddress.receiver_name,
            order_history: newOrderHistory,
            order_state_text: getOrderStateName(x.order_state),
            orderDetailData,
        });

        const api = {
            "status": true,
            "data": newData,
        };

        res.json(api);
    }

    /**
     * menampilkan wishlist customer
     */
    showMyWishlists = async (req, res) => {
        const token = Util.getTokenFromRequest(req);
        const { data: { id: idCustomer } } = Util.decodeToken(token);
        const { page } = req.query;
        const limit = 10;
        let options = {
            where: {
                id_customer: idCustomer
            },
            offset: ((page - 1) * limit),
            limit: Number.parseInt(limit, 10),
            order: [['createdAt', 'DESC']],
            include: [{
                model: Product,
            }],
        };
        let calculateOptions = {
            where: {
                id_customer: idCustomer
            },
            attributes: [[Db.fn('COUNT', Db.col('id')), 'total']],
            raw: true
        }

        Product.hasMany(Wishlist, { foreignKey: 'id_product', sourceKey: 'id' });
        Wishlist.belongsTo(Product, { foreignKey: 'id_product', sourceKey: 'id' });
        const results = await Wishlist.findAll(options);
        const calculateAmountOfData = await Wishlist.findOne(calculateOptions);
        const amountOfData = calculateAmountOfData.total;

        const api = {
            "status": true,
            "data": Util.stringifyThenParse(results),
            "page_total": Math.ceil(amountOfData / limit)
        };

        res.json(api);
    }

    /**
     * menambahkan wishlist
     */
    addToWishlist = async (req, res) => {
        const token = Util.getTokenFromRequest(req);
        const { data: { id: idCustomer } } = Util.decodeToken(token);
        const { id_product: idProduct } = req.body;

        const execStoredProcedure = await StoredProcedure.run('sp_save_wishlist', [
            idCustomer,
            idProduct,
        ]);

        const data = execStoredProcedure.result;
        const api = {
            "status": true,
            "data": data
        };

        res.json(api);
    }

    /**
     * [ADMIN] menampilkan semua order customer
     */
    showOrders = async (req, res) => {
        /**
         * mandatory param: limit, page
        */
       const { limit, page, filterText: search } = req.query;
       let options = {
           offset: ((page - 1) * limit),
           limit: Number.parseInt(limit, 10),
           order: [['createdAt', 'DESC']],
           include: [{
               model: Customer,
           }],
       };
       let calculateOptions = {
           attributes: [[Db.fn('COUNT', Db.col('id')), 'total']],
           raw: true
       }

       if (search) {
           const filter = {
               reference: {
                   [Op.regexp]: `(${search})`,
               }
           }
           options = Object.assign({}, options, {
               where: options.where ? Object.assign({}, options.where, filter) : filter,
           });
           calculateOptions = Object.assign({}, calculateOptions, {
               where: calculateOptions.where ? Object.assign({}, calculateOptions.where, filter) : filter,
           });
       }

       Customer.hasMany(Order, { foreignKey: 'id_customer', sourceKey: 'id' });
       Order.belongsTo(Customer, { foreignKey: 'id_customer', sourceKey: 'id' });
       const data = await Order.findAll(options);

       const calculateAmountOfData = await Order.findOne(calculateOptions);

       const amountOfData = calculateAmountOfData.total;

       const api = {
           "status": true,
           "data": data,
           "page_total": Math.ceil(amountOfData / limit)
       };

       res.json(api);
    }

    /**
     * [ADMIN] menampilkan order customer
     */
    showOrderDetail = async (req, res) => {
        const { id } = req.params;

        const data = await Order.findOne({
            where: {
                id
            }
        });

        const x = Util.stringifyThenParse(data);
        const parseAddress = JSON.parse(x.address_delivery);
        const cityData = await City.findOne({
            where: {
                id: parseAddress.id_city
            }
        });
        const provinceData = await Province.findOne({
            where: {
                id: cityData.id_province
            }
        });
        const orderHistoryData = await OrderHistory.findAll({
            where: {
                id_order: id,
            },
            order: [['createdAt', 'DESC']],
        });
        const parsedOrderHistoryData = Util.stringifyThenParse(orderHistoryData);
        const newOrderHistory = parsedOrderHistoryData.map((oh) => (Object.assign({}, oh, { order_state_text: getOrderStateName(oh.order_state) })));

        const orderDetailData = await OrderDetail.findAll({
            where: {
                id_order: id,
            },
            order: [['product_name', 'DESC']],
        });

        const newData = Object.assign({}, x, {
            address_delivery_text: `${parseAddress.address} ${cityData.name} ${provinceData.name}`,
            postcode: parseAddress.postcode,
            receiver_name: parseAddress.receiver_name,
            order_history: newOrderHistory,
            order_state_text: getOrderStateName(x.order_state),
            orderDetailData,
        });

        const api = {
            "status": true,
            "data": newData,
        };

        res.json(api);
    }

    /**
     * cek status pesanan
     */
    checkMyOrder = async (req, res) => {
        const api = {
            "status": true,
            "data": []
        };

        res.json(api);
    }

    /**
     * menampilkan history ulasan
     */
    showMyReviews = async (req, res) => {
        const api = {
            "status": true,
            "data": []
        };

        res.json(api);

    }

    /**
     * membuat ulasan
     */
    createReview = async (req, res) => {
        const token = Util.getTokenFromRequest(req);
        const { data: { id: idCustomer } } = Util.decodeToken(token);
        const { files } = req;
        const {
            id_product,
            id_order,
            rating,
            description,
        } = JSON.parse(req.body.data);

        const newFiles = [];
        let fileName = "";
        let loopIndex = 0;

        while (loopIndex < files.length) {
            const imageDir = path.resolve(__dirname).replace("dist/controllers", "");
            fileName = `${getFileName()}.png`;
            await sharp(`${imageDir}${files[loopIndex].path}`)
                .resize(500)
                .png()
                .toFile(`${imageDir}public/uploads/${fileName}`);
            fs.unlinkSync(`${imageDir}${files[loopIndex].path}`);

            newFiles.push(fileName)

            loopIndex += 1;
        }

        const ratingCreated = await ProductRating.create({
            id_product,
            id_user: idCustomer,
            id_order,
            rating,
            description,
        });

        const ratingProducts = Util.stringifyThenParse(ratingCreated);

        const photos = newFiles.map(x => ({
            tablename: "ProductRatings",
            filename: x,
            id_reference: ratingProducts.id
        }));

        const fileStorage = await FileStorage.bulkCreate(photos);

        const data = Object.assign({}, ratingProducts, { images: fileStorage });

        const api = {
            "status": true,
            "data": data
        };

        res.json(api);
    }

    /**
     * mengedit ulasan
     */
    updateReview = async (req, res) => {
        const token = Util.getTokenFromRequest(req);
        const { data: { id: idCustomer } } = Util.decodeToken(token);
        const { files } = req;
        const {
            id,
            id_product,
            id_order,
            rating,
            description,
        } = JSON.parse(req.body.data);
        const tableName = "ProductRatings";

        let fileStorage = await FileStorage.findAll({
            where: {
                id_reference: id,
                tablename: tableName,
            }
        });

        Util.stringifyThenParse(fileStorage).forEach(x => {
            try {
                fs.unlinkSync(`public/uploads/${x.filename}`);
            } catch (err) {
                console.log(err);
            }
        });

        await FileStorage.destroy({
            where: {
                id_reference: id,
                tablename: tableName,
            }
        });

        const newFiles = [];
        let fileName = "";
        let loopIndex = 0;

        while (loopIndex < files.length) {
            const imageDir = path.resolve(__dirname).replace("dist/controllers", "");
            fileName = `${getFileName()}.png`;
            await sharp(`${imageDir}${files[loopIndex].path}`)
                .resize(500)
                .png()
                .toFile(`${imageDir}public/uploads/${fileName}`);
            fs.unlinkSync(`${imageDir}${files[loopIndex].path}`);

            newFiles.push(fileName)

            loopIndex += 1;
        }

        const ratingFound = await ProductRating.findById(id);
        await ratingFound.update({
            id_product,
            id_user: idCustomer,
            id_order,
            rating,
            description,
        });

        const ratingProducts = Util.stringifyThenParse(ratingFound);

        const photos = newFiles.map(x => ({
            tablename: tableName,
            filename: x,
            id_reference: ratingProducts.id
        }));

        fileStorage = await FileStorage.bulkCreate(photos);

        const data = Object.assign({}, ratingProducts, { images: fileStorage });

        const api = {
            "status": true,
            "data": data
        };

        res.json(api);
    }

    /**
     * [ADMIN] menampilkan history order customer
     */
    showOrderHistories = async (req, res) => {
        const { id_order } = req.body;

        const data = await OrderHistory.findAll({
            where: {
                id_order
            }
        });

        const api = {
            "status": true,
            "data": data
        };

        res.json(api);
    }

    /**
     * [ADMIN] autocomplete - menampilkan provinsi
     */
    getProvinces = async (req, res) => {
        let api = {
            "status": false,
            "msg": 'error',
        };
        const { sorted, limit: _limit, filterText, orId } = req.query;

        let limit = 5;

        if (_limit) {
            limit = _limit;
        }

        const options = {
            limit: Number.parseInt(limit, 10),
            order: [['name', 'ASC']],
        };

        if (sorted) {
            const { id, desc } = JSON.parse(sorted);
            Object.assign(options, {
                order: Db.literal(`${id} ${(desc ? 'DESC' : 'ASC')}`)
            });
        }

        if (filterText) {
            const filter = {
                name: {
                    [Op.regexp]: `(${filterText})`,
                }
            }
            Object.assign(options, {
                where: filter,
            });
        }

        if (orId) {
            const orOptions = [{
                id: orId
            }];
            if (filterText) {
                orOptions.push({
                    name: {
                        [Op.regexp]: `(${filterText})`,
                    }
                })
            }
            const filter = {
                [Op.or]: orOptions
            }
            Object.assign(options, {
                where: filter,
            });
        }
        const results = await Province.findAll(options);

        if (results) {
            api = {
                "status": true,
                "data": results,
                "msg": 'success',
            }
        }

        res.json(api);
    }

    /**
     * [ADMIN] autocomplete - menampilkan customer
     */
    getSelectCustomers = async (req, res) => {
        let api = {
            "status": false,
            "msg": 'error',
        };
        const { sorted, limit: _limit, filterText, orId } = req.query;

        let limit = 5;

        if (_limit) {
            limit = _limit;
        }

        const options = {
            limit: Number.parseInt(limit, 10),
            order: [['name', 'ASC']],
        };

        if (sorted) {
            const { id, desc } = JSON.parse(sorted);
            Object.assign(options, {
                order: Db.literal(`${id} ${(desc ? 'DESC' : 'ASC')}`)
            });
        }

        if (filterText) {
            const filter = {
                name: {
                    [Op.regexp]: `(${filterText})`,
                }
            }
            Object.assign(options, {
                where: filter,
            });
        }

        if (orId) {
            const orOptions = [{
                id: orId
            }];
            if (filterText) {
                orOptions.push({
                    name: {
                        [Op.regexp]: `(${filterText})`,
                    }
                })
            }
            const filter = {
                [Op.or]: orOptions
            }
            Object.assign(options, {
                where: filter,
            });
        }
        const results = await Customer.findAll(options);

        if (results) {
            api = {
                "status": true,
                "data": results,
                "msg": 'success',
            }
        }

        res.json(api);
    }

    /**
     * [ADMIN] autocomplete - menampilkan kota
     */
    getCities = async (req, res) => {
        let api = {
            "status": false,
            "msg": 'error',
        };
        const { sorted, limit: _limit, filterText, idProvince, orId } = req.query;

        let limit = 5;

        if (_limit) {
            limit = _limit;
        }

        const options = {
            limit: Number.parseInt(limit, 10),
            order: [['name', 'ASC']],
        };

        if (sorted) {
            const { id, desc } = JSON.parse(sorted);
            Object.assign(options, {
                order: Db.literal(`${id} ${(desc ? 'DESC' : 'ASC')}`)
            });
        }

        if (filterText) {
            const filter = {
                name: {
                    [Op.regexp]: `(${filterText})`,
                }
            }
            if (idProvince) {
                Object.assign(filter, {
                    id_province: idProvince,
                });
            }
            Object.assign(options, {
                where: filter,
            });
        }

        if (orId) {
            const orOptions = [{
                id: orId
            }];
            if (filterText) {
                orOptions.push({
                    name: {
                        [Op.regexp]: `(${filterText})`,
                    }
                })
            }
            const filter = {
                [Op.or]: orOptions
            }
            Object.assign(options, {
                where: filter,
            });
        }

        const results = await City.findAll(options);

        if (results) {
            api = {
                "status": true,
                "data": results,
                "msg": 'success',
            }
        }

        res.json(api);
    }

    /**
     * [ADMIN] autocomplete - menampilkan kecamatan
     */
    getSubDistricts = async (req, res) => {
        let api = {
            "status": false,
            "msg": 'error',
        };
        const { filterText, idCity, orId } = req.query;
        let _idCity = 0;
        let _q = "";

        if (filterText) {
            _q = filterText;
        }

        if (orId) {
            _q = orId;
            if (filterText) {
                _q = filterText;
            }
        }

        if (idCity) {
            _idCity = idCity;
        }

        const execStoredProcedure = await StoredProcedure.run('sp_get_sub_districts', [
            _idCity,
            `'${_q}'`,
        ], true);
        const data = execStoredProcedure.results.map((x) => ({ id: x.sub_district, name: x.sub_district }));
    
        if (data) {
            api = {
                "status": true,
                "data": data,
                "msg": 'success',
            }
        }

        res.json(api);
    }

    /**
     * [ADMIN] autocomplete - menampilkan kelurahan
     */
    getUrbans = async (req, res) => {
        let api = {
            "status": false,
            "msg": 'error',
        };
        const { filterText, idCity, subDistrict, orId } = req.query;
        let _idCity = 0;
        let _q = "";
        let _orId = 0;

        if (filterText) {
            _q = filterText;
        }

        if (idCity) {
            _idCity = idCity;
        }

        if (orId) {
            _orId = orId;
        }

        const execStoredProcedure = await StoredProcedure.run('sp_get_urbans', [
            _idCity,
            `'${subDistrict}'`,
            `'${_q}'`,
            _orId,
        ], true);
        const data = execStoredProcedure.results.map((x) => ({ id: x.id, name: x.urban, postal_code: x.postal_code }));
    
        if (data) {
            api = {
                "status": true,
                "data": data,
                "msg": 'success',
            }
        }

        res.json(api);
    }

    /**
     * menampilkan provinsi
     */
    showProvinces = async (req, res) => {
        const data = await Province.findAll({
            order: [['name', 'ASC']]
        });

        const api = {
            "status": true,
            "data": data
        };

        res.json(api);

    }

    /**
     * menampilkan semua kota
     */
    showTopCities = async (req, res) => {
        const sql = `SELECT p.origin, c.name, c.id_province FROM Products p JOIN Cities c ON c.id = p.origin GROUP BY p.origin ORDER BY COUNT(p.origin) DESC LIMIT 5`;
        const data = await QueryBuilder.query(sql, { type: Sequelize.QueryTypes.SELECT })

        const api = {
            "status": true,
            "data": data
        };

        res.json(api);
    }

    /**
     * menampilkan semua kota
     */
    showAllCities = async (req, res) => {
        const sql = `SELECT c.*, p.name AS province_name FROM Cities c 
                     JOIN Provinces p ON p.id = c.id_province 
                     ORDER BY province_name, name ASC`;
        const data = await QueryBuilder.query(sql, { type: Sequelize.QueryTypes.SELECT })

        const api = {
            "status": true,
            "data": data
        };

        res.json(api);
    }

    /**
     * menampilkan provinsi dengan subdata kota
     */
    showCitiesGroup = async (req, res) => {
        const { search } = req.query;

        City.belongsTo(Province, { foreignKey: 'id_province', sourceKey: 'id' });
        Province.hasMany(City, { foreignKey: 'id_province', sourceKey: 'id' });
        
        let options = {
            model: City,
        }

        if (search) {
            const filter = {
                name: {
                    [Op.regexp]: `(${search})`,
                }
            }
            options = Object.assign({}, options, {
                where: filter,
            });
        }

        const data = await Province.findAll({
            order: [['name', 'ASC']],
            include: [options]
        });

        const api = {
            "status": true,
            "data": data
        };

        res.json(api);
    }

    /**
     * menampilkan kota berdasarkan provinsi
     */
    showProvinceCities = async (req, res) => {
        const { id_province } = req.params;

        const data = await City.findAll({
            where: {
                id_province
            },
            order: [['name', 'ASC']]
        });

        const api = {
            "status": true,
            "data": data
        };

        res.json(api);
    }

    /**
     * [ADMIN] menampilkan data customer
     */
    showCustomers = async (req, res) => {
        /**
         * mandatory param: limit, page, is_unlimited
        */
        const { limit: _limit, page: _page, filterText: search } = req.query;
        let limit = 5,
            page = 1;
        let options = {
            raw: true,
            order: [['createdAt', 'DESC']],
        };
        let calculateOptions = {
            attributes: [[Db.fn('COUNT', Db.col('id')), 'total']],
            raw: true
        }

        if (_limit) {
            limit = _limit;
        }

        if (_page) {
            page = _page;
        }

        options = Object.assign({}, options, {
            offset: ((page - 1) * limit),
            limit: Number.parseInt(limit, 10),
        });

        if (search) {
            const filter = {
                name: {
                    [Op.regexp]: `(${search})`,
                }
            }
            options = Object.assign({}, options, {
                where: options.where ? Object.assign({}, options.where, filter) : filter,
            });
            calculateOptions = Object.assign({}, calculateOptions, {
                where: calculateOptions.where ? Object.assign({}, calculateOptions.where, filter) : filter,
            });
        }

        const data = await Customer.findAll(options);
        const calculateAmountOfData = await Customer.findOne(calculateOptions);
        const amountOfData = calculateAmountOfData.total;

        const api = {
            "status": true,
            "data": data,
            "page_total": Math.ceil(amountOfData / limit)
        };

        res.json(api);
    }

    /**
     * [ADMIN] menampilkan detail customer
     */
    showCustomer = async (req, res) => {
        const { id } = req.params;

        let api = {
            "status": false,
            "data": "customer_not_found"
        };

        if (id === 'me') {
            const token = Util.getTokenFromRequest(req);
            const { data: { id: myId } } = Util.decodeToken(token);

            const data = await Customer.findOne({
                where: {
                    id: myId
                }
            });

            api = {
                "status": true,
                "data": data
            };
        } else {
            const data = await Customer.findOne({
                where: {
                    id
                }
            });

            api = {
                "status": true,
                "data": data
            };
        }



        res.json(api);
    }

    /**
     * [ADMIN] create customer
     */
    createCustomer = async (req, res) => {
        const {
            name,
            email,
            passwd
        } = req.body;
        let api = {
            "status": false,
            "data": "duplicate_email"
        };
        const isExist = await checkDuplicateEmail({ email, id: null });

        if (!isExist) {
            const data = await Customer.create({
                name,
                email,
                passwd: bcrypt.hashSync(passwd, SALT_PASSWD),
            });

            api = {
                "status": true,
                "data": Util.stringifyThenParse(data)
            };
        }

        res.json(api);
    }

    /**
     * [ADMIN] update customer
     */
    updateCustomer = async (req, res) => {
        const {
            id,
            name,
            email,
            passwd
        } = req.body;
        let api = {
            "status": false,
            "data": "duplicate_email"
        };

        const isExist = await checkDuplicateEmail({ email, id });

        if (!isExist) {
            const customer = await Customer.findById(id)
            const data = await customer.update({
                name,
                email,
                passwd,
            });
            api = {
                "status": true,
                "data": Util.stringifyThenParse(data)
            };
        }

        res.json(api);
    }

    /**
     * [ADMIN] delete customer
     */
    deleteCustomer = async (req, res) => {
        // const { id } = req.body;
        // let data = await Customer.destroy({
        //     where: {
        //         id
        //     }
        // });

        // const api = {
        //     "status": true,
        //     "data": Util.stringifyThenParse(data)
        // };

        const api = {
            "status": true,
            "msg": "success"
        };

        res.json(api);
    }

    /**
     * [ADMIN] bulk delete customer
     */
    bulkDeleteCustomer = async (req, res) => {
        // const { id } = req.body;
        // let data = await Customer.destroy({
        //     where: {
        //         id
        //     }
        // });

        // const api = {
        //     "status": true,
        //     "data": Util.stringifyThenParse(data)
        // };

        const api = {
            "status": true,
            "msg": "success"
        };

        res.json(api);
    }

    /**
     * [ADMIN] update customer password
     */
    updateCustomerPassword = async (req, res) => {
        const { id, new_password, confirm_new_password } = req.body;
        let api = {
            "status": false,
            "msg": "Konfirmasi password Anda masih belum benar",
        };

        const customer = await Customer.findById(id);
        const isConfirmed = String(new_password) === String(confirm_new_password);
        if (isConfirmed) {
            const data = await customer.update({
                passwd: bcrypt.hashSync(new_password, SALT_PASSWD),
            });

            const newData = Util.stringifyThenParse(data);

            delete newData.passwd;

            api = {
                "status": true,
                "data": newData
            };
        }

        res.json(api);
    }

    /**
     * [ADMIN] menampilkan detail data karyawan
     */
    showEmployee = async (req, res) => {
        const token = Util.getTokenFromRequest(req);
        const { data: { id } } = Util.decodeToken(token);

        const data = await Employee.findOne({
            where: {
                id
            }
        });

        const api = {
            "status": true,
            "data": data
        };

        res.json(api);
    }

    /**
     * [ADMIN] menampilkan konfirmasi pembayaran
     */
    showPaymentConfirmations = async (req, res) => {
        let api = {
            "status": false,
            "msg": 'error',
        };
        const { sorted, limit: _limit, page: _page, filterText, } = req.query;

        let limit = 0,
            page = 1;

        const options = {
            order: [['id', 'ASC']],
        };

        if (_limit) {
            if (_page) {
                page = _page;
            }

            limit = _limit;
            Object.assign(options, {
                offset: ((page - 1) * limit),
                limit: Number.parseInt(limit, 10),
            });
        }

        const calculateOptions = {
            attributes: [[Db.fn('COUNT', Db.col('id')), 'total']],
            raw: true
        }

        if (sorted) {
            const { id, desc } = JSON.parse(sorted);
            Object.assign(options, {
                order: Db.literal(`${id} ${(desc ? 'DESC' : 'ASC')}`)
            });
        }

        if (filterText) {
            const filter = {
                reference_number: {
                    [Op.regexp]: `(${filterText})`,
                }
            }
            Object.assign(options, {
                where: filter,
            });
            Object.assign(calculateOptions, {
                where: filter,
            });
        }

        const results = await PaymentConfirmation.findAll(options);
        const calculateAmountOfData = await PaymentConfirmation.findOne(calculateOptions);
        const amountOfData = calculateAmountOfData.total;

        if (results) {
            api = {
                "status": true,
                "data": results,
                "page_total": Math.ceil(amountOfData / limit),
                "msg": 'success',
            }
        }

        res.json(api);
    }

    /**
     * [ADMIN] menerima konfirmasi pembayaran
     */
    acceptPayment = async (req, res) => {
        try {
            const {
                id,
            } = req.body;
            const execStoredProcedure = await StoredProcedure.run('sp_process_order_1', [
                id,
                '1', // id_employee
                1,
            ]);
    
            const data = execStoredProcedure.result;
    
            const api = {
                "status": true,
                "data": data,
            };
            res.json(api);
        } catch (err) {
            console.log(err);
            res.json({ status: false, error: err });
        }
    }

    /**
     * [ADMIN] menolak konfirmasi pembayaran
     */
    rejectPayment = async (req, res) => {
        try {
            const {
                id,
            } = req.body;
            const execStoredProcedure = await StoredProcedure.run('sp_process_order_1', [
                id,
                '1', // id_employee
                0,
            ]);
    
            const data = execStoredProcedure.result;
    
            const api = {
                "status": true,
                "data": data,
            };
            res.json(api);
        } catch (err) {
            console.log(err);
            res.json({ status: false, error: err });
        }
    }

    /**
     * [ADMIN] memproses order setelah dibayar
     */
    processOrder = async (req, res) => {
        try {
            const {
                id,
            } = req.body;
            const execStoredProcedure = await StoredProcedure.run('sp_process_order_2', [
                id,
                '1', // id_employee
            ]);
    
            const data = execStoredProcedure.result;
    
            const api = {
                "status": true,
                "data": data,
            };
            res.json(api);
        } catch (err) {
            console.log(err);
            res.json({ status: false, error: err });
        }
    }

    /**
     * [ADMIN] kirim order
     */
    shipOrder = async (req, res) => {
        try {
            const {
                id,
                shipment_number,
            } = req.body;
            const orderRes = await Order.findOne({
                where: {
                    id,
                }
            });
            const shipping = JSON.parse(orderRes.shipping);
            const newShipping = Object.assign({}, shipping, {
                shipment_number
            });

            const execStoredProcedure = await StoredProcedure.run('sp_process_order_3', [
                id,
                `'${JSON.stringify(newShipping)}'`,
                '1', // id_employee
            ]);
    
            const data = execStoredProcedure.result;
    
            const api = {
                "status": true,
                "data": data,
            };
            res.json(api);
        } catch (err) {
            console.log(err);
            res.json({ status: false, error: err });
        }
    }

    /**
     * ambil data provinsi dari rajaongkir
     */
    getProvincesFromThirdParty = async (req, res) => {
        const param = {
            key: RAJA_ONGKIR_API_KEY
        }
        request.get(`https://api.rajaongkir.com/starter/province`)
            .query(param)
            .end((err, response) => {
                let api = {};

                if (err) {
                    api = {
                        "status": false,
                        "error": err.message
                    };

                    res.json(api);
                } else {
                    const provinces = response.body.rajaongkir.results;
                    const dataToInsert = provinces.map(x => ({
                        id: x.province_id,
                        name: x.province
                    }));

                    Province.bulkCreate(dataToInsert).then((created) => {
                        api = {
                            "status": true,
                            "data": created
                        };

                        res.json(api);
                    });
                }
            });
    }

    /**
     * ambil data kota dari rajaongkir
     */
    getCitiesFromThirdParty = async (req, res) => {
        const param = {
            key: RAJA_ONGKIR_API_KEY
        }
        request.get(`https://api.rajaongkir.com/starter/city`)
            .query(param)
            .end((err, response) => {
                let api = {};

                if (err) {
                    api = {
                        "status": false,
                        "error": err.message
                    };

                    res.json(api);
                } else {
                    const cities = response.body.rajaongkir.results;
                    const dataToInsert = cities.map(x => ({
                        id: x.city_id,
                        name: `${x.type} ${x.city_name}`,
                        id_province: x.province_id
                    }));

                    City.bulkCreate(dataToInsert).then((created) => {
                        api = {
                            "status": true,
                            "data": created
                        };

                        res.json(api);
                    });
                }
            });
    }

    /**
     * cek ongkos kirim
     */
    checkShipmentCost = async (req, res) => {
        try {
            const {
                destination,
                weight,
                courier
            } = req.body;
            var param = {
                key: RAJA_ONGKIR_API_KEY,
                origin: '256', // malang
                destination,
                weight,
                courier
            }
    
            request.post(`https://api.rajaongkir.com/starter/cost`)
                .send(param)
                .end((err, response) => {
                    let api = {}
                    if (err) {
                        api = {
                            "status": false,
                            "error": err.message
                        };
                    } else {
                        api = {
                            "status": true,
                            "data": response.body.rajaongkir.results
                        };
                    }
    
                    res.json(api);
                });
        } catch (err) {
            console.log('DEBUG-ERR: ', err);
        }
    }

    forProductMigration = async (req, res) => {
        // let rawdata = fs.readFileSync(path.join(__dirname, "../../public/json/aneka_kue.json"));
        // let rawdata = fs.readFileSync(path.join(__dirname, "../../public/json/bumbu_sambal.json"));
        // let rawdata = fs.readFileSync(path.join(__dirname, "../../public/json/dodol.json"));
        // let rawdata = fs.readFileSync(path.join(__dirname, "../../public/json/herbal.json"));
        // let rawdata = fs.readFileSync(path.join(__dirname, "../../public/json/kacang.json"));
        // let rawdata = fs.readFileSync(path.join(__dirname, "../../public/json/kerajinan.json"));
        // let rawdata = fs.readFileSync(path.join(__dirname, "../../public/json/keripik.json"));
        // let rawdata = fs.readFileSync(path.join(__dirname, "../../public/json/kerupuk.json"));
        // let rawdata = fs.readFileSync(path.join(__dirname, "../../public/json/kopi.json"));
        // let rawdata = fs.readFileSync(path.join(__dirname, "../../public/json/kue_kering.json"));
        // let rawdata = fs.readFileSync(path.join(__dirname, "../../public/json/lauk_pauk.json"));
        // let rawdata = fs.readFileSync(path.join(__dirname, "../../public/json/makanan_basah.json"));
        // let rawdata = fs.readFileSync(path.join(__dirname, "../../public/json/manisan.json"));
        // let rawdata = fs.readFileSync(path.join(__dirname, "../../public/json/minuman_segar.json"));
        // let rawdata = fs.readFileSync(path.join(__dirname, "../../public/json/permen_coklat.json"));
        let rawdata = fs.readFileSync(path.join(__dirname, "../../public/json/teh.json"));
        let results = JSON.parse(rawdata);
        const data = [];
        let index = 0;

        while (index < results.length) {
            const result = results[index];

            const cityRes = await City.findOne({
                where: {
                    name: {
                        [Op.like]: `%${result.origin}%`,
                    },
                }
            });

            let origin = 1;

            if (cityRes) {
                origin = cityRes.id;
            }

            const productCreated = await Product.create({
                id_category: result.idCategory,
                name: result.productName,
                slug: result.slug,
                price: result.price,
                description: result.description,
                origin,
                weight: result.weight,
                popular_score: 0,
                available_qty: result.available_qty,
                is_active: 1,
                primary_img: result.images.length > 0 ? result.images[0] : ''
            });

            const photos = result.images.map(x => ({
                tablename: "Products",
                filename: x,
                id_reference: productCreated.id
            }));

            const fileStorage = await FileStorage.bulkCreate(photos);
            const newData = Object.assign(productCreated, { images: fileStorage });
            data.push(newData);

            index = index + 1;
        }

        const api = {
            "status": true,
            "data": data
        };

        res.json(api);
    }
}

const api = new Api();

export default api;