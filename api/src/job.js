import kue from 'kue';
import moment from 'moment';
import { ModelName } from './constants';
import ModelFactory from './utils/model_factory';
import Util from './utils';

const queue = kue.createQueue();

const PaymentConfirmation = ModelFactory.createModel(ModelName.PAYMENT_CONFIRMATION);

export const start = () => {
    queue.process('confirmPayment', (job, done) =>{
        PaymentConfirmation.create({
            reference_number: job.data.reference,
            is_auto: true,
            transfer_date: moment().format('YYYY-MM-DD HH:mm:ss'),
        }).then((res) => {
            console.log('PAYMENT-SUCCESS: ', Util.stringifyThenParse(res));
            done();
        }).catch((err) => {
            console.log('PAYMENT-ERROR: ', err);
            done();
        });
    });
}