import express from 'express';
import path from 'path';
import logger from 'morgan';
import cookieParser from 'cookie-parser';
import bodyParser from 'body-parser';
import jwt from 'express-jwt';
import cors from 'cors';
import { SECRET_KEY } from './constants';
import routes from './routes';
import * as Job from './job';

var app = express();

app.use(cors());
app.use(logger("dev"));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, "../public")));
app.use(
    jwt({ secret: SECRET_KEY }).unless({
            path: [
                    '/',
                    /^\/api\/login\/.*/,
                    '/api/register',
                    '/api/products',
                    /^\/api\/product\/.*/,
                    '/api/categories',
                    '/api/cities',
                    '/api/cities-group',
                    '/api/top-cities',
                    '/api/get-google-url',
                    '/api/setup-google-account',
                    /^\/uploads\/.*/,
            ],
    }),
);

app.use("/", routes);

app.use(function (req, res, next) {
  var err = new Error("Not Found");
  err.status = 404;
  next(err);
});

app.use(function (err, req, res, next) {
  const message = err.message;
  const error = req.app.get("env") === "development" ? err : {};

  res.status(err.status || 500);
  res.json({ 
    status: false, 
    error,
    msg: message 
  });
});

Job.start();

module.exports = app;
