import keyMirror from "key-mirror";

const SECRET_KEY = "5eGWWbXHpe3xehfRTcRDXE3cxS9hZqzVJs49QYjYvvQQt2Nuda";

const ModelName = keyMirror({
    ADDRESS: null,
    BACKOFFICE_MENU: null,
    CART: null,
    CART_DETAIL: null,
    CATEGORY: null,
    CITY: null,
    CUSTOMER: null,
    EMPLOYEE: null,
    FILE_STORAGE: null,
    META_DATA: null,
    OPNAME_STOCK: null,
    ORDER: null,
    ORDER_DETAIL: null,
    ORDER_HISTORY: null,
    PAYMENT_CONFIRMATION: null,
    POSTAL_CODE: null,
    PRODUCT: null,
    PRODUCT_RATING: null,
    PROVINCE: null,
    STOCK: null,
    WISHLIST: null,
});

export { SECRET_KEY, ModelName }