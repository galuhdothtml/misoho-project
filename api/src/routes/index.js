import express from 'express';
import fs from "fs";
import mime from "mime";
import Api from '../controllers/api';
import multer from "multer";

const createFilename = () => (Math.random().toString(36).substring(2, 5) + Math.random().toString(36).substring(2, 5));
const storage = multer.diskStorage({
  destination: function(req, file, cb) {
    const dir = `public/uploads/temp`;

    if (!fs.existsSync(dir)){
        fs.mkdirSync(dir);
    }
    cb(null, `${dir}/`);
  },
  filename: function(req, file, cb) {
    const fileExtension = mime.getExtension(file.mimetype);
    const filename = createFilename() + "." + fileExtension;
    
    cb(null, filename);
  }
});
var upload = multer({ storage: storage });

const router = express.Router();

router.get('/', Api.index);
router.post('/api/register', Api.register);
router.post('/api/login/:type', Api.login);
router.post('/api/uploadFiles', upload.array("photos", 12), Api.uploadFiles);
router.post('/api/uploadFile', upload.single('photo'), Api.uploadFile);
router.get('/api/get-google-url', Api.getGoogleUrl);
router.post('/api/setup-google-account', Api.setupGoogleAccount);
router.get('/api/appmenu', Api.appMenu);
router.get('/api/products', Api.showProducts);
router.get('/api/sub_districts', Api.getSubDistricts);
router.get('/api/urbans', Api.getUrbans);
router.get('/api/backoffice-products', Api.getBackofficeProducts);
router.get('/api/backoffice-categories', Api.getBackofficeCategories);
router.get('/api/v2/customers', Api.getSelectCustomers);
router.get('/api/v2/provinces', Api.getProvinces);
router.get('/api/v2/cities', Api.getCities);
router.get('/api/categories', Api.showCategories);
router.get('/api/category/:id', Api.showCategory);
router.post('/api/category', Api.createCategory);
router.put('/api/category', upload.single('photos'), Api.updateCategory);
router.delete('/api/category', Api.deleteCategory);
router.delete('/api/category/bulk', Api.bulkDeleteCategory);
router.get('/api/carts', Api.showMyCarts);
router.post('/api/add_to_cart', Api.addToCart);
router.post('/api/update_cart', Api.updateCart);
router.post('/api/delete_cart', Api.deleteCart);
router.get('/api/product/:id', Api.showProductDetail);
router.get('/api/my/dashboard', Api.showMyDashboard);
router.get('/api/my/account', Api.showMyAccountInformation);
router.get('/api/addresses', Api.showAddresses);
router.get('/api/address/:id', Api.showAddress);
router.post('/api/set_default_address', Api.setDefaultAddress);
router.post('/api/address', Api.createAddress);
router.put('/api/address', Api.updateAddress);
router.delete('/api/address', Api.deleteAddress);
router.delete('/api/address/bulk', Api.bulkDeleteAddress);
router.get('/api/my/addresses', Api.showMyAddresses);
router.get('/api/my/address/:id', Api.showMyAddress);
router.get('/api/my/orders', Api.showMyOrders);
router.get('/api/my/order_detail/:id', Api.showMyOrderDetail);
router.get('/api/my/wishlists', Api.showMyWishlists);
router.post('/api/add_to_wishlist', Api.addToWishlist);
router.get('/api/orders', Api.showOrders);
router.get('/api/order_detail/:id', Api.showOrderDetail);
router.get('/api/check_my_order', Api.checkMyOrder);
router.get('/api/my/reviews', Api.showMyReviews);
router.post('/api/review', upload.array("photos", 12), Api.createReview);
router.put('/api/review', upload.array("photos", 12), Api.updateReview);
router.get('/api/order_histories/:id_order', Api.showOrderHistories);
router.get('/api/customers', Api.showCustomers);
router.get('/api/customer/:id', Api.showCustomer);
router.post('/api/customer', Api.createCustomer);
router.put('/api/customer', Api.updateCustomer);
router.delete('/api/customer', Api.deleteCustomer);
router.delete('/api/customer/bulk', Api.bulkDeleteCustomer);
router.get('/api/employee', Api.showEmployee);
router.post('/api/upload_profile_photo', upload.single('photos'), Api.uploadProfilePhoto);
router.get('/api/product_images/:id', Api.showProductImages);
router.post('/api/product', upload.array("photos", 12), Api.createProduct);
router.put('/api/product', upload.array("photos", 12),  Api.updateProduct);
router.delete('/api/product', Api.deleteProduct);
router.delete('/api/product/bulk', Api.bulkDeleteProduct);
router.get("/api/province_cities/:id_province", Api.showProvinceCities);
router.get("/api/cities", Api.showAllCities);
router.get("/api/cities-group", Api.showCitiesGroup);
router.get("/api/top-cities", Api.showTopCities);
router.get("/api/provinces", Api.showProvinces);
router.post("/api/cekongkir", Api.checkShipmentCost);
router.post("/api/checkout_order", Api.checkoutOrder);
router.post("/api/update_my_profile", Api.updateMyProfile);
router.post("/api/update_my_password", Api.updateMyPassword);
router.post("/api/update_customer_password", Api.updateCustomerPassword);
router.get('/api/payment_confirmations', Api.showPaymentConfirmations);
router.post("/api/accept_payment", Api.acceptPayment);
router.post("/api/reject_payment", Api.rejectPayment);
router.post("/api/process_order", Api.processOrder);
router.post("/api/ship_order", Api.shipOrder);
// router.get("/api/rajaongkir/provinces", Api.getProvincesFromThirdParty);
// router.get("/api/rajaongkir/cities", Api.getCitiesFromThirdParty);
// router.post("/api/for_product_migration", Api.forProductMigration);

export default router;