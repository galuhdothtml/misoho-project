import Sequelize from "sequelize";
import Db from "./db";

const run = (sp_name, values, showAll = false) => {
    var _sql = 'CALL ' + sp_name;
    _sql += '(';

    if (values.length > 0) {
        for (var i = 0; i < values.length; i++) {
            if (i == (values.length - 1)) {
                _sql += values[i];
            } else {
                _sql += values[i] + ', ';
            }
        }
    }

    _sql += ');';

    if (showAll) {
        return Db.query(_sql).then((res) => {
            return { results: res }
        });
    }

    return Db.query(_sql, { type: Sequelize.QueryTypes.SELECT }).then((res) => {
        return { result: res[0]['0'] }
    });
}

const StoredProcedure = {
    run
}

export default StoredProcedure;