import { google } from 'googleapis';

const googleConfig = {
  clientId: '367167507682-fg01jb9soi8c2glptq5akuap56b9oloa.apps.googleusercontent.com',
  clientSecret: 'Se8XIpz1rIKoj7g8UwWtDEsA',
  redirect: 'http://localhost:8080/google-redirect'
};

const defaultScope = [
  'https://www.googleapis.com/auth/plus.me',
  'https://www.googleapis.com/auth/userinfo.email',
  'https://www.googleapis.com/auth/userinfo.profile'
];

const createConnection = () => {
  return new google.auth.OAuth2(
    googleConfig.clientId,
    googleConfig.clientSecret,
    googleConfig.redirect
  );
}

const getConnectionUrl = (auth) => {
  return auth.generateAuthUrl({
    access_type: 'offline',
    prompt: 'consent', // access type and approval prompt will force a new refresh token to be made each time signs in
    scope: defaultScope
  });
}

const urlGoogle = () => {
  const auth = createConnection(); // this is from previous step
  const url = getConnectionUrl(auth);
  return url;
}

const getGooglePlusApi = (auth) => {
  return google.plus({ version: 'v1', auth });
}

const getGoogleAccountFromCode = async (code) => {
  // get the auth "tokens" from the request
  const auth = createConnection(); // this is from previous step
  const data = await auth.getToken(code);
  const tokens = data.tokens;
  const response = await getGoogleAccountFromToken(tokens);

  return response;
}

const getGoogleAccountFromToken = async (tokens) => {
  // get the auth "tokens" from the request
  const auth = createConnection(); // this is from previous step
  // add the tokens to the google api so we have access to the account
  auth.setCredentials(tokens);

  // connect to google plus - need this to get the user's email
  const plus = getGooglePlusApi(auth);
  const me = await plus.people.get({ userId: 'me' });

  // get the google id and email
  const userGoogleId = me.data.id;
  const userGoogleEmail = me.data.emails && me.data.emails.length && me.data.emails[0].value;
  const userGoogleDisplayName = me.data.displayName;

  // return so we can login or sign up the user
  return {
    id: userGoogleId,
    email: userGoogleEmail,
    displayName: userGoogleDisplayName,
    tokens: tokens, // you can save these to the user if you ever want to get their details without making them log in again
  };
}

const Util = {
  urlGoogle,
  getGoogleAccountFromCode,
  getGoogleAccountFromToken,
}

export default Util;