import db from "./db";
import { ModelName } from "../constants";

const createModel = (type) => {
    if(type === ModelName.ADDRESS) {
        return db.import("../models/address");
    } else if(type === ModelName.BACKOFFICE_MENU) {
        return db.import("../models/backofficemenu");
    } else if(type === ModelName.CART) {
        return db.import("../models/cart");
    } else if(type === ModelName.CART_DETAIL) {
        return db.import("../models/cartdetail");
    } else if(type === ModelName.CATEGORY) {
        return db.import("../models/category");
    } else if(type === ModelName.CITY) {
        return db.import("../models/city");
    } else if(type === ModelName.CUSTOMER) {
        return db.import("../models/customer");
    } else if(type === ModelName.EMPLOYEE) {
        return db.import("../models/employee");
    } else if(type === ModelName.FILE_STORAGE) {
        return db.import("../models/filestorage");
    } else if(type === ModelName.META_DATA) {
        return db.import("../models/metadata");
    } else if(type === ModelName.ORDER) {
        return db.import("../models/order");
    } else if(type === ModelName.ORDER_DETAIL) {
        return db.import("../models/orderdetail");
    } else if(type === ModelName.ORDER_HISTORY) {
        return db.import("../models/orderhistory");
    } else if(type === ModelName.PAYMENT_CONFIRMATION) {
        return db.import("../models/paymentconfirmation");
    } else if(type === ModelName.PRODUCT) {
        return db.import("../models/product");
    } else if(type === ModelName.PRODUCT_RATING) {
        return db.import("../models/productrating");
    } else if(type === ModelName.PROVINCE) {
        return db.import("../models/province");
    } else if(type === ModelName.WISHLIST) {
        return db.import("../models/wishlist");
    } else if(type === ModelName.STOCK) {
        return db.import("../models/stock");
    } else if(type === ModelName.OPNAME_STOCK) {
        return db.import("../models/opnamestock");
    } else if(type === ModelName.POSTAL_CODE) {
        return db.import("../models/postalcode");
    }

    return db;
}

const ModelFactory = {
    createModel
}

export default ModelFactory;