import moment from 'moment';
import jwt from 'jsonwebtoken';
import { SECRET_KEY } from '../constants';

const getOrderStateText = (val) => {
    if (String(val) === '1') {
        return "Belum Dibayar";
    } else if (String(val) === '2') {
        return "Sudah Dibayar";
    } else if (String(val) === '3') {
        return "Sedang Diproses";
    } else if (String(val) === '4') {
        return "Sudah Dikirim";
    }

    return val;
}

const formatModelDateTime = (val, DataTypes) => {
    return Object.assign({}, val, {
        createdAt: {
            type: DataTypes.DATE,
            get: function () {
                return moment.utc(this.getDataValue('createdAt')).format('YYYY-MM-DD HH:mm:ss')
            }
        },
        updatedAt: {
            type: DataTypes.DATE,
            get: function () {
                return moment.utc(this.getDataValue('updatedAt')).format('YYYY-MM-DD HH:mm:ss')
            }
        },
    })
}

const getTokenFromRequest = (req) => {
    if (req.headers.authorization && req.headers.authorization.split(' ')[0] === 'Bearer') {
        return req.headers.authorization.split(' ')[1];
    }

    return '';
}

const decodeToken = (token) => {
    return jwt.verify(token, SECRET_KEY);
}

const stringifyThenParse = (data) => {
    return JSON.parse(JSON.stringify(data));
}

const slugify = (val) => {
    return val
        .toLowerCase()
        .replace(/ /g, '-')
        .replace(/[^\w-]+/g, '');
}

const randChar = (length = 5) => {
    var result           = '';
    var characters       = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';
    var charactersLength = characters.length;
    for ( var i = 0; i < length; i++ ) {
       result += characters.charAt(Math.floor(Math.random() * charactersLength));
    }
    return result;
}

const Util = {
    formatModelDateTime,
    getTokenFromRequest,
    decodeToken,
    stringifyThenParse,
    slugify,
    randChar,
    getOrderStateText
}

export default Util;