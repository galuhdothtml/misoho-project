'use strict';
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('MetaData', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      id_reference: {
        allowNull: false,
        type: Sequelize.INTEGER
      },
      table_name: {
        allowNull: false,
        type: Sequelize.STRING
      },
      meta_title: {
        allowNull: false,
        type: Sequelize.TEXT
      },
      meta_keywords: {
        allowNull: false,
        type: Sequelize.TEXT
      },
      meta_description: {
        allowNull: false,
        type: Sequelize.TEXT
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('MetaData');
  }
};