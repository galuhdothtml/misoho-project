'use strict';
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('OrderHistories', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      id_order: {
        allowNull: false,
        type: Sequelize.INTEGER
      },
      id_employee: {
        allowNull: false,
        type: Sequelize.INTEGER
      },
      order_state: {
        allowNull: false,
        type: Sequelize.STRING(2) 
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('OrderHistories');
  }
};