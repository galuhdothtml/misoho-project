'use strict';
var moment = require('moment');

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert('BackofficeMenus', [
      {
        id: '1',
        title: 'Dashboard',
        createdAt: moment().format('YYYY-MM-DD HH:mm:ss'),
        updatedAt: moment().format('YYYY-MM-DD HH:mm:ss'),
        icon: 'fa-dashboard',
        content: JSON.stringify({ link: 'dashboard' }),
      },
      {
        id: '2',
        title: 'Master',
        createdAt: moment().format('YYYY-MM-DD HH:mm:ss'),
        updatedAt: moment().format('YYYY-MM-DD HH:mm:ss'),
        icon: 'fa-table',
        content: JSON.stringify([
          {
            id: '2.1',
            title: 'Produk',
            link: 'produk-produk',
          }, {
            id: '2.2',
            title: 'Kategori',
            link: 'produk-kategori',
          }, {
            id: '2.3',
            title: 'Pelanggan',
            link: 'pelanggan-pelanggan',
          }, {
            id: '2.4',
            title: 'Alamat Pelanggan',
            link: 'alamat-pelanggan',
          },
        ])
      }, {
        id: '3',
        title: 'Pesanan',
        createdAt: moment().format('YYYY-MM-DD HH:mm:ss'),
        updatedAt: moment().format('YYYY-MM-DD HH:mm:ss'),
        icon: 'fa-shopping-cart',
        content: JSON.stringify([
          {
            id: '3.1',
            title: 'Pesanan',
            link: 'pesanan-pesanan',
          }, {
            id: '3.2',
            title: 'Histori Pesanan',
            link: 'pesanan-histori-pesanan',
          },
        ])
      }, {
        id: '4',
        title: 'Inventori',
        createdAt: moment().format('YYYY-MM-DD HH:mm:ss'),
        updatedAt: moment().format('YYYY-MM-DD HH:mm:ss'),
        icon: 'fa-cubes',
        content: JSON.stringify([
          {
            id: '4.1',
            title: 'Daftar Stok',
            link: 'daftar-stok',
          }, {
            id: '4.2',
            title: 'Log Aktifitas Stok',
            link: 'log-aktifitas-stok',
          },
        ])
      },
      {
        id: '5',
        title: 'Voucher',
        createdAt: moment().format('YYYY-MM-DD HH:mm:ss'),
        updatedAt: moment().format('YYYY-MM-DD HH:mm:ss'),
        icon: 'fa-ticket',
        content: JSON.stringify([
          {
            id: '5.1',
            title: 'Daftar Voucher',
            link: 'daftar-voucher',
          }, {
            id: '5.2',
            title: 'Voucher Digunakan',
            link: 'voucher-digunakan',
          },
        ])
      },
      {
        id: '6',
        title: 'Konfirmasi Pembayaran',
        createdAt: moment().format('YYYY-MM-DD HH:mm:ss'),
        updatedAt: moment().format('YYYY-MM-DD HH:mm:ss'),
        icon: 'fa-credit-card',
        content: JSON.stringify({ link: 'konfirmasi-pembayaran' })
      },
      {
        id: '7',
        title: 'Pengaturan',
        createdAt: moment().format('YYYY-MM-DD HH:mm:ss'),
        updatedAt: moment().format('YYYY-MM-DD HH:mm:ss'),
        icon: 'fa-gears',
        content: JSON.stringify([
          {
            id: '7.1',
            title: 'Pengguna',
            link: 'pengguna',
          },
          {
            id: '7.2',
            title: 'Ganti Password',
            link: 'ganti-password',
          },
        ])
      }], {});
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('BackofficeMenus', null, {});
  }
};
