"use strict";

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _db = require("./db");

var _db2 = _interopRequireDefault(_db);

var _constants = require("../constants");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var createModel = function createModel(type) {
    if (type === _constants.ModelName.ADDRESS) {
        return _db2.default.import("../models/address");
    } else if (type === _constants.ModelName.BACKOFFICE_MENU) {
        return _db2.default.import("../models/backofficemenu");
    } else if (type === _constants.ModelName.CART) {
        return _db2.default.import("../models/cart");
    } else if (type === _constants.ModelName.CART_DETAIL) {
        return _db2.default.import("../models/cartdetail");
    } else if (type === _constants.ModelName.CATEGORY) {
        return _db2.default.import("../models/category");
    } else if (type === _constants.ModelName.CITY) {
        return _db2.default.import("../models/city");
    } else if (type === _constants.ModelName.CUSTOMER) {
        return _db2.default.import("../models/customer");
    } else if (type === _constants.ModelName.EMPLOYEE) {
        return _db2.default.import("../models/employee");
    } else if (type === _constants.ModelName.FILE_STORAGE) {
        return _db2.default.import("../models/filestorage");
    } else if (type === _constants.ModelName.META_DATA) {
        return _db2.default.import("../models/metadata");
    } else if (type === _constants.ModelName.ORDER) {
        return _db2.default.import("../models/order");
    } else if (type === _constants.ModelName.ORDER_DETAIL) {
        return _db2.default.import("../models/orderdetail");
    } else if (type === _constants.ModelName.ORDER_HISTORY) {
        return _db2.default.import("../models/orderhistory");
    } else if (type === _constants.ModelName.PAYMENT_CONFIRMATION) {
        return _db2.default.import("../models/paymentconfirmation");
    } else if (type === _constants.ModelName.PRODUCT) {
        return _db2.default.import("../models/product");
    } else if (type === _constants.ModelName.PRODUCT_RATING) {
        return _db2.default.import("../models/productrating");
    } else if (type === _constants.ModelName.PROVINCE) {
        return _db2.default.import("../models/province");
    } else if (type === _constants.ModelName.WISHLIST) {
        return _db2.default.import("../models/wishlist");
    } else if (type === _constants.ModelName.STOCK) {
        return _db2.default.import("../models/stock");
    } else if (type === _constants.ModelName.OPNAME_STOCK) {
        return _db2.default.import("../models/opnamestock");
    } else if (type === _constants.ModelName.POSTAL_CODE) {
        return _db2.default.import("../models/postalcode");
    }

    return _db2.default;
};

var ModelFactory = {
    createModel: createModel
};

exports.default = ModelFactory;