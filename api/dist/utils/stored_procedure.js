"use strict";

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _sequelize = require("sequelize");

var _sequelize2 = _interopRequireDefault(_sequelize);

var _db = require("./db");

var _db2 = _interopRequireDefault(_db);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var run = function run(sp_name, values) {
    var showAll = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : false;

    var _sql = 'CALL ' + sp_name;
    _sql += '(';

    if (values.length > 0) {
        for (var i = 0; i < values.length; i++) {
            if (i == values.length - 1) {
                _sql += values[i];
            } else {
                _sql += values[i] + ', ';
            }
        }
    }

    _sql += ');';

    if (showAll) {
        return _db2.default.query(_sql).then(function (res) {
            return { results: res };
        });
    }

    return _db2.default.query(_sql, { type: _sequelize2.default.QueryTypes.SELECT }).then(function (res) {
        return { result: res[0]['0'] };
    });
};

var StoredProcedure = {
    run: run
};

exports.default = StoredProcedure;