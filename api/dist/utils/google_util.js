'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _regenerator = require('babel-runtime/regenerator');

var _regenerator2 = _interopRequireDefault(_regenerator);

var _asyncToGenerator2 = require('babel-runtime/helpers/asyncToGenerator');

var _asyncToGenerator3 = _interopRequireDefault(_asyncToGenerator2);

var _googleapis = require('googleapis');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var googleConfig = {
  clientId: '367167507682-fg01jb9soi8c2glptq5akuap56b9oloa.apps.googleusercontent.com',
  clientSecret: 'Se8XIpz1rIKoj7g8UwWtDEsA',
  redirect: 'http://localhost:8080/google-redirect'
};

var defaultScope = ['https://www.googleapis.com/auth/plus.me', 'https://www.googleapis.com/auth/userinfo.email', 'https://www.googleapis.com/auth/userinfo.profile'];

var createConnection = function createConnection() {
  return new _googleapis.google.auth.OAuth2(googleConfig.clientId, googleConfig.clientSecret, googleConfig.redirect);
};

var getConnectionUrl = function getConnectionUrl(auth) {
  return auth.generateAuthUrl({
    access_type: 'offline',
    prompt: 'consent', // access type and approval prompt will force a new refresh token to be made each time signs in
    scope: defaultScope
  });
};

var urlGoogle = function urlGoogle() {
  var auth = createConnection(); // this is from previous step
  var url = getConnectionUrl(auth);
  return url;
};

var getGooglePlusApi = function getGooglePlusApi(auth) {
  return _googleapis.google.plus({ version: 'v1', auth: auth });
};

var getGoogleAccountFromCode = function () {
  var _ref = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee(code) {
    var auth, data, tokens, response;
    return _regenerator2.default.wrap(function _callee$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            // get the auth "tokens" from the request
            auth = createConnection(); // this is from previous step

            _context.next = 3;
            return auth.getToken(code);

          case 3:
            data = _context.sent;
            tokens = data.tokens;
            _context.next = 7;
            return getGoogleAccountFromToken(tokens);

          case 7:
            response = _context.sent;
            return _context.abrupt('return', response);

          case 9:
          case 'end':
            return _context.stop();
        }
      }
    }, _callee, undefined);
  }));

  return function getGoogleAccountFromCode(_x) {
    return _ref.apply(this, arguments);
  };
}();

var getGoogleAccountFromToken = function () {
  var _ref2 = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee2(tokens) {
    var auth, plus, me, userGoogleId, userGoogleEmail, userGoogleDisplayName;
    return _regenerator2.default.wrap(function _callee2$(_context2) {
      while (1) {
        switch (_context2.prev = _context2.next) {
          case 0:
            // get the auth "tokens" from the request
            auth = createConnection(); // this is from previous step
            // add the tokens to the google api so we have access to the account

            auth.setCredentials(tokens);

            // connect to google plus - need this to get the user's email
            plus = getGooglePlusApi(auth);
            _context2.next = 5;
            return plus.people.get({ userId: 'me' });

          case 5:
            me = _context2.sent;


            // get the google id and email
            userGoogleId = me.data.id;
            userGoogleEmail = me.data.emails && me.data.emails.length && me.data.emails[0].value;
            userGoogleDisplayName = me.data.displayName;

            // return so we can login or sign up the user

            return _context2.abrupt('return', {
              id: userGoogleId,
              email: userGoogleEmail,
              displayName: userGoogleDisplayName,
              tokens: tokens // you can save these to the user if you ever want to get their details without making them log in again
            });

          case 10:
          case 'end':
            return _context2.stop();
        }
      }
    }, _callee2, undefined);
  }));

  return function getGoogleAccountFromToken(_x2) {
    return _ref2.apply(this, arguments);
  };
}();

var Util = {
  urlGoogle: urlGoogle,
  getGoogleAccountFromCode: getGoogleAccountFromCode,
  getGoogleAccountFromToken: getGoogleAccountFromToken
};

exports.default = Util;