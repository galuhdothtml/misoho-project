'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _stringify = require('babel-runtime/core-js/json/stringify');

var _stringify2 = _interopRequireDefault(_stringify);

var _assign = require('babel-runtime/core-js/object/assign');

var _assign2 = _interopRequireDefault(_assign);

var _moment = require('moment');

var _moment2 = _interopRequireDefault(_moment);

var _jsonwebtoken = require('jsonwebtoken');

var _jsonwebtoken2 = _interopRequireDefault(_jsonwebtoken);

var _constants = require('../constants');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var getOrderStateText = function getOrderStateText(val) {
    if (String(val) === '1') {
        return "Belum Dibayar";
    } else if (String(val) === '2') {
        return "Sudah Dibayar";
    } else if (String(val) === '3') {
        return "Sedang Diproses";
    } else if (String(val) === '4') {
        return "Sudah Dikirim";
    }

    return val;
};

var formatModelDateTime = function formatModelDateTime(val, DataTypes) {
    return (0, _assign2.default)({}, val, {
        createdAt: {
            type: DataTypes.DATE,
            get: function get() {
                return _moment2.default.utc(this.getDataValue('createdAt')).format('YYYY-MM-DD HH:mm:ss');
            }
        },
        updatedAt: {
            type: DataTypes.DATE,
            get: function get() {
                return _moment2.default.utc(this.getDataValue('updatedAt')).format('YYYY-MM-DD HH:mm:ss');
            }
        }
    });
};

var getTokenFromRequest = function getTokenFromRequest(req) {
    if (req.headers.authorization && req.headers.authorization.split(' ')[0] === 'Bearer') {
        return req.headers.authorization.split(' ')[1];
    }

    return '';
};

var decodeToken = function decodeToken(token) {
    return _jsonwebtoken2.default.verify(token, _constants.SECRET_KEY);
};

var stringifyThenParse = function stringifyThenParse(data) {
    return JSON.parse((0, _stringify2.default)(data));
};

var slugify = function slugify(val) {
    return val.toLowerCase().replace(/ /g, '-').replace(/[^\w-]+/g, '');
};

var randChar = function randChar() {
    var length = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : 5;

    var result = '';
    var characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';
    var charactersLength = characters.length;
    for (var i = 0; i < length; i++) {
        result += characters.charAt(Math.floor(Math.random() * charactersLength));
    }
    return result;
};

var Util = {
    formatModelDateTime: formatModelDateTime,
    getTokenFromRequest: getTokenFromRequest,
    decodeToken: decodeToken,
    stringifyThenParse: stringifyThenParse,
    slugify: slugify,
    randChar: randChar,
    getOrderStateText: getOrderStateText
};

exports.default = Util;