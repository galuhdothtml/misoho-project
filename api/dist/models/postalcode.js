'use strict';

module.exports = function (sequelize, DataTypes) {
  var PostalCode = sequelize.define('PostalCode', {
    urban: DataTypes.STRING,
    sub_district: DataTypes.STRING,
    city: DataTypes.STRING,
    province_code: DataTypes.INTEGER,
    postal_code: DataTypes.STRING
  }, { timestamps: false });
  PostalCode.associate = function (models) {
    // associations can be defined here
  };
  return PostalCode;
};