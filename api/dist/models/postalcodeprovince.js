'use strict';

module.exports = function (sequelize, DataTypes) {
  var PostalCodeProvince = sequelize.define('PostalCodeProvince', {
    province_name: DataTypes.STRING,
    province_name_en: DataTypes.STRING,
    province_code: DataTypes.INTEGER
  }, {});
  PostalCodeProvince.associate = function (models) {
    // associations can be defined here
  };
  return PostalCodeProvince;
};