'use strict';

var _utils = require('../utils');

var _utils2 = _interopRequireDefault(_utils);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

module.exports = function (sequelize, DataTypes) {
  var BackofficeMenu = sequelize.define('BackofficeMenu', _utils2.default.formatModelDateTime({
    title: DataTypes.STRING,
    content: DataTypes.TEXT,
    icon: DataTypes.STRING
  }, DataTypes), {});
  BackofficeMenu.associate = function (models) {
    // associations can be defined here
  };
  return BackofficeMenu;
};