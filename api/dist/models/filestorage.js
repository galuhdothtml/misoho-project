'use strict';

var _utils = require('../utils');

var _utils2 = _interopRequireDefault(_utils);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

module.exports = function (sequelize, DataTypes) {
  var FileStorage = sequelize.define('FileStorage', _utils2.default.formatModelDateTime({
    tablename: DataTypes.STRING,
    filename: DataTypes.TEXT,
    id_reference: DataTypes.INTEGER
  }, DataTypes), {});
  FileStorage.associate = function (models) {
    // associations can be defined here
  };
  return FileStorage;
};