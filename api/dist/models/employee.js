'use strict';

var _utils = require('../utils');

var _utils2 = _interopRequireDefault(_utils);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

module.exports = function (sequelize, DataTypes) {
  var Employee = sequelize.define('Employee', _utils2.default.formatModelDateTime({
    name: DataTypes.STRING,
    username: DataTypes.STRING,
    email: DataTypes.STRING,
    passwd: DataTypes.STRING,
    type: DataTypes.STRING(2),
    privileges: DataTypes.TEXT,
    is_active: DataTypes.BOOLEAN
  }, DataTypes), {});
  Employee.associate = function (models) {
    // associations can be defined here
  };
  return Employee;
};