'use strict';

var _utils = require('../utils');

var _utils2 = _interopRequireDefault(_utils);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

module.exports = function (sequelize, DataTypes) {
  var Address = sequelize.define('Address', _utils2.default.formatModelDateTime({
    id_city: DataTypes.INTEGER,
    id_customer: DataTypes.INTEGER,
    alias: DataTypes.STRING,
    receiver_name: DataTypes.STRING,
    address: DataTypes.TEXT,
    sub_district: DataTypes.STRING,
    id_postal_code: DataTypes.INTEGER,
    postcode: DataTypes.STRING,
    phone: DataTypes.STRING,
    is_default: DataTypes.BOOLEAN
  }, DataTypes), {});
  Address.associate = function (models) {
    // associations can be defined here
  };
  return Address;
};