'use strict';

var _utils = require('../utils');

var _utils2 = _interopRequireDefault(_utils);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

module.exports = function (sequelize, DataTypes) {
  var OpnameStock = sequelize.define('OpnameStock', _utils2.default.formatModelDateTime({
    id_product: DataTypes.INTEGER,
    system_qty: DataTypes.DECIMAL(19, 4),
    actual_qty: DataTypes.DECIMAL(19, 4),
    is_draft: DataTypes.BOOLEAN
  }, DataTypes), {});
  OpnameStock.associate = function (models) {
    // associations can be defined here
  };
  return OpnameStock;
};