'use strict';

var _utils = require('../utils');

var _utils2 = _interopRequireDefault(_utils);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

module.exports = function (sequelize, DataTypes) {
  var MetaData = sequelize.define('MetaData', _utils2.default.formatModelDateTime({
    id_reference: DataTypes.INTEGER,
    table_name: DataTypes.STRING,
    meta_title: DataTypes.TEXT,
    meta_keywords: DataTypes.TEXT,
    meta_description: DataTypes.TEXT
  }, DataTypes), {});
  MetaData.associate = function (models) {
    // associations can be defined here
  };
  return MetaData;
};