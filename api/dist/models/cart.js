'use strict';

var _utils = require('../utils');

var _utils2 = _interopRequireDefault(_utils);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

module.exports = function (sequelize, DataTypes) {
  var Cart = sequelize.define('Cart', _utils2.default.formatModelDateTime({
    id_customer: DataTypes.STRING
  }, DataTypes), {});
  Cart.associate = function (models) {
    // associations can be defined here
  };
  return Cart;
};