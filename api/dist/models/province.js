'use strict';

var _utils = require('../utils');

var _utils2 = _interopRequireDefault(_utils);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

module.exports = function (sequelize, DataTypes) {
  var Province = sequelize.define('Province', _utils2.default.formatModelDateTime({
    name: DataTypes.STRING
  }, DataTypes), {});
  Province.associate = function (models) {
    // associations can be defined here
  };
  return Province;
};