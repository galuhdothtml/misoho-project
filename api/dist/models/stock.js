'use strict';

var _utils = require('../utils');

var _utils2 = _interopRequireDefault(_utils);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

module.exports = function (sequelize, DataTypes) {
  var Stock = sequelize.define('Stock', _utils2.default.formatModelDateTime({
    id_product: DataTypes.INTEGER,
    qty: DataTypes.DECIMAL(19, 4),
    min_qty: DataTypes.DECIMAL(19, 4)
  }, DataTypes), {});
  Stock.associate = function (models) {
    // associations can be defined here
  };
  return Stock;
};