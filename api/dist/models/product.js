'use strict';

var _utils = require('../utils');

var _utils2 = _interopRequireDefault(_utils);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

module.exports = function (sequelize, DataTypes) {
  var Product = sequelize.define('Product', _utils2.default.formatModelDateTime({
    id_category: DataTypes.INTEGER,
    name: DataTypes.STRING,
    slug: DataTypes.STRING,
    price: DataTypes.DECIMAL(19, 4),
    description: DataTypes.TEXT,
    origin: DataTypes.INTEGER,
    weight: DataTypes.DECIMAL(19, 4),
    popular_score: DataTypes.DECIMAL(19, 4),
    is_active: DataTypes.BOOLEAN,
    primary_img: DataTypes.TEXT
  }, DataTypes), {});
  Product.associate = function (models) {
    // associations can be defined here
  };
  return Product;
};