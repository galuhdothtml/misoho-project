'use strict';

var _utils = require('../utils');

var _utils2 = _interopRequireDefault(_utils);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

module.exports = function (sequelize, DataTypes) {
  var TopSearch = sequelize.define('TopSearch', _utils2.default.formatModelDateTime({
    keyword: DataTypes.STRING
  }, DataTypes), {});
  TopSearch.associate = function (models) {
    // associations can be defined here
  };
  return TopSearch;
};