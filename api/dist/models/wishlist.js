'use strict';

var _utils = require('../utils');

var _utils2 = _interopRequireDefault(_utils);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

module.exports = function (sequelize, DataTypes) {
  var Wishlist = sequelize.define('Wishlist', _utils2.default.formatModelDateTime({
    id_product: DataTypes.STRING,
    id_customer: DataTypes.STRING
  }, DataTypes), {});
  Wishlist.associate = function (models) {
    // associations can be defined here
  };
  return Wishlist;
};