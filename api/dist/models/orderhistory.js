'use strict';

var _utils = require('../utils');

var _utils2 = _interopRequireDefault(_utils);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

module.exports = function (sequelize, DataTypes) {
  var OrderHistory = sequelize.define('OrderHistory', _utils2.default.formatModelDateTime({
    id_order: DataTypes.INTEGER,
    id_employee: DataTypes.INTEGER,
    order_state: DataTypes.STRING(2)
  }, DataTypes), {});
  OrderHistory.associate = function (models) {
    // associations can be defined here
  };
  return OrderHistory;
};