'use strict';

var _utils = require('../utils');

var _utils2 = _interopRequireDefault(_utils);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

module.exports = function (sequelize, DataTypes) {
  var Customer = sequelize.define('Customer', _utils2.default.formatModelDateTime({
    name: DataTypes.STRING,
    email: DataTypes.STRING,
    passwd: DataTypes.STRING,
    newsletter: DataTypes.BOOLEAN,
    is_active: DataTypes.BOOLEAN,
    is_confirmed: DataTypes.BOOLEAN
  }, DataTypes), {});
  Customer.associate = function (models) {
    // associations can be defined here
  };
  return Customer;
};