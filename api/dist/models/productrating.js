'use strict';

var _utils = require('../utils');

var _utils2 = _interopRequireDefault(_utils);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

module.exports = function (sequelize, DataTypes) {
  var ProductRating = sequelize.define('ProductRating', _utils2.default.formatModelDateTime({
    id_product: DataTypes.INTEGER,
    id_user: DataTypes.INTEGER,
    id_order: DataTypes.INTEGER,
    rating: DataTypes.DECIMAL(19, 4),
    description: DataTypes.TEXT
  }, DataTypes), {});
  ProductRating.associate = function (models) {
    // associations can be defined here
  };
  return ProductRating;
};