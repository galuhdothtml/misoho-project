'use strict';

var _utils = require('../utils');

var _utils2 = _interopRequireDefault(_utils);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

module.exports = function (sequelize, DataTypes) {
  var Order = sequelize.define('Order', _utils2.default.formatModelDateTime({
    reference: DataTypes.STRING,
    id_customer: DataTypes.INTEGER,
    address_delivery: DataTypes.TEXT,
    order_state: DataTypes.STRING(2),
    payment: DataTypes.STRING(2),
    shipping: DataTypes.STRING(2),
    subtotal: DataTypes.DECIMAL(19, 4),
    total_shipping: DataTypes.DECIMAL(19, 4),
    grand_total: DataTypes.DECIMAL(19, 4)
  }, DataTypes), {});
  Order.associate = function (models) {
    // associations can be defined here
  };
  return Order;
};