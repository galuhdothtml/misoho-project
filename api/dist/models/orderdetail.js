'use strict';

var _utils = require('../utils');

var _utils2 = _interopRequireDefault(_utils);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

module.exports = function (sequelize, DataTypes) {
  var OrderDetail = sequelize.define('OrderDetail', _utils2.default.formatModelDateTime({
    id_order: DataTypes.INTEGER,
    id_product: DataTypes.INTEGER,
    product_name: DataTypes.STRING,
    product_price: DataTypes.DECIMAL(19, 4),
    qty: DataTypes.DECIMAL(19, 4)
  }, DataTypes), {});
  OrderDetail.associate = function (models) {
    // associations can be defined here
  };
  return OrderDetail;
};