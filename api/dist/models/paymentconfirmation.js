'use strict';

var _moment = require('moment');

var _moment2 = _interopRequireDefault(_moment);

var _utils = require('../utils');

var _utils2 = _interopRequireDefault(_utils);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

module.exports = function (sequelize, DataTypes) {
  var PaymentConfirmation = sequelize.define('PaymentConfirmation', _utils2.default.formatModelDateTime({
    reference_number: DataTypes.STRING,
    is_auto: DataTypes.BOOLEAN,
    transfer_date: {
      type: DataTypes.DATE,
      get: function get() {
        return _moment2.default.utc(this.getDataValue('transfer_date')).format('YYYY-MM-DD HH:mm:ss');
      }
    }
  }, DataTypes), {});
  PaymentConfirmation.associate = function (models) {
    // associations can be defined here
  };
  return PaymentConfirmation;
};