"use strict";

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _promise = require("babel-runtime/core-js/promise");

var _promise2 = _interopRequireDefault(_promise);

var _stringify = require("babel-runtime/core-js/json/stringify");

var _stringify2 = _interopRequireDefault(_stringify);

var _defineProperty2 = require("babel-runtime/helpers/defineProperty");

var _defineProperty3 = _interopRequireDefault(_defineProperty2);

var _parseInt = require("babel-runtime/core-js/number/parse-int");

var _parseInt2 = _interopRequireDefault(_parseInt);

var _assign = require("babel-runtime/core-js/object/assign");

var _assign2 = _interopRequireDefault(_assign);

var _regenerator = require("babel-runtime/regenerator");

var _regenerator2 = _interopRequireDefault(_regenerator);

var _asyncToGenerator2 = require("babel-runtime/helpers/asyncToGenerator");

var _asyncToGenerator3 = _interopRequireDefault(_asyncToGenerator2);

var _classCallCheck2 = require("babel-runtime/helpers/classCallCheck");

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _sequelize = require("sequelize");

var _sequelize2 = _interopRequireDefault(_sequelize);

var _superagent = require("superagent");

var _superagent2 = _interopRequireDefault(_superagent);

var _moment = require("moment");

var _moment2 = _interopRequireDefault(_moment);

var _jsonwebtoken = require("jsonwebtoken");

var _jsonwebtoken2 = _interopRequireDefault(_jsonwebtoken);

var _path = require("path");

var _path2 = _interopRequireDefault(_path);

var _bcrypt = require("bcrypt");

var _bcrypt2 = _interopRequireDefault(_bcrypt);

var _fs = require("fs");

var _fs2 = _interopRequireDefault(_fs);

var _sharp = require("sharp");

var _sharp2 = _interopRequireDefault(_sharp);

var _kue = require("kue");

var _kue2 = _interopRequireDefault(_kue);

var _constants = require("../constants");

var _stored_procedure = require("../utils/stored_procedure");

var _stored_procedure2 = _interopRequireDefault(_stored_procedure);

var _db = require("../utils/db");

var _db2 = _interopRequireDefault(_db);

var _model_factory = require("../utils/model_factory");

var _model_factory2 = _interopRequireDefault(_model_factory);

var _utils = require("../utils");

var _utils2 = _interopRequireDefault(_utils);

var _google_util = require("../utils/google_util");

var _google_util2 = _interopRequireDefault(_google_util);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var SALT_ROUNDS = 10;
var SALT_PASSWD = _bcrypt2.default.genSaltSync(SALT_ROUNDS);
var RAJA_ONGKIR_API_KEY = '323fb606f44b43b4febb6470be051f2c';
var Op = _sequelize2.default.Op;
var Address = _model_factory2.default.createModel(_constants.ModelName.ADDRESS);
var BackofficeMenu = _model_factory2.default.createModel(_constants.ModelName.BACKOFFICE_MENU);
var Product = _model_factory2.default.createModel(_constants.ModelName.PRODUCT);
var Category = _model_factory2.default.createModel(_constants.ModelName.CATEGORY);
var Employee = _model_factory2.default.createModel(_constants.ModelName.EMPLOYEE);
var Customer = _model_factory2.default.createModel(_constants.ModelName.CUSTOMER);
var Cart = _model_factory2.default.createModel(_constants.ModelName.CART);
var CartDetail = _model_factory2.default.createModel(_constants.ModelName.CART_DETAIL);
var Wishlist = _model_factory2.default.createModel(_constants.ModelName.WISHLIST);
var OrderHistory = _model_factory2.default.createModel(_constants.ModelName.ORDER_HISTORY);
var OrderDetail = _model_factory2.default.createModel(_constants.ModelName.ORDER_DETAIL);
var FileStorage = _model_factory2.default.createModel(_constants.ModelName.FILE_STORAGE);
var Province = _model_factory2.default.createModel(_constants.ModelName.PROVINCE);
var City = _model_factory2.default.createModel(_constants.ModelName.CITY);
var Order = _model_factory2.default.createModel(_constants.ModelName.ORDER);
var Stock = _model_factory2.default.createModel(_constants.ModelName.STOCK);
var PaymentConfirmation = _model_factory2.default.createModel(_constants.ModelName.PAYMENT_CONFIRMATION);
var PostalCode = _model_factory2.default.createModel(_constants.ModelName.POSTAL_CODE);
var ProductRating = _model_factory2.default.createModel(_constants.ModelName.PRODUCT_RATING);
var QueryBuilder = _model_factory2.default.createModel('default');

var getOrderStateName = function getOrderStateName(val) {
    var retval = "";

    if (String(val) === "1") {
        retval = "Menunggu Pembayaran";
    } else if (String(val) === "2") {
        retval = "Terbayar";
    } else if (String(val) === "3") {
        retval = "Sedang Diproses";
    } else if (String(val) === "4") {
        retval = "Sudah Dikirim";
    } else if (String(val) === "5") {
        retval = "Sudah Diterima";
    }

    return retval;
};

var getFileName = function getFileName() {
    var length = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : 4;

    var result = '';
    var characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    var charactersLength = characters.length;
    for (var i = 0; i < length; i++) {
        result += characters.charAt(Math.floor(Math.random() * charactersLength));
    }
    return result;
};

var checkDuplicateEmail = function checkDuplicateEmail(_ref) {
    var email = _ref.email,
        id = _ref.id;

    if (String(id).trim().length > 0) {
        return Customer.findOne({
            where: {
                email: email,
                id: { $not: id }
            }
        });
    }

    return Customer.findOne({
        where: {
            email: email
        }
    });
};

var queue = _kue2.default.createQueue();

var Api = function Api() {
    var _this = this;

    (0, _classCallCheck3.default)(this, Api);

    this.index = function (req, res) {
        var data = 'Mister Oleh Oleh REST API';
        res.json({ status: true, data: data });
    };

    this.uploadFile = function (req, res) {
        var api = {
            "status": true,
            "data": req.file,
            "msg": 'success'
        };

        res.json(api);
    };

    this.uploadFiles = function (req, res) {
        var api = {
            "status": true,
            "data": req.files,
            "msg": 'success'
        };

        res.json(api);
    };

    this.getGoogleUrl = function () {
        var _ref2 = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee(req, res) {
            var api;
            return _regenerator2.default.wrap(function _callee$(_context) {
                while (1) {
                    switch (_context.prev = _context.next) {
                        case 0:
                            api = {
                                "status": false,
                                "msg": 'error'
                            };


                            api = {
                                "status": true,
                                "data": _google_util2.default.urlGoogle(),
                                "msg": 'success'
                            };

                            res.json(api);

                        case 3:
                        case "end":
                            return _context.stop();
                    }
                }
            }, _callee, _this);
        }));

        return function (_x2, _x3) {
            return _ref2.apply(this, arguments);
        };
    }();

    this.setupGoogleAccount = function () {
        var _ref3 = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee2(req, res) {
            var code, api, data, result, exist, payload, savedData;
            return _regenerator2.default.wrap(function _callee2$(_context2) {
                while (1) {
                    switch (_context2.prev = _context2.next) {
                        case 0:
                            code = req.body.code;
                            api = {
                                "status": false,
                                "msg": 'error'
                            };
                            data = {};
                            _context2.next = 5;
                            return _google_util2.default.getGoogleAccountFromCode(code);

                        case 5:
                            result = _context2.sent;
                            _context2.next = 8;
                            return Customer.findOne({
                                where: { email: result.email },
                                raw: true
                            });

                        case 8:
                            exist = _context2.sent;

                            if (!exist) {
                                _context2.next = 15;
                                break;
                            }

                            data = (0, _assign2.default)({}, exist, {
                                type: 'user'
                            });

                            delete data.passwd;

                            api = {
                                "status": true,
                                "data": data,
                                "token": _jsonwebtoken2.default.sign({ data: data }, _constants.SECRET_KEY, { expiresIn: '17520h' })
                            };
                            _context2.next = 20;
                            break;

                        case 15:
                            payload = {
                                name: result.displayName,
                                email: result.email,
                                passwd: _bcrypt2.default.hashSync(String(_utils2.default.randChar(6)), SALT_PASSWD)
                            };
                            _context2.next = 18;
                            return Customer.create(payload);

                        case 18:
                            savedData = _context2.sent;

                            if (savedData) {
                                data = (0, _assign2.default)({}, _utils2.default.stringifyThenParse(savedData), {
                                    type: 'user'
                                });

                                delete data.passwd;

                                api = {
                                    "status": true,
                                    "data": data,
                                    "token": _jsonwebtoken2.default.sign({ data: data }, _constants.SECRET_KEY, { expiresIn: '17520h' })
                                };
                            }

                        case 20:

                            res.json(api);

                        case 21:
                        case "end":
                            return _context2.stop();
                    }
                }
            }, _callee2, _this);
        }));

        return function (_x4, _x5) {
            return _ref3.apply(this, arguments);
        };
    }();

    this.register = function () {
        var _ref4 = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee3(req, res) {
            var _req$body, name, email, password, confirm_password, isPasswordValid, api, isExist, data, newData;

            return _regenerator2.default.wrap(function _callee3$(_context3) {
                while (1) {
                    switch (_context3.prev = _context3.next) {
                        case 0:
                            _req$body = req.body, name = _req$body.name, email = _req$body.email, password = _req$body.password, confirm_password = _req$body.confirm_password;
                            isPasswordValid = String(password) === String(confirm_password);
                            api = {
                                status: false,
                                msg: "wrong_confirm_password"
                            };

                            if (!isPasswordValid) {
                                _context3.next = 17;
                                break;
                            }

                            _context3.next = 6;
                            return checkDuplicateEmail({ email: email, id: null });

                        case 6:
                            isExist = _context3.sent;

                            if (!isExist) {
                                _context3.next = 11;
                                break;
                            }

                            api = {
                                status: false,
                                msg: "email_already_exist"
                            };
                            _context3.next = 17;
                            break;

                        case 11:
                            _context3.next = 13;
                            return Customer.create({
                                name: name,
                                email: email,
                                passwd: _bcrypt2.default.hashSync(password, SALT_PASSWD)
                            });

                        case 13:
                            data = _context3.sent;
                            newData = (0, _assign2.default)({}, _utils2.default.stringifyThenParse(data), {
                                type: 'user'
                            });

                            delete newData.passwd;

                            api = {
                                status: true,
                                data: data,
                                token: _jsonwebtoken2.default.sign({ data: newData }, _constants.SECRET_KEY, { expiresIn: '17520h' }),
                                msg: "success"
                            };

                        case 17:

                            res.json(api);

                        case 18:
                        case "end":
                            return _context3.stop();
                    }
                }
            }, _callee3, _this);
        }));

        return function (_x6, _x7) {
            return _ref4.apply(this, arguments);
        };
    }();

    this.login = function () {
        var _ref5 = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee4(req, res) {
            var type, _req$body2, email, password, isPasswordValid, data, _api, result, _req$body3, username, _password, _isPasswordValid, _data, _result, _api2;

            return _regenerator2.default.wrap(function _callee4$(_context4) {
                while (1) {
                    switch (_context4.prev = _context4.next) {
                        case 0:
                            type = req.params.type;

                            if (!(type === 'user')) {
                                _context4.next = 15;
                                break;
                            }

                            _req$body2 = req.body, email = _req$body2.email, password = _req$body2.password;
                            isPasswordValid = false;
                            data = {};
                            _api = {
                                "status": false,
                                "data": null
                            };

                            if (!(email && String(email).length > 0 && password && String(password).length > 0)) {
                                _context4.next = 12;
                                break;
                            }

                            _context4.next = 9;
                            return Customer.findOne({
                                where: { email: email },
                                raw: true
                            });

                        case 9:
                            result = _context4.sent;


                            if (result) {
                                isPasswordValid = _bcrypt2.default.compareSync(password, result.passwd);
                                data = (0, _assign2.default)({}, result, {
                                    type: 'user'
                                });

                                delete data.passwd;
                            }

                            if (isPasswordValid) {
                                _api = {
                                    "status": true,
                                    "data": data,
                                    "token": _jsonwebtoken2.default.sign({ data: data }, _constants.SECRET_KEY, { expiresIn: '17520h' })
                                };
                            }

                        case 12:

                            res.json(_api);
                            _context4.next = 26;
                            break;

                        case 15:
                            if (!(type === 'employee')) {
                                _context4.next = 26;
                                break;
                            }

                            _req$body3 = req.body, username = _req$body3.username, _password = _req$body3.password;
                            _isPasswordValid = false;
                            _data = {};
                            _context4.next = 21;
                            return Employee.findOne({
                                where: { username: username },
                                raw: true
                            });

                        case 21:
                            _result = _context4.sent;


                            if (_result) {
                                _isPasswordValid = _bcrypt2.default.compareSync(_password, _result.passwd);
                                _data = (0, _assign2.default)({}, _result, {
                                    type: 'employee'
                                });

                                delete _data.passwd;
                            }

                            _api2 = {
                                "status": false,
                                "data": null
                            };


                            if (_isPasswordValid) {
                                _api2 = {
                                    "status": true,
                                    "data": _data,
                                    "token": _jsonwebtoken2.default.sign({ data: _data }, _constants.SECRET_KEY, { expiresIn: '17520h' })
                                };
                            }

                            res.json(_api2);

                        case 26:
                        case "end":
                            return _context4.stop();
                    }
                }
            }, _callee4, _this);
        }));

        return function (_x8, _x9) {
            return _ref5.apply(this, arguments);
        };
    }();

    this.appMenu = function () {
        var _ref6 = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee5(req, res) {
            var api, results, data;
            return _regenerator2.default.wrap(function _callee5$(_context5) {
                while (1) {
                    switch (_context5.prev = _context5.next) {
                        case 0:
                            api = {
                                "status": false,
                                "msg": 'error'
                            };
                            _context5.next = 3;
                            return BackofficeMenu.findAll();

                        case 3:
                            results = _context5.sent;


                            if (results) {
                                data = results.map(function (x) {
                                    if (!Array.isArray(JSON.parse(x.content))) {
                                        var _JSON$parse = JSON.parse(x.content),
                                            link = _JSON$parse.link;

                                        var newObj = (0, _assign2.default)(_utils2.default.stringifyThenParse(x), { link: link });

                                        delete newObj.content;

                                        return newObj;
                                    }

                                    return (0, _assign2.default)(x, { content: JSON.parse(x.content) });
                                });

                                api = {
                                    "status": true,
                                    "data": data,
                                    "msg": 'success'
                                };
                            }

                            res.json(api);

                        case 6:
                        case "end":
                            return _context5.stop();
                    }
                }
            }, _callee5, _this);
        }));

        return function (_x10, _x11) {
            return _ref6.apply(this, arguments);
        };
    }();

    this.getBackofficeProducts = function () {
        var _ref7 = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee6(req, res) {
            var api, _req$query, sorted, _limit, _page, filterText, limit, page, options, calculateOptions, _JSON$parse2, id, desc, filter, results, data, calculateAmountOfData, amountOfData;

            return _regenerator2.default.wrap(function _callee6$(_context6) {
                while (1) {
                    switch (_context6.prev = _context6.next) {
                        case 0:
                            api = {
                                "status": false,
                                "msg": 'error'
                            };
                            _req$query = req.query, sorted = _req$query.sorted, _limit = _req$query.limit, _page = _req$query.page, filterText = _req$query.filterText;
                            limit = 5, page = 1;


                            if (_limit) {
                                limit = _limit;
                            }

                            if (_page) {
                                page = _page;
                            }

                            options = {
                                offset: (page - 1) * limit,
                                limit: (0, _parseInt2.default)(limit, 10),
                                order: [['updatedAt', 'DESC']],
                                include: [{
                                    model: Category
                                }]
                            };
                            calculateOptions = {
                                attributes: [[_db2.default.fn('COUNT', _db2.default.col('id')), 'total']],
                                raw: true
                            };


                            if (sorted) {
                                _JSON$parse2 = JSON.parse(sorted), id = _JSON$parse2.id, desc = _JSON$parse2.desc;

                                (0, _assign2.default)(options, {
                                    order: _db2.default.literal(id + " " + (desc ? 'DESC' : 'ASC'))
                                });
                            }

                            if (filterText) {
                                filter = {
                                    name: (0, _defineProperty3.default)({}, Op.regexp, "(" + filterText + ")")
                                };

                                (0, _assign2.default)(options, {
                                    where: filter
                                });
                                (0, _assign2.default)(calculateOptions, {
                                    where: filter
                                });
                            }

                            Category.hasMany(Product, { foreignKey: 'id_category', sourceKey: 'id' });
                            Product.belongsTo(Category, { foreignKey: 'id_category', sourceKey: 'id' });
                            _context6.next = 13;
                            return Product.findAll(options);

                        case 13:
                            results = _context6.sent;
                            data = _utils2.default.stringifyThenParse(results).map(function (x) {
                                var category_name = x.Category.name;

                                delete x.Category;

                                return (0, _assign2.default)({}, x, { category_name: category_name });
                            });
                            _context6.next = 17;
                            return Product.findOne(calculateOptions);

                        case 17:
                            calculateAmountOfData = _context6.sent;
                            amountOfData = calculateAmountOfData.total;


                            if (data) {
                                api = {
                                    "status": true,
                                    "data": data,
                                    "page_total": Math.ceil(amountOfData / limit),
                                    "msg": 'success'
                                };
                            }

                            res.json(api);

                        case 21:
                        case "end":
                            return _context6.stop();
                    }
                }
            }, _callee6, _this);
        }));

        return function (_x12, _x13) {
            return _ref7.apply(this, arguments);
        };
    }();

    this.uploadProfilePhoto = function () {
        var _ref8 = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee7(req, res) {
            var token, _Util$decodeToken, idCustomer, fileStorageFound, fileName, api;

            return _regenerator2.default.wrap(function _callee7$(_context7) {
                while (1) {
                    switch (_context7.prev = _context7.next) {
                        case 0:
                            token = _utils2.default.getTokenFromRequest(req);
                            _Util$decodeToken = _utils2.default.decodeToken(token), idCustomer = _Util$decodeToken.data.id;
                            _context7.next = 4;
                            return FileStorage.findOne({
                                where: {
                                    id_reference: idCustomer,
                                    tablename: 'Customers'
                                }
                            });

                        case 4:
                            fileStorageFound = _context7.sent;


                            if (fileStorageFound) {
                                try {
                                    _fs2.default.unlinkSync("public/uploads/" + fileStorageFound.filename);
                                } catch (err) {
                                    console.log(err);
                                }
                            }

                            _context7.next = 8;
                            return FileStorage.destroy({
                                where: {
                                    id_reference: idCustomer,
                                    tablename: 'Customers'
                                }
                            });

                        case 8:
                            fileName = null;


                            if (req.file) {
                                fileName = req.file.filename;
                            }

                            _context7.next = 12;
                            return FileStorage.create({
                                tablename: "Customers",
                                filename: fileName,
                                id_reference: idCustomer
                            });

                        case 12:
                            api = {
                                "status": true,
                                "data": fileName
                            };


                            res.json(api);

                        case 14:
                        case "end":
                            return _context7.stop();
                    }
                }
            }, _callee7, _this);
        }));

        return function (_x14, _x15) {
            return _ref8.apply(this, arguments);
        };
    }();

    this.showProducts = function () {
        var _ref9 = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee8(req, res) {
            var _req$query2, _limit, _page, id_category, sort, origin, search, min_price, max_price, limit, page, options, calculateOptions, filter, priceRangeFilter, products, data, calculateAmountOfData, amountOfData, api;

            return _regenerator2.default.wrap(function _callee8$(_context8) {
                while (1) {
                    switch (_context8.prev = _context8.next) {
                        case 0:
                            _req$query2 = req.query, _limit = _req$query2.limit, _page = _req$query2.page, id_category = _req$query2.id_category, sort = _req$query2.sort, origin = _req$query2.origin, search = _req$query2.search, min_price = _req$query2.min_price, max_price = _req$query2.max_price;
                            limit = 5, page = 1;


                            if (_limit) {
                                limit = _limit;
                            }

                            if (_page) {
                                page = _page;
                            }

                            options = {
                                offset: (page - 1) * limit,
                                limit: (0, _parseInt2.default)(limit, 10),
                                order: [['createdAt', 'DESC']],
                                include: [{
                                    model: Category
                                }]
                            };
                            calculateOptions = {
                                attributes: [[_db2.default.fn('COUNT', _db2.default.col('id')), 'total']],
                                raw: true
                            };


                            if (id_category && String(id_category) !== '') {
                                options = (0, _assign2.default)({}, options, {
                                    where: { id_category: id_category }
                                });
                                calculateOptions = (0, _assign2.default)({}, calculateOptions, {
                                    where: { id_category: id_category }
                                });
                            }

                            if (origin) {
                                options = (0, _assign2.default)({}, options, {
                                    where: options.where ? (0, _assign2.default)({}, options.where, { origin: origin }) : { origin: origin }
                                });
                                calculateOptions = (0, _assign2.default)({}, calculateOptions, {
                                    where: calculateOptions.where ? (0, _assign2.default)({}, calculateOptions.where, { origin: origin }) : { origin: origin }
                                });
                            }

                            if (search) {
                                filter = {
                                    name: (0, _defineProperty3.default)({}, Op.regexp, "(" + search + ")")
                                };

                                options = (0, _assign2.default)({}, options, {
                                    where: options.where ? (0, _assign2.default)({}, options.where, filter) : filter
                                });
                                calculateOptions = (0, _assign2.default)({}, calculateOptions, {
                                    where: calculateOptions.where ? (0, _assign2.default)({}, calculateOptions.where, filter) : filter
                                });
                            }

                            priceRangeFilter = {};


                            if (min_price && max_price) {
                                priceRangeFilter = {
                                    price: (0, _defineProperty3.default)({}, Op.between, [min_price, max_price])
                                };
                            } else if (min_price) {
                                priceRangeFilter = {
                                    price: (0, _defineProperty3.default)({}, Op.gte, min_price)
                                };
                            } else if (max_price) {
                                priceRangeFilter = {
                                    price: (0, _defineProperty3.default)({}, Op.lte, max_price)
                                };
                            }

                            options = (0, _assign2.default)({}, options, {
                                where: options.where ? (0, _assign2.default)({}, options.where, priceRangeFilter) : priceRangeFilter
                            });
                            calculateOptions = (0, _assign2.default)({}, calculateOptions, {
                                where: calculateOptions.where ? (0, _assign2.default)({}, calculateOptions.where, priceRangeFilter) : priceRangeFilter
                            });

                            if (!sort) {
                                _context8.next = 26;
                                break;
                            }

                            _context8.t0 = sort;
                            _context8.next = _context8.t0 === 'TERBARU' ? 17 : _context8.t0 === 'TERPOPULER' ? 19 : _context8.t0 === 'HARGA_TERENDAH' ? 21 : _context8.t0 === 'HARGA_TERTINGGI' ? 23 : 25;
                            break;

                        case 17:
                            options = (0, _assign2.default)({}, options, {
                                order: [['createdAt', 'DESC']]
                            });
                            return _context8.abrupt("break", 26);

                        case 19:
                            options = (0, _assign2.default)({}, options, {
                                order: [['popular_score', 'DESC']]
                            });
                            return _context8.abrupt("break", 26);

                        case 21:
                            options = (0, _assign2.default)({}, options, {
                                order: [['price', 'ASC']]
                            });
                            return _context8.abrupt("break", 26);

                        case 23:
                            options = (0, _assign2.default)({}, options, {
                                order: [['price', 'DESC']]
                            });
                            return _context8.abrupt("break", 26);

                        case 25:
                            options = (0, _assign2.default)({}, options, {
                                order: [['createdAt', 'DESC']]
                            });

                        case 26:

                            Category.hasMany(Product, { foreignKey: 'id_category', sourceKey: 'id' });
                            Product.belongsTo(Category, { foreignKey: 'id_category', sourceKey: 'id' });
                            _context8.next = 30;
                            return Product.findAll(options);

                        case 30:
                            products = _context8.sent;
                            data = _utils2.default.stringifyThenParse(products).map(function (x) {
                                var category_name = x.Category.name;

                                delete x.Category;

                                return (0, _assign2.default)({}, x, { category_name: category_name });
                            });
                            _context8.next = 34;
                            return Product.findOne(calculateOptions);

                        case 34:
                            calculateAmountOfData = _context8.sent;
                            amountOfData = calculateAmountOfData.total;
                            api = {
                                "status": true,
                                "data": data,
                                "page_total": Math.ceil(amountOfData / limit)
                            };


                            res.json(api);

                        case 38:
                        case "end":
                            return _context8.stop();
                    }
                }
            }, _callee8, _this);
        }));

        return function (_x16, _x17) {
            return _ref9.apply(this, arguments);
        };
    }();

    this.getBackofficeCategories = function () {
        var _ref10 = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee9(req, res) {
            var api, _req$query3, sorted, _limit, _page, filterText, limit, page, options, calculateOptions, _JSON$parse3, id, desc, filter, results, calculateAmountOfData, amountOfData;

            return _regenerator2.default.wrap(function _callee9$(_context9) {
                while (1) {
                    switch (_context9.prev = _context9.next) {
                        case 0:
                            api = {
                                "status": false,
                                "msg": 'error'
                            };
                            _req$query3 = req.query, sorted = _req$query3.sorted, _limit = _req$query3.limit, _page = _req$query3.page, filterText = _req$query3.filterText;
                            limit = 0, page = 1;
                            options = {
                                order: [['id', 'ASC']]
                            };


                            if (_limit) {
                                if (_page) {
                                    page = _page;
                                }

                                limit = _limit;
                                (0, _assign2.default)(options, {
                                    offset: (page - 1) * limit,
                                    limit: (0, _parseInt2.default)(limit, 10)
                                });
                            }

                            calculateOptions = {
                                attributes: [[_db2.default.fn('COUNT', _db2.default.col('id')), 'total']],
                                raw: true
                            };


                            if (sorted) {
                                _JSON$parse3 = JSON.parse(sorted), id = _JSON$parse3.id, desc = _JSON$parse3.desc;

                                (0, _assign2.default)(options, {
                                    order: _db2.default.literal(id + " " + (desc ? 'DESC' : 'ASC'))
                                });
                            }

                            if (filterText) {
                                filter = {
                                    name: (0, _defineProperty3.default)({}, Op.regexp, "(" + filterText + ")")
                                };

                                (0, _assign2.default)(options, {
                                    where: filter
                                });
                                (0, _assign2.default)(calculateOptions, {
                                    where: filter
                                });
                            }

                            _context9.next = 10;
                            return Category.findAll(options);

                        case 10:
                            results = _context9.sent;
                            _context9.next = 13;
                            return Category.findOne(calculateOptions);

                        case 13:
                            calculateAmountOfData = _context9.sent;
                            amountOfData = calculateAmountOfData.total;


                            if (results) {
                                api = {
                                    "status": true,
                                    "data": results,
                                    "page_total": Math.ceil(amountOfData / limit),
                                    "msg": 'success'
                                };
                            }

                            res.json(api);

                        case 17:
                        case "end":
                            return _context9.stop();
                    }
                }
            }, _callee9, _this);
        }));

        return function (_x18, _x19) {
            return _ref10.apply(this, arguments);
        };
    }();

    this.showCategories = function () {
        var _ref11 = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee10(req, res) {
            var _req$query4, _limit, _page, search, is_unlimited, with_image, isUnlimited, limit, page, options, calculateOptions, filter, data, newData, fileStorages, parsedImages, calculateAmountOfData, amountOfData, api;

            return _regenerator2.default.wrap(function _callee10$(_context10) {
                while (1) {
                    switch (_context10.prev = _context10.next) {
                        case 0:
                            /**
                             * mandatory param: limit, page, is_unlimited
                            */
                            _req$query4 = req.query, _limit = _req$query4.limit, _page = _req$query4.page, search = _req$query4.search, is_unlimited = _req$query4.is_unlimited, with_image = _req$query4.with_image;
                            isUnlimited = is_unlimited && is_unlimited === 'Y';
                            limit = 5, page = 1;
                            options = {
                                raw: true,
                                order: [['createdAt', 'DESC']]
                            };
                            calculateOptions = {
                                attributes: [[_db2.default.fn('COUNT', _db2.default.col('id')), 'total']],
                                raw: true
                            };


                            if (_limit) {
                                limit = _limit;
                            }

                            if (_page) {
                                page = _page;
                            }

                            if (!isUnlimited) {
                                options = (0, _assign2.default)({}, options, {
                                    offset: (page - 1) * limit,
                                    limit: (0, _parseInt2.default)(limit, 10)
                                });
                            }

                            if (search) {
                                filter = {
                                    name: (0, _defineProperty3.default)({}, Op.regexp, "(" + search + ")")
                                };

                                options = (0, _assign2.default)({}, options, {
                                    where: options.where ? (0, _assign2.default)({}, options.where, filter) : filter
                                });
                                calculateOptions = (0, _assign2.default)({}, calculateOptions, {
                                    where: calculateOptions.where ? (0, _assign2.default)({}, calculateOptions.where, filter) : filter
                                });
                            }

                            _context10.next = 11;
                            return Category.findAll(options);

                        case 11:
                            data = _context10.sent;
                            newData = _utils2.default.stringifyThenParse(data);

                            if (!with_image) {
                                _context10.next = 19;
                                break;
                            }

                            _context10.next = 16;
                            return FileStorage.findAll({
                                where: {
                                    tablename: 'Categories'
                                }
                            });

                        case 16:
                            fileStorages = _context10.sent;
                            parsedImages = _utils2.default.stringifyThenParse(fileStorages);

                            newData = newData.map(function (x) {
                                var foundImg = parsedImages.find(function (pImg) {
                                    return String(pImg.id_reference) === String(x.id);
                                });

                                if (foundImg) {
                                    return (0, _assign2.default)({}, x, { img: foundImg.filename });
                                }

                                return x;
                            });

                        case 19:
                            _context10.next = 21;
                            return Category.findOne(calculateOptions);

                        case 21:
                            calculateAmountOfData = _context10.sent;
                            amountOfData = calculateAmountOfData.total;
                            api = {
                                "status": true,
                                "data": newData,
                                "page_total": !isUnlimited ? Math.ceil(amountOfData / limit) : 1
                            };


                            res.json(api);

                        case 25:
                        case "end":
                            return _context10.stop();
                    }
                }
            }, _callee10, _this);
        }));

        return function (_x20, _x21) {
            return _ref11.apply(this, arguments);
        };
    }();

    this.checkoutOrder = function () {
        var _ref12 = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee11(req, res) {
            var _req$body4, id_address, shipping_courier, shipping_service, token, _Util$decodeToken2, idCustomer, reference, found, address, _api3, weightResult, weight, shippingParam, shippingRes, shippingCosts, shippingCost, execStoredProcedure, data;

            return _regenerator2.default.wrap(function _callee11$(_context11) {
                while (1) {
                    switch (_context11.prev = _context11.next) {
                        case 0:
                            _context11.prev = 0;
                            _req$body4 = req.body, id_address = _req$body4.id_address, shipping_courier = _req$body4.shipping_courier, shipping_service = _req$body4.shipping_service;
                            token = _utils2.default.getTokenFromRequest(req);
                            _Util$decodeToken2 = _utils2.default.decodeToken(token), idCustomer = _Util$decodeToken2.data.id;
                            reference = "MO-" + _utils2.default.randChar(8);
                            _context11.next = 7;
                            return Order.findOne({
                                where: {
                                    reference: reference
                                }
                            });

                        case 7:
                            found = _context11.sent;

                        case 8:
                            if (!found) {
                                _context11.next = 15;
                                break;
                            }

                            reference = "MO-" + _utils2.default.randChar(8);
                            _context11.next = 12;
                            return Order.findOne({
                                where: {
                                    reference: reference
                                }
                            });

                        case 12:
                            found = _context11.sent;
                            _context11.next = 8;
                            break;

                        case 15:
                            _context11.next = 17;
                            return Address.findOne({
                                where: {
                                    id: id_address,
                                    id_customer: idCustomer
                                }
                            });

                        case 17:
                            address = _context11.sent;
                            _api3 = {
                                "status": false,
                                "msg": "address_not_found"
                            };

                            if (!address) {
                                _context11.next = 39;
                                break;
                            }

                            _context11.next = 22;
                            return _db2.default.query("SELECT IFNULL(SUM(p.weight*cd.qty), 0) AS weight_total FROM CartDetails cd \n                JOIN Products p ON p.id = cd.id_product \n                JOIN Carts ca ON ca.id = cd.id_cart \n                JOIN Customers c ON c.id = ca.id_customer WHERE ca.id_customer = " + idCustomer, { type: _db2.default.QueryTypes.SELECT });

                        case 22:
                            weightResult = _context11.sent;
                            weight = 0;


                            if (weightResult.length > 0) {
                                weight = weightResult[0].weight_total;
                            }

                            _api3 = {
                                "status": false,
                                "msg": "weight_zero_is_not_allowed"
                            };

                            if (!(weight > 0)) {
                                _context11.next = 39;
                                break;
                            }

                            shippingParam = {
                                key: RAJA_ONGKIR_API_KEY,
                                origin: '256', // malang
                                destination: address.id_city,
                                weight: weight,
                                courier: shipping_courier
                            };
                            _context11.next = 30;
                            return _superagent2.default.post("https://api.rajaongkir.com/starter/cost").send(shippingParam);

                        case 30:
                            shippingRes = _context11.sent;
                            shippingCosts = shippingRes.body.rajaongkir.results[0].costs;
                            shippingCost = shippingCosts.find(function (x) {
                                return x.service === shipping_service;
                            }).cost[0];
                            _context11.next = 35;
                            return _stored_procedure2.default.run('sp_checkout_order', [idCustomer, "'" + reference + "'", "'" + (0, _stringify2.default)(address) + "'", "'" + (0, _stringify2.default)({ courier: shipping_courier, service: shipping_service }) + "'", shippingCost.value, '1' // id_employee
                            ]);

                        case 35:
                            execStoredProcedure = _context11.sent;
                            data = execStoredProcedure.result;


                            _api3 = {
                                "status": true,
                                "data": data
                            };

                            queue.create('confirmPayment', data).delay(180 * 1000).save(); // 3 menit

                        case 39:

                            res.json(_api3);
                            _context11.next = 46;
                            break;

                        case 42:
                            _context11.prev = 42;
                            _context11.t0 = _context11["catch"](0);

                            console.log(_context11.t0);
                            res.json({ status: false, error: _context11.t0 });

                        case 46:
                        case "end":
                            return _context11.stop();
                    }
                }
            }, _callee11, _this, [[0, 42]]);
        }));

        return function (_x22, _x23) {
            return _ref12.apply(this, arguments);
        };
    }();

    this.showCategory = function () {
        var _ref13 = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee12(req, res) {
            var id, data, rawCategoryImg, categoryImg, imageDir, imageBuf, results, newData, api;
            return _regenerator2.default.wrap(function _callee12$(_context12) {
                while (1) {
                    switch (_context12.prev = _context12.next) {
                        case 0:
                            id = req.params.id;
                            _context12.next = 3;
                            return Category.findOne({
                                where: {
                                    id: id
                                }
                            });

                        case 3:
                            data = _context12.sent;
                            _context12.next = 6;
                            return FileStorage.findOne({
                                where: {
                                    tablename: 'Categories',
                                    id_reference: id
                                }
                            });

                        case 6:
                            rawCategoryImg = _context12.sent;
                            categoryImg = "";

                            if (!rawCategoryImg) {
                                _context12.next = 14;
                                break;
                            }

                            imageDir = _path2.default.resolve(__dirname).replace("dist/controllers", "");
                            _context12.next = 12;
                            return (0, _sharp2.default)(imageDir + "public/uploads/" + rawCategoryImg.filename).png().toBuffer();

                        case 12:
                            imageBuf = _context12.sent;

                            categoryImg = "data:image/png;base64," + imageBuf.toString('base64');

                        case 14:
                            results = _utils2.default.stringifyThenParse(data);
                            newData = (0, _assign2.default)({}, results, {
                                base64: categoryImg,
                                imgData: rawCategoryImg
                            });
                            api = {
                                "status": true,
                                "data": newData
                            };


                            res.json(api);

                        case 18:
                        case "end":
                            return _context12.stop();
                    }
                }
            }, _callee12, _this);
        }));

        return function (_x24, _x25) {
            return _ref13.apply(this, arguments);
        };
    }();

    this.updateMyProfile = function () {
        var _ref14 = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee13(req, res) {
            var token, _Util$decodeToken3, idCustomer, _req$body5, name, email, customer, data, newData, api;

            return _regenerator2.default.wrap(function _callee13$(_context13) {
                while (1) {
                    switch (_context13.prev = _context13.next) {
                        case 0:
                            token = _utils2.default.getTokenFromRequest(req);
                            _Util$decodeToken3 = _utils2.default.decodeToken(token), idCustomer = _Util$decodeToken3.data.id;
                            _req$body5 = req.body, name = _req$body5.name, email = _req$body5.email;
                            _context13.next = 5;
                            return Customer.findById(idCustomer);

                        case 5:
                            customer = _context13.sent;
                            _context13.next = 8;
                            return customer.update({
                                name: name,
                                email: email
                            });

                        case 8:
                            data = _context13.sent;
                            newData = _utils2.default.stringifyThenParse(data);


                            delete newData.passwd;

                            api = {
                                "status": true,
                                "data": newData
                            };


                            res.json(api);

                        case 13:
                        case "end":
                            return _context13.stop();
                    }
                }
            }, _callee13, _this);
        }));

        return function (_x26, _x27) {
            return _ref14.apply(this, arguments);
        };
    }();

    this.updateMyPassword = function () {
        var _ref15 = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee14(req, res) {
            var token, _Util$decodeToken4, idCustomer, _req$body6, old_password, new_password, confirm_new_password, api, customer, isPasswordValid, isConfirmed, data, newData;

            return _regenerator2.default.wrap(function _callee14$(_context14) {
                while (1) {
                    switch (_context14.prev = _context14.next) {
                        case 0:
                            token = _utils2.default.getTokenFromRequest(req);
                            _Util$decodeToken4 = _utils2.default.decodeToken(token), idCustomer = _Util$decodeToken4.data.id;
                            _req$body6 = req.body, old_password = _req$body6.old_password, new_password = _req$body6.new_password, confirm_new_password = _req$body6.confirm_new_password;
                            api = {
                                "status": false,
                                "msg": "Password lama Anda masih belum benar"
                            };
                            _context14.next = 6;
                            return Customer.findById(idCustomer);

                        case 6:
                            customer = _context14.sent;
                            isPasswordValid = _bcrypt2.default.compareSync(old_password, customer.passwd);

                            if (!isPasswordValid) {
                                _context14.next = 20;
                                break;
                            }

                            isConfirmed = String(new_password) === String(confirm_new_password);

                            if (!isConfirmed) {
                                _context14.next = 19;
                                break;
                            }

                            _context14.next = 13;
                            return customer.update({
                                passwd: _bcrypt2.default.hashSync(new_password, SALT_PASSWD)
                            });

                        case 13:
                            data = _context14.sent;
                            newData = _utils2.default.stringifyThenParse(data);


                            delete newData.passwd;

                            api = {
                                "status": true,
                                "data": newData
                            };
                            _context14.next = 20;
                            break;

                        case 19:
                            api = {
                                "status": false,
                                "msg": "Konfirmasi password Anda masih belum benar"
                            };

                        case 20:

                            res.json(api);

                        case 21:
                        case "end":
                            return _context14.stop();
                    }
                }
            }, _callee14, _this);
        }));

        return function (_x28, _x29) {
            return _ref15.apply(this, arguments);
        };
    }();

    this.createCategory = function () {
        var _ref16 = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee15(req, res) {
            var _req$body7, name, description, data, api;

            return _regenerator2.default.wrap(function _callee15$(_context15) {
                while (1) {
                    switch (_context15.prev = _context15.next) {
                        case 0:
                            _req$body7 = req.body, name = _req$body7.name, description = _req$body7.description;
                            _context15.next = 3;
                            return Category.create({
                                name: name,
                                slug: _utils2.default.slugify(name),
                                description: description
                            });

                        case 3:
                            data = _context15.sent;
                            api = {
                                "status": true,
                                "data": _utils2.default.stringifyThenParse(data)
                            };


                            res.json(api);

                        case 6:
                        case "end":
                            return _context15.stop();
                    }
                }
            }, _callee15, _this);
        }));

        return function (_x30, _x31) {
            return _ref16.apply(this, arguments);
        };
    }();

    this.updateCategory = function () {
        var _ref17 = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee16(req, res) {
            var _JSON$parse4, id, name, description, api, category, fileStorageFound, fileName, data;

            return _regenerator2.default.wrap(function _callee16$(_context16) {
                while (1) {
                    switch (_context16.prev = _context16.next) {
                        case 0:
                            _JSON$parse4 = JSON.parse(req.body.data), id = _JSON$parse4.id, name = _JSON$parse4.name, description = _JSON$parse4.description;
                            api = {
                                "status": false,
                                "data": "doesn't exist"
                            };
                            _context16.next = 4;
                            return Category.findById(id);

                        case 4:
                            category = _context16.sent;

                            if (!category) {
                                _context16.next = 19;
                                break;
                            }

                            _context16.next = 8;
                            return FileStorage.findOne({
                                where: {
                                    id_reference: id,
                                    tablename: 'Categories'
                                }
                            });

                        case 8:
                            fileStorageFound = _context16.sent;

                            if (!fileStorageFound) {
                                _context16.next = 13;
                                break;
                            }

                            try {
                                _fs2.default.unlinkSync("public/uploads/" + fileStorageFound.filename);
                            } catch (err) {
                                console.log(err);
                            }

                            _context16.next = 13;
                            return FileStorage.destroy({
                                where: {
                                    id_reference: id,
                                    tablename: 'Categories'
                                }
                            });

                        case 13:
                            if (!req.file) {
                                _context16.next = 17;
                                break;
                            }

                            fileName = req.file.filename;
                            _context16.next = 17;
                            return FileStorage.create({
                                tablename: "Categories",
                                filename: fileName,
                                id_reference: id
                            });

                        case 17:
                            data = category.update({
                                name: name,
                                slug: _utils2.default.slugify(name),
                                description: description
                            });


                            api = {
                                "status": true,
                                "data": _utils2.default.stringifyThenParse(data)
                            };

                        case 19:

                            res.json(api);

                        case 20:
                        case "end":
                            return _context16.stop();
                    }
                }
            }, _callee16, _this);
        }));

        return function (_x32, _x33) {
            return _ref17.apply(this, arguments);
        };
    }();

    this.deleteCategory = function () {
        var _ref18 = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee17(req, res) {
            var api;
            return _regenerator2.default.wrap(function _callee17$(_context17) {
                while (1) {
                    switch (_context17.prev = _context17.next) {
                        case 0:
                            // const { id } = req.body;

                            // let data = await Category.destroy({
                            //     where: {
                            //         id
                            //     }
                            // });

                            // const api = {
                            //     "status": true,
                            //     "data": Util.stringifyThenParse(data)
                            // };

                            api = {
                                "status": true,
                                "msg": "success"
                            };


                            res.json(api);

                        case 2:
                        case "end":
                            return _context17.stop();
                    }
                }
            }, _callee17, _this);
        }));

        return function (_x34, _x35) {
            return _ref18.apply(this, arguments);
        };
    }();

    this.bulkDeleteCategory = function () {
        var _ref19 = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee18(req, res) {
            var api;
            return _regenerator2.default.wrap(function _callee18$(_context18) {
                while (1) {
                    switch (_context18.prev = _context18.next) {
                        case 0:
                            api = {
                                "status": true,
                                "msg": "success"
                            };


                            res.json(api);

                        case 2:
                        case "end":
                            return _context18.stop();
                    }
                }
            }, _callee18, _this);
        }));

        return function (_x36, _x37) {
            return _ref19.apply(this, arguments);
        };
    }();

    this.showProductDetail = function () {
        var _ref20 = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee20(req, res) {
            var id, imageDir, data, fileStorages, productData, images, stockFound, availableQty, minQty, newProductRatingPromises, newProductRatings, api;
            return _regenerator2.default.wrap(function _callee20$(_context20) {
                while (1) {
                    switch (_context20.prev = _context20.next) {
                        case 0:
                            id = req.params.id;
                            imageDir = _path2.default.resolve(__dirname).replace("dist/controllers", "");


                            City.belongsTo(Province, { foreignKey: 'id_province', sourceKey: 'id' });
                            Province.hasMany(City, { foreignKey: 'id_province', sourceKey: 'id' });

                            Product.belongsTo(City, { foreignKey: 'origin', sourceKey: 'id' });
                            City.hasMany(Product, { foreignKey: 'origin', sourceKey: 'id' });

                            ProductRating.belongsTo(Product, { foreignKey: 'id_product', sourceKey: 'id' });
                            Product.hasMany(ProductRating, { foreignKey: 'id_product', sourceKey: 'id' });

                            Product.belongsTo(Category, { foreignKey: 'id_category', sourceKey: 'id' });
                            Category.hasMany(Product, { foreignKey: 'id_category', sourceKey: 'id' });
                            _context20.next = 12;
                            return Product.findOne({
                                where: {
                                    id: id
                                },
                                include: [{
                                    model: Category
                                }, {
                                    model: ProductRating
                                }, {
                                    model: City,
                                    include: [{
                                        model: Province
                                    }]
                                }]
                            });

                        case 12:
                            data = _context20.sent;
                            _context20.next = 15;
                            return FileStorage.findAll({
                                where: {
                                    id_reference: id,
                                    tablename: 'Products'
                                }
                            });

                        case 15:
                            fileStorages = _context20.sent;
                            productData = _utils2.default.stringifyThenParse(data);

                            productData = (0, _assign2.default)(productData, {
                                cityName: productData.City.name,
                                provinceId: productData.City.id_province,
                                provinceName: productData.City.Province.name
                            });

                            delete productData.City;

                            images = _utils2.default.stringifyThenParse(fileStorages);
                            _context20.next = 22;
                            return Stock.findOne({
                                where: {
                                    id_product: id
                                }
                            });

                        case 22:
                            stockFound = _context20.sent;
                            availableQty = 0;
                            minQty = 0;


                            if (stockFound) {
                                availableQty = stockFound.qty;
                                minQty = stockFound.min_qty;
                            }

                            newProductRatingPromises = productData.ProductRatings.map(function () {
                                var _ref21 = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee19(pr) {
                                    var prImages, profilePhoto, customer, parsedCustomer;
                                    return _regenerator2.default.wrap(function _callee19$(_context19) {
                                        while (1) {
                                            switch (_context19.prev = _context19.next) {
                                                case 0:
                                                    _context19.next = 2;
                                                    return FileStorage.findAll({
                                                        where: {
                                                            id_reference: pr.id,
                                                            tablename: 'ProductRatings'
                                                        }
                                                    });

                                                case 2:
                                                    prImages = _context19.sent;
                                                    _context19.next = 5;
                                                    return FileStorage.findOne({
                                                        where: {
                                                            tablename: 'Customers',
                                                            id_reference: pr.id_user
                                                        }
                                                    });

                                                case 5:
                                                    profilePhoto = _context19.sent;
                                                    _context19.next = 8;
                                                    return Customer.findOne({
                                                        where: {
                                                            id: pr.id_user
                                                        }
                                                    });

                                                case 8:
                                                    customer = _context19.sent;
                                                    parsedCustomer = _utils2.default.stringifyThenParse(customer);


                                                    delete parsedCustomer.passwd;

                                                    return _context19.abrupt("return", (0, _assign2.default)({}, pr, { customer: parsedCustomer, images: prImages, profilePhoto: profilePhoto }));

                                                case 12:
                                                case "end":
                                                    return _context19.stop();
                                            }
                                        }
                                    }, _callee19, _this);
                                }));

                                return function (_x40) {
                                    return _ref21.apply(this, arguments);
                                };
                            }());
                            _context20.next = 29;
                            return _promise2.default.all(newProductRatingPromises);

                        case 29:
                            newProductRatings = _context20.sent;
                            api = {
                                "status": true,
                                "data": (0, _assign2.default)({}, productData, { available_qty: availableQty, min_qty: minQty, images: images, ProductRatings: newProductRatings })
                            };


                            res.json(api);

                        case 32:
                        case "end":
                            return _context20.stop();
                    }
                }
            }, _callee20, _this);
        }));

        return function (_x38, _x39) {
            return _ref20.apply(this, arguments);
        };
    }();

    this.showProductImages = function () {
        var _ref22 = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee21(req, res) {
            var id, imageDir, data, newImages, loopIndex, images, x, imageBuf, base64Data, newData, api;
            return _regenerator2.default.wrap(function _callee21$(_context21) {
                while (1) {
                    switch (_context21.prev = _context21.next) {
                        case 0:
                            id = req.params.id;
                            imageDir = _path2.default.resolve(__dirname).replace("dist/controllers", "");
                            _context21.next = 4;
                            return FileStorage.findAll({
                                where: {
                                    id_reference: id,
                                    tablename: 'Products'
                                }
                            });

                        case 4:
                            data = _context21.sent;
                            newImages = [];
                            loopIndex = 0;
                            images = _utils2.default.stringifyThenParse(data);

                        case 8:
                            if (!(loopIndex < images.length)) {
                                _context21.next = 19;
                                break;
                            }

                            x = images[loopIndex];
                            _context21.next = 12;
                            return (0, _sharp2.default)(imageDir + "public/uploads/" + x.filename).resize(500).png().toBuffer();

                        case 12:
                            imageBuf = _context21.sent;
                            base64Data = "data:image/png;base64," + imageBuf.toString('base64');
                            newData = (0, _assign2.default)({}, x, { base64Data: base64Data });

                            newImages.push(newData);
                            loopIndex += 1;
                            _context21.next = 8;
                            break;

                        case 19:
                            api = {
                                "status": true,
                                "data": newImages
                            };


                            res.json(api);

                        case 21:
                        case "end":
                            return _context21.stop();
                    }
                }
            }, _callee21, _this);
        }));

        return function (_x41, _x42) {
            return _ref22.apply(this, arguments);
        };
    }();

    this.createProduct = function () {
        var _ref23 = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee22(req, res) {
            var paramData, id_category, name, price, description, origin, weight, available_qty, min_qty, photosValue, newFiles, productCreated, product, photos, fileStorage, data, api;
            return _regenerator2.default.wrap(function _callee22$(_context22) {
                while (1) {
                    switch (_context22.prev = _context22.next) {
                        case 0:
                            paramData = req.body;
                            id_category = paramData.id_category, name = paramData.name, price = paramData.price, description = paramData.description, origin = paramData.origin, weight = paramData.weight, available_qty = paramData.available_qty, min_qty = paramData.min_qty, photosValue = paramData.photos;
                            newFiles = [];


                            if (photosValue) {
                                newFiles = photosValue;
                            }

                            _context22.next = 6;
                            return Product.create({
                                id_category: id_category,
                                name: name,
                                slug: _utils2.default.slugify(name),
                                price: price,
                                description: description,
                                origin: origin,
                                weight: weight,
                                popular_score: 0,
                                is_active: 1,
                                primary_img: newFiles.length > 0 ? newFiles[0] : ''
                            });

                        case 6:
                            productCreated = _context22.sent;
                            product = _utils2.default.stringifyThenParse(productCreated);
                            photos = newFiles.map(function (x) {
                                var oldPath = "public/uploads/temp/" + x;
                                var newPath = "public/uploads/" + x;
                                _fs2.default.renameSync(oldPath, newPath);

                                return {
                                    tablename: "Products",
                                    filename: x,
                                    id_reference: product.id
                                };
                            });
                            _context22.next = 11;
                            return Stock.create({
                                id_product: product.id,
                                qty: available_qty,
                                min_qty: min_qty
                            });

                        case 11:
                            _context22.next = 13;
                            return FileStorage.bulkCreate(photos);

                        case 13:
                            fileStorage = _context22.sent;
                            data = (0, _assign2.default)({}, product, { images: fileStorage });
                            api = {
                                "status": true,
                                "data": data
                            };


                            res.json(api);

                        case 17:
                        case "end":
                            return _context22.stop();
                    }
                }
            }, _callee22, _this);
        }));

        return function (_x43, _x44) {
            return _ref23.apply(this, arguments);
        };
    }();

    this.updateProduct = function () {
        var _ref24 = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee23(req, res) {
            var paramData, id, id_category, name, price, description, origin, weight, min_qty, photosValue, newFiles, fileStorage, product, updateData, resProduct, stockFound, photos, data, api;
            return _regenerator2.default.wrap(function _callee23$(_context23) {
                while (1) {
                    switch (_context23.prev = _context23.next) {
                        case 0:
                            paramData = req.body;
                            id = paramData.id, id_category = paramData.id_category, name = paramData.name, price = paramData.price, description = paramData.description, origin = paramData.origin, weight = paramData.weight, min_qty = paramData.min_qty, photosValue = paramData.photos;
                            newFiles = [];


                            if (photosValue) {
                                newFiles = photosValue;
                            }

                            _context23.next = 6;
                            return FileStorage.findAll({
                                where: {
                                    id_reference: id,
                                    tablename: 'Products'
                                }
                            });

                        case 6:
                            fileStorage = _context23.sent;
                            _context23.next = 9;
                            return FileStorage.destroy({
                                where: {
                                    id_reference: id,
                                    tablename: 'Products'
                                }
                            });

                        case 9:
                            _context23.next = 11;
                            return Product.findById(id);

                        case 11:
                            product = _context23.sent;
                            _context23.next = 14;
                            return product.update({
                                id_category: id_category,
                                name: name,
                                slug: _utils2.default.slugify(name),
                                price: price,
                                description: description,
                                origin: origin,
                                weight: weight,
                                primary_img: newFiles.length > 0 ? newFiles[0] : ''
                            });

                        case 14:
                            updateData = _context23.sent;
                            resProduct = _utils2.default.stringifyThenParse(product);
                            _context23.next = 18;
                            return Stock.findOne({
                                where: {
                                    id_product: resProduct.id
                                }
                            });

                        case 18:
                            stockFound = _context23.sent;
                            _context23.next = 21;
                            return stockFound.update({
                                min_qty: min_qty
                            });

                        case 21:
                            photos = newFiles.map(function (x) {
                                try {
                                    var oldPath = "public/uploads/temp/" + x;
                                    if (_fs2.default.existsSync(oldPath)) {
                                        var newPath = "public/uploads/" + x;
                                        _fs2.default.renameSync(oldPath, newPath);
                                    }
                                } catch (err) {
                                    console.error(err);
                                }

                                return {
                                    tablename: "Products",
                                    filename: x,
                                    id_reference: resProduct.id
                                };
                            });
                            _context23.next = 24;
                            return FileStorage.bulkCreate(photos);

                        case 24:
                            fileStorage = _context23.sent;
                            data = (0, _assign2.default)({}, _utils2.default.stringifyThenParse(updateData), { images: fileStorage });
                            api = {
                                "status": true,
                                "data": data
                            };


                            res.json(api);

                        case 28:
                        case "end":
                            return _context23.stop();
                    }
                }
            }, _callee23, _this);
        }));

        return function (_x45, _x46) {
            return _ref24.apply(this, arguments);
        };
    }();

    this.deleteProduct = function () {
        var _ref25 = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee24(req, res) {
            var api;
            return _regenerator2.default.wrap(function _callee24$(_context24) {
                while (1) {
                    switch (_context24.prev = _context24.next) {
                        case 0:
                            // const {
                            //     id,
                            // } = req.body;

                            // let product = await Product.destroy({
                            //     where: {
                            //         id
                            //     }
                            // });

                            // const fileStorage = await FileStorage.findAll({
                            //     where: {
                            //         id_reference: id,
                            //         tablename: 'Products',
                            //     }
                            // });

                            // Util.stringifyThenParse(fileStorage).forEach(x => {
                            //     try {
                            //         fs.unlinkSync(`public/uploads/${x.filename}`);
                            //     } catch (err) {
                            //         console.log(err);
                            //     }
                            // });

                            // await FileStorage.destroy({
                            //     where: {
                            //         id_reference: id,
                            //         tablename: 'Products',
                            //     }
                            // });

                            // await CartDetail.destroy({
                            //     where: {
                            //         id_product: id,
                            //     }
                            // });

                            // await Wishlist.destroy({
                            //     where: {
                            //         id_product: id,
                            //     }
                            // });

                            // const api = {
                            //     "status": true,
                            //     "data": Util.stringifyThenParse(product)
                            // };

                            api = {
                                "status": true,
                                "msg": "success"
                            };


                            res.json(api);

                        case 2:
                        case "end":
                            return _context24.stop();
                    }
                }
            }, _callee24, _this);
        }));

        return function (_x47, _x48) {
            return _ref25.apply(this, arguments);
        };
    }();

    this.bulkDeleteProduct = function () {
        var _ref26 = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee25(req, res) {
            var api;
            return _regenerator2.default.wrap(function _callee25$(_context25) {
                while (1) {
                    switch (_context25.prev = _context25.next) {
                        case 0:
                            // const { ids } = req.body;
                            // const deleteIds = JSON.parse(ids);

                            // let product = await Product.destroy({
                            //     where: {
                            //         id: deleteIds
                            //     }
                            // });

                            // const fileStorage = await FileStorage.findAll({
                            //     where: {
                            //         id_reference: deleteIds,
                            //         tablename: 'Products',
                            //     }
                            // });

                            // Util.stringifyThenParse(fileStorage).forEach(x => {
                            //     try {
                            //         fs.unlinkSync(`public/uploads/${x.filename}`);
                            //     } catch (err) {
                            //         console.log(err);
                            //     }
                            // });

                            // await FileStorage.destroy({
                            //     where: {
                            //         id_reference: deleteIds,
                            //         tablename: 'Products',
                            //     }
                            // });

                            // await CartDetail.destroy({
                            //     where: {
                            //         id_product: deleteIds,
                            //     }
                            // });

                            // await Wishlist.destroy({
                            //     where: {
                            //         id_product: deleteIds,
                            //     }
                            // });

                            // const api = {
                            //     "status": true,
                            //     "data": Util.stringifyThenParse(product)
                            // };

                            api = {
                                "status": true,
                                "msg": "success"
                            };


                            res.json(api);

                        case 2:
                        case "end":
                            return _context25.stop();
                    }
                }
            }, _callee25, _this);
        }));

        return function (_x49, _x50) {
            return _ref26.apply(this, arguments);
        };
    }();

    this.showMyCarts = function () {
        var _ref27 = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee26(req, res) {
            var token, _Util$decodeToken5, idCustomer, result, resultData, totalWeight, carts, data, api;

            return _regenerator2.default.wrap(function _callee26$(_context26) {
                while (1) {
                    switch (_context26.prev = _context26.next) {
                        case 0:
                            token = _utils2.default.getTokenFromRequest(req);
                            _Util$decodeToken5 = _utils2.default.decodeToken(token), idCustomer = _Util$decodeToken5.data.id;


                            Cart.hasMany(CartDetail, { foreignKey: 'id_cart', sourceKey: 'id' });
                            CartDetail.belongsTo(Cart, { foreignKey: 'id_cart', sourceKey: 'id' });
                            Product.hasMany(CartDetail, { foreignKey: 'id_product', sourceKey: 'id' });
                            CartDetail.belongsTo(Product, { foreignKey: 'id_product', sourceKey: 'id' });
                            _context26.next = 8;
                            return Cart.findOne({
                                where: {
                                    id_customer: idCustomer
                                },
                                include: [{
                                    model: CartDetail,
                                    include: [{
                                        model: Product
                                    }]
                                }]
                            });

                        case 8:
                            result = _context26.sent;
                            resultData = _utils2.default.stringifyThenParse(result);
                            totalWeight = 0;
                            carts = [];


                            if (resultData) {
                                carts = resultData.CartDetails.map(function (x) {
                                    totalWeight = totalWeight + parseFloat(x.Product.weight, 10) * parseFloat(x.qty, 10);

                                    return {
                                        id: x.id,
                                        id_product: x.id_product,
                                        qty: parseFloat(x.qty, 10),
                                        product_name: x.Product.name,
                                        product_slug: x.Product.slug,
                                        product_price: x.Product.price,
                                        product_weight: parseFloat(x.Product.weight, 10),
                                        primary_img: x.Product.primary_img
                                    };
                                });
                            }

                            data = (0, _assign2.default)({}, resultData, { carts: carts, total_weight: totalWeight });


                            delete data.CartDetails;

                            api = {
                                "status": true,
                                "data": data
                            };


                            res.json(api);

                        case 17:
                        case "end":
                            return _context26.stop();
                    }
                }
            }, _callee26, _this);
        }));

        return function (_x51, _x52) {
            return _ref27.apply(this, arguments);
        };
    }();

    this.showMyDashboard = function () {
        var _ref28 = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee27(req, res) {
            var api;
            return _regenerator2.default.wrap(function _callee27$(_context27) {
                while (1) {
                    switch (_context27.prev = _context27.next) {
                        case 0:
                            api = {
                                "status": true,
                                "data": "coming soon"
                            };


                            res.json(api);

                        case 2:
                        case "end":
                            return _context27.stop();
                    }
                }
            }, _callee27, _this);
        }));

        return function (_x53, _x54) {
            return _ref28.apply(this, arguments);
        };
    }();

    this.showMyAccountInformation = function () {
        var _ref29 = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee28(req, res) {
            var token, _Util$decodeToken6, idCustomer, data, rawProfilePhoto, profilePhoto, imageDir, imageBuf, results, newData, api;

            return _regenerator2.default.wrap(function _callee28$(_context28) {
                while (1) {
                    switch (_context28.prev = _context28.next) {
                        case 0:
                            token = _utils2.default.getTokenFromRequest(req);
                            _Util$decodeToken6 = _utils2.default.decodeToken(token), idCustomer = _Util$decodeToken6.data.id;
                            _context28.next = 4;
                            return Customer.findOne({
                                where: {
                                    id: idCustomer
                                }
                            });

                        case 4:
                            data = _context28.sent;
                            _context28.next = 7;
                            return FileStorage.findOne({
                                where: {
                                    tablename: 'Customers',
                                    id_reference: idCustomer
                                }
                            });

                        case 7:
                            rawProfilePhoto = _context28.sent;
                            profilePhoto = "";

                            if (!rawProfilePhoto) {
                                _context28.next = 15;
                                break;
                            }

                            imageDir = _path2.default.resolve(__dirname).replace("dist/controllers", "");
                            _context28.next = 13;
                            return (0, _sharp2.default)(imageDir + "public/uploads/" + rawProfilePhoto.filename).png().toBuffer();

                        case 13:
                            imageBuf = _context28.sent;

                            profilePhoto = "data:image/png;base64," + imageBuf.toString('base64');

                        case 15:
                            results = _utils2.default.stringifyThenParse(data);


                            delete results.passwd;

                            newData = (0, _assign2.default)({}, results, {
                                profile_photo: profilePhoto
                            });
                            api = {
                                "status": true,
                                "data": newData
                            };


                            res.json(api);

                        case 20:
                        case "end":
                            return _context28.stop();
                    }
                }
            }, _callee28, _this);
        }));

        return function (_x55, _x56) {
            return _ref29.apply(this, arguments);
        };
    }();

    this.addToCart = function () {
        var _ref30 = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee29(req, res) {
            var token, _Util$decodeToken7, idCustomer, _req$body8, idProduct, qty, execStoredProcedure, data, _api4;

            return _regenerator2.default.wrap(function _callee29$(_context29) {
                while (1) {
                    switch (_context29.prev = _context29.next) {
                        case 0:
                            token = _utils2.default.getTokenFromRequest(req);
                            _Util$decodeToken7 = _utils2.default.decodeToken(token), idCustomer = _Util$decodeToken7.data.id;
                            _req$body8 = req.body, idProduct = _req$body8.id_product, qty = _req$body8.qty;
                            _context29.prev = 3;
                            _context29.next = 6;
                            return _stored_procedure2.default.run('sp_add_to_cart', [idCustomer, idProduct, qty]);

                        case 6:
                            execStoredProcedure = _context29.sent;
                            data = execStoredProcedure.result;
                            _api4 = {
                                "status": true,
                                "data": data
                            };

                            res.json(_api4);
                            _context29.next = 15;
                            break;

                        case 12:
                            _context29.prev = 12;
                            _context29.t0 = _context29["catch"](3);

                            res.json({ status: false, error: _context29.t0 });

                        case 15:
                        case "end":
                            return _context29.stop();
                    }
                }
            }, _callee29, _this, [[3, 12]]);
        }));

        return function (_x57, _x58) {
            return _ref30.apply(this, arguments);
        };
    }();

    this.updateCart = function () {
        var _ref31 = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee30(req, res) {
            var token, _Util$decodeToken8, idCustomer, _req$body9, idProduct, qty, execStoredProcedure, data, _api5;

            return _regenerator2.default.wrap(function _callee30$(_context30) {
                while (1) {
                    switch (_context30.prev = _context30.next) {
                        case 0:
                            token = _utils2.default.getTokenFromRequest(req);
                            _Util$decodeToken8 = _utils2.default.decodeToken(token), idCustomer = _Util$decodeToken8.data.id;
                            _req$body9 = req.body, idProduct = _req$body9.id_product, qty = _req$body9.qty;
                            _context30.prev = 3;
                            _context30.next = 6;
                            return _stored_procedure2.default.run('sp_update_cart', [idCustomer, idProduct, qty]);

                        case 6:
                            execStoredProcedure = _context30.sent;
                            data = execStoredProcedure.result;
                            _api5 = {
                                "status": true,
                                "data": data
                            };

                            res.json(_api5);
                            _context30.next = 15;
                            break;

                        case 12:
                            _context30.prev = 12;
                            _context30.t0 = _context30["catch"](3);

                            res.json({ status: false, error: _context30.t0 });

                        case 15:
                        case "end":
                            return _context30.stop();
                    }
                }
            }, _callee30, _this, [[3, 12]]);
        }));

        return function (_x59, _x60) {
            return _ref31.apply(this, arguments);
        };
    }();

    this.deleteCart = function () {
        var _ref32 = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee31(req, res) {
            var token, _Util$decodeToken9, idCustomer, idProduct, execStoredProcedure, data, _api6;

            return _regenerator2.default.wrap(function _callee31$(_context31) {
                while (1) {
                    switch (_context31.prev = _context31.next) {
                        case 0:
                            token = _utils2.default.getTokenFromRequest(req);
                            _Util$decodeToken9 = _utils2.default.decodeToken(token), idCustomer = _Util$decodeToken9.data.id;
                            idProduct = req.body.id_product;
                            _context31.prev = 3;
                            _context31.next = 6;
                            return _stored_procedure2.default.run('sp_delete_cart', [idCustomer, idProduct]);

                        case 6:
                            execStoredProcedure = _context31.sent;
                            data = execStoredProcedure.result;
                            _api6 = {
                                "status": true,
                                "data": data
                            };

                            res.json(_api6);
                            _context31.next = 15;
                            break;

                        case 12:
                            _context31.prev = 12;
                            _context31.t0 = _context31["catch"](3);

                            res.json({ status: false, error: _context31.t0 });

                        case 15:
                        case "end":
                            return _context31.stop();
                    }
                }
            }, _callee31, _this, [[3, 12]]);
        }));

        return function (_x61, _x62) {
            return _ref32.apply(this, arguments);
        };
    }();

    this.showAddresses = function () {
        var _ref33 = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee32(req, res) {
            var _req$query5, limit, page, search, options, calculateOptions, filter, data, calculateAmountOfData, amountOfData, api;

            return _regenerator2.default.wrap(function _callee32$(_context32) {
                while (1) {
                    switch (_context32.prev = _context32.next) {
                        case 0:
                            /**
                             * mandatory param: limit, page
                            */
                            _req$query5 = req.query, limit = _req$query5.limit, page = _req$query5.page, search = _req$query5.filterText;
                            options = {
                                offset: (page - 1) * limit,
                                limit: (0, _parseInt2.default)(limit, 10),
                                order: [['createdAt', 'DESC']],
                                include: [{
                                    model: Customer
                                }]
                            };
                            calculateOptions = {
                                attributes: [[_db2.default.fn('COUNT', _db2.default.col('id')), 'total']],
                                raw: true
                            };


                            if (search) {
                                filter = {
                                    alias: (0, _defineProperty3.default)({}, Op.regexp, "(" + search + ")")
                                };

                                options = (0, _assign2.default)({}, options, {
                                    where: options.where ? (0, _assign2.default)({}, options.where, filter) : filter
                                });
                                calculateOptions = (0, _assign2.default)({}, calculateOptions, {
                                    where: calculateOptions.where ? (0, _assign2.default)({}, calculateOptions.where, filter) : filter
                                });
                            }

                            Customer.hasMany(Address, { foreignKey: 'id_customer', sourceKey: 'id' });
                            Address.belongsTo(Customer, { foreignKey: 'id_customer', sourceKey: 'id' });
                            _context32.next = 8;
                            return Address.findAll(options);

                        case 8:
                            data = _context32.sent;
                            _context32.next = 11;
                            return Address.findOne(calculateOptions);

                        case 11:
                            calculateAmountOfData = _context32.sent;
                            amountOfData = calculateAmountOfData.total;
                            api = {
                                "status": true,
                                "data": data,
                                "page_total": Math.ceil(amountOfData / limit)
                            };


                            res.json(api);

                        case 15:
                        case "end":
                            return _context32.stop();
                    }
                }
            }, _callee32, _this);
        }));

        return function (_x63, _x64) {
            return _ref33.apply(this, arguments);
        };
    }();

    this.showAddress = function () {
        var _ref34 = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee33(req, res) {
            var id, data, api;
            return _regenerator2.default.wrap(function _callee33$(_context33) {
                while (1) {
                    switch (_context33.prev = _context33.next) {
                        case 0:
                            id = req.params.id;


                            Customer.hasMany(Address, { foreignKey: 'id_customer', sourceKey: 'id' });
                            Address.belongsTo(Customer, { foreignKey: 'id_customer', sourceKey: 'id' });
                            City.hasMany(Address, { foreignKey: 'id_city', sourceKey: 'id' });
                            Address.belongsTo(City, { foreignKey: 'id_city', sourceKey: 'id' });
                            _context33.next = 7;
                            return Address.findOne({
                                where: {
                                    id: id
                                },
                                include: [{
                                    model: City
                                }, {
                                    model: Customer
                                }]
                            });

                        case 7:
                            data = _context33.sent;
                            api = {
                                "status": true,
                                "data": data
                            };


                            res.json(api);

                        case 10:
                        case "end":
                            return _context33.stop();
                    }
                }
            }, _callee33, _this);
        }));

        return function (_x65, _x66) {
            return _ref34.apply(this, arguments);
        };
    }();

    this.createAddress = function () {
        var _ref35 = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee34(req, res) {
            var token, _Util$decodeToken10, _Util$decodeToken10$d, idCustomer, type, _req$body10, id_city, id_customer, alias, receiver_name, address, sub_district, id_postal_code, postcode, phone, data, calculateOptions, calculateAmountOfData, isFirstTime, options, addressData, api;

            return _regenerator2.default.wrap(function _callee34$(_context34) {
                while (1) {
                    switch (_context34.prev = _context34.next) {
                        case 0:
                            token = _utils2.default.getTokenFromRequest(req);
                            _Util$decodeToken10 = _utils2.default.decodeToken(token), _Util$decodeToken10$d = _Util$decodeToken10.data, idCustomer = _Util$decodeToken10$d.id, type = _Util$decodeToken10$d.type;
                            _req$body10 = req.body, id_city = _req$body10.cityId, id_customer = _req$body10.id_customer, alias = _req$body10.alias, receiver_name = _req$body10.receiver_name, address = _req$body10.address, sub_district = _req$body10.sub_district, id_postal_code = _req$body10.id_postal_code, postcode = _req$body10.postcode, phone = _req$body10.phone;
                            data = void 0;

                            if (!(type === 'user')) {
                                _context34.next = 10;
                                break;
                            }

                            _context34.next = 7;
                            return Address.create({
                                id_city: id_city,
                                id_customer: idCustomer,
                                alias: alias,
                                receiver_name: receiver_name,
                                address: address,
                                sub_district: sub_district,
                                id_postal_code: id_postal_code,
                                postcode: postcode,
                                phone: phone,
                                is_default: false
                            });

                        case 7:
                            data = _context34.sent;
                            _context34.next = 13;
                            break;

                        case 10:
                            _context34.next = 12;
                            return Address.create({
                                id_city: id_city,
                                id_customer: id_customer,
                                alias: alias,
                                receiver_name: receiver_name,
                                address: address,
                                sub_district: sub_district,
                                id_postal_code: id_postal_code,
                                postcode: postcode,
                                phone: phone,
                                is_default: false
                            });

                        case 12:
                            data = _context34.sent;

                        case 13:
                            calculateOptions = {
                                where: {
                                    id_customer: idCustomer
                                },
                                attributes: [[_db2.default.fn('COUNT', _db2.default.col('id')), 'total']],
                                raw: true
                            };
                            _context34.next = 16;
                            return Address.findOne(calculateOptions);

                        case 16:
                            calculateAmountOfData = _context34.sent;
                            isFirstTime = calculateAmountOfData.total === 1;

                            if (!isFirstTime) {
                                _context34.next = 26;
                                break;
                            }

                            options = {
                                where: {
                                    id_customer: idCustomer
                                }
                            };
                            _context34.next = 22;
                            return Address.findOne(options);

                        case 22:
                            addressData = _context34.sent;
                            _context34.next = 25;
                            return addressData.update({
                                is_default: true
                            });

                        case 25:
                            data = _context34.sent;

                        case 26:
                            api = {
                                "status": true,
                                "data": _utils2.default.stringifyThenParse(data)
                            };


                            res.json(api);

                        case 28:
                        case "end":
                            return _context34.stop();
                    }
                }
            }, _callee34, _this);
        }));

        return function (_x67, _x68) {
            return _ref35.apply(this, arguments);
        };
    }();

    this.updateAddress = function () {
        var _ref36 = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee35(req, res) {
            var token, _Util$decodeToken11, _Util$decodeToken11$d, idCustomer, type, _req$body11, id, id_city, id_customer, alias, receiver_name, address, sub_district, id_postal_code, postcode, phone, data, addressData, options, api;

            return _regenerator2.default.wrap(function _callee35$(_context35) {
                while (1) {
                    switch (_context35.prev = _context35.next) {
                        case 0:
                            token = _utils2.default.getTokenFromRequest(req);
                            _Util$decodeToken11 = _utils2.default.decodeToken(token), _Util$decodeToken11$d = _Util$decodeToken11.data, idCustomer = _Util$decodeToken11$d.id, type = _Util$decodeToken11$d.type;
                            _req$body11 = req.body, id = _req$body11.id, id_city = _req$body11.cityId, id_customer = _req$body11.id_customer, alias = _req$body11.alias, receiver_name = _req$body11.receiver_name, address = _req$body11.address, sub_district = _req$body11.sub_district, id_postal_code = _req$body11.id_postal_code, postcode = _req$body11.postcode, phone = _req$body11.phone;
                            data = { message: 'data tidak ditemukan' }, addressData = void 0, options = {
                                where: {
                                    id: id
                                }
                            };

                            if (!(type === 'user')) {
                                _context35.next = 15;
                                break;
                            }

                            options = {
                                where: {
                                    id: id,
                                    id_customer: idCustomer
                                }
                            };
                            _context35.next = 8;
                            return Address.findOne(options);

                        case 8:
                            addressData = _context35.sent;

                            if (!addressData) {
                                _context35.next = 13;
                                break;
                            }

                            _context35.next = 12;
                            return addressData.update({
                                id_city: id_city,
                                id_customer: idCustomer,
                                alias: alias,
                                receiver_name: receiver_name,
                                address: address,
                                sub_district: sub_district,
                                id_postal_code: id_postal_code,
                                postcode: postcode,
                                phone: phone,
                                is_default: false
                            });

                        case 12:
                            data = _context35.sent;

                        case 13:
                            _context35.next = 21;
                            break;

                        case 15:
                            _context35.next = 17;
                            return Address.findOne(options);

                        case 17:
                            addressData = _context35.sent;
                            _context35.next = 20;
                            return addressData.update({
                                id_city: id_city,
                                id_customer: id_customer,
                                alias: alias,
                                receiver_name: receiver_name,
                                address: address,
                                sub_district: sub_district,
                                id_postal_code: id_postal_code,
                                postcode: postcode,
                                phone: phone,
                                is_default: false
                            });

                        case 20:
                            data = _context35.sent;

                        case 21:
                            api = {
                                "status": true,
                                "data": _utils2.default.stringifyThenParse(data)
                            };


                            res.json(api);

                        case 23:
                        case "end":
                            return _context35.stop();
                    }
                }
            }, _callee35, _this);
        }));

        return function (_x69, _x70) {
            return _ref36.apply(this, arguments);
        };
    }();

    this.deleteAddress = function () {
        var _ref37 = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee36(req, res) {
            var token, _Util$decodeToken12, _Util$decodeToken12$d, idCustomer, type, id, data, api;

            return _regenerator2.default.wrap(function _callee36$(_context36) {
                while (1) {
                    switch (_context36.prev = _context36.next) {
                        case 0:
                            token = _utils2.default.getTokenFromRequest(req);
                            _Util$decodeToken12 = _utils2.default.decodeToken(token), _Util$decodeToken12$d = _Util$decodeToken12.data, idCustomer = _Util$decodeToken12$d.id, type = _Util$decodeToken12$d.type;
                            id = req.body.id;
                            data = null;

                            if (!(type === 'user')) {
                                _context36.next = 10;
                                break;
                            }

                            _context36.next = 7;
                            return Address.destroy({
                                where: {
                                    id: id,
                                    id_customer: idCustomer
                                }
                            });

                        case 7:
                            data = _context36.sent;
                            _context36.next = 13;
                            break;

                        case 10:
                            _context36.next = 12;
                            return Address.destroy({
                                where: {
                                    id: id
                                }
                            });

                        case 12:
                            data = _context36.sent;

                        case 13:
                            api = {
                                "status": true,
                                data: data,
                                "msg": "success"
                            };


                            res.json(api);

                        case 15:
                        case "end":
                            return _context36.stop();
                    }
                }
            }, _callee36, _this);
        }));

        return function (_x71, _x72) {
            return _ref37.apply(this, arguments);
        };
    }();

    this.bulkDeleteAddress = function () {
        var _ref38 = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee37(req, res) {
            var api;
            return _regenerator2.default.wrap(function _callee37$(_context37) {
                while (1) {
                    switch (_context37.prev = _context37.next) {
                        case 0:
                            api = {
                                "status": true,
                                "msg": "success"
                            };


                            res.json(api);

                        case 2:
                        case "end":
                            return _context37.stop();
                    }
                }
            }, _callee37, _this);
        }));

        return function (_x73, _x74) {
            return _ref38.apply(this, arguments);
        };
    }();

    this.setDefaultAddress = function () {
        var _ref39 = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee38(req, res) {
            var token, _Util$decodeToken13, idCustomer, id_address, execStoredProcedure, data, api;

            return _regenerator2.default.wrap(function _callee38$(_context38) {
                while (1) {
                    switch (_context38.prev = _context38.next) {
                        case 0:
                            token = _utils2.default.getTokenFromRequest(req);
                            _Util$decodeToken13 = _utils2.default.decodeToken(token), idCustomer = _Util$decodeToken13.data.id;
                            id_address = req.body.id_address;
                            _context38.next = 5;
                            return _stored_procedure2.default.run('sp_set_default_address', [idCustomer, id_address]);

                        case 5:
                            execStoredProcedure = _context38.sent;
                            data = execStoredProcedure.result;
                            api = {
                                "status": true,
                                "data": data
                            };


                            res.json(api);

                        case 9:
                        case "end":
                            return _context38.stop();
                    }
                }
            }, _callee38, _this);
        }));

        return function (_x75, _x76) {
            return _ref39.apply(this, arguments);
        };
    }();

    this.showMyAddresses = function () {
        var _ref40 = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee39(req, res) {
            var token, _Util$decodeToken14, idCustomer, _req$query6, page, search, limit, options, calculateOptions, filter, data, calculateAmountOfData, amountOfData, results, api;

            return _regenerator2.default.wrap(function _callee39$(_context39) {
                while (1) {
                    switch (_context39.prev = _context39.next) {
                        case 0:
                            /**
                             * mandatory param: page
                            */
                            token = _utils2.default.getTokenFromRequest(req);
                            _Util$decodeToken14 = _utils2.default.decodeToken(token), idCustomer = _Util$decodeToken14.data.id;
                            _req$query6 = req.query, page = _req$query6.page, search = _req$query6.search;
                            limit = 5;
                            options = {
                                where: {
                                    id_customer: idCustomer
                                },
                                offset: (page - 1) * limit,
                                limit: (0, _parseInt2.default)(limit, 10),
                                order: [['is_default', 'DESC']],
                                include: [{
                                    model: PostalCode
                                }, {
                                    model: City,
                                    include: [{
                                        model: Province
                                    }]
                                }]
                            };
                            calculateOptions = {
                                where: {
                                    id_customer: idCustomer
                                },
                                attributes: [[_db2.default.fn('COUNT', _db2.default.col('id')), 'total']],
                                raw: true
                            };


                            if (search) {
                                filter = {
                                    address: (0, _defineProperty3.default)({}, Op.regexp, "(" + search + ")")
                                };

                                options = (0, _assign2.default)({}, options, {
                                    where: options.where ? (0, _assign2.default)({}, options.where, filter) : filter
                                });
                                calculateOptions = (0, _assign2.default)({}, calculateOptions, {
                                    where: calculateOptions.where ? (0, _assign2.default)({}, calculateOptions.where, filter) : filter
                                });
                            }

                            Address.belongsTo(PostalCode, { foreignKey: 'id_postal_code', sourceKey: 'id' });
                            PostalCode.hasMany(Address, { foreignKey: 'id_postal_code', sourceKey: 'id' });
                            City.belongsTo(Province, { foreignKey: 'id_province', sourceKey: 'id' });
                            Province.hasMany(City, { foreignKey: 'id_province', sourceKey: 'id' });
                            Address.belongsTo(City, { foreignKey: 'id_city', sourceKey: 'id' });
                            City.hasMany(Address, { foreignKey: 'id_city', sourceKey: 'id' });
                            _context39.next = 15;
                            return Address.findAll(options);

                        case 15:
                            data = _context39.sent;
                            _context39.next = 18;
                            return Address.findOne(calculateOptions);

                        case 18:
                            calculateAmountOfData = _context39.sent;
                            amountOfData = calculateAmountOfData.total;
                            results = _utils2.default.stringifyThenParse(data).map(function (x) {
                                var retval = (0, _assign2.default)({}, x, {
                                    city_name: x.City.name,
                                    province_name: x.City.Province.name,
                                    id_province: x.City.id_province
                                });

                                delete retval.City;

                                return retval;
                            });
                            api = {
                                "status": true,
                                "data": results,
                                "page_total": Math.ceil(amountOfData / limit)
                            };


                            res.json(api);

                        case 23:
                        case "end":
                            return _context39.stop();
                    }
                }
            }, _callee39, _this);
        }));

        return function (_x77, _x78) {
            return _ref40.apply(this, arguments);
        };
    }();

    this.showMyAddress = function () {
        var _ref41 = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee40(req, res) {
            var token, _Util$decodeToken15, idCustomer, id, data, x, newData, api;

            return _regenerator2.default.wrap(function _callee40$(_context40) {
                while (1) {
                    switch (_context40.prev = _context40.next) {
                        case 0:
                            token = _utils2.default.getTokenFromRequest(req);
                            _Util$decodeToken15 = _utils2.default.decodeToken(token), idCustomer = _Util$decodeToken15.data.id;
                            id = req.params.id;

                            Address.belongsTo(PostalCode, { foreignKey: 'id_postal_code', sourceKey: 'id' });
                            PostalCode.hasMany(Address, { foreignKey: 'id_postal_code', sourceKey: 'id' });
                            City.belongsTo(Province, { foreignKey: 'id_province', sourceKey: 'id' });
                            Province.hasMany(City, { foreignKey: 'id_province', sourceKey: 'id' });
                            Address.belongsTo(City, { foreignKey: 'id_city', sourceKey: 'id' });
                            City.hasMany(Address, { foreignKey: 'id_city', sourceKey: 'id' });
                            _context40.next = 11;
                            return Address.findOne({
                                where: {
                                    id: id,
                                    id_customer: idCustomer
                                },
                                include: [{
                                    model: PostalCode
                                }, {
                                    model: City,
                                    include: [{
                                        model: Province
                                    }]
                                }]
                            });

                        case 11:
                            data = _context40.sent;
                            x = _utils2.default.stringifyThenParse(data);
                            newData = (0, _assign2.default)({}, x, {
                                city_name: x.City.name,
                                id_province: x.City.Province.id,
                                province_name: x.City.Province.name
                            });


                            delete newData.City;

                            api = {
                                "status": true,
                                "data": newData
                            };


                            res.json(api);

                        case 17:
                        case "end":
                            return _context40.stop();
                    }
                }
            }, _callee40, _this);
        }));

        return function (_x79, _x80) {
            return _ref41.apply(this, arguments);
        };
    }();

    this.showMyOrders = function () {
        var _ref42 = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee43(req, res) {
            var imageDir, token, _Util$decodeToken16, idCustomer, _req$query7, limit, page, search, options, calculateOptions, filter, data, calculateAmountOfData, amountOfData, newData, loopIndex, formattedData, x, newProductRatingPromises, newProductRatings, formatData, api;

            return _regenerator2.default.wrap(function _callee43$(_context43) {
                while (1) {
                    switch (_context43.prev = _context43.next) {
                        case 0:
                            imageDir = _path2.default.resolve(__dirname).replace("dist/controllers", "");
                            token = _utils2.default.getTokenFromRequest(req);
                            _Util$decodeToken16 = _utils2.default.decodeToken(token), idCustomer = _Util$decodeToken16.data.id;
                            _req$query7 = req.query, limit = _req$query7.limit, page = _req$query7.page, search = _req$query7.filterText;
                            options = {
                                offset: (page - 1) * limit,
                                limit: (0, _parseInt2.default)(limit, 10),
                                order: [['createdAt', 'DESC']],
                                include: [{
                                    model: Customer
                                }, {
                                    model: OrderDetail,
                                    include: [Product]
                                }, {
                                    model: ProductRating
                                }],
                                where: {
                                    id_customer: idCustomer
                                }
                            };
                            calculateOptions = {
                                attributes: [[_db2.default.fn('COUNT', _db2.default.col('id')), 'total']],
                                raw: true,
                                where: {
                                    id_customer: idCustomer
                                }
                            };


                            if (search) {
                                filter = {
                                    reference: (0, _defineProperty3.default)({}, Op.regexp, "(" + search + ")")
                                };

                                options = (0, _assign2.default)({}, options, {
                                    where: options.where ? (0, _assign2.default)({}, options.where, filter) : filter
                                });
                                calculateOptions = (0, _assign2.default)({}, calculateOptions, {
                                    where: calculateOptions.where ? (0, _assign2.default)({}, calculateOptions.where, filter) : filter
                                });
                            }

                            Order.hasMany(ProductRating, { foreignKey: 'id_order', sourceKey: 'id' });
                            ProductRating.belongsTo(Order, { foreignKey: 'id_order', sourceKey: 'id' });
                            Customer.hasMany(Order, { foreignKey: 'id_customer', sourceKey: 'id' });
                            Order.belongsTo(Customer, { foreignKey: 'id_customer', sourceKey: 'id' });
                            Order.hasMany(OrderDetail, { foreignKey: 'id_order', sourceKey: 'id' });
                            OrderDetail.belongsTo(Order, { foreignKey: 'id_order', sourceKey: 'id' });
                            Product.hasOne(OrderDetail, { foreignKey: 'id_product', sourceKey: 'id' });
                            OrderDetail.belongsTo(Product, { foreignKey: 'id_product', sourceKey: 'id' });
                            _context43.next = 17;
                            return Order.findAll(options);

                        case 17:
                            data = _context43.sent;
                            _context43.next = 20;
                            return Order.findOne(calculateOptions);

                        case 20:
                            calculateAmountOfData = _context43.sent;
                            amountOfData = calculateAmountOfData.total;
                            newData = _utils2.default.stringifyThenParse(data);
                            loopIndex = 0;
                            formattedData = [];

                        case 25:
                            if (!(loopIndex < newData.length)) {
                                _context43.next = 36;
                                break;
                            }

                            x = newData[loopIndex];
                            newProductRatingPromises = x.ProductRatings.map(function () {
                                var _ref43 = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee42(pr) {
                                    var prImages, prImagePromises, prImageData;
                                    return _regenerator2.default.wrap(function _callee42$(_context42) {
                                        while (1) {
                                            switch (_context42.prev = _context42.next) {
                                                case 0:
                                                    _context42.next = 2;
                                                    return FileStorage.findAll({
                                                        where: {
                                                            id_reference: pr.id,
                                                            tablename: 'ProductRatings'
                                                        }
                                                    });

                                                case 2:
                                                    prImages = _context42.sent;
                                                    prImagePromises = prImages.map(function () {
                                                        var _ref44 = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee41(img) {
                                                            var imageBuf, base64Data, newPrImageData;
                                                            return _regenerator2.default.wrap(function _callee41$(_context41) {
                                                                while (1) {
                                                                    switch (_context41.prev = _context41.next) {
                                                                        case 0:
                                                                            _context41.next = 2;
                                                                            return (0, _sharp2.default)(imageDir + "public/uploads/" + img.filename).resize(500).png().toBuffer();

                                                                        case 2:
                                                                            imageBuf = _context41.sent;
                                                                            base64Data = "data:image/png;base64," + imageBuf.toString('base64');
                                                                            newPrImageData = (0, _assign2.default)({}, _utils2.default.stringifyThenParse(img), { base64Data: base64Data });
                                                                            return _context41.abrupt("return", newPrImageData);

                                                                        case 6:
                                                                        case "end":
                                                                            return _context41.stop();
                                                                    }
                                                                }
                                                            }, _callee41, _this);
                                                        }));

                                                        return function (_x84) {
                                                            return _ref44.apply(this, arguments);
                                                        };
                                                    }());
                                                    _context42.next = 6;
                                                    return _promise2.default.all(prImagePromises);

                                                case 6:
                                                    prImageData = _context42.sent;
                                                    return _context42.abrupt("return", (0, _assign2.default)({}, pr, { images: prImageData }));

                                                case 8:
                                                case "end":
                                                    return _context42.stop();
                                            }
                                        }
                                    }, _callee42, _this);
                                }));

                                return function (_x83) {
                                    return _ref43.apply(this, arguments);
                                };
                            }());
                            _context43.next = 30;
                            return _promise2.default.all(newProductRatingPromises);

                        case 30:
                            newProductRatings = _context43.sent;
                            formatData = (0, _assign2.default)({}, x, {
                                ProductRatings: newProductRatings
                            });


                            formattedData.push(formatData);

                            loopIndex += 1;
                            _context43.next = 25;
                            break;

                        case 36:
                            api = {
                                "status": true,
                                "data": formattedData,
                                "page_total": Math.ceil(amountOfData / limit)
                            };


                            res.json(api);

                        case 38:
                        case "end":
                            return _context43.stop();
                    }
                }
            }, _callee43, _this);
        }));

        return function (_x81, _x82) {
            return _ref42.apply(this, arguments);
        };
    }();

    this.showMyOrderDetail = function () {
        var _ref45 = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee44(req, res) {
            var token, _Util$decodeToken17, idCustomer, id, data, x, parseAddress, cityData, provinceData, orderHistoryData, parsedOrderHistoryData, newOrderHistory, orderDetailData, newData, api;

            return _regenerator2.default.wrap(function _callee44$(_context44) {
                while (1) {
                    switch (_context44.prev = _context44.next) {
                        case 0:
                            token = _utils2.default.getTokenFromRequest(req);
                            _Util$decodeToken17 = _utils2.default.decodeToken(token), idCustomer = _Util$decodeToken17.data.id;
                            id = req.params.id;
                            _context44.next = 5;
                            return Order.findOne({
                                where: {
                                    id: id,
                                    id_customer: idCustomer
                                }
                            });

                        case 5:
                            data = _context44.sent;
                            x = _utils2.default.stringifyThenParse(data);
                            parseAddress = JSON.parse(x.address_delivery);
                            _context44.next = 10;
                            return City.findOne({
                                where: {
                                    id: parseAddress.id_city
                                }
                            });

                        case 10:
                            cityData = _context44.sent;
                            _context44.next = 13;
                            return Province.findOne({
                                where: {
                                    id: cityData.id_province
                                }
                            });

                        case 13:
                            provinceData = _context44.sent;
                            _context44.next = 16;
                            return OrderHistory.findAll({
                                where: {
                                    id_order: id
                                },
                                order: [['createdAt', 'DESC']]
                            });

                        case 16:
                            orderHistoryData = _context44.sent;
                            parsedOrderHistoryData = _utils2.default.stringifyThenParse(orderHistoryData);
                            newOrderHistory = parsedOrderHistoryData.map(function (oh) {
                                return (0, _assign2.default)({}, oh, { order_state_text: getOrderStateName(oh.order_state) });
                            });


                            Product.hasOne(OrderDetail, { foreignKey: 'id_product', sourceKey: 'id' });
                            OrderDetail.belongsTo(Product, { foreignKey: 'id_product', sourceKey: 'id' });
                            _context44.next = 23;
                            return OrderDetail.findAll({
                                include: [Product],
                                where: {
                                    id_order: id
                                },
                                order: [['product_name', 'DESC']]
                            });

                        case 23:
                            orderDetailData = _context44.sent;
                            newData = (0, _assign2.default)({}, x, {
                                address_delivery_text: parseAddress.address + " " + cityData.name + " " + provinceData.name,
                                postcode: parseAddress.postcode,
                                receiver_name: parseAddress.receiver_name,
                                order_history: newOrderHistory,
                                order_state_text: getOrderStateName(x.order_state),
                                orderDetailData: orderDetailData
                            });
                            api = {
                                "status": true,
                                "data": newData
                            };


                            res.json(api);

                        case 27:
                        case "end":
                            return _context44.stop();
                    }
                }
            }, _callee44, _this);
        }));

        return function (_x85, _x86) {
            return _ref45.apply(this, arguments);
        };
    }();

    this.showMyWishlists = function () {
        var _ref46 = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee45(req, res) {
            var token, _Util$decodeToken18, idCustomer, page, limit, options, calculateOptions, results, calculateAmountOfData, amountOfData, api;

            return _regenerator2.default.wrap(function _callee45$(_context45) {
                while (1) {
                    switch (_context45.prev = _context45.next) {
                        case 0:
                            token = _utils2.default.getTokenFromRequest(req);
                            _Util$decodeToken18 = _utils2.default.decodeToken(token), idCustomer = _Util$decodeToken18.data.id;
                            page = req.query.page;
                            limit = 10;
                            options = {
                                where: {
                                    id_customer: idCustomer
                                },
                                offset: (page - 1) * limit,
                                limit: (0, _parseInt2.default)(limit, 10),
                                order: [['createdAt', 'DESC']],
                                include: [{
                                    model: Product
                                }]
                            };
                            calculateOptions = {
                                where: {
                                    id_customer: idCustomer
                                },
                                attributes: [[_db2.default.fn('COUNT', _db2.default.col('id')), 'total']],
                                raw: true
                            };


                            Product.hasMany(Wishlist, { foreignKey: 'id_product', sourceKey: 'id' });
                            Wishlist.belongsTo(Product, { foreignKey: 'id_product', sourceKey: 'id' });
                            _context45.next = 10;
                            return Wishlist.findAll(options);

                        case 10:
                            results = _context45.sent;
                            _context45.next = 13;
                            return Wishlist.findOne(calculateOptions);

                        case 13:
                            calculateAmountOfData = _context45.sent;
                            amountOfData = calculateAmountOfData.total;
                            api = {
                                "status": true,
                                "data": _utils2.default.stringifyThenParse(results),
                                "page_total": Math.ceil(amountOfData / limit)
                            };


                            res.json(api);

                        case 17:
                        case "end":
                            return _context45.stop();
                    }
                }
            }, _callee45, _this);
        }));

        return function (_x87, _x88) {
            return _ref46.apply(this, arguments);
        };
    }();

    this.addToWishlist = function () {
        var _ref47 = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee46(req, res) {
            var token, _Util$decodeToken19, idCustomer, idProduct, execStoredProcedure, data, api;

            return _regenerator2.default.wrap(function _callee46$(_context46) {
                while (1) {
                    switch (_context46.prev = _context46.next) {
                        case 0:
                            token = _utils2.default.getTokenFromRequest(req);
                            _Util$decodeToken19 = _utils2.default.decodeToken(token), idCustomer = _Util$decodeToken19.data.id;
                            idProduct = req.body.id_product;
                            _context46.next = 5;
                            return _stored_procedure2.default.run('sp_save_wishlist', [idCustomer, idProduct]);

                        case 5:
                            execStoredProcedure = _context46.sent;
                            data = execStoredProcedure.result;
                            api = {
                                "status": true,
                                "data": data
                            };


                            res.json(api);

                        case 9:
                        case "end":
                            return _context46.stop();
                    }
                }
            }, _callee46, _this);
        }));

        return function (_x89, _x90) {
            return _ref47.apply(this, arguments);
        };
    }();

    this.showOrders = function () {
        var _ref48 = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee47(req, res) {
            var _req$query8, limit, page, search, options, calculateOptions, filter, data, calculateAmountOfData, amountOfData, api;

            return _regenerator2.default.wrap(function _callee47$(_context47) {
                while (1) {
                    switch (_context47.prev = _context47.next) {
                        case 0:
                            /**
                             * mandatory param: limit, page
                            */
                            _req$query8 = req.query, limit = _req$query8.limit, page = _req$query8.page, search = _req$query8.filterText;
                            options = {
                                offset: (page - 1) * limit,
                                limit: (0, _parseInt2.default)(limit, 10),
                                order: [['createdAt', 'DESC']],
                                include: [{
                                    model: Customer
                                }]
                            };
                            calculateOptions = {
                                attributes: [[_db2.default.fn('COUNT', _db2.default.col('id')), 'total']],
                                raw: true
                            };


                            if (search) {
                                filter = {
                                    reference: (0, _defineProperty3.default)({}, Op.regexp, "(" + search + ")")
                                };

                                options = (0, _assign2.default)({}, options, {
                                    where: options.where ? (0, _assign2.default)({}, options.where, filter) : filter
                                });
                                calculateOptions = (0, _assign2.default)({}, calculateOptions, {
                                    where: calculateOptions.where ? (0, _assign2.default)({}, calculateOptions.where, filter) : filter
                                });
                            }

                            Customer.hasMany(Order, { foreignKey: 'id_customer', sourceKey: 'id' });
                            Order.belongsTo(Customer, { foreignKey: 'id_customer', sourceKey: 'id' });
                            _context47.next = 8;
                            return Order.findAll(options);

                        case 8:
                            data = _context47.sent;
                            _context47.next = 11;
                            return Order.findOne(calculateOptions);

                        case 11:
                            calculateAmountOfData = _context47.sent;
                            amountOfData = calculateAmountOfData.total;
                            api = {
                                "status": true,
                                "data": data,
                                "page_total": Math.ceil(amountOfData / limit)
                            };


                            res.json(api);

                        case 15:
                        case "end":
                            return _context47.stop();
                    }
                }
            }, _callee47, _this);
        }));

        return function (_x91, _x92) {
            return _ref48.apply(this, arguments);
        };
    }();

    this.showOrderDetail = function () {
        var _ref49 = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee48(req, res) {
            var id, data, x, parseAddress, cityData, provinceData, orderHistoryData, parsedOrderHistoryData, newOrderHistory, orderDetailData, newData, api;
            return _regenerator2.default.wrap(function _callee48$(_context48) {
                while (1) {
                    switch (_context48.prev = _context48.next) {
                        case 0:
                            id = req.params.id;
                            _context48.next = 3;
                            return Order.findOne({
                                where: {
                                    id: id
                                }
                            });

                        case 3:
                            data = _context48.sent;
                            x = _utils2.default.stringifyThenParse(data);
                            parseAddress = JSON.parse(x.address_delivery);
                            _context48.next = 8;
                            return City.findOne({
                                where: {
                                    id: parseAddress.id_city
                                }
                            });

                        case 8:
                            cityData = _context48.sent;
                            _context48.next = 11;
                            return Province.findOne({
                                where: {
                                    id: cityData.id_province
                                }
                            });

                        case 11:
                            provinceData = _context48.sent;
                            _context48.next = 14;
                            return OrderHistory.findAll({
                                where: {
                                    id_order: id
                                },
                                order: [['createdAt', 'DESC']]
                            });

                        case 14:
                            orderHistoryData = _context48.sent;
                            parsedOrderHistoryData = _utils2.default.stringifyThenParse(orderHistoryData);
                            newOrderHistory = parsedOrderHistoryData.map(function (oh) {
                                return (0, _assign2.default)({}, oh, { order_state_text: getOrderStateName(oh.order_state) });
                            });
                            _context48.next = 19;
                            return OrderDetail.findAll({
                                where: {
                                    id_order: id
                                },
                                order: [['product_name', 'DESC']]
                            });

                        case 19:
                            orderDetailData = _context48.sent;
                            newData = (0, _assign2.default)({}, x, {
                                address_delivery_text: parseAddress.address + " " + cityData.name + " " + provinceData.name,
                                postcode: parseAddress.postcode,
                                receiver_name: parseAddress.receiver_name,
                                order_history: newOrderHistory,
                                order_state_text: getOrderStateName(x.order_state),
                                orderDetailData: orderDetailData
                            });
                            api = {
                                "status": true,
                                "data": newData
                            };


                            res.json(api);

                        case 23:
                        case "end":
                            return _context48.stop();
                    }
                }
            }, _callee48, _this);
        }));

        return function (_x93, _x94) {
            return _ref49.apply(this, arguments);
        };
    }();

    this.checkMyOrder = function () {
        var _ref50 = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee49(req, res) {
            var api;
            return _regenerator2.default.wrap(function _callee49$(_context49) {
                while (1) {
                    switch (_context49.prev = _context49.next) {
                        case 0:
                            api = {
                                "status": true,
                                "data": []
                            };


                            res.json(api);

                        case 2:
                        case "end":
                            return _context49.stop();
                    }
                }
            }, _callee49, _this);
        }));

        return function (_x95, _x96) {
            return _ref50.apply(this, arguments);
        };
    }();

    this.showMyReviews = function () {
        var _ref51 = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee50(req, res) {
            var api;
            return _regenerator2.default.wrap(function _callee50$(_context50) {
                while (1) {
                    switch (_context50.prev = _context50.next) {
                        case 0:
                            api = {
                                "status": true,
                                "data": []
                            };


                            res.json(api);

                        case 2:
                        case "end":
                            return _context50.stop();
                    }
                }
            }, _callee50, _this);
        }));

        return function (_x97, _x98) {
            return _ref51.apply(this, arguments);
        };
    }();

    this.createReview = function () {
        var _ref52 = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee51(req, res) {
            var token, _Util$decodeToken20, idCustomer, files, _JSON$parse5, id_product, id_order, rating, description, newFiles, fileName, loopIndex, imageDir, ratingCreated, ratingProducts, photos, fileStorage, data, api;

            return _regenerator2.default.wrap(function _callee51$(_context51) {
                while (1) {
                    switch (_context51.prev = _context51.next) {
                        case 0:
                            token = _utils2.default.getTokenFromRequest(req);
                            _Util$decodeToken20 = _utils2.default.decodeToken(token), idCustomer = _Util$decodeToken20.data.id;
                            files = req.files;
                            _JSON$parse5 = JSON.parse(req.body.data), id_product = _JSON$parse5.id_product, id_order = _JSON$parse5.id_order, rating = _JSON$parse5.rating, description = _JSON$parse5.description;
                            newFiles = [];
                            fileName = "";
                            loopIndex = 0;

                        case 7:
                            if (!(loopIndex < files.length)) {
                                _context51.next = 17;
                                break;
                            }

                            imageDir = _path2.default.resolve(__dirname).replace("dist/controllers", "");

                            fileName = getFileName() + ".png";
                            _context51.next = 12;
                            return (0, _sharp2.default)("" + imageDir + files[loopIndex].path).resize(500).png().toFile(imageDir + "public/uploads/" + fileName);

                        case 12:
                            _fs2.default.unlinkSync("" + imageDir + files[loopIndex].path);

                            newFiles.push(fileName);

                            loopIndex += 1;
                            _context51.next = 7;
                            break;

                        case 17:
                            _context51.next = 19;
                            return ProductRating.create({
                                id_product: id_product,
                                id_user: idCustomer,
                                id_order: id_order,
                                rating: rating,
                                description: description
                            });

                        case 19:
                            ratingCreated = _context51.sent;
                            ratingProducts = _utils2.default.stringifyThenParse(ratingCreated);
                            photos = newFiles.map(function (x) {
                                return {
                                    tablename: "ProductRatings",
                                    filename: x,
                                    id_reference: ratingProducts.id
                                };
                            });
                            _context51.next = 24;
                            return FileStorage.bulkCreate(photos);

                        case 24:
                            fileStorage = _context51.sent;
                            data = (0, _assign2.default)({}, ratingProducts, { images: fileStorage });
                            api = {
                                "status": true,
                                "data": data
                            };


                            res.json(api);

                        case 28:
                        case "end":
                            return _context51.stop();
                    }
                }
            }, _callee51, _this);
        }));

        return function (_x99, _x100) {
            return _ref52.apply(this, arguments);
        };
    }();

    this.updateReview = function () {
        var _ref53 = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee52(req, res) {
            var token, _Util$decodeToken21, idCustomer, files, _JSON$parse6, id, id_product, id_order, rating, description, tableName, fileStorage, newFiles, fileName, loopIndex, imageDir, ratingFound, ratingProducts, photos, data, api;

            return _regenerator2.default.wrap(function _callee52$(_context52) {
                while (1) {
                    switch (_context52.prev = _context52.next) {
                        case 0:
                            token = _utils2.default.getTokenFromRequest(req);
                            _Util$decodeToken21 = _utils2.default.decodeToken(token), idCustomer = _Util$decodeToken21.data.id;
                            files = req.files;
                            _JSON$parse6 = JSON.parse(req.body.data), id = _JSON$parse6.id, id_product = _JSON$parse6.id_product, id_order = _JSON$parse6.id_order, rating = _JSON$parse6.rating, description = _JSON$parse6.description;
                            tableName = "ProductRatings";
                            _context52.next = 7;
                            return FileStorage.findAll({
                                where: {
                                    id_reference: id,
                                    tablename: tableName
                                }
                            });

                        case 7:
                            fileStorage = _context52.sent;


                            _utils2.default.stringifyThenParse(fileStorage).forEach(function (x) {
                                try {
                                    _fs2.default.unlinkSync("public/uploads/" + x.filename);
                                } catch (err) {
                                    console.log(err);
                                }
                            });

                            _context52.next = 11;
                            return FileStorage.destroy({
                                where: {
                                    id_reference: id,
                                    tablename: tableName
                                }
                            });

                        case 11:
                            newFiles = [];
                            fileName = "";
                            loopIndex = 0;

                        case 14:
                            if (!(loopIndex < files.length)) {
                                _context52.next = 24;
                                break;
                            }

                            imageDir = _path2.default.resolve(__dirname).replace("dist/controllers", "");

                            fileName = getFileName() + ".png";
                            _context52.next = 19;
                            return (0, _sharp2.default)("" + imageDir + files[loopIndex].path).resize(500).png().toFile(imageDir + "public/uploads/" + fileName);

                        case 19:
                            _fs2.default.unlinkSync("" + imageDir + files[loopIndex].path);

                            newFiles.push(fileName);

                            loopIndex += 1;
                            _context52.next = 14;
                            break;

                        case 24:
                            _context52.next = 26;
                            return ProductRating.findById(id);

                        case 26:
                            ratingFound = _context52.sent;
                            _context52.next = 29;
                            return ratingFound.update({
                                id_product: id_product,
                                id_user: idCustomer,
                                id_order: id_order,
                                rating: rating,
                                description: description
                            });

                        case 29:
                            ratingProducts = _utils2.default.stringifyThenParse(ratingFound);
                            photos = newFiles.map(function (x) {
                                return {
                                    tablename: tableName,
                                    filename: x,
                                    id_reference: ratingProducts.id
                                };
                            });
                            _context52.next = 33;
                            return FileStorage.bulkCreate(photos);

                        case 33:
                            fileStorage = _context52.sent;
                            data = (0, _assign2.default)({}, ratingProducts, { images: fileStorage });
                            api = {
                                "status": true,
                                "data": data
                            };


                            res.json(api);

                        case 37:
                        case "end":
                            return _context52.stop();
                    }
                }
            }, _callee52, _this);
        }));

        return function (_x101, _x102) {
            return _ref53.apply(this, arguments);
        };
    }();

    this.showOrderHistories = function () {
        var _ref54 = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee53(req, res) {
            var id_order, data, api;
            return _regenerator2.default.wrap(function _callee53$(_context53) {
                while (1) {
                    switch (_context53.prev = _context53.next) {
                        case 0:
                            id_order = req.body.id_order;
                            _context53.next = 3;
                            return OrderHistory.findAll({
                                where: {
                                    id_order: id_order
                                }
                            });

                        case 3:
                            data = _context53.sent;
                            api = {
                                "status": true,
                                "data": data
                            };


                            res.json(api);

                        case 6:
                        case "end":
                            return _context53.stop();
                    }
                }
            }, _callee53, _this);
        }));

        return function (_x103, _x104) {
            return _ref54.apply(this, arguments);
        };
    }();

    this.getProvinces = function () {
        var _ref55 = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee54(req, res) {
            var api, _req$query9, sorted, _limit, filterText, orId, limit, options, _JSON$parse7, id, desc, filter, orOptions, _filter, results;

            return _regenerator2.default.wrap(function _callee54$(_context54) {
                while (1) {
                    switch (_context54.prev = _context54.next) {
                        case 0:
                            api = {
                                "status": false,
                                "msg": 'error'
                            };
                            _req$query9 = req.query, sorted = _req$query9.sorted, _limit = _req$query9.limit, filterText = _req$query9.filterText, orId = _req$query9.orId;
                            limit = 5;


                            if (_limit) {
                                limit = _limit;
                            }

                            options = {
                                limit: (0, _parseInt2.default)(limit, 10),
                                order: [['name', 'ASC']]
                            };


                            if (sorted) {
                                _JSON$parse7 = JSON.parse(sorted), id = _JSON$parse7.id, desc = _JSON$parse7.desc;

                                (0, _assign2.default)(options, {
                                    order: _db2.default.literal(id + " " + (desc ? 'DESC' : 'ASC'))
                                });
                            }

                            if (filterText) {
                                filter = {
                                    name: (0, _defineProperty3.default)({}, Op.regexp, "(" + filterText + ")")
                                };

                                (0, _assign2.default)(options, {
                                    where: filter
                                });
                            }

                            if (orId) {
                                orOptions = [{
                                    id: orId
                                }];

                                if (filterText) {
                                    orOptions.push({
                                        name: (0, _defineProperty3.default)({}, Op.regexp, "(" + filterText + ")")
                                    });
                                }
                                _filter = (0, _defineProperty3.default)({}, Op.or, orOptions);

                                (0, _assign2.default)(options, {
                                    where: _filter
                                });
                            }
                            _context54.next = 10;
                            return Province.findAll(options);

                        case 10:
                            results = _context54.sent;


                            if (results) {
                                api = {
                                    "status": true,
                                    "data": results,
                                    "msg": 'success'
                                };
                            }

                            res.json(api);

                        case 13:
                        case "end":
                            return _context54.stop();
                    }
                }
            }, _callee54, _this);
        }));

        return function (_x105, _x106) {
            return _ref55.apply(this, arguments);
        };
    }();

    this.getSelectCustomers = function () {
        var _ref56 = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee55(req, res) {
            var api, _req$query10, sorted, _limit, filterText, orId, limit, options, _JSON$parse8, id, desc, filter, orOptions, _filter3, results;

            return _regenerator2.default.wrap(function _callee55$(_context55) {
                while (1) {
                    switch (_context55.prev = _context55.next) {
                        case 0:
                            api = {
                                "status": false,
                                "msg": 'error'
                            };
                            _req$query10 = req.query, sorted = _req$query10.sorted, _limit = _req$query10.limit, filterText = _req$query10.filterText, orId = _req$query10.orId;
                            limit = 5;


                            if (_limit) {
                                limit = _limit;
                            }

                            options = {
                                limit: (0, _parseInt2.default)(limit, 10),
                                order: [['name', 'ASC']]
                            };


                            if (sorted) {
                                _JSON$parse8 = JSON.parse(sorted), id = _JSON$parse8.id, desc = _JSON$parse8.desc;

                                (0, _assign2.default)(options, {
                                    order: _db2.default.literal(id + " " + (desc ? 'DESC' : 'ASC'))
                                });
                            }

                            if (filterText) {
                                filter = {
                                    name: (0, _defineProperty3.default)({}, Op.regexp, "(" + filterText + ")")
                                };

                                (0, _assign2.default)(options, {
                                    where: filter
                                });
                            }

                            if (orId) {
                                orOptions = [{
                                    id: orId
                                }];

                                if (filterText) {
                                    orOptions.push({
                                        name: (0, _defineProperty3.default)({}, Op.regexp, "(" + filterText + ")")
                                    });
                                }
                                _filter3 = (0, _defineProperty3.default)({}, Op.or, orOptions);

                                (0, _assign2.default)(options, {
                                    where: _filter3
                                });
                            }
                            _context55.next = 10;
                            return Customer.findAll(options);

                        case 10:
                            results = _context55.sent;


                            if (results) {
                                api = {
                                    "status": true,
                                    "data": results,
                                    "msg": 'success'
                                };
                            }

                            res.json(api);

                        case 13:
                        case "end":
                            return _context55.stop();
                    }
                }
            }, _callee55, _this);
        }));

        return function (_x107, _x108) {
            return _ref56.apply(this, arguments);
        };
    }();

    this.getCities = function () {
        var _ref57 = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee56(req, res) {
            var api, _req$query11, sorted, _limit, filterText, idProvince, orId, limit, options, _JSON$parse9, id, desc, filter, orOptions, _filter5, results;

            return _regenerator2.default.wrap(function _callee56$(_context56) {
                while (1) {
                    switch (_context56.prev = _context56.next) {
                        case 0:
                            api = {
                                "status": false,
                                "msg": 'error'
                            };
                            _req$query11 = req.query, sorted = _req$query11.sorted, _limit = _req$query11.limit, filterText = _req$query11.filterText, idProvince = _req$query11.idProvince, orId = _req$query11.orId;
                            limit = 5;


                            if (_limit) {
                                limit = _limit;
                            }

                            options = {
                                limit: (0, _parseInt2.default)(limit, 10),
                                order: [['name', 'ASC']]
                            };


                            if (sorted) {
                                _JSON$parse9 = JSON.parse(sorted), id = _JSON$parse9.id, desc = _JSON$parse9.desc;

                                (0, _assign2.default)(options, {
                                    order: _db2.default.literal(id + " " + (desc ? 'DESC' : 'ASC'))
                                });
                            }

                            if (filterText) {
                                filter = {
                                    name: (0, _defineProperty3.default)({}, Op.regexp, "(" + filterText + ")")
                                };

                                if (idProvince) {
                                    (0, _assign2.default)(filter, {
                                        id_province: idProvince
                                    });
                                }
                                (0, _assign2.default)(options, {
                                    where: filter
                                });
                            }

                            if (orId) {
                                orOptions = [{
                                    id: orId
                                }];

                                if (filterText) {
                                    orOptions.push({
                                        name: (0, _defineProperty3.default)({}, Op.regexp, "(" + filterText + ")")
                                    });
                                }
                                _filter5 = (0, _defineProperty3.default)({}, Op.or, orOptions);

                                (0, _assign2.default)(options, {
                                    where: _filter5
                                });
                            }

                            _context56.next = 10;
                            return City.findAll(options);

                        case 10:
                            results = _context56.sent;


                            if (results) {
                                api = {
                                    "status": true,
                                    "data": results,
                                    "msg": 'success'
                                };
                            }

                            res.json(api);

                        case 13:
                        case "end":
                            return _context56.stop();
                    }
                }
            }, _callee56, _this);
        }));

        return function (_x109, _x110) {
            return _ref57.apply(this, arguments);
        };
    }();

    this.getSubDistricts = function () {
        var _ref58 = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee57(req, res) {
            var api, _req$query12, filterText, idCity, orId, _idCity, _q, execStoredProcedure, data;

            return _regenerator2.default.wrap(function _callee57$(_context57) {
                while (1) {
                    switch (_context57.prev = _context57.next) {
                        case 0:
                            api = {
                                "status": false,
                                "msg": 'error'
                            };
                            _req$query12 = req.query, filterText = _req$query12.filterText, idCity = _req$query12.idCity, orId = _req$query12.orId;
                            _idCity = 0;
                            _q = "";


                            if (filterText) {
                                _q = filterText;
                            }

                            if (orId) {
                                _q = orId;
                                if (filterText) {
                                    _q = filterText;
                                }
                            }

                            if (idCity) {
                                _idCity = idCity;
                            }

                            _context57.next = 9;
                            return _stored_procedure2.default.run('sp_get_sub_districts', [_idCity, "'" + _q + "'"], true);

                        case 9:
                            execStoredProcedure = _context57.sent;
                            data = execStoredProcedure.results.map(function (x) {
                                return { id: x.sub_district, name: x.sub_district };
                            });


                            if (data) {
                                api = {
                                    "status": true,
                                    "data": data,
                                    "msg": 'success'
                                };
                            }

                            res.json(api);

                        case 13:
                        case "end":
                            return _context57.stop();
                    }
                }
            }, _callee57, _this);
        }));

        return function (_x111, _x112) {
            return _ref58.apply(this, arguments);
        };
    }();

    this.getUrbans = function () {
        var _ref59 = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee58(req, res) {
            var api, _req$query13, filterText, idCity, subDistrict, orId, _idCity, _q, _orId, execStoredProcedure, data;

            return _regenerator2.default.wrap(function _callee58$(_context58) {
                while (1) {
                    switch (_context58.prev = _context58.next) {
                        case 0:
                            api = {
                                "status": false,
                                "msg": 'error'
                            };
                            _req$query13 = req.query, filterText = _req$query13.filterText, idCity = _req$query13.idCity, subDistrict = _req$query13.subDistrict, orId = _req$query13.orId;
                            _idCity = 0;
                            _q = "";
                            _orId = 0;


                            if (filterText) {
                                _q = filterText;
                            }

                            if (idCity) {
                                _idCity = idCity;
                            }

                            if (orId) {
                                _orId = orId;
                            }

                            _context58.next = 10;
                            return _stored_procedure2.default.run('sp_get_urbans', [_idCity, "'" + subDistrict + "'", "'" + _q + "'", _orId], true);

                        case 10:
                            execStoredProcedure = _context58.sent;
                            data = execStoredProcedure.results.map(function (x) {
                                return { id: x.id, name: x.urban, postal_code: x.postal_code };
                            });


                            if (data) {
                                api = {
                                    "status": true,
                                    "data": data,
                                    "msg": 'success'
                                };
                            }

                            res.json(api);

                        case 14:
                        case "end":
                            return _context58.stop();
                    }
                }
            }, _callee58, _this);
        }));

        return function (_x113, _x114) {
            return _ref59.apply(this, arguments);
        };
    }();

    this.showProvinces = function () {
        var _ref60 = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee59(req, res) {
            var data, api;
            return _regenerator2.default.wrap(function _callee59$(_context59) {
                while (1) {
                    switch (_context59.prev = _context59.next) {
                        case 0:
                            _context59.next = 2;
                            return Province.findAll({
                                order: [['name', 'ASC']]
                            });

                        case 2:
                            data = _context59.sent;
                            api = {
                                "status": true,
                                "data": data
                            };


                            res.json(api);

                        case 5:
                        case "end":
                            return _context59.stop();
                    }
                }
            }, _callee59, _this);
        }));

        return function (_x115, _x116) {
            return _ref60.apply(this, arguments);
        };
    }();

    this.showTopCities = function () {
        var _ref61 = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee60(req, res) {
            var sql, data, api;
            return _regenerator2.default.wrap(function _callee60$(_context60) {
                while (1) {
                    switch (_context60.prev = _context60.next) {
                        case 0:
                            sql = "SELECT p.origin, c.name, c.id_province FROM Products p JOIN Cities c ON c.id = p.origin GROUP BY p.origin ORDER BY COUNT(p.origin) DESC LIMIT 5";
                            _context60.next = 3;
                            return QueryBuilder.query(sql, { type: _sequelize2.default.QueryTypes.SELECT });

                        case 3:
                            data = _context60.sent;
                            api = {
                                "status": true,
                                "data": data
                            };


                            res.json(api);

                        case 6:
                        case "end":
                            return _context60.stop();
                    }
                }
            }, _callee60, _this);
        }));

        return function (_x117, _x118) {
            return _ref61.apply(this, arguments);
        };
    }();

    this.showAllCities = function () {
        var _ref62 = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee61(req, res) {
            var sql, data, api;
            return _regenerator2.default.wrap(function _callee61$(_context61) {
                while (1) {
                    switch (_context61.prev = _context61.next) {
                        case 0:
                            sql = "SELECT c.*, p.name AS province_name FROM Cities c \n                     JOIN Provinces p ON p.id = c.id_province \n                     ORDER BY province_name, name ASC";
                            _context61.next = 3;
                            return QueryBuilder.query(sql, { type: _sequelize2.default.QueryTypes.SELECT });

                        case 3:
                            data = _context61.sent;
                            api = {
                                "status": true,
                                "data": data
                            };


                            res.json(api);

                        case 6:
                        case "end":
                            return _context61.stop();
                    }
                }
            }, _callee61, _this);
        }));

        return function (_x119, _x120) {
            return _ref62.apply(this, arguments);
        };
    }();

    this.showCitiesGroup = function () {
        var _ref63 = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee62(req, res) {
            var search, options, filter, data, api;
            return _regenerator2.default.wrap(function _callee62$(_context62) {
                while (1) {
                    switch (_context62.prev = _context62.next) {
                        case 0:
                            search = req.query.search;


                            City.belongsTo(Province, { foreignKey: 'id_province', sourceKey: 'id' });
                            Province.hasMany(City, { foreignKey: 'id_province', sourceKey: 'id' });

                            options = {
                                model: City
                            };


                            if (search) {
                                filter = {
                                    name: (0, _defineProperty3.default)({}, Op.regexp, "(" + search + ")")
                                };

                                options = (0, _assign2.default)({}, options, {
                                    where: filter
                                });
                            }

                            _context62.next = 7;
                            return Province.findAll({
                                order: [['name', 'ASC']],
                                include: [options]
                            });

                        case 7:
                            data = _context62.sent;
                            api = {
                                "status": true,
                                "data": data
                            };


                            res.json(api);

                        case 10:
                        case "end":
                            return _context62.stop();
                    }
                }
            }, _callee62, _this);
        }));

        return function (_x121, _x122) {
            return _ref63.apply(this, arguments);
        };
    }();

    this.showProvinceCities = function () {
        var _ref64 = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee63(req, res) {
            var id_province, data, api;
            return _regenerator2.default.wrap(function _callee63$(_context63) {
                while (1) {
                    switch (_context63.prev = _context63.next) {
                        case 0:
                            id_province = req.params.id_province;
                            _context63.next = 3;
                            return City.findAll({
                                where: {
                                    id_province: id_province
                                },
                                order: [['name', 'ASC']]
                            });

                        case 3:
                            data = _context63.sent;
                            api = {
                                "status": true,
                                "data": data
                            };


                            res.json(api);

                        case 6:
                        case "end":
                            return _context63.stop();
                    }
                }
            }, _callee63, _this);
        }));

        return function (_x123, _x124) {
            return _ref64.apply(this, arguments);
        };
    }();

    this.showCustomers = function () {
        var _ref65 = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee64(req, res) {
            var _req$query14, _limit, _page, search, limit, page, options, calculateOptions, filter, data, calculateAmountOfData, amountOfData, api;

            return _regenerator2.default.wrap(function _callee64$(_context64) {
                while (1) {
                    switch (_context64.prev = _context64.next) {
                        case 0:
                            /**
                             * mandatory param: limit, page, is_unlimited
                            */
                            _req$query14 = req.query, _limit = _req$query14.limit, _page = _req$query14.page, search = _req$query14.filterText;
                            limit = 5, page = 1;
                            options = {
                                raw: true,
                                order: [['createdAt', 'DESC']]
                            };
                            calculateOptions = {
                                attributes: [[_db2.default.fn('COUNT', _db2.default.col('id')), 'total']],
                                raw: true
                            };


                            if (_limit) {
                                limit = _limit;
                            }

                            if (_page) {
                                page = _page;
                            }

                            options = (0, _assign2.default)({}, options, {
                                offset: (page - 1) * limit,
                                limit: (0, _parseInt2.default)(limit, 10)
                            });

                            if (search) {
                                filter = {
                                    name: (0, _defineProperty3.default)({}, Op.regexp, "(" + search + ")")
                                };

                                options = (0, _assign2.default)({}, options, {
                                    where: options.where ? (0, _assign2.default)({}, options.where, filter) : filter
                                });
                                calculateOptions = (0, _assign2.default)({}, calculateOptions, {
                                    where: calculateOptions.where ? (0, _assign2.default)({}, calculateOptions.where, filter) : filter
                                });
                            }

                            _context64.next = 10;
                            return Customer.findAll(options);

                        case 10:
                            data = _context64.sent;
                            _context64.next = 13;
                            return Customer.findOne(calculateOptions);

                        case 13:
                            calculateAmountOfData = _context64.sent;
                            amountOfData = calculateAmountOfData.total;
                            api = {
                                "status": true,
                                "data": data,
                                "page_total": Math.ceil(amountOfData / limit)
                            };


                            res.json(api);

                        case 17:
                        case "end":
                            return _context64.stop();
                    }
                }
            }, _callee64, _this);
        }));

        return function (_x125, _x126) {
            return _ref65.apply(this, arguments);
        };
    }();

    this.showCustomer = function () {
        var _ref66 = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee65(req, res) {
            var id, api, token, _Util$decodeToken22, myId, data, _data2;

            return _regenerator2.default.wrap(function _callee65$(_context65) {
                while (1) {
                    switch (_context65.prev = _context65.next) {
                        case 0:
                            id = req.params.id;
                            api = {
                                "status": false,
                                "data": "customer_not_found"
                            };

                            if (!(id === 'me')) {
                                _context65.next = 11;
                                break;
                            }

                            token = _utils2.default.getTokenFromRequest(req);
                            _Util$decodeToken22 = _utils2.default.decodeToken(token), myId = _Util$decodeToken22.data.id;
                            _context65.next = 7;
                            return Customer.findOne({
                                where: {
                                    id: myId
                                }
                            });

                        case 7:
                            data = _context65.sent;


                            api = {
                                "status": true,
                                "data": data
                            };
                            _context65.next = 15;
                            break;

                        case 11:
                            _context65.next = 13;
                            return Customer.findOne({
                                where: {
                                    id: id
                                }
                            });

                        case 13:
                            _data2 = _context65.sent;


                            api = {
                                "status": true,
                                "data": _data2
                            };

                        case 15:

                            res.json(api);

                        case 16:
                        case "end":
                            return _context65.stop();
                    }
                }
            }, _callee65, _this);
        }));

        return function (_x127, _x128) {
            return _ref66.apply(this, arguments);
        };
    }();

    this.createCustomer = function () {
        var _ref67 = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee66(req, res) {
            var _req$body12, name, email, passwd, api, isExist, data;

            return _regenerator2.default.wrap(function _callee66$(_context66) {
                while (1) {
                    switch (_context66.prev = _context66.next) {
                        case 0:
                            _req$body12 = req.body, name = _req$body12.name, email = _req$body12.email, passwd = _req$body12.passwd;
                            api = {
                                "status": false,
                                "data": "duplicate_email"
                            };
                            _context66.next = 4;
                            return checkDuplicateEmail({ email: email, id: null });

                        case 4:
                            isExist = _context66.sent;

                            if (isExist) {
                                _context66.next = 10;
                                break;
                            }

                            _context66.next = 8;
                            return Customer.create({
                                name: name,
                                email: email,
                                passwd: _bcrypt2.default.hashSync(passwd, SALT_PASSWD)
                            });

                        case 8:
                            data = _context66.sent;


                            api = {
                                "status": true,
                                "data": _utils2.default.stringifyThenParse(data)
                            };

                        case 10:

                            res.json(api);

                        case 11:
                        case "end":
                            return _context66.stop();
                    }
                }
            }, _callee66, _this);
        }));

        return function (_x129, _x130) {
            return _ref67.apply(this, arguments);
        };
    }();

    this.updateCustomer = function () {
        var _ref68 = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee67(req, res) {
            var _req$body13, id, name, email, passwd, api, isExist, customer, data;

            return _regenerator2.default.wrap(function _callee67$(_context67) {
                while (1) {
                    switch (_context67.prev = _context67.next) {
                        case 0:
                            _req$body13 = req.body, id = _req$body13.id, name = _req$body13.name, email = _req$body13.email, passwd = _req$body13.passwd;
                            api = {
                                "status": false,
                                "data": "duplicate_email"
                            };
                            _context67.next = 4;
                            return checkDuplicateEmail({ email: email, id: id });

                        case 4:
                            isExist = _context67.sent;

                            if (isExist) {
                                _context67.next = 13;
                                break;
                            }

                            _context67.next = 8;
                            return Customer.findById(id);

                        case 8:
                            customer = _context67.sent;
                            _context67.next = 11;
                            return customer.update({
                                name: name,
                                email: email,
                                passwd: passwd
                            });

                        case 11:
                            data = _context67.sent;

                            api = {
                                "status": true,
                                "data": _utils2.default.stringifyThenParse(data)
                            };

                        case 13:

                            res.json(api);

                        case 14:
                        case "end":
                            return _context67.stop();
                    }
                }
            }, _callee67, _this);
        }));

        return function (_x131, _x132) {
            return _ref68.apply(this, arguments);
        };
    }();

    this.deleteCustomer = function () {
        var _ref69 = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee68(req, res) {
            var api;
            return _regenerator2.default.wrap(function _callee68$(_context68) {
                while (1) {
                    switch (_context68.prev = _context68.next) {
                        case 0:
                            // const { id } = req.body;
                            // let data = await Customer.destroy({
                            //     where: {
                            //         id
                            //     }
                            // });

                            // const api = {
                            //     "status": true,
                            //     "data": Util.stringifyThenParse(data)
                            // };

                            api = {
                                "status": true,
                                "msg": "success"
                            };


                            res.json(api);

                        case 2:
                        case "end":
                            return _context68.stop();
                    }
                }
            }, _callee68, _this);
        }));

        return function (_x133, _x134) {
            return _ref69.apply(this, arguments);
        };
    }();

    this.bulkDeleteCustomer = function () {
        var _ref70 = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee69(req, res) {
            var api;
            return _regenerator2.default.wrap(function _callee69$(_context69) {
                while (1) {
                    switch (_context69.prev = _context69.next) {
                        case 0:
                            // const { id } = req.body;
                            // let data = await Customer.destroy({
                            //     where: {
                            //         id
                            //     }
                            // });

                            // const api = {
                            //     "status": true,
                            //     "data": Util.stringifyThenParse(data)
                            // };

                            api = {
                                "status": true,
                                "msg": "success"
                            };


                            res.json(api);

                        case 2:
                        case "end":
                            return _context69.stop();
                    }
                }
            }, _callee69, _this);
        }));

        return function (_x135, _x136) {
            return _ref70.apply(this, arguments);
        };
    }();

    this.updateCustomerPassword = function () {
        var _ref71 = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee70(req, res) {
            var _req$body14, id, new_password, confirm_new_password, api, customer, isConfirmed, data, newData;

            return _regenerator2.default.wrap(function _callee70$(_context70) {
                while (1) {
                    switch (_context70.prev = _context70.next) {
                        case 0:
                            _req$body14 = req.body, id = _req$body14.id, new_password = _req$body14.new_password, confirm_new_password = _req$body14.confirm_new_password;
                            api = {
                                "status": false,
                                "msg": "Konfirmasi password Anda masih belum benar"
                            };
                            _context70.next = 4;
                            return Customer.findById(id);

                        case 4:
                            customer = _context70.sent;
                            isConfirmed = String(new_password) === String(confirm_new_password);

                            if (!isConfirmed) {
                                _context70.next = 13;
                                break;
                            }

                            _context70.next = 9;
                            return customer.update({
                                passwd: _bcrypt2.default.hashSync(new_password, SALT_PASSWD)
                            });

                        case 9:
                            data = _context70.sent;
                            newData = _utils2.default.stringifyThenParse(data);


                            delete newData.passwd;

                            api = {
                                "status": true,
                                "data": newData
                            };

                        case 13:

                            res.json(api);

                        case 14:
                        case "end":
                            return _context70.stop();
                    }
                }
            }, _callee70, _this);
        }));

        return function (_x137, _x138) {
            return _ref71.apply(this, arguments);
        };
    }();

    this.showEmployee = function () {
        var _ref72 = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee71(req, res) {
            var token, _Util$decodeToken23, id, data, api;

            return _regenerator2.default.wrap(function _callee71$(_context71) {
                while (1) {
                    switch (_context71.prev = _context71.next) {
                        case 0:
                            token = _utils2.default.getTokenFromRequest(req);
                            _Util$decodeToken23 = _utils2.default.decodeToken(token), id = _Util$decodeToken23.data.id;
                            _context71.next = 4;
                            return Employee.findOne({
                                where: {
                                    id: id
                                }
                            });

                        case 4:
                            data = _context71.sent;
                            api = {
                                "status": true,
                                "data": data
                            };


                            res.json(api);

                        case 7:
                        case "end":
                            return _context71.stop();
                    }
                }
            }, _callee71, _this);
        }));

        return function (_x139, _x140) {
            return _ref72.apply(this, arguments);
        };
    }();

    this.showPaymentConfirmations = function () {
        var _ref73 = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee72(req, res) {
            var api, _req$query15, sorted, _limit, _page, filterText, limit, page, options, calculateOptions, _JSON$parse10, id, desc, filter, results, calculateAmountOfData, amountOfData;

            return _regenerator2.default.wrap(function _callee72$(_context72) {
                while (1) {
                    switch (_context72.prev = _context72.next) {
                        case 0:
                            api = {
                                "status": false,
                                "msg": 'error'
                            };
                            _req$query15 = req.query, sorted = _req$query15.sorted, _limit = _req$query15.limit, _page = _req$query15.page, filterText = _req$query15.filterText;
                            limit = 0, page = 1;
                            options = {
                                order: [['id', 'ASC']]
                            };


                            if (_limit) {
                                if (_page) {
                                    page = _page;
                                }

                                limit = _limit;
                                (0, _assign2.default)(options, {
                                    offset: (page - 1) * limit,
                                    limit: (0, _parseInt2.default)(limit, 10)
                                });
                            }

                            calculateOptions = {
                                attributes: [[_db2.default.fn('COUNT', _db2.default.col('id')), 'total']],
                                raw: true
                            };


                            if (sorted) {
                                _JSON$parse10 = JSON.parse(sorted), id = _JSON$parse10.id, desc = _JSON$parse10.desc;

                                (0, _assign2.default)(options, {
                                    order: _db2.default.literal(id + " " + (desc ? 'DESC' : 'ASC'))
                                });
                            }

                            if (filterText) {
                                filter = {
                                    reference_number: (0, _defineProperty3.default)({}, Op.regexp, "(" + filterText + ")")
                                };

                                (0, _assign2.default)(options, {
                                    where: filter
                                });
                                (0, _assign2.default)(calculateOptions, {
                                    where: filter
                                });
                            }

                            _context72.next = 10;
                            return PaymentConfirmation.findAll(options);

                        case 10:
                            results = _context72.sent;
                            _context72.next = 13;
                            return PaymentConfirmation.findOne(calculateOptions);

                        case 13:
                            calculateAmountOfData = _context72.sent;
                            amountOfData = calculateAmountOfData.total;


                            if (results) {
                                api = {
                                    "status": true,
                                    "data": results,
                                    "page_total": Math.ceil(amountOfData / limit),
                                    "msg": 'success'
                                };
                            }

                            res.json(api);

                        case 17:
                        case "end":
                            return _context72.stop();
                    }
                }
            }, _callee72, _this);
        }));

        return function (_x141, _x142) {
            return _ref73.apply(this, arguments);
        };
    }();

    this.acceptPayment = function () {
        var _ref74 = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee73(req, res) {
            var id, execStoredProcedure, data, _api7;

            return _regenerator2.default.wrap(function _callee73$(_context73) {
                while (1) {
                    switch (_context73.prev = _context73.next) {
                        case 0:
                            _context73.prev = 0;
                            id = req.body.id;
                            _context73.next = 4;
                            return _stored_procedure2.default.run('sp_process_order_1', [id, '1', // id_employee
                            1]);

                        case 4:
                            execStoredProcedure = _context73.sent;
                            data = execStoredProcedure.result;
                            _api7 = {
                                "status": true,
                                "data": data
                            };

                            res.json(_api7);
                            _context73.next = 14;
                            break;

                        case 10:
                            _context73.prev = 10;
                            _context73.t0 = _context73["catch"](0);

                            console.log(_context73.t0);
                            res.json({ status: false, error: _context73.t0 });

                        case 14:
                        case "end":
                            return _context73.stop();
                    }
                }
            }, _callee73, _this, [[0, 10]]);
        }));

        return function (_x143, _x144) {
            return _ref74.apply(this, arguments);
        };
    }();

    this.rejectPayment = function () {
        var _ref75 = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee74(req, res) {
            var id, execStoredProcedure, data, _api8;

            return _regenerator2.default.wrap(function _callee74$(_context74) {
                while (1) {
                    switch (_context74.prev = _context74.next) {
                        case 0:
                            _context74.prev = 0;
                            id = req.body.id;
                            _context74.next = 4;
                            return _stored_procedure2.default.run('sp_process_order_1', [id, '1', // id_employee
                            0]);

                        case 4:
                            execStoredProcedure = _context74.sent;
                            data = execStoredProcedure.result;
                            _api8 = {
                                "status": true,
                                "data": data
                            };

                            res.json(_api8);
                            _context74.next = 14;
                            break;

                        case 10:
                            _context74.prev = 10;
                            _context74.t0 = _context74["catch"](0);

                            console.log(_context74.t0);
                            res.json({ status: false, error: _context74.t0 });

                        case 14:
                        case "end":
                            return _context74.stop();
                    }
                }
            }, _callee74, _this, [[0, 10]]);
        }));

        return function (_x145, _x146) {
            return _ref75.apply(this, arguments);
        };
    }();

    this.processOrder = function () {
        var _ref76 = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee75(req, res) {
            var id, execStoredProcedure, data, _api9;

            return _regenerator2.default.wrap(function _callee75$(_context75) {
                while (1) {
                    switch (_context75.prev = _context75.next) {
                        case 0:
                            _context75.prev = 0;
                            id = req.body.id;
                            _context75.next = 4;
                            return _stored_procedure2.default.run('sp_process_order_2', [id, '1'] // id_employee
                            );

                        case 4:
                            execStoredProcedure = _context75.sent;
                            data = execStoredProcedure.result;
                            _api9 = {
                                "status": true,
                                "data": data
                            };

                            res.json(_api9);
                            _context75.next = 14;
                            break;

                        case 10:
                            _context75.prev = 10;
                            _context75.t0 = _context75["catch"](0);

                            console.log(_context75.t0);
                            res.json({ status: false, error: _context75.t0 });

                        case 14:
                        case "end":
                            return _context75.stop();
                    }
                }
            }, _callee75, _this, [[0, 10]]);
        }));

        return function (_x147, _x148) {
            return _ref76.apply(this, arguments);
        };
    }();

    this.shipOrder = function () {
        var _ref77 = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee76(req, res) {
            var _req$body15, id, shipment_number, orderRes, shipping, newShipping, execStoredProcedure, data, _api10;

            return _regenerator2.default.wrap(function _callee76$(_context76) {
                while (1) {
                    switch (_context76.prev = _context76.next) {
                        case 0:
                            _context76.prev = 0;
                            _req$body15 = req.body, id = _req$body15.id, shipment_number = _req$body15.shipment_number;
                            _context76.next = 4;
                            return Order.findOne({
                                where: {
                                    id: id
                                }
                            });

                        case 4:
                            orderRes = _context76.sent;
                            shipping = JSON.parse(orderRes.shipping);
                            newShipping = (0, _assign2.default)({}, shipping, {
                                shipment_number: shipment_number
                            });
                            _context76.next = 9;
                            return _stored_procedure2.default.run('sp_process_order_3', [id, "'" + (0, _stringify2.default)(newShipping) + "'", '1'] // id_employee
                            );

                        case 9:
                            execStoredProcedure = _context76.sent;
                            data = execStoredProcedure.result;
                            _api10 = {
                                "status": true,
                                "data": data
                            };

                            res.json(_api10);
                            _context76.next = 19;
                            break;

                        case 15:
                            _context76.prev = 15;
                            _context76.t0 = _context76["catch"](0);

                            console.log(_context76.t0);
                            res.json({ status: false, error: _context76.t0 });

                        case 19:
                        case "end":
                            return _context76.stop();
                    }
                }
            }, _callee76, _this, [[0, 15]]);
        }));

        return function (_x149, _x150) {
            return _ref77.apply(this, arguments);
        };
    }();

    this.getProvincesFromThirdParty = function () {
        var _ref78 = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee77(req, res) {
            var param;
            return _regenerator2.default.wrap(function _callee77$(_context77) {
                while (1) {
                    switch (_context77.prev = _context77.next) {
                        case 0:
                            param = {
                                key: RAJA_ONGKIR_API_KEY
                            };

                            _superagent2.default.get("https://api.rajaongkir.com/starter/province").query(param).end(function (err, response) {
                                var api = {};

                                if (err) {
                                    api = {
                                        "status": false,
                                        "error": err.message
                                    };

                                    res.json(api);
                                } else {
                                    var provinces = response.body.rajaongkir.results;
                                    var dataToInsert = provinces.map(function (x) {
                                        return {
                                            id: x.province_id,
                                            name: x.province
                                        };
                                    });

                                    Province.bulkCreate(dataToInsert).then(function (created) {
                                        api = {
                                            "status": true,
                                            "data": created
                                        };

                                        res.json(api);
                                    });
                                }
                            });

                        case 2:
                        case "end":
                            return _context77.stop();
                    }
                }
            }, _callee77, _this);
        }));

        return function (_x151, _x152) {
            return _ref78.apply(this, arguments);
        };
    }();

    this.getCitiesFromThirdParty = function () {
        var _ref79 = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee78(req, res) {
            var param;
            return _regenerator2.default.wrap(function _callee78$(_context78) {
                while (1) {
                    switch (_context78.prev = _context78.next) {
                        case 0:
                            param = {
                                key: RAJA_ONGKIR_API_KEY
                            };

                            _superagent2.default.get("https://api.rajaongkir.com/starter/city").query(param).end(function (err, response) {
                                var api = {};

                                if (err) {
                                    api = {
                                        "status": false,
                                        "error": err.message
                                    };

                                    res.json(api);
                                } else {
                                    var cities = response.body.rajaongkir.results;
                                    var dataToInsert = cities.map(function (x) {
                                        return {
                                            id: x.city_id,
                                            name: x.type + " " + x.city_name,
                                            id_province: x.province_id
                                        };
                                    });

                                    City.bulkCreate(dataToInsert).then(function (created) {
                                        api = {
                                            "status": true,
                                            "data": created
                                        };

                                        res.json(api);
                                    });
                                }
                            });

                        case 2:
                        case "end":
                            return _context78.stop();
                    }
                }
            }, _callee78, _this);
        }));

        return function (_x153, _x154) {
            return _ref79.apply(this, arguments);
        };
    }();

    this.checkShipmentCost = function () {
        var _ref80 = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee79(req, res) {
            var _req$body16, destination, weight, courier, param;

            return _regenerator2.default.wrap(function _callee79$(_context79) {
                while (1) {
                    switch (_context79.prev = _context79.next) {
                        case 0:
                            try {
                                _req$body16 = req.body, destination = _req$body16.destination, weight = _req$body16.weight, courier = _req$body16.courier;
                                param = {
                                    key: RAJA_ONGKIR_API_KEY,
                                    origin: '256', // malang
                                    destination: destination,
                                    weight: weight,
                                    courier: courier
                                };


                                _superagent2.default.post("https://api.rajaongkir.com/starter/cost").send(param).end(function (err, response) {
                                    var api = {};
                                    if (err) {
                                        api = {
                                            "status": false,
                                            "error": err.message
                                        };
                                    } else {
                                        api = {
                                            "status": true,
                                            "data": response.body.rajaongkir.results
                                        };
                                    }

                                    res.json(api);
                                });
                            } catch (err) {
                                console.log('DEBUG-ERR: ', err);
                            }

                        case 1:
                        case "end":
                            return _context79.stop();
                    }
                }
            }, _callee79, _this);
        }));

        return function (_x155, _x156) {
            return _ref80.apply(this, arguments);
        };
    }();

    this.forProductMigration = function () {
        var _ref81 = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee80(req, res) {
            var rawdata, results, data, index, _loop, api;

            return _regenerator2.default.wrap(function _callee80$(_context81) {
                while (1) {
                    switch (_context81.prev = _context81.next) {
                        case 0:
                            // let rawdata = fs.readFileSync(path.join(__dirname, "../../public/json/aneka_kue.json"));
                            // let rawdata = fs.readFileSync(path.join(__dirname, "../../public/json/bumbu_sambal.json"));
                            // let rawdata = fs.readFileSync(path.join(__dirname, "../../public/json/dodol.json"));
                            // let rawdata = fs.readFileSync(path.join(__dirname, "../../public/json/herbal.json"));
                            // let rawdata = fs.readFileSync(path.join(__dirname, "../../public/json/kacang.json"));
                            // let rawdata = fs.readFileSync(path.join(__dirname, "../../public/json/kerajinan.json"));
                            // let rawdata = fs.readFileSync(path.join(__dirname, "../../public/json/keripik.json"));
                            // let rawdata = fs.readFileSync(path.join(__dirname, "../../public/json/kerupuk.json"));
                            // let rawdata = fs.readFileSync(path.join(__dirname, "../../public/json/kopi.json"));
                            // let rawdata = fs.readFileSync(path.join(__dirname, "../../public/json/kue_kering.json"));
                            // let rawdata = fs.readFileSync(path.join(__dirname, "../../public/json/lauk_pauk.json"));
                            // let rawdata = fs.readFileSync(path.join(__dirname, "../../public/json/makanan_basah.json"));
                            // let rawdata = fs.readFileSync(path.join(__dirname, "../../public/json/manisan.json"));
                            // let rawdata = fs.readFileSync(path.join(__dirname, "../../public/json/minuman_segar.json"));
                            // let rawdata = fs.readFileSync(path.join(__dirname, "../../public/json/permen_coklat.json"));
                            rawdata = _fs2.default.readFileSync(_path2.default.join(__dirname, "../../public/json/teh.json"));
                            results = JSON.parse(rawdata);
                            data = [];
                            index = 0;
                            _loop = /*#__PURE__*/_regenerator2.default.mark(function _loop() {
                                var result, cityRes, origin, productCreated, photos, fileStorage, newData;
                                return _regenerator2.default.wrap(function _loop$(_context80) {
                                    while (1) {
                                        switch (_context80.prev = _context80.next) {
                                            case 0:
                                                result = results[index];
                                                _context80.next = 3;
                                                return City.findOne({
                                                    where: {
                                                        name: (0, _defineProperty3.default)({}, Op.like, "%" + result.origin + "%")
                                                    }
                                                });

                                            case 3:
                                                cityRes = _context80.sent;
                                                origin = 1;


                                                if (cityRes) {
                                                    origin = cityRes.id;
                                                }

                                                _context80.next = 8;
                                                return Product.create({
                                                    id_category: result.idCategory,
                                                    name: result.productName,
                                                    slug: result.slug,
                                                    price: result.price,
                                                    description: result.description,
                                                    origin: origin,
                                                    weight: result.weight,
                                                    popular_score: 0,
                                                    available_qty: result.available_qty,
                                                    is_active: 1,
                                                    primary_img: result.images.length > 0 ? result.images[0] : ''
                                                });

                                            case 8:
                                                productCreated = _context80.sent;
                                                photos = result.images.map(function (x) {
                                                    return {
                                                        tablename: "Products",
                                                        filename: x,
                                                        id_reference: productCreated.id
                                                    };
                                                });
                                                _context80.next = 12;
                                                return FileStorage.bulkCreate(photos);

                                            case 12:
                                                fileStorage = _context80.sent;
                                                newData = (0, _assign2.default)(productCreated, { images: fileStorage });

                                                data.push(newData);

                                                index = index + 1;

                                            case 16:
                                            case "end":
                                                return _context80.stop();
                                        }
                                    }
                                }, _loop, _this);
                            });

                        case 5:
                            if (!(index < results.length)) {
                                _context81.next = 9;
                                break;
                            }

                            return _context81.delegateYield(_loop(), "t0", 7);

                        case 7:
                            _context81.next = 5;
                            break;

                        case 9:
                            api = {
                                "status": true,
                                "data": data
                            };


                            res.json(api);

                        case 11:
                        case "end":
                            return _context81.stop();
                    }
                }
            }, _callee80, _this);
        }));

        return function (_x157, _x158) {
            return _ref81.apply(this, arguments);
        };
    }();

    _moment2.default.locale('id');
}

/**
 * [ADMIN] menampilkan produk 
 */


/**
 * menampilkan produk
 */


/**
 * [ADMIN] menampilkan kategori 
 */


/**
 * menampilkan kategori
 */


/**
 * checkout order
 */


/**
 * menampilkan detail kategori
 */


/**
 * update profile user
 */


/**
 * update password user
 */


/**
 * [ADMIN] create kategori
 */


/**
 * [ADMIN] update kategori
 */


/**
 * [ADMIN] hapus kategori
 */


/**
 * menampilkan detail produk
 */


/**
 * menampilkan images produk
 */


/**
 * [ADMIN] create produk
 */


/**
 * [ADMIN] update produk
 */


/**
 * [ADMIN] hapus produk
 */


/**
 * menampilkan keranjang belanja customer
 */


/**
 * menampilkan dashboard customer
 */


/**
 * menampilkan informasi akun customer
 */


/**
 * menambahkan produk ke dalam keranjang belanja
 */


/**
 * mengubah qty produk keranjang belanja
 */


/**
 * menghapus produk keranjang belanja
 */


/**
 * [ADMIN] menampilkan alamat semua customer
 */


/**
 * [ADMIN] menampilkan alamat customer
 */


/**
 * create alamat customer
 */


/**
 * update alamat customer
 */


/**
 * hapus alamat customer
 */


/**
 * [ADMIN] bulk hapus alamat customer
 */


/**
 * set default address
 */


/**
 * menampilkan alamat customer
 */


/**
 * menampilkan detail alamat customer
 */


/**
 * menampilkan order customer
 */


/**
 * menampilkan detail order customer
 */


/**
 * menampilkan wishlist customer
 */


/**
 * menambahkan wishlist
 */


/**
 * [ADMIN] menampilkan semua order customer
 */


/**
 * [ADMIN] menampilkan order customer
 */


/**
 * cek status pesanan
 */


/**
 * menampilkan history ulasan
 */


/**
 * membuat ulasan
 */


/**
 * mengedit ulasan
 */


/**
 * [ADMIN] menampilkan history order customer
 */


/**
 * [ADMIN] autocomplete - menampilkan provinsi
 */


/**
 * [ADMIN] autocomplete - menampilkan customer
 */


/**
 * [ADMIN] autocomplete - menampilkan kota
 */


/**
 * [ADMIN] autocomplete - menampilkan kecamatan
 */


/**
 * [ADMIN] autocomplete - menampilkan kelurahan
 */


/**
 * menampilkan provinsi
 */


/**
 * menampilkan semua kota
 */


/**
 * menampilkan semua kota
 */


/**
 * menampilkan provinsi dengan subdata kota
 */


/**
 * menampilkan kota berdasarkan provinsi
 */


/**
 * [ADMIN] menampilkan data customer
 */


/**
 * [ADMIN] menampilkan detail customer
 */


/**
 * [ADMIN] create customer
 */


/**
 * [ADMIN] update customer
 */


/**
 * [ADMIN] delete customer
 */


/**
 * [ADMIN] bulk delete customer
 */


/**
 * [ADMIN] update customer password
 */


/**
 * [ADMIN] menampilkan detail data karyawan
 */


/**
 * [ADMIN] menampilkan konfirmasi pembayaran
 */


/**
 * [ADMIN] menerima konfirmasi pembayaran
 */


/**
 * [ADMIN] menolak konfirmasi pembayaran
 */


/**
 * [ADMIN] memproses order setelah dibayar
 */


/**
 * [ADMIN] kirim order
 */


/**
 * ambil data provinsi dari rajaongkir
 */


/**
 * ambil data kota dari rajaongkir
 */


/**
 * cek ongkos kirim
 */
;

var api = new Api();

exports.default = api;