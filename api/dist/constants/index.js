"use strict";

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.ModelName = exports.SECRET_KEY = undefined;

var _keyMirror = require("key-mirror");

var _keyMirror2 = _interopRequireDefault(_keyMirror);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var SECRET_KEY = "5eGWWbXHpe3xehfRTcRDXE3cxS9hZqzVJs49QYjYvvQQt2Nuda";

var ModelName = (0, _keyMirror2.default)({
    ADDRESS: null,
    BACKOFFICE_MENU: null,
    CART: null,
    CART_DETAIL: null,
    CATEGORY: null,
    CITY: null,
    CUSTOMER: null,
    EMPLOYEE: null,
    FILE_STORAGE: null,
    META_DATA: null,
    OPNAME_STOCK: null,
    ORDER: null,
    ORDER_DETAIL: null,
    ORDER_HISTORY: null,
    PAYMENT_CONFIRMATION: null,
    POSTAL_CODE: null,
    PRODUCT: null,
    PRODUCT_RATING: null,
    PROVINCE: null,
    STOCK: null,
    WISHLIST: null
});

exports.SECRET_KEY = SECRET_KEY;
exports.ModelName = ModelName;