'use strict';

module.exports = {
  up: function up(queryInterface, Sequelize) {
    return queryInterface.createTable('Orders', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      reference: {
        allowNull: false,
        type: Sequelize.STRING
      },
      id_customer: {
        allowNull: false,
        type: Sequelize.INTEGER
      },
      address_delivery: {
        allowNull: false,
        type: Sequelize.TEXT
      },
      order_state: {
        allowNull: false,
        type: Sequelize.STRING(2)
      },
      payment: {
        allowNull: false,
        type: Sequelize.STRING(2)
      },
      shipping: {
        allowNull: false,
        type: Sequelize.TEXT
      },
      subtotal: {
        allowNull: false,
        type: Sequelize.DECIMAL(19, 4)
      },
      total_shipping: {
        allowNull: false,
        type: Sequelize.DECIMAL(19, 4)
      },
      grand_total: {
        allowNull: false,
        type: Sequelize.DECIMAL(19, 4)
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: function down(queryInterface, Sequelize) {
    return queryInterface.dropTable('Orders');
  }
};