'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.start = undefined;

var _kue = require('kue');

var _kue2 = _interopRequireDefault(_kue);

var _moment = require('moment');

var _moment2 = _interopRequireDefault(_moment);

var _constants = require('./constants');

var _model_factory = require('./utils/model_factory');

var _model_factory2 = _interopRequireDefault(_model_factory);

var _utils = require('./utils');

var _utils2 = _interopRequireDefault(_utils);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var queue = _kue2.default.createQueue();

var PaymentConfirmation = _model_factory2.default.createModel(_constants.ModelName.PAYMENT_CONFIRMATION);

var start = exports.start = function start() {
    queue.process('confirmPayment', function (job, done) {
        PaymentConfirmation.create({
            reference_number: job.data.reference,
            is_auto: true,
            transfer_date: (0, _moment2.default)().format('YYYY-MM-DD HH:mm:ss')
        }).then(function (res) {
            console.log('PAYMENT-SUCCESS: ', _utils2.default.stringifyThenParse(res));
            done();
        }).catch(function (err) {
            console.log('PAYMENT-ERROR: ', err);
            done();
        });
    });
};