"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _express = require("express");

var _express2 = _interopRequireDefault(_express);

var _fs = require("fs");

var _fs2 = _interopRequireDefault(_fs);

var _mime = require("mime");

var _mime2 = _interopRequireDefault(_mime);

var _api = require("../controllers/api");

var _api2 = _interopRequireDefault(_api);

var _multer = require("multer");

var _multer2 = _interopRequireDefault(_multer);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var createFilename = function createFilename() {
  return Math.random().toString(36).substring(2, 5) + Math.random().toString(36).substring(2, 5);
};
var storage = _multer2.default.diskStorage({
  destination: function destination(req, file, cb) {
    var dir = "public/uploads/temp";

    if (!_fs2.default.existsSync(dir)) {
      _fs2.default.mkdirSync(dir);
    }
    cb(null, dir + "/");
  },
  filename: function filename(req, file, cb) {
    var fileExtension = _mime2.default.getExtension(file.mimetype);
    var filename = createFilename() + "." + fileExtension;

    cb(null, filename);
  }
});
var upload = (0, _multer2.default)({ storage: storage });

var router = _express2.default.Router();

router.get('/', _api2.default.index);
router.post('/api/register', _api2.default.register);
router.post('/api/login/:type', _api2.default.login);
router.post('/api/uploadFiles', upload.array("photos", 12), _api2.default.uploadFiles);
router.post('/api/uploadFile', upload.single('photo'), _api2.default.uploadFile);
router.get('/api/get-google-url', _api2.default.getGoogleUrl);
router.post('/api/setup-google-account', _api2.default.setupGoogleAccount);
router.get('/api/appmenu', _api2.default.appMenu);
router.get('/api/products', _api2.default.showProducts);
router.get('/api/sub_districts', _api2.default.getSubDistricts);
router.get('/api/urbans', _api2.default.getUrbans);
router.get('/api/backoffice-products', _api2.default.getBackofficeProducts);
router.get('/api/backoffice-categories', _api2.default.getBackofficeCategories);
router.get('/api/v2/customers', _api2.default.getSelectCustomers);
router.get('/api/v2/provinces', _api2.default.getProvinces);
router.get('/api/v2/cities', _api2.default.getCities);
router.get('/api/categories', _api2.default.showCategories);
router.get('/api/category/:id', _api2.default.showCategory);
router.post('/api/category', _api2.default.createCategory);
router.put('/api/category', upload.single('photos'), _api2.default.updateCategory);
router.delete('/api/category', _api2.default.deleteCategory);
router.delete('/api/category/bulk', _api2.default.bulkDeleteCategory);
router.get('/api/carts', _api2.default.showMyCarts);
router.post('/api/add_to_cart', _api2.default.addToCart);
router.post('/api/update_cart', _api2.default.updateCart);
router.post('/api/delete_cart', _api2.default.deleteCart);
router.get('/api/product/:id', _api2.default.showProductDetail);
router.get('/api/my/dashboard', _api2.default.showMyDashboard);
router.get('/api/my/account', _api2.default.showMyAccountInformation);
router.get('/api/addresses', _api2.default.showAddresses);
router.get('/api/address/:id', _api2.default.showAddress);
router.post('/api/set_default_address', _api2.default.setDefaultAddress);
router.post('/api/address', _api2.default.createAddress);
router.put('/api/address', _api2.default.updateAddress);
router.delete('/api/address', _api2.default.deleteAddress);
router.delete('/api/address/bulk', _api2.default.bulkDeleteAddress);
router.get('/api/my/addresses', _api2.default.showMyAddresses);
router.get('/api/my/address/:id', _api2.default.showMyAddress);
router.get('/api/my/orders', _api2.default.showMyOrders);
router.get('/api/my/order_detail/:id', _api2.default.showMyOrderDetail);
router.get('/api/my/wishlists', _api2.default.showMyWishlists);
router.post('/api/add_to_wishlist', _api2.default.addToWishlist);
router.get('/api/orders', _api2.default.showOrders);
router.get('/api/order_detail/:id', _api2.default.showOrderDetail);
router.get('/api/check_my_order', _api2.default.checkMyOrder);
router.get('/api/my/reviews', _api2.default.showMyReviews);
router.post('/api/review', upload.array("photos", 12), _api2.default.createReview);
router.put('/api/review', upload.array("photos", 12), _api2.default.updateReview);
router.get('/api/order_histories/:id_order', _api2.default.showOrderHistories);
router.get('/api/customers', _api2.default.showCustomers);
router.get('/api/customer/:id', _api2.default.showCustomer);
router.post('/api/customer', _api2.default.createCustomer);
router.put('/api/customer', _api2.default.updateCustomer);
router.delete('/api/customer', _api2.default.deleteCustomer);
router.delete('/api/customer/bulk', _api2.default.bulkDeleteCustomer);
router.get('/api/employee', _api2.default.showEmployee);
router.post('/api/upload_profile_photo', upload.single('photos'), _api2.default.uploadProfilePhoto);
router.get('/api/product_images/:id', _api2.default.showProductImages);
router.post('/api/product', upload.array("photos", 12), _api2.default.createProduct);
router.put('/api/product', upload.array("photos", 12), _api2.default.updateProduct);
router.delete('/api/product', _api2.default.deleteProduct);
router.delete('/api/product/bulk', _api2.default.bulkDeleteProduct);
router.get("/api/province_cities/:id_province", _api2.default.showProvinceCities);
router.get("/api/cities", _api2.default.showAllCities);
router.get("/api/cities-group", _api2.default.showCitiesGroup);
router.get("/api/top-cities", _api2.default.showTopCities);
router.get("/api/provinces", _api2.default.showProvinces);
router.post("/api/cekongkir", _api2.default.checkShipmentCost);
router.post("/api/checkout_order", _api2.default.checkoutOrder);
router.post("/api/update_my_profile", _api2.default.updateMyProfile);
router.post("/api/update_my_password", _api2.default.updateMyPassword);
router.post("/api/update_customer_password", _api2.default.updateCustomerPassword);
router.get('/api/payment_confirmations', _api2.default.showPaymentConfirmations);
router.post("/api/accept_payment", _api2.default.acceptPayment);
router.post("/api/reject_payment", _api2.default.rejectPayment);
router.post("/api/process_order", _api2.default.processOrder);
router.post("/api/ship_order", _api2.default.shipOrder);
// router.get("/api/rajaongkir/provinces", Api.getProvincesFromThirdParty);
// router.get("/api/rajaongkir/cities", Api.getCitiesFromThirdParty);
// router.post("/api/for_product_migration", Api.forProductMigration);

exports.default = router;