'use strict';

var _express = require('express');

var _express2 = _interopRequireDefault(_express);

var _path = require('path');

var _path2 = _interopRequireDefault(_path);

var _morgan = require('morgan');

var _morgan2 = _interopRequireDefault(_morgan);

var _cookieParser = require('cookie-parser');

var _cookieParser2 = _interopRequireDefault(_cookieParser);

var _bodyParser = require('body-parser');

var _bodyParser2 = _interopRequireDefault(_bodyParser);

var _expressJwt = require('express-jwt');

var _expressJwt2 = _interopRequireDefault(_expressJwt);

var _cors = require('cors');

var _cors2 = _interopRequireDefault(_cors);

var _constants = require('./constants');

var _routes = require('./routes');

var _routes2 = _interopRequireDefault(_routes);

var _job = require('./job');

var Job = _interopRequireWildcard(_job);

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var app = (0, _express2.default)();

app.use((0, _cors2.default)());
app.use((0, _morgan2.default)("dev"));
app.use(_bodyParser2.default.json());
app.use(_bodyParser2.default.urlencoded({ extended: false }));
app.use((0, _cookieParser2.default)());
app.use(_express2.default.static(_path2.default.join(__dirname, "../public")));
app.use((0, _expressJwt2.default)({ secret: _constants.SECRET_KEY }).unless({
  path: ['/', /^\/api\/login\/.*/, '/api/register', '/api/products', /^\/api\/product\/.*/, '/api/categories', '/api/cities', '/api/cities-group', '/api/top-cities', '/api/get-google-url', '/api/setup-google-account', /^\/uploads\/.*/]
}));

app.use("/", _routes2.default);

app.use(function (req, res, next) {
  var err = new Error("Not Found");
  err.status = 404;
  next(err);
});

app.use(function (err, req, res, next) {
  var message = err.message;
  var error = req.app.get("env") === "development" ? err : {};

  res.status(err.status || 500);
  res.json({
    status: false,
    error: error,
    msg: message
  });
});

Job.start();

module.exports = app;